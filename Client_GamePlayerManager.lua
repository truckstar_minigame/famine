local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Player = Devil.getTable("Game.Famine.Client_GamePlayer")
local PlayerManager = inherit(Devil.Framework.ClientObjectBase,Devil.getTable("Game.Famine.Client_GamePlayerManager"))
function PlayerManager:construction(parameter)
    self.mPlayers = {}

    for id, player in pairs(Framework.singleton():getPlayerManager().mPlayers) do
        self:_createPlayer(id)
    end
    Framework.singleton():getPlayerManager():addEventListener(
        "PlayerIn",
        self,
        function(inst, parameter)
            echo("devilwalk","Client_GamePlayerManager:construction:PlayerIn:"..tostring(parameter.mPlayerID))
            self:_createPlayer(parameter.mPlayerID)
        end,
        self
    )
    Framework.singleton():getPlayerManager():addEventListener(
        "PlayerRemoved",
        self,
        function(inst, parameter)
            echo("devilwalk","Client_GamePlayerManager:construction:PlayerRemoved:"..tostring(parameter.mPlayerID))
            self:_destroyPlayer(parameter.mPlayerID)
        end,
        self
    )
end

function PlayerManager:destruction()
    for _,player in pairs(self.mPlayers) do
        delete(player)
    end
    self.mPlayers = nil

    Framework.singleton():getPlayerManager():removeEventListener(
        "PlayerIn",
        self
    )
    Framework.singleton():getPlayerManager():removeEventListener(
        "PlayerRemoved",
        self
    )
end

function PlayerManager:getPlayerByID(playerID)
    playerID = playerID or GetPlayerId()
    for _,player in pairs(self.mPlayers) do
        if player:getID() == playerID then
            return player
        end
    end
end

function PlayerManager:update(deltaTime)
    for _,player in pairs(self.mPlayers) do
        player:update(deltaTime)
    end
end

function PlayerManager:_createPlayer(playerID)
    echo("devilwalk","PlayerManager:_createPlayer:playerID:"..tostring(playerID))
    local ret = new(Player,{mInitMembers = {mID = playerID}})
    self.mPlayers[#self.mPlayers + 1] = ret
    return ret
end

function PlayerManager:_destroyPlayer(playerID)
    echo("devilwalk","PlayerManager:_destroyPlayer:playerID:"..tostring(playerID))
    for k,player in pairs(self.mPlayers) do
        if player:getID() == playerID then
            delete(player)
            table.remove(self.mPlayers,k)
            break
        end
    end
end

function PlayerManager:_getSendKey()
    return "PlayerManager"
end
