local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Host_Game")
local Scene = Devil.getTable("Game.Famine.Host_GameScene")
local Building = Devil.getTable("Game.Famine.Host_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")

local NatureSceneDispatcherTask = inherit(Framework.DistributedTaskDispatcher,Devil.getTable("Game.Famine.Host_GameNatureSceneTaskDispatcher"))

function NatureSceneDispatcherTask:construction(parameter)
end

function NatureSceneDispatcherTask:destruction(parameter)
end

function NatureSceneDispatcherTask:_getSendKey()
    return "GameNatureSceneTask"
end
