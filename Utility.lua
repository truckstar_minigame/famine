local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Host_Game = Devil.getTable("Game.Famine.Host_Game")
local Client_Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")

function GameUtility.calcPlayerLevelUpExp(level)
    return 100 * level
end

function GameUtility.calcPlayerHP(level)
    return Config.Player.mHP + level * Config.Player.mHPAdditionPerLevel
end

function GameUtility.calcPlayerFood(level)
    return Config.Player.mFood + level * Config.Player.mFoodAdditionPerLevel
end

function GameUtility.calcPlayerWater(level)
    return Config.Player.mWater + level * Config.Player.mWaterAdditionPerLevel
end

function GameUtility.calcPlayerStamina(level)
    return Config.Player.mStamina + level * Config.Player.mStaminaAdditionPerLevel
end

function GameUtility.calcPlayerOxygen(level)
    return Config.Player.mOxygen + level * Config.Player.mOxygenAdditionPerLevel
end

function GameUtility.calcPlayerWeight(level)
    return Config.Player.mWeight + level * Config.Player.mWeightAdditionPerLevel
end

function GameUtility.calcPlayerMovementSpeed(level)
    return Config.Player.mMovementSpeed + level * Config.Player.mMovementSpeedAdditionPerLevel
end

function GameUtility.calcPlayerMeleeDamage(level)
    return Config.Player.mMeleeDamage + level * Config.Player.mMeleeDamageAdditionPerLevel
end

function GameUtility.calcPlayerFortitude(level)
    return Config.Player.mFortitude + level * Config.Player.mFortitudeAdditionPerLevel
end

function GameUtility.calcPlayerCraftingSpeed(level)
    return Config.Player.mCraftingSpeed + level * Config.Player.mCraftingSpeedAdditionPerLevel
end

function GameUtility.calcTime(time)
    local left_minite = time % 60
    local hour = math.floor(time / 60)
    local left_hour = hour % 24
    local day = math.floor(hour / 24)
    local left_day = day % 30
    local month = math.floor(day / 30)
    local left_month = month % 12
    local year = math.floor(month / 12)
    return {mYear = year,mMonth = left_month,mDay = left_day,mHour = left_hour,mMinite = left_minite}
end

function GameUtility.calcSeason(time)
    local day_time = GameUtility.calcTime(time)
    local month = day_time.mMonth + 1
    local day = day_time.mDay + 1
    if month >= 3 and month <= 5 then
        return "春", (month - 3) * 30 + day
    elseif month >= 6 and month <= 8 then
        return "夏", (month - 6) * 30 + day
    elseif month >= 9 and month <= 11 then
        return "秋", (month - 9) * 30 + day
    else
        local days
        if month == 12 then
            days = day
        else
            days = month * 30 + day
        end
        return "冬", days
    end
end

function GameUtility.calcTemperature(time)
    local season, season_day = GameUtility.calcSeason(time)
    local ret
    if season == "春" then
        ret = 10 + season_day / 89 * 10
    elseif season == "夏" then
        if season_day < 45 then
            ret = 20 + season_day / 45 * 20
        else
            ret = 40 - (89 - season_day) / 45 * 10
        end
    elseif season == "秋" then
        ret = 30 - season_day / 89 * 25
    else
        if season_day < 45 then
            ret = 5 - season_day / 45 * 30
        else
            ret = -25 + (89 - season_day) / 45 * 35
        end
    end
    local day_time = GameUtility.calcTime(time)
    local hour = day_time.mHour
    if hour < 12 then
        ret = ret + hour / 12
    else
        ret = ret - hour / 12
    end
    return ret
end

function GameUtility.calcBuildingBound(pivot, blockSize)
    return new(Utility.BlockAABB, {mPivot = pivot, mVolume = blockSize})
end
