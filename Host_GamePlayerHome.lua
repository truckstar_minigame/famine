local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Host_Game")
local Scene = Devil.getTable("Game.Famine.Host_GameScene")
local Building = Devil.getTable("Game.Famine.Host_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")
local BigWorld = Devil.getTable("Game.Famine.Host_GameBigWorld")
local SceneManager = Devil.getTable("Game.Famine.Host_GameSceneManager")

local Home = inherit(Scene, Devil.getTable("Game.Famine.Host_GamePlayerHome"))
Home.sFreeSlots = {}
Home.sYLen = 33
Home.sEdgeLen = 65
Home.sBuildingEdgeLen = Home.sEdgeLen - 6
Home.sDoorBlockID = 200
for i = 1, 16 do
    Home.sFreeSlots[#Home.sFreeSlots + 1] = i
end
function Home.nextFreeSlot()
    local ret = Home.sFreeSlots[#Home.sFreeSlots]
    table.remove(Home.sFreeSlots)
    return ret
end
function Home.initialize(entries)
    if not Home.sInitialized then
        Home.sInitialized = true
        local unbreak_block_id = 123
        local build_block_id = 2218
        local player_x, player_y, player_z = Utility.getHomePosition()
        for x = 1, Home.sEdgeLen * 5 do
            for z = 1, Home.sEdgeLen do
                local block
                local chunk_x, chunk_z = x % Home.sEdgeLen, z % Home.sEdgeLen
                if
                (chunk_x <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_x > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2) or
                    (chunk_z <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_z > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2)
                then
                    block = {x, 1, z, unbreak_block_id}
                else
                    block = {x, 1, z, build_block_id}
                end
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for x = 1, Home.sEdgeLen * 5 do
            for z = Home.sEdgeLen * 4 + 1, Home.sEdgeLen * 5 do
                local block
                local chunk_x, chunk_z = x % Home.sEdgeLen, z % Home.sEdgeLen
                if
                (chunk_x <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_x > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2) or
                    (chunk_z <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_z > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2)
                then
                    block = {x, 1, z, unbreak_block_id}
                else
                    block = {x, 1, z, build_block_id}
                end
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for z = Home.sEdgeLen + 1, Home.sEdgeLen * 4 do
            for x = 1, Home.sEdgeLen do
                local block
                local chunk_x, chunk_z = x % Home.sEdgeLen, z % Home.sEdgeLen
                if
                (chunk_x <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_x > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2) or
                    (chunk_z <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_z > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2)
                then
                    block = {x, 1, z, unbreak_block_id}
                else
                    block = {x, 1, z, build_block_id}
                end
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for z = Home.sEdgeLen + 1, Home.sEdgeLen * 4 do
            for x = Home.sEdgeLen * 4 + 1, Home.sEdgeLen * 5 do
                local block
                local chunk_x, chunk_z = x % Home.sEdgeLen, z % Home.sEdgeLen
                if
                (chunk_x <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_x > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2) or
                    (chunk_z <= (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2 or
                    chunk_z > Home.sBuildingEdgeLen + (Home.sEdgeLen - Home.sBuildingEdgeLen) / 2)
                then
                    block = {x, 1, z, unbreak_block_id}
                else
                    block = {x, 1, z, build_block_id}
                end
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for x = 0, Home.sEdgeLen * 5 + 1 do
            for y = 2, Home.sYLen do
                local block = {x, y, 0, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for x = 0, Home.sEdgeLen * 5 + 1 do
            for y = 2, Home.sYLen do
                local block = {x, y, Home.sEdgeLen * 5 + 1, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for z = 0, Home.sEdgeLen * 5 + 1 do
            for y = 2, Home.sYLen do
                local block = {0, y, z, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for z = 0, Home.sEdgeLen * 5 + 1 do
            for y = 2, Home.sYLen do
                local block = {Home.sEdgeLen * 5 + 1, y, z, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for x = Home.sEdgeLen + 1, Home.sEdgeLen * 4 do
            for y = 2, Home.sYLen + 1 do
                local block = {x, y, Home.sEdgeLen + 1, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for x = Home.sEdgeLen + 1, Home.sEdgeLen * 4 do
            for y = 2, Home.sYLen + 1 do
                local block = {x, y, Home.sEdgeLen * 4, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for z = Home.sEdgeLen + 1, Home.sEdgeLen * 4 do
            for y = 2, Home.sYLen + 1 do
                local block = {Home.sEdgeLen + 1, y, z, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        for z = Home.sEdgeLen + 1, Home.sEdgeLen * 4 do
            for y = 2, Home.sYLen + 1 do
                local block = {Home.sEdgeLen * 4, y, z, 2217}
                Utility.setBlock(player_x + block[1], player_y + block[2], player_z + block[3], block[4])
                block = nil
            end
        end
        
        Home.sBound =
            new(
                Utility.BlockAABB,
                {mPivot = vector3d:new(x, y, z), mVolume = vector3d:new(Home.sEdgeLen * 4, Home.sYLen, Home.sEdgeLen * 4)}
        )
        Home.sEntries = {{}, {}, {}, {}}
        if entries then
            for k, entry in pairs(entries) do
                Home.sEntries[k] = entry
            end
        end
        Home.sBuildTimer = new(Utility.Timer)
    end
end

function Home.uninitialize()
    Home.sFreeSlots = nil
    delete(Home.sBound)
    Home.sBound = nil
    Home.sEntries = nil
    delete(Home.sBuildTimer)
    Home.sBuildTimer = nil
    Home.mListeners = nil
    Home.sInitialized = nil
    local player_x, player_y, player_z = Utility.getHomePosition()
    for x = 1, Home.sEdgeLen * 5 do
        for z = 1, Home.sEdgeLen * 5 do
            for y = 1, 250 do
                SetBlock(player_x + x, player_y + y, player_z + z, 0)
            end
        end
    end
end

function Home.setNextScene(index, scene)
    Home.sEntries[index].mScene = scene
    Home.notify("SetNextScene", {mIndex = index, mScene = scene})
end

function Home.addListener(listener)
    Home.mListeners = Home.mListeners or {}
    Home.mListeners[tostring(listener)] = listener
end

function Home.removeListener(listener)
    Home.mListeners = Home.mListeners or {}
    Home.mListeners[tostring(listener)] = nil
end

function Home.notify(event, parameter)
    if Home.mListeners then
        for _, listener in pairs(Home.mListeners) do
            listener:onHomeEvent(event, parameter)
        end
    end
end

function Home:construction(parameter)
    echo("devilwalk", "Host_GamePlayerHome:construction:self.mID:" .. tostring(self.mID))
    self.mSlot = Home.nextFreeSlot()
    self.mEntries = {
        {mPosition = self:getPosition() + vector3d:new(0, 1, 0)},
        {mPosition = self:getPosition() + vector3d:new(Home.sEdgeLen - 1, 1, 0)},
        {mPosition = self:getPosition() + vector3d:new(0, 1, Home.sEdgeLen - 1)},
        {mPosition = self:getPosition() + vector3d:new(Home.sEdgeLen - 1, 1, Home.sEdgeLen - 1)}
    }
    for _, entry in pairs(self.mEntries) do
        Utility.setBlock(entry.mPosition[1], entry.mPosition[2], entry.mPosition[3], Home.sDoorBlockID)
    end
    Framework.singleton():getCommandQueueManager():post(
        new(
            Command_Callback,
            {
                mDebug = "Host_GamePlayerHome:construction:travel player",
                mExecutingCallback = function(command)
                    if Home.sBuildTimer:total() > 3 then
                        self:getTravelPosition(
                            math.random(1, #self.mEntries),
                            function(pos)
                                SetEntityBlockPos(self.mPlayerID, pos[1], pos[2], pos[3])
                            end
                        )
                        command.mState = Command.EState.Finish
                    end
                end
            }
    )
    )
    for k, entry in pairs(Home.sEntries) do
        self:onHomeEvent("SetNextScene", {mIndex = k, mScene = entry.mScene})
    end
    Home.addListener(self)
end

function Home:destruction()
    Home.removeListener(self)
    for _, entry in pairs(self.mEntries) do
        Utility.setBlock(entry.mPosition[1], entry.mPosition[2], entry.mPosition[3])
    end
    if Home.sFreeSlots then
        Home.sFreeSlots[#Home.sFreeSlots + 1] = self.mSlot
    end
end

function Home:getSlot()
    return self.mSlot
end

function Home:getPlayerID()
    return self.mPlayerID
end

function Home:getPosition()
    local row
    local col
    if self.mSlot <= 5 then
        row = 1
        col = self.mSlot
    elseif self.mSlot <= 7 then
        row = 2
        if self.mSlot == 6 then
            col = 1
        else
            col = 5
        end
    elseif self.mSlot <= 9 then
        row = 3
        if self.mSlot == 6 then
            col = 1
        else
            col = 5
        end
    elseif self.mSlot <= 11 then
        row = 4
        if self.mSlot == 6 then
            col = 1
        else
            col = 5
        end
    else
        row = 5
        col = self.mSlot - 11
    end
    local x, y, z = Utility.getHomePosition()
    return vector3d:new({x + (col - 1) * Home.sEdgeLen + 1, 1, z + (row - 1) * Home.sEdgeLen + 1})
end

function Home:getTravelPosition(travelIndex, callback)
    local ret = vector3d:new(self.mEntries[travelIndex].mPosition)
    if travelIndex == 1 then
        ret = ret + vector3d:new(3, 0, 3)
    elseif travelIndex == 2 then
        ret = ret + vector3d:new(-3, 0, 3)
    elseif travelIndex == 3 then
        ret = ret + vector3d:new(3, 0, -3)
    elseif travelIndex == 4 then
        ret = ret + vector3d:new(-3, 0, -3)
    end
    callback(ret)
end

function Home:getBound()
    self.mBound =
        self.mBound or
        new(
            Utility.BlockAABB,
            {
                mPivot = self:getPosition() +
                vector3d:new({math.floor(Home.sEdgeLen / 2), 0, math.floor(Home.sEdgeLen / 2)}),
                mVolume = {Home.sEdgeLen, Home.sYLen, Home.sEdgeLen}
            }
    )
    return self.mBound
end

function Home:getBuildBound()
    self.mBuildBound =
        self.mBuildBound or
        new(
            Utility.BlockAABB,
            {
                mPivot = self:getPosition() +
                vector3d:new({math.floor(Home.sEdgeLen / 2), 0, math.floor(Home.sEdgeLen / 2)}),
                mVolume = {Home.sBuildingEdgeLen, Home.sYLen, Home.sBuildingEdgeLen}
            }
    )
    return self.mBuildBound
end

function Home:getNextScene(blockPosition)
    local entry_index =
        Utility.arrayIndex(
            self.mEntries,
            function(e)
                return vector3d:new(e.mPosition):equals(vector3d:new(blockPosition))
            end
    )
    local entry = Home.sEntries[entry_index]
    if not entry.mScene then
        local type = SceneManager.EScene.Custom
        if not Utility.arrayGet(Game.singleton():getSceneManager().mScenes, function(e) return (e:getType() == SceneManager.EScene.Nature) or (e:getType() == SceneManager.EScene.BigWorld) end) then
            type = SceneManager.EScene.Nature
        end
        Home.setNextScene(entry_index, Game.singleton():getSceneManager():createScene({mLastScene = {mInstance = self, mEntryIndex = entry_index}, mType = type}))
    end
    return entry.mScene
end

function Home:onHomeEvent(event, parameter)
    if event == "SetNextScene" then
        self.mEntries[parameter.mIndex].mScene = parameter.mScene
        if parameter.mScene then
            local pos = self.mEntries[parameter.mIndex].mPosition
            Utility.setBlock(pos[1], pos[2] + 1, pos[3], 211)
            local SignIO = require("SignIO")
            local sign = SignIO:new(pos[1], pos[2] + 1, pos[3]);
            if parameter.mScene:getTerrainConfigIndex() then
                sign:write("传送点:\n" .. Config.Terrain[parameter.mScene:getTerrainConfigIndex()].mName)
            elseif parameter.mScene:getType() == SceneManager.EScene.BigWorld then
                sign:write("传送点:\n大世界")
            elseif parameter.mScene:getType() == SceneManager.EScene.Nature then
                sign:write("传送点:\n神奇世界" .. tostring(parameter.mScene:getID()))
            end
        end
    end
end

function Home:_getSendKey()
    return "GamePlayerHome/" .. tostring(self.mPlayerID)
end
