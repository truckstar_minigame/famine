local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Game = Devil.getTable("Game.Famine.Host_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")

local Building = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_GameBuilding"))
function Building:construction(parameter)
    self.mItemConfigIndex = parameter.mItemConfigIndex
    self.mPivot = parameter.mPivot
    self.mFacing = parameter.mFacing
    self.mBlockDirection = parameter.mBlockDirection
    self.mProperty = new(Devil.Game.Famine.GameBuildingProperty, {mID = tostring(self.mPlayerID) .. "/" .. tostring(self.mID)})
    local buildings = self:getScene():intersectBuildingPoint(self:getBound():min(), true)
    if buildings then
        self.mParentID = buildings[1]:getID()
    end
    
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if building_cfg.mModel then
        local model_cfg = clone(building_cfg.mModel)
        model_cfg.mFacing = self.mFacing
        self.mEntityClientKey = Framework.singleton():getEntityCustomManager():createEntity({mType = "EntityNPCOnline", mX = self.mPivot[1], mY = self.mPivot[2], mZ = self.mPivot[3], mModel = model_cfg, mTextures = clone(building_cfg.mTextures)}, function(hostKey)
            self.mProperty:safeWrite("mEntityHostKey", hostKey)
        end)
    end
    local blocks = self:getBound():blocks()
    for _, block in pairs(blocks) do
        Utility.setBlock(block[1], block[2], block[3], building_cfg.mBlockID or 2217, self.mBlockDirection)
    end
    if building_cfg.mBag then
        self.mLocalBag = {mItems = {}}
    end
    if building_cfg.mEnable then
        self:_setEnable(false)
    end
end

function Building:destruction()
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if building_cfg.mBag then
        self.mLocalBag = nil
    end
    if building_cfg.mEnable then
        end
    if self.mBound then
        local blocks = self.mBound:blocks()
        for _, block in pairs(blocks) do
            Utility.setBlock(block[1], block[2], block[3])
        end
    end
    if self.mEntityClientKey then
        Framework.singleton():getEntityCustomManager():destroyEntity(self.mEntityClientKey)
        self.mEntityClientKey = nil
    end
    self.mProperty:safeWrite("mEntityHostKey")
    self.mProperty:safeWrite("mBag")
    self.mProperty:safeWrite("mEnable")
    self.mProperty:safeWrite("mCrafting")
    delete(self.mProperty)
    self.mProperty = nil
    self.mEntityClientKey = nil
    delete(self.mBound)
    self.mBound = nil
    self.mParentID = nil
end

function Building:update(deltaTime)
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    local enable = self.mProperty:cache().mEnable
    if building_cfg.mEnable and building_cfg.mEnable.mAuto then
        enable = true
    end
    if enable then
        if building_cfg.mEnable.mEnergyResources then
            if self.mCost then
                self.mCost = self.mCost - deltaTime
                if self.mCost <= 0 then
                    self.mCost = nil
                end
            else
                local item_slot
                local res
                for _, test_item in pairs(self.mLocalBag.mItems) do
                    for _, res_item in pairs(building_cfg.mEnable.mEnergyResources) do
                        if Config.Item[test_item.mConfigIndex].mName == res_item.mName then
                            item_slot = test_item.mBagSlot
                            res = res_item
                            break
                        end
                    end
                    if item_slot and res then
                        break
                    end
                end
                if item_slot and res then
                    self.mCost = res.mTime
                    self:_changeBagItems(nil, {{mSlot = item_slot, mCount = 1}})
                else
                    self:_setEnable(false)
                end
            end
        end
    end
    if building_cfg.mAutoDestroy then
        if building_cfg.mAutoDestroy.mTime then
            self.mAutoDestoryTime = self.mAutoDestoryTime or 0
            if self.mAutoDestoryTime >= building_cfg.mAutoDestroy.mTime then
                Game.singleton():getSceneManager():getSceneByID(self.mSceneID):destroyBuilding(self)
            else
                self.mAutoDestoryTime = self.mAutoDestoryTime + deltaTime
            end
        end
    end
    if self.mParentID and not self:getParent() then
        self:getScene():destroyBuilding(self)
    end
    
    self:_refreshGenerates(deltaTime)
    self:_refreshBag(deltaTime)
    self:_refreshCrafting(deltaTime)
end

function Building:getItemConfig()
    return Config.Item[self.mItemConfigIndex]
end

function Building:getConfig()
    return Utility.arrayGet(Config.Building, function(e) return e.mName == self:getItemConfig().mName end)
end

function Building:getID()
    return self.mID
end

function Building:getProperty()
    return self.mProperty
end

function Building:getPlayerID()
    return self.mPlayerID
end

function Building:getSceneID()
    return self.mSceneID
end

function Building:getPlayer()
    return Game.singleton():getPlayerManager():getPlayerByID(self.mPlayerID)
end

function Building:getScene()
    return Game.singleton():getSceneManager():getSceneByID(self.mSceneID)
end

function Building:getParent()
    if self.mParentID then
        return self:getScene():getBuildingByID(self.mParentID)
    end
end

function Building:getBound()
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if not self.mBound then
        self.mBound = GameUtility.calcBuildingBound(self.mPivot, building_cfg.mBlockSize)
        self.mBound:rotationAroundYAxis(self.mFacing)
    end
    return self.mBound
end

function Building:_getSendKey()
    return "GameBuilding/" .. tostring(self.mPlayerID) .. "/" .. "/" .. tostring(self.mSceneID) .. tostring(self.mID)
end

function Building:_receiveMessage(parameter)
    if parameter.mMessage == "Interactive" then
        self:_interactive()
    elseif parameter.mMessage == "AddBagItem" then
        local add_rets, _ = self:_changeBagItems({parameter.mParameter})
        self:responseToClient(parameter.mFrom, "AddBagItem", {mResponseCallbackKey = parameter.mParameter.mResponseCallbackKey, mCount = add_rets[1]})
    elseif parameter.mMessage == "RemoveBagItem" then
        local _, remove_rets = self:_changeBagItems(nil, {parameter.mParameter})
        self:responseToClient(parameter.mFrom, "RemoveBagItem", {mResponseCallbackKey = parameter.mParameter.mResponseCallbackKey, mCount = remove_rets[1]})
    elseif parameter.mMessage == "UseBlueprint" then
        local blueprint_cfg = Config.Blueprint[parameter.mParameter.mConfigIndex]
        if not blueprint_cfg.mResources or (self.mLocalBag and self.mLocalBag.mItems) then
            local remove_items = {}
            for _, res in pairs(blueprint_cfg.mResources) do
                local item_slot =
                    Utility.arrayIndex(
                        self.mLocalBag.mItems,
                        function(item)
                            return Config.Item[item.mConfigIndex].mName == res.mName
                        end
                )
                if item_slot and self.mLocalBag.mItems[item_slot].mCount >= res.mCount then
                    remove_items[#remove_items + 1] = {mSlot = item_slot, mCount = res.mCount}
                else
                    remove_items = nil
                    break
                end
            end
            if remove_items then
                self:_changeBagItems(nil, remove_items)
                self.mLocalCrafting = self.mLocalCrafting or {}
                local crafting =
                    Utility.arrayGet(
                        self.mLocalCrafting,
                        function(e)
                            return e.mConfigIndex == parameter.mParameter.mConfigIndex and e.mPlayerID == parameter.mFrom
                        end
                )
                if not crafting then
                    crafting = {mConfigIndex = parameter.mParameter.mConfigIndex, mTime = 0, mSpeed = parameter.mParameter.mSpeed or 1, mPlayerID = parameter.mFrom}
                    self.mLocalCrafting[#self.mLocalCrafting + 1] = crafting
                end
                crafting.mCount = crafting.mCount or 0
                crafting.mCount = crafting.mCount + 1
                self.mProperty:safeWrite("mCrafting", self.mLocalCrafting)
            end
        end
    elseif parameter.mMessage == "SetEnable" then
        self:_setEnable(parameter.mParameter.mValue)
    end
end

function Building:_setEnable(boolean)
    if self.mProperty:cache().mEnable ~= boolean then
        self.mProperty:safeWrite("mEnable", boolean)
        local building_cfg = self:getConfig()
        if self.mProperty:cache().mEnable then
            if building_cfg.mEnable.mModel then
                if self.mEntityClientKey then
                    Framework.singleton():getEntityCustomManager():destroyEntity(self.mEntityClientKey)
                    self.mEntityClientKey = nil
                end
                local model_cfg = clone(building_cfg.mEnable.mModel)
                model_cfg.mFacing = self.mFacing
                self.mEntityClientKey = Framework.singleton():getEntityCustomManager():createEntity({mType = "EntityNPCOnline", mX = self.mPivot[1], mY = self.mPivot[2], mZ = self.mPivot[3], mModel = model_cfg, mTextures = clone(building_cfg.mEnable.mTextures)}, function(hostKey)
                    self.mProperty:safeWrite("mEntityHostKey", hostKey)
                end)
            end
            if building_cfg.mEnable.mBlockID then
                local blocks = self:getBound():blocks()
                for _, block in pairs(blocks) do
                    Utility.setBlock(block[1], block[2], block[3], building_cfg.mEnable.mBlockID, self.mBlockDirection)
                end
            end
        else
            if building_cfg.mEnable.mModel then
                if self.mEntityClientKey then
                    Framework.singleton():getEntityCustomManager():destroyEntity(self.mEntityClientKey)
                    self.mEntityClientKey = nil
                end
                if building_cfg.mModel then
                    local model_cfg = clone(building_cfg.mModel)
                    model_cfg.mFacing = self.mFacing
                    self.mEntityClientKey = Framework.singleton():getEntityCustomManager():createEntity({mType = "EntityNPCOnline", mX = self.mPivot[1], mY = self.mPivot[2], mZ = self.mPivot[3], mModel = model_cfg, mTextures = clone(building_cfg.mTextures)}, function(hostKey)
                        self.mProperty:safeWrite("mEntityHostKey", hostKey)
                    end)
                end
                if building_cfg.mEnable.mBlockID then
                    local blocks = self:getBound():blocks()
                    for _, block in pairs(blocks) do
                        Utility.setBlock(block[1], block[2], block[3], building_cfg.mBlockID or 2217, self.mBlockDirection)
                    end
                end
            end
            self.mCost = nil
        end
    end
end

function Building:_refreshGenerates(deltaTime)
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if building_cfg.mGenerates then
        self.mGeneratings = self.mGeneratings or {}
        for k, generate in pairs(building_cfg.mGenerates) do
            if not generate.mNeedEnable or self.mProperty:cache().mEnable then
                local remove_items = {}
                if generate.mFrom then
                    for _, from in pairs(generate.mFrom) do
                        local item = Utility.arrayGet(self.mLocalBag.mItems, function(item) return (Config.Item[item.mConfigIndex].mName == from.mName) and (item.mCount >= from.mCount) end)
                        if item then
                            remove_items[#remove_items + 1] = {mSlot = item.mBagSlot, mCount = from.mCount}
                        else
                            remove_items = nil
                            break
                        end
                    end
                end
                if remove_items then
                    self.mGeneratings[k] = self.mGeneratings[k] or {}
                    self.mGeneratings[k].mTime = self.mGeneratings[k].mTime or 0
                    self.mGeneratings[k].mTime = self.mGeneratings[k].mTime + deltaTime
                    if self.mGeneratings[k].mTime >= generate.mTime then
                        local add_items = {}
                        for _, to in pairs(generate.mTo) do
                            local cfg_index = Utility.arrayIndex(Config.Item, function(item) return item.mName == to.mName end)
                            add_items[#add_items + 1] = {mConfigIndex = cfg_index, mCount = to.mCount, mHP = Config.Item[cfg_index].mHP}
                        end
                        self:_changeBagItems(add_items, remove_items)
                        self.mGeneratings[k] = nil
                    end
                else
                    self.mGeneratings[k] = nil
                end
            else
                self.mGeneratings[k] = nil
            end
        end
    end
end

function Building:_refreshBag(deltaTime)
    if self.mLocalBag then
        local item_cfg = self:getItemConfig()
        local building_cfg = self:getConfig()
        local remove_items
        for k, bag_item in pairs(self.mLocalBag.mItems) do
            local item_cfg = Config.Item[bag_item.mConfigIndex]
            if item_cfg.mHPSubtractByTime then
                local hp_sub = deltaTime * item_cfg.mHPSubtractByTime
                if building_cfg.mKeepFresh then
                    if not building_cfg.mKeepFresh.mNeedEnable or self.mProperty:cache().mEnable then
                        hp_sub = hp_sub * building_cfg.mKeepFresh.mValue
                    end
                end
                bag_item.mHP = bag_item.mHP - hp_sub
                if bag_item.mHP <= 0 then
                    bag_item.mHP = item_cfg.mHP
                    remove_items = remove_items or {}
                    remove_items[#remove_items + 1] = {mSlot = bag_item.mBagSlot, mCount = 1}
                end
            end
        end
        if remove_items then
            self:_changeBagItems(nil, remove_items)
        end
    end
end

function Building:_refreshCrafting(deltaTime)
    if self.mLocalCrafting and self.mLocalCrafting[1] then
        local crafting = self.mLocalCrafting[1]
        crafting.mTime =
            crafting.mTime + deltaTime * crafting.mSpeed
        local blueprint_cfg = Config.Blueprint[crafting.mConfigIndex]
        if crafting.mTime < blueprint_cfg.mCraftingTime then
            return
        end
        if blueprint_cfg.mGenerates.mItems then
            local add_exp = 0
            local add_bag_items = {}
            for _, v in pairs(blueprint_cfg.mGenerates.mItems) do
                add_exp = add_exp + (v.mExp or 0)
                add_bag_items[#add_bag_items + 1] =
                    {
                        mConfigIndex = Utility.arrayIndex(
                            Config.Item,
                            function(e)
                                return e.mName == v.mName
                            end
                        ),
                        mCount = v.mCount
                    }
            end
            self:_changeBagItems(add_bag_items)
            local player = Game.singleton():getPlayerManager():getPlayerByID(crafting.mPlayerID)
            if player then
                player:sendToClient(player:getID(), "AddExp", {mExp = add_exp})
            end
        end
        crafting.mCount = crafting.mCount - 1
        crafting.mTime = 0
        if crafting.mCount == 0 then
            table.remove(self.mLocalCrafting, 1)
        end
        self.mProperty:safeWrite("mCrafting", self.mLocalCrafting)
    end
end

function Building:_interactive()
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if #building_cfg.mInteractives > 0 then
        self.mInteractiveIndex = self.mInteractiveIndex or 0
        self.mInteractiveIndex = self.mInteractiveIndex + 1
        if self.mInteractiveIndex > #building_cfg.mInteractives then
            self.mInteractiveIndex = nil
        end
        if self.mInteractiveIndex then
            local entity = Framework.singleton():getEntityCustomManager():getEntityByClientKey(self.mEntityClientKey)
            if entity.mHostKey then
                if building_cfg.mInteractives[self.mInteractiveIndex].mModel then
                    entity:setModel(building_cfg.mInteractives[self.mInteractiveIndex].mModel)
                end
            end
        else
            local entity = Framework.singleton():getEntityCustomManager():getEntityByClientKey(self.mEntityClientKey)
            if entity.mHostKey then
                entity:setModel(building_cfg.mModel)
            end
        end
    end
end

function Building:_changeBagItems(addItems, removeItems)
    local function _addBagItem(item)
        local function _emptySlot()
            local item_cfg = self:getItemConfig()
            local building_cfg = self:getConfig()
            local slot = 1
            while self.mLocalBag.mItems[slot] do
                slot = slot + 1
            end
            if building_cfg.mBag.mSlotCount and slot > building_cfg.mBag.mSlotCount then
                slot = nil
            end
            return slot
        end
        local old_count = item.mCount
        local function _add(parameter)
            local item_cfg = Config.Item[parameter.mConfigIndex]
            local bag_item = Utility.arrayGet(self.mLocalBag.mItems, function(e) return e.mConfigIndex == parameter.mConfigIndex and (not item_cfg.mOverlap or item_cfg.mOverlap > e.mCount) end)
            if bag_item then
                bag_item.mCount = bag_item.mCount + parameter.mCount
                parameter.mCount = 0
                if item_cfg.mOverlap then
                    parameter.mCount = bag_item.mCount - item_cfg.mOverlap
                    bag_item.mCount = item_cfg.mOverlap
                end
                if parameter.mCount > 0 then
                    local slot = _emptySlot()
                    if slot then
                        self.mLocalBag.mItems[slot] = {mConfigIndex = parameter.mConfigIndex, mCount = 0, mHP = parameter.mHP, mBagSlot = slot}
                        _add(parameter)
                    end
                end
            else
                local slot = _emptySlot()
                if slot then
                    self.mLocalBag.mItems[slot] = {mConfigIndex = parameter.mConfigIndex, mCount = 0, mHP = parameter.mHP, mBagSlot = slot}
                    _add(parameter)
                end
            end
        end
        _add(item)
        return old_count - item.mCount
    end
    local function _removeBagItem(item)
        if item.mSlot then
            if self.mLocalBag.mItems[item.mSlot] then
                local remove_count = math.min(item.mCount or 999999999999999, self.mLocalBag.mItems[item.mSlot].mCount)
                self.mLocalBag.mItems[item.mSlot].mCount = self.mLocalBag.mItems[item.mSlot].mCount - remove_count
                if self.mLocalBag.mItems[item.mSlot].mCount <= 0 then
                    self.mLocalBag.mItems[item.mSlot] = nil
                end
                return remove_count
            end
        elseif item.mConfigIndex then
            local remove_count = item.mCount or 999999999999999
            local last_count = remove_count
            for k, bag_item in pairs(self.mLocalBag.mItems) do
                if bag_item.mConfigIndex == item.mConfigIndex then
                    local next_last_count = math.max(last_count - bag_item.mCount, 0)
                    bag_item.mCount = math.max(0, bag_item.mCount - last_count)
                    if bag_item.mCount == 0 then
                        self.mLocalBag.mItems[k] = nil
                    end
                    last_count = next_last_count
                    if last_count == 0 then
                        break
                    end
                end
            end
            return remove_count - last_count
        end
    end
    local add_bag_item_rets = {}
    if addItems then
        for k, item in pairs(addItems) do
            add_bag_item_rets[k] = _addBagItem(item)
        end
    end
    local remove_bag_item_rets = {}
    if removeItems then
        for k, item in pairs(removeItems) do
            remove_bag_item_rets[k] = _removeBagItem(item)
        end
    end
    self.mProperty:safeWrite("mBag", self.mLocalBag)
    return add_bag_item_rets, remove_bag_item_rets
end
-----------------------------------------------------------------------------------------------------------------------------------------------
local House = inherit(Building, Devil.getTable("Game.Famine.Host_GameBuilding.House"))

function House:construction(parameter)
    local blocks = self:getBound():blocks()
    for _, block in pairs(blocks) do
        Utility.setBlock(block[1], block[2], block[3])
    end
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    local auto_block_id
    if building_cfg.mName == "茅草屋" then
        auto_block_id = 186
    elseif building_cfg.mName == "木屋" then
        auto_block_id = 98
    elseif building_cfg.mName == "石屋" then
        auto_block_id = 56
    end
    local blocks = self:getBound():borderBlocks()
    if auto_block_id then
        for _, block in pairs(blocks) do
            Utility.setBlock(block[1], block[2], block[3], auto_block_id)
        end
    else
        for _, block in pairs(blocks) do
            Utility.setBlock(block[1], block[2], block[3], 2217)
        end
    end
end

function House:destruction(parameter)
    local blocks = self:getBound():borderBlocks()
    for _, block in pairs(blocks) do
        Utility.setBlock(block[1], block[2], block[3])
    end
    delete(self.mBound)
    self.mBound = nil
end
-----------------------------------------------------------------------------------------------------------------------------------------------
local HouseComponent = inherit(Building, Devil.getTable("Game.Famine.Host_GameBuilding.HouseComponent"))

function HouseComponent:construction(parameter)
end

function HouseComponent:destruction()
end

function HouseComponent:_interactive()
    self._super._interactive(self)
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if Utility.arrayContain(building_cfg.mTypes, "窗") or Utility.arrayContain(building_cfg.mTypes, "门") then
        if self.mInteractiveIndex then
            local blocks = self:getBound():blocks()
            for _, block in pairs(blocks) do
                Utility.setBlock(block[1], block[2], block[3])
            end
        else
            local blocks = self:getBound():blocks()
            for _, block in pairs(blocks) do
                Utility.setBlock(block[1], block[2], block[3], 2217)
            end
        end
    end
end
