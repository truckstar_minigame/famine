local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Host_Game")
local Config = Devil.getTable("Game.Famine.Config")

local ResourceManager = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_GameResourceManager"))
function ResourceManager:construction(parameter)
    self.mMiningResources = {}
    self.mResourceSourcesRefreshes = {}
end

function ResourceManager:destruction()
    self.mMiningResources = nil
    self.mResourceSourcesRefreshes = nil
end

function ResourceManager:update(deltaTime)
    local index = 1
    while index < #self.mResourceSourcesRefreshes do
        local res_src = self.mResourceSourcesRefreshes[index]
        local cfg = Config.ResourceSource[res_src.mConfigIndex]
        if cfg.mRefreshTime >= res_src.mRefreshTimer:total() then
            Utility.setBlock(res_src.mBlockPosition[1], res_src.mBlockPosition[2], res_src.mBlockPosition[3], cfg.mBlockID)
            table.remove(self.mResourceSourcesRefreshes, index)
        else
            index = index + 1
        end
    end
end

function ResourceManager:mining(parameter)
    local key
    local resource_cfg_index
    if parameter.mBlockPosition then
        key = tostring(parameter.mBlockPosition[1]) .. "," .. tostring(parameter.mBlockPosition[2]) .. "," .. tostring(parameter.mBlockPosition[3])
        local block_id = GetBlockId(parameter.mBlockPosition[1], parameter.mBlockPosition[2], parameter.mBlockPosition[3])
        resource_cfg_index =
            Utility.arrayIndex(
                Config.ResourceSource,
                function(e)
                    return e.mBlockID == block_id
                end
    )
    elseif parameter.mMonsterID then
        key = parameter.mMonsterID
        local monster = Game.singleton():getMonsterManager():getMonsterByID(parameter.mMonsterID)
        resource_cfg_index =
            Utility.arrayIndex(
                Config.ResourceSource,
                function(e)
                    return e.mMonsterName == monster:getConfig().mName
                end)
    end
    if resource_cfg_index then
        self.mMiningResources[key] = self.mMiningResources[key] or {mHP = Config.ResourceSource[resource_cfg_index].mHP, mMonsterID = parameter.mMonsterID}
        local res = self.mMiningResources[key]
        res.mHP = res.mHP - parameter.mDamage
        if res.mHP <= 0 then
            if Config.ResourceSource[resource_cfg_index].mRefreshTime then
                self.mResourceSourcesRefreshes[#self.mResourceSourcesRefreshes + 1] = {
                    mBlockPosition = parameter.mBlockPosition,
                    mConfigIndex = resource_cfg_index,
                    mRefreshTimer = new(Utility.Timer)
                }
            end
            if res.mMonsterID then
                Game.singleton():getMonsterManager():destroyMonster(Game.singleton():getMonsterManager():getMonsterByID(parameter.mMonsterID))
            else
                Utility.setBlock(parameter.mBlockPosition[1], parameter.mBlockPosition[2], parameter.mBlockPosition[3])
            end
            res.mMonsterID = nil
            res.mHP = nil
            self.mMiningResources[key] = nil
            return true
        end
    end
end

function ResourceManager:_getSendKey()
    return "GameResourceManager"
end

function ResourceManager:_receiveMessage(parameter)
end
