local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Monster = Devil.getTable("Game.Famine.Client_GameMonster")
local MonsterManager = inherit(Devil.Framework.ClientObjectBase, Devil.getTable("Game.Famine.Client_GameMonsterManager"))
function MonsterManager:construction(parameter)
    self.mMonsters = {}
    self:requestToHost("Sync",nil,function(parameter)
        for _,monster in pairs(parameter.mMonsters) do
            self:_createMonster(monster)
        end
    end)
end

function MonsterManager:destruction()
    for _, monster in pairs(self.mMonsters) do
        delete(monster)
    end
    self.mMonsters = nil
end

function MonsterManager:update(deltaTime)
    for _, monster in pairs(self.mMonsters) do
        monster:update(deltaTime)
    end
end

function MonsterManager:getMonsterByID(id)
    return Utility.arrayGet(self.mMonsters, function(e) return e:getID() == id end)
end

function MonsterManager:getMonsterByEntityID(entityID)
    return Utility.arrayGet(self.mMonsters, function(e) if e:getEntity() then return e:getEntity().entityId == entityID end end)
end

function MonsterManager:intersectMonsters(bounds)
    local ret
    for _, bound in pairs(bounds) do
        local r = Utility.arrayGet(self.mMonsters,
            function(e)
                if e:getEntity() then
                    local range = vector3d:new(e:getConfig().mBound or {1, 1, 1})
                    local aabb = new(Utility.AABB, {mMin = range * 0.5 * (-1) + e:getEntity():getPosition(), mMax = range * 0.5 + e:getEntity():getPosition()})
                    return bound:intersectAABB(aabb)
                end
            end, true)
        if r then
            for _, monster in pairs(r) do
                ret = ret or {}
                ret[tostring(monster)] = monster
            end
        end
    end
    if ret then
        ret = Utility.array(ret)
        return ret
    end
end

function MonsterManager:_getSendKey()
    return "GameMonsterManager"
end

function MonsterManager:_receiveMessage(parameter)
    if parameter.mMessage == "CreateMonster" then
        self:_createMonster(parameter.mParameter)
    elseif parameter.mMessage == "DestroyMonster" then
        self:_destroyMonster(parameter.mParameter.mID)
    end
end

function MonsterManager:_createMonster(parameter)
    local cfg = Config.Monster[parameter.mConfigIndex]
    local init_members = {mID = parameter.mID}
    local ret
    ret = new(Monster, {mInitMembers = init_members, mConfigIndex = parameter.mConfigIndex})
    self.mMonsters[#self.mMonsters + 1] = ret
    return ret
end

function MonsterManager:_destroyMonster(id)
    Utility.arrayRemove(self.mMonsters, function(e) return e:getID() == id end, delete)
end
