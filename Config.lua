local Config = Devil.getTable("Game.Famine.Config")
Config.Player = {
    mHP = 100, -- 血量
    mFood = 100, -- 饥饿
    mWater = 100, -- 水
    mStamina = 100, -- 体力
    mOxygen = 100, -- 氧气
    mWeight = 100, -- 负重
    mMovementSpeed = 1, -- 移速
    mMeleeDamage = 1, -- 近战攻击
    mFortitude = 0, -- 环境抵抗
    mCraftingSpeed = 1, -- 制造速度
    mHPAdditionPerLevel = 10,
    mFoodAdditionPerLevel = 10,
    mWaterAdditionPerLevel = 10,
    mStaminaAdditionPerLevel = 10,
    mOxygenAdditionPerLevel = 10,
    mWeightAdditionPerLevel = 10,
    mMovementSpeedAdditionPerLevel = 0.01,
    mMeleeDamageAdditionPerLevel = 0.1,
    mFortitudeAdditionPerLevel = 2,
    mCraftingSpeedAdditionPerLevel = 0.1,
    -----------------------------------------------------------------------------------------------
    mRunCost = 2, -- 跑步每秒消耗体力
    mSwimCost = 2, -- 游泳每秒消耗体力
    mMeleeAttackCost = 1, -- 近战攻击每秒消耗体力
    mCollectCost = 1, -- 采集每秒消耗体力
    mStaminaRecoverInterval = 3, -- 体力恢复间隔时间
    mStaminaRecoverSpeed = 10, -- 每秒恢复体力
    mHPRecoverSpeed = 0.01,
    mLevelUpPropertyPoint = 1,
    mLevelUpBlueprintPoint = 10,
    mExpObtainStand = 1,
    mFoodCost = 0.1,
    mWaterCost = 0.1,
    mRunSpeedScale = 2,
    mMeleeAttackSpeed = 0.8
}

Config.Default = {
    Avatar = {
        Head = {
            mModel = {
                mFemale = {mFile = "gameassets/models/characters/Avatar_girl_hair_B_RAF(Avatar_girl_animation).fbx"},
                mMale = {mFile = "gameassets/models/characters/Avatar_boy_hair_B_RAF(Avatar_boy_animation).fbx"}
            },
            mTextures = {
                mFemale = {
                    [2] = {mFile = "gameassets/textures/characters/face/eye_boy_A_fps14_emoji004.png"},
                    [3] = {mFile = "gameassets/textures/characters/face/mouth_fps14_emoji004.png"}
                },
                mMale = {
                    [2] = {mFile = "gameassets/textures/characters/face/eye_boy_A_fps14_emoji004.png"},
                    [3] = {mFile = "gameassets/textures/characters/face/mouth_fps14_emoji004.png"}
                }
            }
        },
        Body = {
            mModel = {
                mFemale = {mFile = "gameassets/models/characters/Avatar_girl_body_RAF(Avatar_girl_animation).fbx"},
                mMale = {mFile = "gameassets/models/characters/Avatar_boy_body_RAF(Avatar_boy_animation).fbx"}
            },
            mWaist = {
                mModel = {
                    mFemale = {mFile = "gameassets/models/characters/Avatar_girl_waist_RAF(Avatar_girl_animation).fbx"},
                    mMale = {mFile = "gameassets/models/characters/Avatar_boy_waist_RAF(Avatar_boy_animation).fbx"}
                }
            }
        },
        Leg = {
            mModel = {
                mFemale = {mFile = "gameassets/models/characters/Avatar_girl_leg_RAF(Avatar_girl_animation).fbx"},
                mMale = {mFile = "gameassets/models/characters/Avatar_boy_leg_RAF(Avatar_boy_animation).fbx"}
            }
        },
        Hand = {
            mModel = {
                mFemale = {mFile = "gameassets/models/characters/Avatar_girl_hand_RAF(Avatar_girl_animation).fbx"},
                mMale = {mFile = "gameassets/models/characters/Avatar_boy_hand_RAF(Avatar_boy_animation).fbx"}
            }
        },
        Foot = {
            mModel = {
                mFemale = {mFile = "gameassets/models/characters/Avatar_girl_foot_RAF(Avatar_girl_animation).fbx"},
                mMale = {mFile = "gameassets/models/characters/Avatar_boy_foot_RAF(Avatar_boy_animation).fbx"}
            }
        }
    }
}

Config.Item = {
    {
        mTypes = {"武器", "斧"},
        mName = "石斧",
        mWeight = 2,
        mOverlap = 1,
        mHP = 100,
        mAttack = {mSelfDamage = 1, mDamage = 2, },
        mCollect = {
            mSelfDamage = 1,
            mDamage = 1,
            mEfficiency = {
                {mName = "石头", mValue = 2},
                {mName = "燧石", mValue = 1.25},
                {mName = "木头", mValue = 2},
                {mName = "茅草", mValue = 1.25},
                {mName = "生肉", mValue = 1.25},
                {mName = "兽皮", mValue = 2}
            }
        },
        mIcon = {mResource = {hash = "FrtWCZqIXGlsXc7t4CBl4B-9ZfOf", pid = "177493", ext = "png"}},
        mModel = {mFile = "gameassets/models/characters/Avatar_doublehand_RAF(Avatar_girl_animation).fbx"}
    },
    {
        mTypes = {"武器", "镐"},
        mName = "石镐",
        mWeight = 2,
        mOverlap = 1,
        mHP = 100,
        mAttack = {mSelfDamage = 1, mDamage = 2, },
        mCollect = {
            mSelfDamage = 1,
            mDamage = 1,
            mEfficiency = {
                {mName = "石头", mValue = 1.25},
                {mName = "燧石", mValue = 2},
                {mName = "木头", mValue = 1.25},
                {mName = "茅草", mValue = 2},
                {mName = "生肉", mValue = 2},
                {mName = "兽皮", mValue = 1.25}
            }
        },
        mIcon = {mResource = {hash = "Fni0PIrbo7j-FQB6s9Y8ObyERWWY", pid = "177494", ext = "png"}},
        mModel = {mFile = "gameassets/models/characters/Avatar_draft_RAF(Avatar_girl_animation).fbx"}
    },
    {
        mTypes = {"建筑", "放置物", "手持物"},
        mName = "火把",
        mWeight = 1,
        mIcon = {mResource = {hash = "FttNdkhtt-Vo4KJvcPfrcxBPTtk8", pid = "177492", ext = "png"}}
    },
    {
        mTypes = {"建筑"},
        mName = "篝火",
        mWeight = 4,
        mIcon = {mResource = {ext = "png", hash = "Fnh8U6GbJOkwILznY0Fsjvie2-fj", name = "火堆", pid = "172071"}},
    },
    {mTypes = {"建筑"}, mName = "茅草屋", mWeight = 30},
    {mTypes = {"建筑"}, mName = "木屋", mWeight = 60},
    {mTypes = {"建筑"}, mName = "石屋", mWeight = 90},
    {
        mTypes = {"建筑"},
        mName = "研磨器",
        mWeight = 4,
        mIcon = {mResource = {ext = "png", hash = "FnC2hjcFfflBntejJy4zBzwdLJU7", name = "研磨", pid = "172080"}},
    },
    {
        mTypes = {"武器"},
        mName = "弩",
        mWeight = 4,
        mOverlap = 1,
        mIcon = {mResource = {ext = "png", hash = "FsRKPiUaXIsIsGrG2k-PbmcPGD3D", name = "弩", pid = "172074"}},
        mModel = {mResource = {hash = "FvWI92KRQuOaNhDVxE8p0BbURoiO", pid = "157812", ext = "FBX"}, mScaling = 0.15}--[[mTextures = {{mResource = {hash = "Fr9qbW1PRWsrGNoErg2MQpGavq1l", pid = "135967", ext = "png", }}}]]
    },
    {
        mTypes = {"建筑"},
        mName = "锅",
        mWeight = 4,
        mIcon = {mResource = {ext = "png", hash = "FleQpixWp6Vmm8gullXP_n14D2Ps", name = "锅", pid = "172069"}},
    },
    {
        mTypes = {"资源"},
        mName = "石头",
        mWeight = 0.5,
        mIcon = {mResource = {ext = "png", hash = "FsGzzhSohhK15SH1mjJ4Wz3R8UgY", name = "shro", pid = "157758"}}
    },
    {
        mTypes = {"资源"},
        mName = "燧石",
        mWeight = 0.05,
        mIcon = {mResource = {ext = "png", hash = "Fn7xafIt5Xfemir7G66AAOYJtNBF", name = "suishi", pid = "157759"}}
    },
    {
        mTypes = {"资源"},
        mName = "沙",
        mWeight = 0.05,
    },
    {
        mTypes = {"资源"},
        mName = "铁",
        mWeight = 1,
    },
    {
        mTypes = {"资源"},
        mName = "茅草",
        mWeight = 0.01,
        mIcon = {mResource = {ext = "png", hash = "FiAFQ4Ss8YLne_WeV6o_M66o87cl", name = "maoca", pid = "157742"}}
    },
    {
        mTypes = {"资源"},
        mName = "木头",
        mWeight = 0.5,
        mIcon = {mResource = {ext = "png", hash = "FoLkouqwTuwjrpeCtZBflhZLSbVu", name = "mutou", pid = "157745"}}
    },
    {
        mTypes = {"资源"},
        mName = "纤维",
        mWeight = 0.01,
        mIcon = {mResource = {ext = "png", hash = "Fg2v9eYgRXuTfN0ogkdG8kLtMgys", name = "xianwei", pid = "157761"}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "浆果(白)",
        mWeight = 0.1,
        mHP = 60,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "FgT6aebBqBRHDnWT5pUsi9KY4nKy", name = "jiangguo", pid = "157736"}},
        mUse = {mPlayerPropertyChange = {mFood = 1, mWater = -1, mFaint = -1}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "浆果(黑)",
        mWeight = 0.1,
        mHP = 60,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "Fmzqvof1DaYc4tRSatZr-oqdCySZ", name = "jiangguo", pid = "157737"}},
        mUse = {mPlayerPropertyChange = {mFood = 1, mWater = 0.1, mFaint = 1}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "浆果(蓝)",
        mWeight = 0.1,
        mHP = 60,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "FvfDDgQPP9mIUjXALZNuOY6bFTBq", name = "jiangguol", pid = "157740"}},
        mUse = {mPlayerPropertyChange = {mFood = 1, mWater = 0.1}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "浆果(黄)",
        mWeight = 0.1,
        mHP = 60,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "FrBdtEpFZwXdsrFfstAnHcfhNChT", name = "jiangguohu", pid = "157739"}},
        mUse = {mPlayerPropertyChange = {mFood = 1, mWater = 0.1}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "浆果(紫)",
        mWeight = 0.1,
        mHP = 60,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "Fs7hRyUvH3YQNg9wWJ3L9apw-Udt", name = "jiangguozi", pid = "157741"}},
        mUse = {mPlayerPropertyChange = {mFood = 1, mWater = 0.1}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "浆果(红)",
        mWeight = 0.1,
        mHP = 60,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "Fuc6pKCm99wOI_5m_FMY2Pf7sswy", name = "jiangguoh", pid = "157738"}},
        mUse = {mPlayerPropertyChange = {mFood = 1, mWater = 0.1}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "生肉",
        mWeight = 0.1,
        mHP = 100,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "FqEjVD2_ctuJx7sc6uMIViYsaxdb", name = "shengrou", pid = "157748"}},
        mUse = {mPlayerPropertyChange = {mFood = 10, mHP = -5}}
    },
    {
        mTypes = {"资源", "食物"},
        mName = "熟肉",
        mWeight = 0.1,
        mHP = 200,
        mHPSubtractByTime = 1,
        mIcon = {mResource = {ext = "png", hash = "FsGzzhSohhK15SH1mjJ4Wz3R8UgY", name = "shro", pid = "157758"}},
        mUse = {mPlayerPropertyChange = {mFood = 20, mHP = 5}}
    },
    {
        mTypes = {"资源"},
        mName = "兽皮",
        mWeight = 0.01,
        mIcon = {mResource = {ext = "png", hash = "FlbMkStqxNt_T8fHOD5kzPTomKla", name = "shoupi", pid = "157751"}}
    },
    {
        mTypes = {"资源"},
        mName = "毛皮",
        mWeight = 0.5,
    },
    {
        mTypes = {"资源"},
        mName = "引火粉",
        mWeight = 0.01,
    },
    {
        mTypes = {"建筑"},
        mName = "小箱子",
        mWeight = 5,
        mIcon = {mResource = {ext = "png", hash = "FiVOXMxWnPrbAijXWegBwesqFIsn", name = "小柜子", pid = "172079"}},
    },
    {
        mTypes = {"建筑"},
        mName = "大箱子",
        mWeight = 10,
        mIcon = {mResource = {ext = "png", hash = "FiJ-TB73bvlw9tQCeSVRUZ6zxhTj", name = "大柜子", pid = "172068"}},
    },
    {
        mTypes = {"建筑"},
        mName = "风干箱",
        mWeight = 10,
        mIcon = {mResource = {ext = "png", hash = "FmgP6dDPLvtH-hbX8jvR0t8O64ur", name = "烘", pid = "172070"}},
    },
    {
        mTypes = {"建筑"},
        mName = "床",
        mWeight = 10,
        mIcon = {mResource = {ext = "png", hash = "FrATqJpsAGX3hsvUj5eo8QrdtZz4", name = "床", pid = "172067"}},
    },
    {
        mTypes = {"建筑"},
        mName = "茅草门",
        mWeight = 10,
        mIcon = {mResource = {ext = "png", hash = "FqbMFyiBTCzUm_FzAUSrrxl83nER", name = "木门", pid = "172064"}},
    },
    {
        mTypes = {"建筑"},
        mName = "木门",
        mWeight = 15,
        mIcon = {mResource = {ext = "png", hash = "FiA4-Xoi-wvRXEqgyMWFCWnd8J1E", name = "木门", pid = "172073"}},
    },
    {
        mTypes = {"建筑"},
        mName = "石门",
        mWeight = 20,
        mIcon = {mResource = {ext = "png", hash = "FlaX3uqWt5KtGESxI0HB09fX6_F6", name = "石门", pid = "172076"}},
    },
    {
        mTypes = {"帽子"},
        mName = "布帽",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 10,
        mFortitude = {mHot = -10, mCold = 10},
        mIcon = {
            mFemale = {mResource = {hash = "FpVjvf02Z8_pfBleHCsB0x3bwvHG", pid = "177491", ext = "png"}},
            mMale = {mResource = {hash = "FpVjvf02Z8_pfBleHCsB0x3bwvHG", pid = "177491", ext = "png"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_hair_B_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_hair_B_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {
                [2] = {mFile = "gameassets/textures/characters/face/eye_boy_A_fps14_emoji004.png"},
                [3] = {mFile = "gameassets/textures/characters/face/mouth_fps14_emoji004.png"}
            },
            mMale = {
                [2] = {mFile = "gameassets/textures/characters/face/eye_boy_A_fps14_emoji004.png"},
                [3] = {mFile = "gameassets/textures/characters/face/mouth_fps14_emoji004.png"}
            }
        }
    },
    {
        mTypes = {"衣服"},
        mName = "布衣",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 10,
        mFortitude = {mHot = -10, mCold = 10},
        mIcon = {
            mFemale = {mResource = {ext = "png", hash = "Fq-5e6dEZz_rUQyQHElknBYl42Xd", name = "布衣女", pid = "172063"}},
            mMale = {mResource = {ext = "png", hash = "Fnd4SIaf_fNYF4uc17Xw6Xvz3wxD", name = "布衣男", pid = "172062"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_body_C_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_body_B_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "Fr6RkNuwDv7wUP-4Ku7MGkwpjU93", pid = "157795", ext = "png"}}},
            mMale = {{mResource = {hash = "FrtNfq4SiHg8GppC_zzzOE-Cek0J", pid = "157791", ext = "png"}}}
        }
    },
    {
        mTypes = {"裤子"},
        mName = "布裤子",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 10,
        mFortitude = {mHot = -10, mCold = 10},
        mIcon = {
            mFemale = {mResource = {ext = "png", hash = "FjrnTjjSmOSpw52ygIoW82auaWHu", name = "布裤女", pid = "172059"}},
            mMale = {mResource = {ext = "png", hash = "FtUNaXRWERs-f3kZcAB32a8BIHpF", name = "布裤男", pid = "172058"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_leg_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_leg_B_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "Fqnq8w4qOmugVrXfAeV83ri-PlUc", pid = "157802", ext = "png"}}},
            mMale = {{mResource = {hash = "FrPZ7RWOsT1ggviXwkWpg3LD1Wwo", pid = "157793", ext = "png"}}}
        }
    },
    {
        mTypes = {"手套"},
        mName = "布手套",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 10,
        mFortitude = {mHot = -10, mCold = 10},
        mIcon = {
            mFemale = {mResource = {ext = "png", hash = "FuoZqa12geYJuFdyKKAotORf5KSu", name = "布手套", pid = "172060"}},
            mMale = {mResource = {ext = "png", hash = "FuoZqa12geYJuFdyKKAotORf5KSu", name = "布手套", pid = "172060"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_hand_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_hand_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "FqlwiAyYhm4EeZl7hqe3-NbCI55i", pid = "162037", ext = "png"}}},
            mMale = {{mResource = {hash = "FqlwiAyYhm4EeZl7hqe3-NbCI55i", pid = "162037", ext = "png"}}}
        }
    },
    {
        mTypes = {"鞋子"},
        mName = "布鞋",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 10,
        mFortitude = {mHot = -10, mCold = 10},
        mIcon = {
            mFemale = {mResource = {ext = "png", hash = "FiMjbjmRNo0d8NCIl3JAMSoXI-JV", name = "布鞋子", pid = "172061"}},
            mMale = {mResource = {ext = "png", hash = "FiMjbjmRNo0d8NCIl3JAMSoXI-JV", name = "布鞋子", pid = "172061"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_foot_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_foot_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "Fq0D2g0g1IYue-X2PuYCJiIl0Vlz", pid = "157797", ext = "png"}}},
            mMale = {{mResource = {hash = "Fq0D2g0g1IYue-X2PuYCJiIl0Vlz", pid = "157797", ext = "png"}}}
        }
    },
    {
        mTypes = {"帽子"},
        mName = "兽皮帽",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 15,
        mFortitude = {mHot = -15, mCold = 15},
        mIcon = {
            mFemale = {mResource = {ext = "png", hash = "Fm1OxDHvdIgnwTTvjFaBwQD_G9LT", name = "皮帽子", pid = "177472"}},
            mMale = {mResource = {ext = "png", hash = "Fm1OxDHvdIgnwTTvjFaBwQD_G9LT", name = "皮帽子", pid = "177472"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_hair_B_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_hair_B_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {
                [2] = {mFile = "gameassets/textures/characters/face/eye_boy_A_fps14_emoji004.png"},
                [3] = {mFile = "gameassets/textures/characters/face/mouth_fps14_emoji004.png"}
            },
            mMale = {
                [2] = {mFile = "gameassets/textures/characters/face/eye_boy_A_fps14_emoji004.png"},
                [3] = {mFile = "gameassets/textures/characters/face/mouth_fps14_emoji004.png"}
            }
        }
    },
    {
        mTypes = {"衣服"},
        mName = "兽皮衣",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 15,
        mFortitude = {mHot = -15, mCold = 15},
        mIcon = {
            mFemale = {mResource = {hash = "FtD8Ov4kpQT8gm6O5UwNWSURQUQ4", pid = "172078", ext = "png"}},
            mMale = {mResource = {hash = "FrS3bTShT0GIU19vJfXUVfRz4AQZ", pid = "172077", ext = "png"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_body_C_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_body_B_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "FuEx7Xl5TMjeXHcrPTmGONFy-ORp", pid = "157796", ext = "png"}}},
            mMale = {{mResource = {hash = "FlsWLrUTbMVQazApR1piyqo7WKW8", pid = "157792", ext = "png"}}}
        }
    },
    {
        mTypes = {"裤子"},
        mName = "兽皮裤",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 15,
        mFortitude = {mHot = -15, mCold = 15},
        mIcon = {
            mFemale = {mResource = {hash = "FpPCzXClutrO0-NgLTnYmc--5dM6", pid = "172066", ext = "png"}},
            mMale = {mResource = {hash = "FiDyeCPybCAQ2BpVTIcUJAhTwuNv", pid = "172065", ext = "png"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_leg_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_leg_B_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "FgLW5yOb-1XC4Id6LpkcKclc4113", pid = "157803", ext = "png"}}},
            mMale = {{mResource = {hash = "Fl4Itf0xxcQ9Gt2FDrawOarlHShh", pid = "157794", ext = "png"}}}
        }
    },
    {
        mTypes = {"手套"},
        mName = "兽皮手套",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 15,
        mFortitude = {mHot = -15, mCold = 15},
        mIcon = {
            mFemale = {mResource = {hash = "Fh9xKlTwNuGWKIdgcFHfb76PBzH9", pid = "157747", ext = "png"}},
            mMale = {mResource = {hash = "Fh9xKlTwNuGWKIdgcFHfb76PBzH9", pid = "157747", ext = "png"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_hand_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_hand_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "Fhwz9esU7pNtNqOzpP9IwHZn1K_-", pid = "157800", ext = "png"}}},
            mMale = {{mResource = {hash = "Fhwz9esU7pNtNqOzpP9IwHZn1K_-", pid = "157800", ext = "png"}}}
        }
    },
    {
        mTypes = {"鞋子"},
        mName = "兽皮鞋",
        mWeight = 1,
        mOverlap = 1,
        mHP = 100,
        mDefense = 15,
        mFortitude = {mHot = -15, mCold = 15},
        mIcon = {
            mFemale = {mResource = {hash = "Fs9SqkNAmEmrquwoPCQgs0yQBh7D", pid = "177484", ext = "png"}},
            mMale = {mResource = {hash = "Fs9SqkNAmEmrquwoPCQgs0yQBh7D", pid = "177484", ext = "png"}}
        },
        mModel = {
            mFemale = {mFile = "gameassets/models/characters/Avatar_girl_foot_RAF(Avatar_girl_animation).fbx"},
            mMale = {mFile = "gameassets/models/characters/Avatar_boy_foot_RAF(Avatar_boy_animation).fbx"}
        },
        mTextures = {
            mFemale = {{mResource = {hash = "Fnh67S6BkL7pREhlyMlC4I2RrsFb", pid = "157798", ext = "png"}}},
            mMale = {{mResource = {hash = "Fnh67S6BkL7pREhlyMlC4I2RrsFb", pid = "157798", ext = "png"}}}
        }
    }
}

Config.ResourceSource = {
    {
        mBlockID = 2218,
        mResources = {{mNames = {"石头", "燧石", "纤维"}, mCount = 1}},
        mHP = 3,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}},
        mExp = 1
    },
    {
        mBlockID = 51,
        mResources = {{mNames = {"石头"}, mCount = 2}, {mNames = {"燧石"}, mCount = 2}, {mNames = {"沙"}, mCount = 2}},
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}},
        mExp = 2
    },
    {
        mBlockID = 55,
        mResources = {{mNames = {"石头"}, mCount = 4}, {mNames = {"燧石"}, mCount = 4}, {mNames = {"沙"}, mCount = 4}},
        mHP = 8,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}},
        mExp = 3
    },
    {
        mBlockID = 12,
        mResources = {{mNames = {"石头"}, mCount = 6}, {mNames = {"燧石"}, mCount = 6}, {mNames = {"沙"}, mCount = 6}},
        mHP = 10,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}},
        mExp = 4
    },
    {
        mBlockID = 155,
        mResources = {{mNames = {"石头"}, mCount = 6}, {mNames = {"燧石"}, mCount = 6}, {mNames = {"沙"}, mCount = 6}},
        mHP = 10,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}},
        mExp = 4
    },
    {
        mBlockID = 56,
        mResources = {{mNames = {"石头"}, mCount = 10}, {mNames = {"燧石"}, mCount = 10}},
        mHP = 15,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 5
    },
    {
        mBlockID = 58,
        mResources = {{mNames = {"石头"}, mCount = 15}, {mNames = {"燧石"}, mCount = 15}},
        mHP = 20,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 6
    },
    {
        mBlockID = 60,
        mResources = {{mNames = {"石头"}, mCount = 20}, {mNames = {"燧石"}, mCount = 20}},
        mHP = 25,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 7
    },
    {
        mBlockID = 77,
        mResources = {{mNames = {"石头"}, mCount = 30}, {mNames = {"燧石"}, mCount = 30}},
        mHP = 30,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 8
    },
    {
        mBlockID = 146,
        mResources = {{mNames = {"石头"}, mCount = 30}, {mNames = {"燧石"}, mCount = 30}, {mNames = {"铁"}, mCount = 1}},
        mHP = 50,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 10
    },
    {
        mBlockID = 150,
        mResources = {{mNames = {"石头"}, mCount = 30}, {mNames = {"燧石"}, mCount = 30}, {mNames = {"铁"}, mCount = 2}},
        mHP = 60,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 15
    },
    {
        mBlockID = 2056,
        mResources = {{mNames = {"石头"}, mCount = 30}, {mNames = {"燧石"}, mCount = 30}, {mNames = {"铁"}, mCount = 3}},
        mHP = 70,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 20
    },
    {
        mBlockID = 2077,
        mResources = {{mNames = {"石头"}, mCount = 30}, {mNames = {"燧石"}, mCount = 30}, {mNames = {"铁"}, mCount = 4}},
        mHP = 80,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 25
    },
    {
        mBlockID = 2076,
        mResources = {{mNames = {"石头"}, mCount = 30}, {mNames = {"燧石"}, mCount = 30}, {mNames = {"铁"}, mCount = 5}},
        mHP = 100,
        mSupportTools = {{mType = "斧"}, {mType = "镐"}},
        mExp = 30
    },
    {mBlockID = 98, mResources = {{mNames = {"木头"}, mCount = 10}, {mNames = {"茅草"}, mCount = 10}}, mHP = 10, mExp = 5},
    {mBlockID = 99, mResources = {{mNames = {"木头"}, mCount = 10}, {mNames = {"茅草"}, mCount = 10}}, mHP = 10, mExp = 5},
    {mBlockID = 126, mResources = {{mNames = {"木头"}, mCount = 10}, {mNames = {"茅草"}, mCount = 10}}, mHP = 10, mExp = 5},
    {mBlockID = 128, mResources = {{mNames = {"木头"}, mCount = 10}, {mNames = {"茅草"}, mCount = 10}}, mHP = 10, mExp = 5},
    {mBlockID = 2274, mResources = {{mNames = {"木头"}, mCount = 10}, {mNames = {"茅草"}, mCount = 10}}, mHP = 10, mExp = 5},
    {mBlockID = 2291, mResources = {{mNames = {"木头"}, mCount = 10}, {mNames = {"茅草"}, mCount = 10}}, mHP = 10, mExp = 5},
    {mBlockID = 2078, mResources = {{mNames = {"木头"}, mCount = 10}, {mNames = {"茅草"}, mCount = 10}}, mHP = 10, mExp = 5},
    {
        mBlockID = 113,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 164,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 165,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 114,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 115,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 116,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 2041,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 2043,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 2044,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 132,
        mResources = {
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 118,
        mResources = {{mNames = {"纤维"}, mCount = 20}},
        mHP = 20,
        mSupportTools = {{mName = "手"}, {mName = "镰刀"}}, mExp = 5
    },
    {
        mBlockID = 92,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mBlockID = 85,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mBlockID = 86,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mBlockID = 91,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mBlockID = 129,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mBlockID = 2045,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mBlockID = 2046,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mBlockID = 2215,
        mResources = {
            {mNames = {"木头"}, mCount = 5},
            {mNames = {"茅草"}, mCount = 5},
            {mNames = {"纤维"}, mCount = 5},
            {mNames = {"浆果(红)", "浆果(白)", "浆果(黑)", "浆果(蓝)", "浆果(黄)", "浆果(紫)"}, mCount = 5}
        },
        mHP = 5,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 5
    },
    {
        mMonsterName = "凤王",
        mResources = {
            {mNames = {"生肉"}, mCount = 1},
            {mNames = {"兽皮"}, mCount = 1},
        },
        mHP = 10,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
    {
        mMonsterName = "火焰鸟",
        mResources = {
            {mNames = {"生肉"}, mCount = 1},
            {mNames = {"兽皮"}, mCount = 1},
        },
        mHP = 10,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
    {
        mMonsterName = "肯泰罗",
        mResources = {
            {mNames = {"生肉"}, mCount = 5},
            {mNames = {"兽皮"}, mCount = 5},
        },
        mHP = 100,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
    {
        mMonsterName = "我是肉",
        mResources = {
            {mNames = {"生肉"}, mCount = 5},
            {mNames = {"兽皮"}, mCount = 3},
        },
        mHP = 100,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
    {
        mMonsterName = "咩利羊",
        mResources = {
            {mNames = {"生肉"}, mCount = 5},
            {mNames = {"兽皮"}, mCount = 3},
            {mNames = {"毛皮"}, mCount = 3},
        },
        mHP = 100,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
    {
        mMonsterName = "甲贺忍蛙",
        mResources = {
            {mNames = {"生肉"}, mCount = 1},
            {mNames = {"兽皮"}, mCount = 1},
        },
        mHP = 10,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
    {
        mMonsterName = "风速狗",
        mResources = {
            {mNames = {"生肉"}, mCount = 3},
            {mNames = {"兽皮"}, mCount = 3},
        },
        mHP = 20,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
    {
        mMonsterName = "急冻鸟",
        mResources = {
            {mNames = {"生肉"}, mCount = 1},
            {mNames = {"兽皮"}, mCount = 1},
        },
        mHP = 10,
        mSupportTools = {{mName = "手"}, {mType = "斧"}, {mType = "镐"}}, mExp = 10
    },
}

Config.Blueprint = {
    {
        mGenerates = {mItems = {{mName = "石斧", mCount = 1, mExp = 10}}},
        mResources = {{mName = "燧石", mCount = 1}, {mName = "木头", mCount = 1}, {mName = "茅草", mCount = 10}},
        mCraftingTime = 5,
        mLevel = 1,
        mPoint = 1
    },
    {
        mGenerates = {mItems = {{mName = "石镐", mCount = 1, mExp = 10}}},
        mResources = {{mName = "石头", mCount = 1}, {mName = "木头", mCount = 1}, {mName = "茅草", mCount = 10}},
        mCraftingTime = 5,
        mLevel = 1,
        mPoint = 1
    },
    {
        mGenerates = {mItems = {{mName = "火把", mCount = 1, mExp = 10}}},
        mResources = {{mName = "石头", mCount = 1}, {mName = "木头", mCount = 1}, {mName = "燧石", mCount = 1}},
        mCraftingTime = 5,
        mLevel = 1,
        mPoint = 1
    },
    {
        mGenerates = {mItems = {{mName = "篝火", mCount = 1, mExp = 20}}},
        mResources = {
            {mName = "石头", mCount = 16},
            {mName = "木头", mCount = 2},
            {mName = "燧石", mCount = 1},
            {mName = "茅草", mCount = 12}
        },
        mCraftingTime = 5,
        mLevel = 2,
        mPoint = 2
    },
    {
        mGenerates = {mItems = {{mName = "茅草屋", mCount = 1, mExp = 100}}},
        mResources = {{mName = "木头", mCount = 10}, {mName = "纤维", mCount = 100}, {mName = "茅草", mCount = 200}},
        mCraftingTime = 20,
        mLevel = 2,
        mPoint = 10
    },
    {
        mGenerates = {mItems = {{mName = "茅草门", mCount = 1, mExp = 50}}},
        mResources = {
            {mName = "纤维", mCount = 25},
            {mName = "木头", mCount = 3},
            {mName = "石头", mCount = 2},
            {mName = "茅草", mCount = 50}
        },
        mCraftingTime = 10,
        mLevel = 2,
        mPoint = 5
    },
    {
        mGenerates = {mItems = {{mName = "布帽", mCount = 1, mExp = 20}}},
        mResources = {{mName = "纤维", mCount = 30}},
        mCraftingTime = 10,
        mLevel = 3,
        mPoint = 4
    },
    {
        mGenerates = {mItems = {{mName = "布衣", mCount = 1, mExp = 20}}},
        mResources = {{mName = "纤维", mCount = 30}},
        mCraftingTime = 10,
        mLevel = 3,
        mPoint = 4
    },
    {
        mGenerates = {mItems = {{mName = "布裤子", mCount = 1, mExp = 20}}},
        mResources = {{mName = "纤维", mCount = 30}},
        mCraftingTime = 10,
        mLevel = 3,
        mPoint = 4
    },
    {
        mGenerates = {mItems = {{mName = "布手套", mCount = 1, mExp = 20}}},
        mResources = {{mName = "纤维", mCount = 30}},
        mCraftingTime = 10,
        mLevel = 3,
        mPoint = 4
    },
    {
        mGenerates = {mItems = {{mName = "布鞋", mCount = 1, mExp = 20}}},
        mResources = {{mName = "纤维", mCount = 30}},
        mCraftingTime = 10,
        mLevel = 3,
        mPoint = 4
    },
    {
        mGenerates = {mItems = {{mName = "木屋", mCount = 1, mExp = 200}}},
        mResources = {
            {mName = "石头", mCount = 10},
            {mName = "木头", mCount = 100},
            {mName = "燧石", mCount = 10},
            {mName = "茅草", mCount = 50},
            {mName = "纤维", mCount = 50}
        },
        mCraftingTime = 50,
        mLevel = 4,
        mPoint = 20
    },
    {
        mGenerates = {mItems = {{mName = "木门", mCount = 1, mExp = 80}}},
        mResources = {
            {mName = "石头", mCount = 4},
            {mName = "木头", mCount = 50},
            {mName = "燧石", mCount = 5},
            {mName = "茅草", mCount = 20},
            {mName = "纤维", mCount = 20}
        },
        mCraftingTime = 20,
        mLevel = 4,
        mPoint = 10
    },
    {
        mGenerates = {mItems = {{mName = "锅", mCount = 1, mExp = 50}}},
        mResources = {
            {mName = "石头", mCount = 20},
            {mName = "木头", mCount = 5},
            {mName = "燧石", mCount = 20},
            {mName = "茅草", mCount = 10}
        },
        mCraftingTime = 15,
        mLevel = 4,
        mPoint = 10
    },
    {
        mGenerates = {mItems = {{mName = "小箱子", mCount = 1, mExp = 50}}},
        mResources = {
            {mName = "石头", mCount = 5},
            {mName = "木头", mCount = 20},
            {mName = "燧石", mCount = 5},
            {mName = "茅草", mCount = 10},
            {mName = "纤维", mCount = 5}
        },
        mCraftingTime = 10,
        mLevel = 4,
        mPoint = 10
    },
    {
        mGenerates = {mItems = {{mName = "引火粉", mCount = 1, mExp = 50}}},
        mResources = {
            {mName = "石头", mCount = 1},
            {mName = "燧石", mCount = 1},
        },
        mCraftingTime = 5,
        mLevel = 4,
        mPoint = 3,
        mRequests={{mName = "研磨器"},}
    },
    {
        mGenerates = {mItems = {{mName = "兽皮帽", mCount = 1, mExp = 30}}},
        mResources = {{mName = "兽皮", mCount = 50}, {mName = "纤维", mCount = 50}},
        mCraftingTime = 50,
        mLevel = 5,
        mPoint = 6
    },
    {
        mGenerates = {mItems = {{mName = "兽皮衣", mCount = 1, mExp = 30}}},
        mResources = {{mName = "兽皮", mCount = 50}, {mName = "纤维", mCount = 50}},
        mCraftingTime = 50,
        mLevel = 5,
        mPoint = 6
    },
    {
        mGenerates = {mItems = {{mName = "兽皮裤", mCount = 1, mExp = 30}}},
        mResources = {{mName = "兽皮", mCount = 50}, {mName = "纤维", mCount = 50}},
        mCraftingTime = 50,
        mLevel = 5,
        mPoint = 6
    },
    {
        mGenerates = {mItems = {{mName = "兽皮手套", mCount = 1, mExp = 30}}},
        mResources = {{mName = "兽皮", mCount = 50}, {mName = "纤维", mCount = 50}},
        mCraftingTime = 50,
        mLevel = 5,
        mPoint = 6
    },
    {
        mGenerates = {mItems = {{mName = "兽皮鞋", mCount = 1, mExp = 30}}},
        mResources = {{mName = "兽皮", mCount = 50}, {mName = "纤维", mCount = 50}},
        mCraftingTime = 50,
        mLevel = 5,
        mPoint = 6
    },
    {
        mGenerates = {mItems = {{mName = "床", mCount = 1, mExp = 50}}},
        mResources = {
            {mName = "石头", mCount = 10},
            {mName = "木头", mCount = 50},
            {mName = "燧石", mCount = 10},
            {mName = "茅草", mCount = 50},
            {mName = "纤维", mCount = 50}
        },
        mCraftingTime = 20,
        mLevel = 6,
        mPoint = 15
    },
    {
        mGenerates = {mItems = {{mName = "风干箱", mCount = 1, mExp = 50}}},
        mResources = {
            {mName = "石头", mCount = 10},
            {mName = "木头", mCount = 50},
            {mName = "燧石", mCount = 10},
            {mName = "茅草", mCount = 50}
        },
        mCraftingTime = 20,
        mLevel = 6,
        mPoint = 15
    },
    {
        mGenerates = {mItems = {{mName = "研磨器", mCount = 1, mExp = 50}}},
        mResources = {
            {mName = "石头", mCount = 50},
            {mName = "木头", mCount = 10},
            {mName = "燧石", mCount = 50},
            {mName = "茅草", mCount = 10},
            {mName = "纤维", mCount = 10}
        },
        mCraftingTime = 30,
        mLevel = 6,
        mPoint = 20
    },
    {
        mGenerates = {mItems = {{mName = "石屋", mCount = 1, mExp = 400}}},
        mResources = {
            {mName = "石头", mCount = 300},
            {mName = "木头", mCount = 100},
            {mName = "燧石", mCount = 200},
            {mName = "茅草", mCount = 100},
            {mName = "纤维", mCount = 50}
        },
        mCraftingTime = 50,
        mLevel = 7,
        mPoint = 30
    },
    {
        mGenerates = {mItems = {{mName = "石门", mCount = 1, mExp = 100}}},
        mResources = {
            {mName = "石头", mCount = 100},
            {mName = "木头", mCount = 20},
            {mName = "燧石", mCount = 50},
            {mName = "茅草", mCount = 50},
            {mName = "纤维", mCount = 30}
        },
        mCraftingTime = 50,
        mLevel = 7,
        mPoint = 30
    }
}

Config.Terrain = {
    {mName = "失落神庙", mTypes = {"迷宫"}, mTemplate = {mResource = {hash = "FhI7seVcek3jgvnGuCiBJH9a_2Mp", pid = "187322", ext = "bmax", }}},
    {mName = "青铜古树", mTypes = {"迷宫"}, mTemplate = {mResource = {hash = "Fm-Gofddke2lFW0UYreQFfqEb4L0", pid = "187393", ext = "bmax", }}},
    {mName = "地下古墓", mTypes = {"迷宫"}, mTemplate = {mResource = {hash = "FnvRkpIRT4y-PPzO2i8ynonMzx4f", pid = "187320", ext = "bmax", }}}
}

Config.Buff = {
    {
        mName = "冻僵",
        mIcon = {mResource = {ext = "png", hash = "FqVB6gCGdjHrFkfunjIzrD6k6YZL", name = "冻伤", pid = "177463"}}
    },
    {
        mName = "寒冷",
        mIcon = {mResource = {ext = "png", hash = "FqNKRq9Ki4TXwJmaUSa8oYjTnHqB", name = "寒冷", pid = "177467"}}
    },
    {
        mName = "炎热",
        mIcon = {mResource = {ext = "png", hash = "Fkx30XiFbB1E0KX9APANfZK_D1zQ", name = "炎热", pid = "177480"}}
    },
    {
        mName = "烧焦",
        mIcon = {mResource = {ext = "png", hash = "FkBMfpAKufklU7H_50-VLMIUiQP0", name = "烧焦", pid = "177473"}}
    },
    {
        mName = "骨折",
        mIcon = {mResource = {ext = "png", hash = "Fnh3XClDcVmwig84c09pmD0zhNLl", name = "骨折", pid = "177466"}}
    },
    {
        mName = "饥饿",
        mIcon = {mResource = {ext = "png", hash = "FmPIALy_LqgmFuthWTGuxARqceJ1", name = "饿", pid = "177468"}}
    },
    {
        mName = "口渴",
        mIcon = {mResource = {ext = "png", hash = "FruM8J1Od_gxXCaBBeWvtYtmqb5h", name = "渴", pid = "177469"}}
    },
    {
        mName = "超重",
        mIcon = {mResource = {ext = "png", hash = "FnR1ZIjwP6rrP4QcDnKkfyVEHO2S", name = "重量", pid = "177483"}}
    }
}

Config.Monster = {
    {mType = "和平", mName = "凤王", mHP = 10, mModel = {mFile = "character/CC/03animals/chicken/chicken.x"}, mMoveSpeed = 1, mRunSpeed = 2, mExp = 50, mAttack = {mDamage = 1, mSpeed = 0.8, mRange = 1}},
    {mType = "和平", mName = "火焰鸟", mHP = 10, mModel = {mFile = "character/CC/03animals/bird/bird.x"}, mMoveSpeed = 1, mRunSpeed = 2, mExp = 50, mAttack = {mDamage = 1, mSpeed = 0.8, mRange = 1}},
    {mType = "中立", mName = "肯泰罗", mHP = 100, mModel = {mFile = "character/CC/03animals/cow/cow.x"}, mMoveSpeed = 1, mRunSpeed = 3, mExp = 500, mAttack = {mDamage = 5, mSpeed = 0.8, mRange = 1}},
    {mType = "中立", mName = "我是肉", mHP = 100, mModel = {mFile = "character/CC/03animals/pig/pig.x"}, mMoveSpeed = 1, mRunSpeed = 1.5, mExp = 500, mAttack = {mDamage = 2, mSpeed = 0.8, mRange = 1}},
    {mType = "中立", mName = "咩利羊", mHP = 100, mModel = {mFile = "character/CC/03animals/sheep/sheep.x"}, mMoveSpeed = 1, mRunSpeed = 2.5, mExp = 500, mAttack = {mDamage = 3, mSpeed = 0.8, mRange = 1}},
    {mType = "和平", mName = "甲贺忍蛙", mHP = 10, mModel = {mFile = "character/CC/03animals/frog/frog.x"}, mMoveSpeed = 1, mRunSpeed = 2, mExp = 50, mAttack = {mDamage = 1, mSpeed = 0.8, mRange = 1}},
    {mType = "中立", mName = "风速狗", mHP = 50, mModel = {mFile = "character/CC/03animals/dog/dog.x"}, mMoveSpeed = 1, mRunSpeed = 4, mExp = 200, mAttack = {mDamage = 1, mSpeed = 0.3, mRange = 1}},
    {mType = "和平", mName = "急冻鸟", mHP = 10, mModel = {mFile = "character/CC/03animals/pigeon/pigeon.x"}, mMoveSpeed = 1, mRunSpeed = 2, mExp = 50, mAttack = {mDamage = 1, mSpeed = 0.8, mRange = 1}},
}

Config.Building = {
    {mName = "火把",
        mBlockSize = {1, 1, 1},
        mFortitude = {mHot = -10, mCold = 10, mBlockSize = {3, 3, 3}},
        mBlockID = 100,
        mAutoDestroy = {mTime = 30},
    },
    {mName = "篝火",
        mTypes = {"贮藏"},
        mBlockSize = {1, 1, 1},
        mBag = {mSlotCount = 3},
        mEnable = {
            mBlockID = 2278,
            mEnergyResources = {{mName = "木头", mTime = 60}, {mName = "茅草", mTime = 10}, {mName = "纤维", mTime = 1}},
            mModel = {mResource = {hash = "FhT32XMp1f2ZUWivut_qonJNfc6_", pid = "157811", ext = "FBX"}, mScaling = 0.15},
        },
        mGenerates = {
            {mFrom = {{mName = "生肉", mCount = 1}}, mTo = {{mName = "熟肉", mCount = 1}}, mTime = 10, mNeedEnable = true, },
        },
        mFortitude = {mNeedEnable = true, mHot = -200, mCold = 200, mBlockSize = {7, 7, 7}, mAttenuation = true},
        mModel = {mResource = {hash = "Fmo0S_2TiN-noIMqnCN8-c-Pqpvx", pid = "180992", ext = "FBX"}, mScaling = 0.15},
        mInteractives = {mDistance = 1.75},
        mHP = 100, mDefense = 1,
    },
    {mName = "茅草屋", mTypes = {"房屋"}, mBlockSize = {7, 7, 7}, mFortitude = {mHot = 50, mCold = 50}, mHP = 1000, mDefense = 10,
    },
    {mName = "木屋", mTypes = {"房屋"}, mBlockSize = {9, 7, 9}, mFortitude = {mHot = 50, mCold = 75}, mHP = 5000, mDefense = 20,
    },
    {mName = "石屋", mTypes = {"房屋"}, mWeight = 90, mBlockSize = {11, 7, 11}, mFortitude = {mHot = 50, mCold = 100}, mHP = 10000, mDefense = 50,
    },
    {mName = "研磨器",
        mTypes = {"贮藏"},
        mBlockSize = {1, 1, 1},
        mBag = {mSlotCount = 10},
        mModel = {mResource = {hash = "FiNx8IPX51biB037isB6e95kvmqy", pid = "157813", ext = "FBX"}, mScaling = 0.15}, --[[mTextures = {{mResource = {hash = "FryTwv-H8q43eHLf2-lGQPgro7pb", pid = "135969", ext = "png", }}}]]
        mInteractives = {mDistance = 1.75},
        mHP = 100, mDefense = 1,
    },
    {mName = "锅",
        mTypes = {"贮藏"},
        mBlockSize = {1, 1, 1},
        mBag = {mSlotCount = 3},
        mEnable = {
            mBlockID = 2278,
            mEnergyResources = {{mName = "木头", mTime = 60}, {mName = "茅草", mTime = 10}, {mName = "纤维", mTime = 1}},
        },
        mGenerates = {
            {mFrom = {{mName = "生肉", mCount = 1}}, mTo = {{mName = "熟肉", mCount = 1}}, mTime = 10, mNeedEnable = true, },
        },
        mFortitude = {mNeedEnable = true, mHot = -200, mCold = 200, mBlockSize = {7, 7, 7}, mAttenuation = true},
        mModel = {mResource = {hash = "FpVQuf8n6pUSlzyL61z21yd025ZR", pid = "157810", ext = "FBX"}, mScaling = 0.1}, --[[mTextures = {{mResource = {hash = "FrygTX1OgRSHBx_CbynlwMqLGT62", pid = "135965", ext = "png", }}}]]
        mInteractives = {mDistance = 1.75},
        mHP = 100, mDefense = 1,
    },
    {mName = "小箱子",
        mTypes = {"贮藏"},
        mBlockSize = {1, 1, 1},
        mBag = {mSlotCount = 30},
        mModel = {mFile = "gameassets/furnitures/chuangtougui_A.fbx"},
        mTextures = {{mResource = {hash = "Fr4PHj6EJ50bq53-5Yr_qHzt4cel", pid = "135968", ext = "png"}}},
        mInteractives = {mDistance = 1.75},
        mHP = 100, mDefense = 1,
    },
    {mName = "大箱子",
        mTypes = {"贮藏"},
        mBlockSize = {1, 3, 3},
        mBag = {mSlotCount = 60},
        mModel = {mFile = "gameassets/furnitures/yigui_A.fbx"},
        mTextures = {{mResource = {hash = "FuXleGHn79zTtV2mCn-Ix11Qx4U4", pid = "135964", ext = "png"}}},
        mInteractives = {mDistance = 1.75},
        mHP = 100, mDefense = 1,
    },
    {mName = "风干箱",
        mTypes = {"贮藏"},
        mBlockSize = {1, 2, 1},
        mBag = {mSlotCount = 10},
        mEnable = {
            mAuto = true,
            mEnergyResources = {{mName = "引火粉", mTime = 60}},
        },
        mKeepFresh = {mValue = 0.1, mNeedEnable = true},
        mModel = {mFile = "gameassets/furnitures/dianbingxiang_A.fbx"},
        mTextures = {{mResource = {hash = "FmfG0jH5Wn5t2EpAxPeRXA1Fu8Pv", pid = "135966", ext = "png"}}},
        mInteractives = {mDistance = 1.75},
        mHP = 100, mDefense = 1,
    },
    {mName = "床",
        mBlockSize = {3, 2, 3},
        mModel = {mFile = "gameassets/furnitures/chuang_danren_A.fbx"},
        mTextures = {{mResource = {hash = "FmWYR8ig7I-eMTQ2xpz-JR7DFiJl", pid = "135963", ext = "png"}}},
        mHP = 100, mDefense = 1,
    },
    {mName = "茅草门",
        mTypes = {"门"},
        mBlockSize = {1, 3, 2},
        mModel = {mFile = "gameassets/furnitures/men_A_C.fbx"},
        mTextures = {{mResource = {ext = "png", hash = "FojgHd4TsNNBqYPbi2MTRqBrA8QF", name = "早门", pid = "177495"}}},
        mInteractives = {
            mDistance = 2,
            {
                mModel = {mFile = "gameassets/furnitures/men_A_O.fbx"},
                mTextures = {
                    {mResource = {ext = "png", hash = "FojgHd4TsNNBqYPbi2MTRqBrA8QF", name = "早门", pid = "177495"}}
                }
            }
        },
        mHP = 500, mDefense = 5,
    },
    {mName = "木门",
        mTypes = {"门"},
        mBlockSize = {1, 3, 2},
        mModel = {mFile = "gameassets/furnitures/men_A_C.fbx"},
        mTextures = {{mResource = {ext = "png", hash = "FsN2n_pLnRLA6vREk8g39yWTaSBq", name = "木门", pid = "177497"}}},
        mInteractives = {
            mDistance = 2,
            {
                mModel = {mFile = "gameassets/furnitures/men_A_O.fbx"},
                mTextures = {
                    {mResource = {ext = "png", hash = "FsN2n_pLnRLA6vREk8g39yWTaSBq", name = "木门", pid = "177497"}}
                }
            }
        },
        mHP = 1000, mDefense = 10,
    },
    {mName = "石门",
        mTypes = {"门"},
        mBlockSize = {1, 3, 2},
        mModel = {mFile = "gameassets/furnitures/men_A_C.fbx"},
        mTextures = {{mResource = {ext = "png", hash = "Fjby2HN49bDOdQJ4WzHRkwl4ESwR", name = "石门", pid = "177499"}}},
        mInteractives = {
            mDistance = 2,
            {
                mModel = {mFile = "gameassets/furnitures/men_A_O.fbx"},
                mTextures = {
                    {mResource = {ext = "png", hash = "Fjby2HN49bDOdQJ4WzHRkwl4ESwR", name = "石门", pid = "177499"}}
                }
            }
        },
        mHP = 2000, mDefense = 20,
    },
}

Config.Common = {
    {mName = "已学会", mIcon = {mResource = {hash = "FmGFyIM21qVQPYg8N6N6N218STUB", pid = "177958", ext = "png"}}},
    {mName = "未学会", mIcon = {mResource = {hash = "Fg6f4zKT_6Hv2289PuG7NzRMjR23", pid = "177959", ext = "png"}}}
}

Config.mDebug = false
