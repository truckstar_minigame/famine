local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Monster = Devil.getTable("Game.Famine.Host_GameMonster")

local MonsterManager = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_GameMonsterManager"))
function MonsterManager:construction(parameter)
    self.mMonsters = {}
end

function MonsterManager:destruction()
    for _, monster in pairs(self.mMonsters) do
        delete(monster)
    end
    self.mMonsters = nil
end

function MonsterManager:update(deltaTime)
    for _, monster in pairs(self.mMonsters) do
        monster:update(deltaTime)
    end
end

function MonsterManager:createMonster(parameter)
    local cfg_index = parameter.mConfigIndex or math.random(1, #Config.Monster)
    local cfg = Config.Monster[cfg_index]
    local init_members = {mID = self:_generateNextMonsterID()}
    local ret
    ret = new(Monster, {mInitMembers = init_members, mConfigIndex = cfg_index, mBlockPosition = parameter.mBlockPosition})
    self.mMonsters[#self.mMonsters + 1] = ret
    self:broadcast("CreateMonster",{mID = ret.mID,mConfigIndex = ret.mConfigIndex})
    return ret
end

function MonsterManager:destroyMonster(monster)
    self:broadcast("DestroyMonster",{mID = monster:getID()})
    Utility.arrayRemove(self.mMonsters, function(e) return e == monster end, delete)
end

function MonsterManager:getMonsterByID(id)
    return Utility.arrayGet(self.mMonsters, function(e) return e:getID() == id end)
end

function MonsterManager:_getSendKey()
    return "GameMonsterManager"
end

function MonsterManager:_receiveMessage(parameter)
    if parameter.mMessage == "Sync" then
        local monsters = {}
        if self.mMonsters then
            for _, monster in pairs(self.mMonsters) do
                local s = {mID = monster:getID(), mConfigIndex = monster.mConfigIndex}
                monsters[#monsters + 1] = s
            end
        end
        self:responseToClient(parameter.mFrom, "Sync", {mResponseCallbackKey = parameter.mParameter.mResponseCallbackKey, mMonsters = monsters})
    end
end

function MonsterManager:_generateNextMonsterID()
    self.mNextMonsterID = self.mNextMonsterID or 0
    self.mNextMonsterID = self.mNextMonsterID + 1
    return self.mNextMonsterID
end
