local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Client_Game")
local Scene = Devil.getTable("Game.Famine.Client_GameScene")
local Building = Devil.getTable("Game.Famine.Client_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")
local GameUtility = Devil.getTable("Game.Famine.Utility")

local GameNatureSceneTaskReceiver = inherit(Framework.DistributedTaskReceiver, Devil.getTable("Game.Famine.Client_GameNatureSceneTaskReceiver"))

function GameNatureSceneTaskReceiver:construction()
    self.mCallback = function(parameter, callback)
        local scene = Game.singleton():getSceneManager():getSceneByID(parameter.mID)
        if scene then
            local sampler_size = parameter.mSamplerSize or 256
            local sampler_scale = parameter.mSamplerScale or 1
            local function _calcSamplerUV(blockX, blockZ)
                return ((math.ceil((blockX - 0.5) / sampler_scale) - 1) % sampler_size), ((math.ceil((blockZ - 0.5) / sampler_scale) - 1) % sampler_size)
            end           
            local sampler_uvs = {}
            local x_start = parameter.mChunkX * 16
            local z_start = parameter.mChunkZ * 16
            for x = 1, 16 do
                for z = 1, 16 do
                    local x_pos, z_pos = x_start + x, z_start + z
                    local u, v = _calcSamplerUV(x_pos, z_pos)
                    local sampler_key = v * sampler_size + u + 1
                    if not sampler_uvs[sampler_key] then
                        sampler_uvs[sampler_key] = {u / (sampler_size - 1) * 2 - 1, 0, v / (sampler_size - 1) * 2 - 1}
                    end
                end
            end
            Async("script/Truck/Utility/NoiseThread.lua", function(value)
                callback({mSampler = value,mChunkX = parameter.mChunkX,mChunkZ = parameter.mChunkZ})
            end, {mSeed = parameter.mSeed, mQuality = 3}, sampler_uvs)
        end
    end
end

function GameNatureSceneTaskReceiver:destruction()
end

function GameNatureSceneTaskReceiver:_getSendKey()
    return "GameNatureSceneTask"
end