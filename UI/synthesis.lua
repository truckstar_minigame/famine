local Layout = {
    [1] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 255 0",
            ["y"] = 2,
            ["parent"] = "__root",
            ["zorder"] = 12,
            ["align"] = "_lt",
            ["x"] = 1,
            ["type"] = "container",
            ["height"] = 1,
            ["name"] = "ingame",
            ["events"] = {
            },
        },
    },
    [2] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 600,
            ["y"] = 120,
            ["clip"] = true,
            ["zorder"] = 12,
            ["align"] = "_lt",
            ["x"] = 100,
            ["type"] = "container",
            ["background_res"] = {hash = "FgCGQ9pm0kY1aSdeuQeXsrKQVSsl", pid = "161997", ext = "png", },
            ["name"] = "物品栏",
            ["height"] = 850,
            ["events"] = {
            },
        },
    },
    [3] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["font_color"] = "255",
            ["parent"] = "物品栏",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["background_res"] = {hash = "FojEfRVKveenjMwSv4yHhbBc7t5b", pid = "187310", ext = "png", },
            ["background_res_on"] = {hash = "FkunYt5wDJUlrSS238vJ4VZ5pI51", pid = "162026", ext = "png", },
            ["x"] = 10,
            ["name"] = "切至物品栏按钮",
            ["align"] = "_lt",
        },
    },
    [4] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["font_color"] = "255 255 255",
            ["parent"] = "物品栏",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["background_res"] = {hash = "Fq8TOjSLQklm_WhL7KMnnjWHRldd", pid = "187311", ext = "png", },
            ["background_res_on"] = {hash = "FvFMWitTRWrZ0jPUfAVMaTmVlW8k", pid = "162027", ext = "png", },
            ["x"] = 390,
            ["name"] = "切至制作栏按钮",
            ["align"] = "_lt",
        },
    },
    [5] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 540,
            ["y"] = 78,
            ["clip"] = true,
            ["parent"] = "物品栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 30,
            ["type"] = "container",
            ["name"] = "物品栏辅助功能",
            ["height"] = 75,
            ["events"] = {
            },
        },
    },
    [6] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 150,
            ["y"] = 10,
            ["font_color"] = "50 50 50",
            ["parent"] = "物品栏辅助功能",
            ["text"] = "查找",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "editbox",
            ["x"] = 0,
            ["name"] = "查找栏",
            ["align"] = "_lt",
        },
    },
    [7] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 160,
            ["type"] = "container",
            ["color"] = "0 0 0",
            ["name"] = "预留4",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [8] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留4",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["color"] = "0 0 0",
            ["font_color"] = "255 255 255",
            ["name"] = "预留按钮4",
            ["align"] = "_lt",
        },
    },
    [9] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 220,
            ["type"] = "container",
            ["name"] = "预留5",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [10] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留5",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["color"] = "0 0 0",
            ["height"] = 50,
            ["name"] = "预留按钮5",
            ["events"] = {
            },
        },
    },
    [11] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 280,
            ["type"] = "container",
            ["name"] = "预留6",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [12] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留6",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮6",
            ["events"] = {
            },
        },
    },
    [13] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 340,
            ["type"] = "container",
            ["name"] = "预留7",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [14] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留7",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮7",
            ["events"] = {
            },
        },
    },
    [15] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 425,
            ["type"] = "container",
            ["name"] = "预留8",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [16] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留8",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮8",
            ["events"] = {
            },
        },
    },
    [17] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 485,
            ["type"] = "container",
            ["name"] = "预留9",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [18] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留9",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮9",
            ["events"] = {
            },
        },
    },
    [19] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 540,
            ["color"] = "125 125 125",
            ["y"] = 170,
            ["clip"] = true,
            ["parent"] = "物品栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 30,
            ["type"] = "container",
            ["background_res"] = {hash = "FjWpC1tfDBFZr3tneBOQmycA4aU2", pid = "162025", ext = "png", },
            ["name"] = "物品区",
            ["height"] = 650,
            ["events"] = {
            },
        },
    },
    [20] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["parent"] = "物品区",
            ["zorder"] = 1,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["name"] = "物品",
            ["color"] = "25 125 125",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["background_res_on"] = {hash = "Fmuq3eJUVUDEXBfgutHodyBTqNGD", pid = "161637", ext = "png", },
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [21] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "200 200 200",
            ["y"] = 35,
            ["height"] = 25,
            ["parent"] = "物品",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "物品名称",
            ["x"] = 0,
        },
    },
    [22] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [23] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 450,
            ["y"] = 118,
            ["clip"] = true,
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 734,
            ["type"] = "container",
            ["background_res"] = {hash = "FoSn4Hh0qYJoX92dHTFrkHqdlnWS", pid = "162035", ext = "png", },
            ["name"] = "本人",
            ["height"] = 850,
            ["events"] = {
            },
        },
    },
    [24] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["x"] = 20,
            ["parent"] = "本人",
            ["text"] = "本人",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "本人标题",
            ["align"] = "_lt",
        },
    },
    [25] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 280,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 30,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "合成操作框1",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [26] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 0,
            ["height"] = 90,
            ["parent"] = "合成操作框1",
            ["text"] = "合成",
            ["zorder"] = 1,
            ["text_format"] = 5,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "button",
            ["align"] = "_lt",
            ["name"] = "合成操作按钮1",
            ["x"] = 0,
        },
    },
    [27] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "头部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "头部物品图片",
            ["height"] = 0,
            ["events"] = {
            },
        },
    },
    [28] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 280,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 160,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "合成操作框2",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [29] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 0,
            ["height"] = 90,
            ["parent"] = "合成操作框2",
            ["text"] = "融合",
            ["zorder"] = 1,
            ["text_format"] = 5,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "button",
            ["align"] = "_lt",
            ["name"] = "合成操作按钮2",
            ["x"] = 0,
        },
    },
    [30] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "躯干",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "躯干物品图片",
            ["height"] = 0,
            ["events"] = {
            },
        },
    },
    [31] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 280,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 290,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "合成操作框3",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [32] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 0,
            ["height"] = 90,
            ["parent"] = "合成操作框3",
            ["text"] = "粉碎",
            ["zorder"] = 1,
            ["text_format"] = 5,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "button",
            ["align"] = "_lt",
            ["name"] = "合成操作按钮3",
            ["x"] = 0,
        },
    },
    [33] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "腿部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "腿部物品图片",
            ["height"] = 0,
            ["events"] = {
            },
        },
    },
    [34] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 80,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 350,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "手部",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [35] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "手部",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "手部物品名称",
            ["x"] = 0,
        },
    },
    [36] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "手部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "手部物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [37] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 180,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 350,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "副手",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [38] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "副手",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "副手物品名称",
            ["x"] = 0,
        },
    },
    [39] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "副手",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "副手物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [40] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 280,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 350,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "脚部",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [41] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "脚部",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "脚部物品名称",
            ["x"] = 0,
        },
    },
    [42] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "脚部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "脚部品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [43] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 450,
            ["color"] = "0 0 0",
            ["y"] = 80,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 115,
            ["type"] = "container",
            ["background"] = "",
            ["name"] = "角色信息区",
            ["height"] = 300,
            ["events"] = {
            },
        },
    },
    [44] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 220,
            ["y"] = 0,
            ["height"] = 30,
            ["parent"] = "角色信息区",
            ["text"] = "研磨器",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "合成资源名称",
            ["x"] = 0,
        },
    },
    [45] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 220,
            ["y"] = 55,
            ["height"] = 25,
            ["parent"] = "角色信息区",
            ["text"] = "所有者：人类的部落",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "合成资源所有人",
            ["x"] = 0,
        },
    },
    [46] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 220,
            ["y"] = 110,
            ["height"] = 25,
            ["parent"] = "角色信息区",
            ["text"] = "将资源磨成新物品",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "合成资源解说提示",
            ["x"] = 0,
        },
    },
    -- --------------------------以下UI无用处了
    [47] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 0,
            ["y"] = 100,
            ["height"] = 25,
            ["parent"] = "角色信息区",
            ["text"] = "",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 1,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "足爪",
            ["x"] = 0,
        },
    },
    [48] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 120,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 1,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "天数",
            ["align"] = "_lt",
        },
    },
    [49] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 140,
            ["font_color"] = "255 255 255",
            ["parent"] = "角色信息区",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 1,
            ["type"] = "text",
            ["x"] = 20,
            ["name"] = "时间",
            ["align"] = "_lt",
        },
    },
    [50] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 160,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 0,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "温度",
            ["align"] = "_lt",
        },
    },
    [51] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 200,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 0,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "护甲",
            ["align"] = "_lt",
        },
    },
    [52] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 220,
            ["font_color"] = "255 255 255",
            ["parent"] = "角色信息区",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 0,
            ["type"] = "text",
            ["x"] = 20,
            ["name"] = "低温抗性",
            ["align"] = "_lt",
        },
    },
    [53] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 240,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 0,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "高温抗性",
            ["align"] = "_lt",
        },
    },
    [54] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 390,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "经验条",
            ["height"] = 0,
            ["events"] = {
            },
        },
    },
    [55] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["color"] = "255 0 20",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "经验条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "经验动态条",
            ["height"] = 0,
            ["events"] = {
            },
        },
    },
    [56] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 5,
            ["parent"] = "经验条",
            ["text"] = "",
            ["zorder"] = 1,
            ["x"] = 10,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["height"] = 30,
            ["name"] = "经验数值",
            ["events"] = {
            },
        },
    },
    --------------------以上UI无用处了------------------
    [57] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 450,
            ["color"] = "255 25 255",
            ["y"] = 429,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 1,
            ["type"] = "container",
            ["background"] = "",
            ["name"] = "成长条",
            ["height"] = 420,
            ["events"] = {
            },
        },
    },
    [58] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条1",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [59] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条1",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条1图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [60] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 235,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条1",
            ["text"] = "生命值",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条1名称",
            ["events"] = {
            },
        },
    },
    [61] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条1",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条1数值",
            ["font_color"] = "255 255 255",
        },
    },
    [62] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条1",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条1动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [63] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条1按钮",
            ["events"] = {
            },
        },
    },
    [64] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 37,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条2",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [65] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条2",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条2图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [66] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条2",
            ["text"] = "耐力",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条2名称",
            ["events"] = {
            },
        },
    },
    [67] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条2",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条2数值",
            ["font_color"] = "255 255 255",
        },
    },
    [68] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条2",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条2动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [69] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条2按钮",
            ["events"] = {
            },
        },
    },
    [70] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 74,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条3",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [71] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条3",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条3图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [72] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条3",
            ["text"] = "氧气",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条3名称",
            ["events"] = {
            },
        },
    },
    [73] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条3",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条3数值",
            ["font_color"] = "255 255 255",
        },
    },
    [74] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条3",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条3动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [75] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条3按钮",
            ["events"] = {
            },
        },
    },
    [76] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 111,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条4",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [77] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条4",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条4图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [78] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条4",
            ["text"] = "食物",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条4名称",
            ["events"] = {
            },
        },
    },
    [79] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条4",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条4数值",
            ["font_color"] = "255 255 255",
        },
    },
    [80] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条4",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条4动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [81] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条4按钮",
            ["events"] = {
            },
        },
    },
    [82] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 148,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条5",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [83] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条5",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条5图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [84] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条5",
            ["text"] = "水",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条5名称",
            ["events"] = {
            },
        },
    },
    [85] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条5",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条5数值",
            ["font_color"] = "255 255 255",
        },
    },
    [86] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条5",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条5动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [87] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条5按钮",
            ["events"] = {
            },
        },
    },
    [88] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 185,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条6",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [89] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条6",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条6图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [90] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条6",
            ["text"] = "重量",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条6名称",
            ["events"] = {
            },
        },
    },
    [91] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条6",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条6数值",
            ["font_color"] = "255 255 255",
        },
    },
    [92] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条6",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条6动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [93] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条6按钮",
            ["events"] = {
            },
        },
    },
    [94] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 222,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条7",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [95] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条7",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条7图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [96] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条7",
            ["text"] = "近战伤害",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条7名称",
            ["events"] = {
            },
        },
    },
    [97] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条7",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条7数值",
            ["font_color"] = "255 255 255",
        },
    },
    [98] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条7",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条7动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [99] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条7按钮",
            ["events"] = {
            },
        },
    },
    [100] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 259,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条8",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [101] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条8",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条8图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [102] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条8",
            ["text"] = "移动速度",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条8名称",
            ["events"] = {
            },
        },
    },
    [103] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条8",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条8数值",
            ["font_color"] = "255 255 255",
        },
    },
    [104] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条8",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条8动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [105] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条8按钮",
            ["events"] = {
            },
        },
    },
    [106] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 296,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条9",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [107] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条9",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条9图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [108] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条9",
            ["text"] = "制作技能",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条9名称",
            ["events"] = {
            },
        },
    },
    [109] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条9",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条9数值",
            ["font_color"] = "255 255 255",
        },
    },
    [110] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条9",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条9动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [111] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条9按钮",
            ["events"] = {
            },
        },
    },
    [112] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 333,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条10",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [113] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条10",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条10图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [114] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条10",
            ["text"] = "抗性",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条10名称",
            ["events"] = {
            },
        },
    },
    [115] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条10",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条10数值",
            ["font_color"] = "255 255 255",
        },
    },
    [116] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条10",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条10动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [117] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条10按钮",
            ["events"] = {
            },
        },
    },
    [118] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 440,
            ["y"] = 370,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条11",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [119] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条11",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条11图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [120] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条11",
            ["text"] = "眩晕",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条11名称",
            ["events"] = {
            },
        },
    },
    [121] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 235,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条11",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条11数值",
            ["font_color"] = "255 255 255",
        },
    },
    [122] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条11",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条11动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [123] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 0,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条11按钮",
            ["events"] = {
            },
        },
    },
    -----------------------------------------------------------------------------------
    [124] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 118,
            ["x"] = 1829,
            ["text"] = "X",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "close",
            ["align"] = "_lt",
        },
    },
    [125] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 5,
            ["parent"] = "经验条",
            ["text"] = "可用点数 999",
            ["zorder"] = 1,
            ["x"] = 240,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["text_format"] = 6,
            ["height"] = 25,
            ["name"] = "可用点数",
        },
    },
    [126] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "物品",
            ["zorder"] = 3,
            ["align"] = "_lt",
            ["type"] = "container",
            --["background_res"] = {hash="FidoMko_5tD8vgrTuSVpIVNnjF55",pid="161641",ext="png",},
            ["x"] = 2,
            ["name"] = "物品数量标",
            ["height"] = 25,
        },
    },
    [127] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "物品数量标",
            ["zorder"] = 8,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png", },
            ["x"] = 0,
            ["name"] = "物品数量标图",
            ["height"] = 25,
        },
    },
    [128] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "物品数量标",
            ["text"] = "X999",
            ["zorder"] = 8,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["font_color"] = "0 0 0",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "物品数量标值",
        },
    },
    [129] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 80,
            ["y"] = 5,
            ["parent"] = "物品",
            ["text"] = "88",
            ["zorder"] = 10,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["font_color"] = "200 200 200",
            ["type"] = "text",
            ["text_format"] = 10,
            ["x"] = 5,
            ["height"] = 80,
            ["name"] = "物品重量",
        },
    },
    [130] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "物品",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "物品血条",
            ["height"] = 5,
        },
    },
    [131] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "物品血条动态条",
            ["height"] = 6,
        },
    },
    [132] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 0,
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "物品",
            ["font_color"] = "255 255 255",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "button",
            ["width"] = 90,
            ["name"] = "物品按钮",
            ["height"] = 90,
        },
    },
    --------------------------------------------------
    [133] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 600,
            ["y"] = 120,
            ["clip"] = true,
            ["zorder"] = 12,
            ["align"] = "_lt",
            ["x"] = 1220,
            ["type"] = "container",
            ["background_res"] = {hash = "FgCGQ9pm0kY1aSdeuQeXsrKQVSsl", pid = "161997", ext = "png", },
            ["name"] = "合成物品栏",
            ["height"] = 850,
            ["events"] = {
            },
        },
    },
    [134] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["font_color"] = "255",
            ["parent"] = "合成物品栏",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["background_res"] = {hash = "FojEfRVKveenjMwSv4yHhbBc7t5b", pid = "187310", ext = "png", },
            ["background_res_on"] = {hash = "FkunYt5wDJUlrSS238vJ4VZ5pI51", pid = "162026", ext = "png", },
            ["x"] = 10,
            ["name"] = "切至合成物品栏按钮",
            ["align"] = "_lt",
        },
    },
    [135] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["font_color"] = "255 255 255",
            ["parent"] = "合成物品栏",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["background_res"] = {hash = "Fq8TOjSLQklm_WhL7KMnnjWHRldd", pid = "187311", ext = "png", },
            ["background_res_on"] = {hash = "FvFMWitTRWrZ0jPUfAVMaTmVlW8k", pid = "162027", ext = "png", },
            ["x"] = 390,
            ["name"] = "切至合成制作栏按钮",
            ["align"] = "_lt",
        },
    },
    [136] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 540,
            ["y"] = 78,
            ["clip"] = true,
            ["parent"] = "合成物品栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 30,
            ["type"] = "container",
            ["name"] = "合成物品栏辅助功能",
            ["height"] = 75,
            ["events"] = {
            },
        },
    },
    [137] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 150,
            ["y"] = 10,
            ["font_color"] = "50 50 50",
            ["parent"] = "合成物品栏辅助功能",
            ["text"] = "查找",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "editbox",
            ["x"] = 0,
            ["name"] = "合成查找栏",
            ["align"] = "_lt",
        },
    },
    [138] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "合成物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 160,
            ["type"] = "container",
            ["color"] = "0 0 0",
            ["name"] = "合成预留4",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [139] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "合成预留4",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["color"] = "0 0 0",
            ["font_color"] = "255 255 255",
            ["name"] = "合成预留按钮4",
            ["align"] = "_lt",
        },
    },
    [140] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "合成物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 220,
            ["type"] = "container",
            ["name"] = "合成预留5",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [141] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "合成预留5",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["color"] = "0 0 0",
            ["height"] = 50,
            ["name"] = "合成预留按钮5",
            ["events"] = {
            },
        },
    },
    [142] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "合成物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 280,
            ["type"] = "container",
            ["name"] = "合成预留6",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [143] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "合成预留6",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "合成预留按钮6",
            ["events"] = {
            },
        },
    },
    [144] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "合成物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 340,
            ["type"] = "container",
            ["name"] = "合成预留7",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [145] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "合成预留7",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "合成预留按钮7",
            ["events"] = {
            },
        },
    },
    [146] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "合成物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 425,
            ["type"] = "container",
            ["name"] = "合成预留8",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [147] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "合成预留8",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "合成预留按钮8",
            ["events"] = {
            },
        },
    },
    [148] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "合成物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 485,
            ["type"] = "container",
            ["name"] = "合成预留9",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [149] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "合成预留9",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "合成预留按钮9",
            ["events"] = {
            },
        },
    },
    [150] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 540,
            ["color"] = "125 125 125",
            ["y"] = 170,
            ["clip"] = true,
            ["parent"] = "合成物品栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 30,
            ["type"] = "container",
            ["background_res"] = {hash = "FjWpC1tfDBFZr3tneBOQmycA4aU2", pid = "162025", ext = "png", },
            ["name"] = "合成物品区",
            ["height"] = 650,
            ["events"] = {
            },
        },
    },
    [151] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["parent"] = "合成物品区",
            ["zorder"] = 1,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["name"] = "合成物品",
            ["color"] = "25 125 125",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["background_res_on"] = {hash = "Fmuq3eJUVUDEXBfgutHodyBTqNGD", pid = "161637", ext = "png", },
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [152] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "200 200 200",
            ["y"] = 35,
            ["height"] = 25,
            ["parent"] = "合成物品",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "合成物品名称",
            ["x"] = 0,
        },
    },
    [153] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "合成物品",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "合成物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [154] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "合成物品",
            ["zorder"] = 3,
            ["align"] = "_lt",
            ["type"] = "container",
            --["background_res"] = {hash="FidoMko_5tD8vgrTuSVpIVNnjF55",pid="161641",ext="png",},
            ["x"] = 2,
            ["name"] = "合成物品数量标",
            ["height"] = 25,
        },
    },
    [155] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "合成物品数量标",
            ["zorder"] = 8,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png", },
            ["x"] = 0,
            ["name"] = "合成物品数量标图",
            ["height"] = 25,
        },
    },
    [156] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "合成物品数量标",
            ["text"] = "X999",
            ["zorder"] = 8,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "合成物品数量标值",
        },
    },
    [157] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 80,
            ["y"] = 5,
            ["parent"] = "合成物品",
            ["text"] = "88",
            ["zorder"] = 10,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["font_color"] = "200 200 200",
            ["type"] = "text",
            ["text_format"] = 10,
            ["x"] = 5,
            ["height"] = 80,
            ["name"] = "合成物品重量",
        },
    },
    [158] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "合成物品",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "合成物品血条",
            ["height"] = 5,
        },
    },
    [159] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "合成物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "合成物品血条动态条",
            ["height"] = 6,
        },
    },
    [160] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 0,
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "合成物品",
            ["font_color"] = "255 255 255",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "button",
            ["width"] = 90,
            ["name"] = "合成物品按钮",
            ["height"] = 90,
        },
    },
}

local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Synthesis = inherit(Layout, Devil.getTable("Game.Famine.UI.Synthesis"))

local function _getPropertyColor(scalar, invert)
    if invert then
        if scalar < 0.2 then
            return "0 " .. tostring(math.floor(255 - scalar * 255)) .. " 0"
        elseif scalar < 0.5 then
            return tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " " .. tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor(255 - (scalar + 0.5) * 255)) .. " 0 0"
        end
    else
        if scalar > 0.8 then
            return "0 " .. tostring(math.floor(scalar * 255)) .. " 0"
        elseif scalar > 0.5 then
            return tostring(math.floor((scalar + 0.2) * 255)) .. " " .. tostring(math.floor((scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor((scalar + 0.5) * 255)) .. " 0 0"
        end
    end
end

function Synthesis:construction(parameter)
    self.mUIs = {}
    for _, cfg in ipairs(Layout) do
        local cfg_cpy = clone(cfg)
        cfg_cpy.params.name = "Synthesis/" .. cfg_cpy.params.name .. tostring(self)
        cfg_cpy.params.parent = self.mUIs[cfg.params.parent]
        cfg_cpy.params.align = cfg.params.align or "_lt"
        if cfg.params.type == "button" then
            cfg_cpy.params.onclick = function()
                self:onClick(cfg.params.name)
            end
        end
        local ui = CreateUI(cfg_cpy.params)
        self.mUIs[cfg.params.name] = ui
        if cfg.params.background_res then
            GetResourceImage(
                cfg.params.background_res,
                function(path, err)
                    ui.background = path
                end
        )
        end
    end
    self.mUIs["物品"].visible = false
    self.mUIs["合成物品"].visible = false
    self.mUIs["切至制作栏按钮"].visible = false
    for i = 1, 3 do
        self.mUIs["合成操作按钮" .. tostring(i)].visible = false
    end
    if parameter.mInteractive then
        local item_cfg = parameter.mInteractive:getConfig()
        if item_cfg.mEnable then
            self.mUIs["合成操作按钮2"].visible = true
        end
    end
    for i = 1, 10 do
        self.mUIs["成长条" .. tostring(i)].visible = false
    end
    
    self.mSelectMode_Right = "物品栏"
    
    Game.singleton():getManipulationManager():pushMode("Base")
end

function Synthesis:destruction()
    Game.singleton():getManipulationManager():popMode()
    if self.mDragUI then
        self.mDragUI:destroy()
        self.mDragUI = nil
    end
    for _, ui in pairs(self.mUIs) do
        ui:destroy()
    end
    self.mUIs = nil
    self.mItemGridView_Left = nil
    self.mItemGridView_Right = nil
    self.mGridViewItemUIs_Left = nil
    self.mGridViewItemUIs_Right = nil
    self.mParameter = nil
end

function Synthesis:refresh(parameter)
    self.mParameter = parameter
    self:_refreshLeft(parameter)
    self:_refreshMiddle(parameter)
    self:_refreshRight(parameter)
end

function Synthesis:_refreshLeft(parameter)
    self.mItemGridView_Left =
        self.mItemGridView_Left or
        CreateUI(
            {
                ext = "TABLE",
                type = "container",
                x = 0,
                y = 0,
                width = Layout[19].params.width,
                height = Layout[19].params.height,
                stride = 5,
                item_width = Layout[20].params.width,
                item_height = Layout[20].params.height,
                margin = 10,
                align = "_lt",
                parent = self.mUIs["物品区"]
            }
    )
    self.mGridViewItemUIs_Left = self.mGridViewItemUIs_Left or {}
    self.mBagItems = {}
    local item_index = 1
    if parameter.mPlayer.mBag.mItems then
        for slot, item in pairs(parameter.mPlayer.mBag.mItems) do
            local item_cfg = Config.Item[item.mConfigIndex]
            local equiped
            if parameter.mPlayer.mEquip then
                if Utility.arrayContain(item_cfg.mTypes, "帽子") and parameter.mPlayer.mEquip.mHead then
                    equiped = true
                elseif Utility.arrayContain(item_cfg.mTypes, "衣服") and parameter.mPlayer.mEquip.mBody then
                    equiped = true
                elseif Utility.arrayContain(item_cfg.mTypes, "裤子") and parameter.mPlayer.mEquip.mLeg then
                    equiped = true
                elseif Utility.arrayContain(item_cfg.mTypes, "手套") and parameter.mPlayer.mEquip.mHand then
                    equiped = true
                elseif Utility.arrayContain(item_cfg.mTypes, "鞋子") and parameter.mPlayer.mEquip.mFoot then
                    equiped = true
                end
            end
            if not equiped then
                local item_ui
                local item_index2 = item_index
                self.mGridViewItemUIs_Left[item_index2] = self.mGridViewItemUIs_Left[item_index2] or {}
                local uis = self.mGridViewItemUIs_Left[item_index2]
                local exist_item = self.mItemGridView_Left:getItem(item_index2) or self.mItemGridView_Left:addItem(function(parent)
                    for _, cfg in ipairs(Layout) do
                        if cfg.params.name == "物品"
                            or cfg.params.name == "物品名称"
                            or cfg.params.name == "物品重量"
                            or cfg.params.name == "物品数量标"
                            or cfg.params.name == "物品数量标值"
                            or cfg.params.name == "物品血条"
                            or cfg.params.name == "物品血条动态条"
                            or cfg.params.name == "物品按钮"
                            or cfg.params.name == "物品图片"
                        then
                            local cfg_cpy = clone(cfg)
                            cfg_cpy.params.clip = false
                            local is_item
                            if cfg.params.name == "物品" then
                                cfg_cpy.params.parent = parent
                                is_item = true
                            elseif cfg.params.parent == "物品" then
                                cfg_cpy.params.parent = item_ui
                            else
                                cfg_cpy.params.parent = uis[cfg.params.parent]
                            end
                            cfg_cpy.params.name = cfg.params.name .. "/" .. tostring(item_index2)
                            if cfg.params.type == "button" then
                                cfg_cpy.params.onclick = function(p)
                                    self:onClickItemLeft(item_index2, p)
                                end
                                cfg_cpy.params.candrag = true
                                cfg_cpy.params.ondragbegin = function(p)
                                    self:onDragBeginLeft(item_index2, p)
                                end
                                cfg_cpy.params.ondragend = function(p)
                                    self:onDragEndLeft(item_index2, p)
                                end
                                cfg_cpy.params.ondragmove = function(p)
                                    self:onDragMoveLeft(item_index2, p)
                                end
                                cfg_cpy.params.onmouseenter = function(p)
                                    self:onMouseEnterLeft(item_index2, p)
                                end
                                cfg_cpy.params.onmouseleave = function(p)
                                    Game.singleton():getUIManager():close("ItemInfo")
                                end
                            end
                            local ui = CreateUI(cfg_cpy.params)
                            if is_item then
                                item_ui = ui
                            end
                            
                            uis[cfg.params.name] = ui
                        end
                    end
                end)
                local background_res
                if self.mSelectSide == "Left" and self.mSelectItemIndex == item_index2 then
                    background_res = Layout[20].params.background_res_on
                else
                    background_res = Layout[20].params.background_res
                end
                GetResourceImage(background_res, function(path)
                    uis["物品"].background = path
                end)
                if item_cfg.mIcon then
                    local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
                    if icon_cfg.mFile then
                        uis["物品图片"].background = icon_cfg.mFile
                    elseif icon_cfg.mResource then
                        GetResourceImage(icon_cfg.mResource, function(path, err)
                            uis["物品图片"].background = path
                        end)
                    end
                else
                    uis["物品图片"].background = ""
                end
                uis["物品名称"].text = item_cfg.mName
                uis["物品重量"].text = tostring(item.mCount * item_cfg.mWeight)
                uis["物品数量标值"].text = "X" .. tostring(item.mCount)
                
                uis["物品重量"].visible = true
                uis["物品数量标值"].visible = true
                if item.mHP then
                    uis["物品血条动态条"].width = math.floor(item.mHP / item_cfg.mHP * uis["物品血条"].width)
                    uis["物品血条动态条"].color = "0 255 0"
                    uis["物品血条"].visible = true
                    uis["物品血条动态条"].visible = true
                else
                    uis["物品血条"].visible = false
                    uis["物品血条动态条"].visible = false
                end
                
                
                self.mBagItems[item_index2] = {mSlot = slot}
                item_index = item_index + 1
            end
        end
    end
    while self.mItemGridView_Left:getItem(item_index) do
        self.mItemGridView_Left:destroyItem(item_index)
        table.remove(self.mGridViewItemUIs_Left)
    end
end

function Synthesis:_refreshMiddle(parameter)
    if parameter.mInteractive then
        local item_cfg = parameter.mInteractive:getConfig()
        if item_cfg.mEnable then
            if parameter.mInteractive:getProperty():cache().mEnable then
                if item_cfg.mName == "篝火" then
                    self.mUIs["合成操作按钮2"].text = "熄灭"
                end
            else
                if item_cfg.mName == "篝火" then
                    self.mUIs["合成操作按钮2"].text = "点燃"
                end
            end
        end
    end
end

function Synthesis:_refreshRight(parameter)
    self.mItemGridView_Right =
        self.mItemGridView_Right or
        CreateUI(
            {
                ext = "TABLE",
                type = "container",
                x = 0,
                y = 0,
                width = Layout[150].params.width,
                height = Layout[150].params.height,
                stride = 5,
                item_width = Layout[151].params.width,
                item_height = Layout[151].params.height,
                margin = 10,
                align = "_lt",
                parent = self.mUIs["合成物品区"]
            }
    )
    self.mGridViewItemUIs_Right = self.mGridViewItemUIs_Right or {}
    self.mBagItems_Right = {}
    local item_index = 1
    if self.mSelectMode_Right == "物品栏" then
        GetResourceImage(Layout[134].params.background_res_on, function(path, err)
            self.mUIs[Layout[134].params.name].background = path
        end)
        GetResourceImage(Layout[135].params.background_res, function(path, err)
            self.mUIs[Layout[135].params.name].background = path
        end)
        if parameter.mInteractive:getProperty():cache().mBag and parameter.mInteractive:getProperty():cache().mBag.mItems then
            local bag_items = parameter.mInteractive.mLocalBag or parameter.mInteractive:getProperty():cache().mBag
            bag_items = bag_items.mItems
            for slot, item in pairs(bag_items) do
                local item_cfg = Config.Item[item.mConfigIndex]
                local item_ui
                local item_index2 = item_index
                self.mGridViewItemUIs_Right[item_index2] = self.mGridViewItemUIs_Right[item_index2] or {}
                local uis = self.mGridViewItemUIs_Right[item_index2]
                local exist_item = self.mItemGridView_Right:getItem(item_index2) or self.mItemGridView_Right:addItem(function(parent)
                    for _, cfg in ipairs(Layout) do
                        if cfg.params.name == "合成物品"
                            or cfg.params.name == "合成物品名称"
                            or cfg.params.name == "合成物品重量"
                            or cfg.params.name == "合成物品数量标"
                            or cfg.params.name == "合成物品数量标值"
                            or cfg.params.name == "合成物品血条"
                            or cfg.params.name == "合成物品血条动态条"
                            or cfg.params.name == "合成物品按钮"
                            or cfg.params.name == "合成物品图片"
                        then
                            local cfg_cpy = clone(cfg)
                            cfg_cpy.params.clip = false
                            local is_item
                            if cfg.params.name == "合成物品" then
                                cfg_cpy.params.parent = parent
                                is_item = true
                            elseif cfg.params.parent == "合成物品" then
                                cfg_cpy.params.parent = item_ui
                            else
                                cfg_cpy.params.parent = uis[cfg.params.parent]
                            end
                            cfg_cpy.params.name = cfg.params.name .. "/" .. tostring(item_index2)
                            if cfg.params.type == "button" then
                                cfg_cpy.params.onclick = function(p)
                                    self:onClickItemRight(item_index2, p)
                                end
                            end
                            local ui = CreateUI(cfg_cpy.params)
                            if is_item then
                                item_ui = ui
                            end
                            
                            uis[cfg.params.name] = ui
                        end
                    end
                end)
                local background_res
                if self.mSelectSide == "Right" and self.mSelectItemIndex == item_index2 then
                    background_res = Layout[20].params.background_res_on
                else
                    background_res = Layout[20].params.background_res
                end
                GetResourceImage(background_res, function(path)
                    uis["合成物品"].background = path
                end)
                if item_cfg.mIcon then
                    local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
                    if icon_cfg.mFile then
                        uis["合成物品图片"].background = icon_cfg.mFile
                    elseif icon_cfg.mResource then
                        GetResourceImage(icon_cfg.mResource, function(path, err)
                            uis["合成物品图片"].background = path
                        end)
                    end
                else
                    uis["合成物品图片"].background = ""
                end
                uis["合成物品名称"].text = item_cfg.mName
                uis["合成物品重量"].text = tostring(item.mCount * item_cfg.mWeight)
                uis["合成物品数量标值"].text = "X" .. tostring(item.mCount)
                
                uis["合成物品重量"].visible = true
                uis["合成物品数量标值"].visible = true
                uis["合成物品数量标"].visible = true
                if item.mHP then
                    uis["合成物品血条动态条"].width = math.floor(item.mHP / item_cfg.mHP * uis["合成物品血条"].width)
                    uis["合成物品血条动态条"].color = "0 255 0"
                    uis["合成物品血条"].visible = true
                    uis["合成物品血条动态条"].visible = true
                else
                    uis["合成物品血条"].visible = false
                    uis["合成物品血条动态条"].visible = false
                end
                
                
                self.mBagItems_Right[item_index2] = {mSlot = slot}
                item_index = item_index + 1
            end
        end
    elseif self.mSelectMode_Right == "制作栏" then
        GetResourceImage(Layout[135].params.background_res_on, function(path, err)
            self.mUIs[Layout[135].params.name].background = path
        end)
        GetResourceImage(Layout[134].params.background_res, function(path, err)
            self.mUIs[Layout[134].params.name].background = path
        end)
        if parameter.mPlayer.mBlueprints then
            for slot, item in pairs(parameter.mPlayer.mBlueprints) do
                local blueprint_cfg = Config.Blueprint[item.mConfigIndex]
                if blueprint_cfg.mRequests and Utility.arrayGet(blueprint_cfg.mRequests, function(req) return req.mName == parameter.mInteractive:getConfig().mName end) then
                    local item_cfg = Utility.arrayGet(Config.Item, function(e) return e.mName == blueprint_cfg.mGenerates.mItems[1].mName end)
                    local item_ui
                    local item_index2 = item_index
                    self.mGridViewItemUIs_Right[item_index2] = self.mGridViewItemUIs_Right[item_index2] or {}
                    local uis = self.mGridViewItemUIs_Right[item_index2]
                    local exist_item = self.mItemGridView_Right:getItem(item_index2) or self.mItemGridView_Right:addItem(function(parent)
                        for _, cfg in ipairs(Layout) do
                            if cfg.params.name == "合成物品"
                                or cfg.params.name == "合成物品名称"
                                or cfg.params.name == "合成物品重量"
                                or cfg.params.name == "合成物品数量标"
                                or cfg.params.name == "合成物品数量标值"
                                or cfg.params.name == "合成物品血条"
                                or cfg.params.name == "合成物品血条动态条"
                                or cfg.params.name == "合成物品按钮"
                                or cfg.params.name == "合成物品图片"
                            then
                                local cfg_cpy = clone(cfg)
                                cfg_cpy.params.clip = false
                                local is_item
                                if cfg.params.name == "合成物品" then
                                    cfg_cpy.params.parent = parent
                                    is_item = true
                                elseif cfg.params.parent == "合成物品" then
                                    cfg_cpy.params.parent = item_ui
                                else
                                    cfg_cpy.params.parent = uis[cfg.params.parent]
                                end
                                cfg_cpy.params.name = cfg.params.name .. "/" .. tostring(item_index2)
                                if cfg.params.type == "button" then
                                    cfg_cpy.params.onclick = function(p)
                                        self:onClickItemRight(item_index2, p)
                                    end
                                end
                                local ui = CreateUI(cfg_cpy.params)
                                if is_item then
                                    item_ui = ui
                                end
                                
                                uis[cfg.params.name] = ui
                            end
                        end
                    end)
                    local background_res
                    if self.mSelectSide == "Right" and self.mSelectItemIndex == item_index2 then
                        background_res = Layout[20].params.background_res_on
                    else
                        background_res = Layout[20].params.background_res
                    end
                    GetResourceImage(background_res, function(path)
                        uis["合成物品"].background = path
                    end)
                    if item_cfg.mIcon then
                        local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
                        if icon_cfg.mFile then
                            uis["合成物品图片"].background = icon_cfg.mFile
                        elseif icon_cfg.mResource then
                            GetResourceImage(icon_cfg.mResource, function(path, err)
                                uis["合成物品图片"].background = path
                            end)
                        end
                    else
                        uis["合成物品图片"].background = ""
                    end
                    uis["合成物品名称"].text = item_cfg.mName
                    uis["合成物品重量"].visible = false
                    uis["合成物品数量标"].visible = false
                    uis["合成物品数量标值"].visible = false
                    uis["合成物品血条"].visible = false
                    uis["合成物品血条动态条"].visible = false
                    
                    if parameter.mInteractive.mLocalCrafting then
                        for _, crafting in pairs(parameter.mInteractive.mLocalCrafting) do
                            if crafting.mConfigIndex == item.mConfigIndex then
                                uis["合成物品数量标"].visible = true
                                uis["合成物品数量标值"].visible = true
                                uis["合成物品血条"].visible = true
                                uis["合成物品血条动态条"].visible = true
                                uis["合成物品数量标值"].text = "X" .. tostring(crafting.mCount)
                                uis["合成物品血条动态条"].width = math.floor(crafting.mTime / blueprint_cfg.mCraftingTime * uis["合成物品血条"].width)
                            end
                        end
                    end
                    
                    
                    self.mBagItems_Right[item_index2] = {mConfigIndex = item.mConfigIndex}
                    item_index = item_index + 1
                end
            end
        end
    end
    while self.mItemGridView_Right:getItem(item_index) do
        self.mItemGridView_Right:destroyItem(item_index)
        table.remove(self.mGridViewItemUIs_Right)
    end
end

function Synthesis:onClick(uiName)
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    local player_property = player:getProperty()
    local player_local_property = player:getLocalProperty()
    if uiName == "close" then
        Game.singleton():getUIManager():close("Synthesis")
    elseif uiName == "合成操作按钮2" then
        local item_cfg = self.mParameter.mInteractive:getConfig()
        if item_cfg.mEnable then
            self.mParameter.mInteractive:setEnable(not self.mParameter.mInteractive:getProperty():cache().mEnable)
        end
    elseif uiName == "切至合成物品栏按钮" then
        self.mSelectMode_Right = "物品栏"
    elseif uiName == "切至合成制作栏按钮" then
        self.mSelectMode_Right = "制作栏"
    else
        echo("devilwalk", uiName)
    end
end

function Synthesis:onClickItemLeft(itemIndex, event)
    self.mSelectSide = "Left"
    self.mSelectItemIndex = itemIndex
    if self.mSelectItemIndex then
        if event.button == "right" then
            local item = self.mParameter.mPlayer.mBag.mItems[self.mBagItems[itemIndex].mSlot]
            self.mParameter.mInteractive:addBagItem(item, function(count)
                local player = Game.singleton():getPlayerManager():getPlayerByID()
                player:removeBagItem(self.mBagItems[itemIndex].mSlot, count)
            end)
            self.mSelectItemIndex = nil
        elseif event.button == "left" then
            self:_createPop(itemIndex, event, "left")
        end
    end
end

function Synthesis:onClickItemRight(itemIndex, event)
    self.mSelectSide = "Right"
    self.mSelectItemIndex = itemIndex
    if self.mSelectItemIndex then
        if event.button == "right" then
            if self.mSelectMode_Right == "物品栏" then
                local item = self.mParameter.mInteractive:getProperty():cache().mBag.mItems[self.mBagItems_Right[itemIndex].mSlot]
                local player = Game.singleton():getPlayerManager():getPlayerByID()
                self.mParameter.mInteractive:removeBagItem(self.mBagItems_Right[itemIndex].mSlot, nil, function(count)
                    if count > 0 then
                        local add_item = clone(item)
                        add_item.mCount = count
                        player:addBagItem(add_item)
                    end
                end)
                self.mSelectItemIndex = nil
            elseif self.mSelectMode_Right == "制作栏" then
                self.mParameter.mInteractive:useBlueprint(self.mBagItems_Right[itemIndex].mConfigIndex, 1)
            end
        elseif event.button == "left" then
            self:_createPop(itemIndex, event, "right")
        end
    end
end

function Synthesis:onDragBeginLeft(itemIndex, event)
    local item = self.mParameter.mPlayer.mBag.mItems[self.mBagItems[itemIndex].mSlot]
    if item then
        local item_cfg = Config.Item[item.mConfigIndex]
        if item_cfg.mIcon then
            local icon_cfg = item_cfg.mIcon["m" .. self.mParameter.mPlayer.mSex] or item_cfg.mIcon
            if icon_cfg.mFile then
                self.mDragUI = CreateUI({type = "container", x = event.x - 45, y = event.y - 91, width = 90, height = 90, background = icon_cfg.mFile, align = "_lt", zorder = 100})
            elseif icon_cfg.mResource then
                GetResourceImage(icon_cfg.mResource, function(path, err)
                    self.mDragUI = CreateUI({type = "container", x = event.x - 45, y = event.y - 91, width = 90, height = 90, background = path, align = "_lt", zorder = 100})
                end)
            end
        end
    end
end

function Synthesis:onDragEndLeft(itemIndex, event)
    if self.mDragUI then
        self.mDragUI:destroy()
        self.mDragUI = nil
    end
    local item = self.mParameter.mPlayer.mBag.mItems[self.mBagItems[itemIndex].mSlot]
    if not item then
        return
    end
    local item_cfg = Config.Item[item.mConfigIndex]
    if item_cfg.mUse
        or Utility.arrayContain(item_cfg.mTypes, "建筑")
        or Utility.arrayContain(item_cfg.mTypes, "武器")
    then
        if string.find(event.dragging_dst.name, "InGame/手持栏1") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(1, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏2") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(2, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏3") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(3, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏4") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(4, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏5") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(5, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏6") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(6, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏7") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(7, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏8") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(8, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏9") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(9, self.mBagItems[itemIndex].mSlot)
        elseif string.find(event.dragging_dst.name, "InGame/手持栏10") then
            Game.singleton():getPlayerManager():getPlayerByID():setHandItem(10, self.mBagItems[itemIndex].mSlot)
        end
    end
end

function Synthesis:onDragMoveLeft(itemIndex, event)
    if self.mDragUI then
        self.mDragUI.x = event.x - 45
        self.mDragUI.y = event.y - 91
    end
end

function Synthesis:onMouseEnterLeft(itemIndex, event)
    Game.singleton():getUIManager():show("ItemInfo", 1, {mBagSlot = self.mBagItems[itemIndex].mSlot, mX = event.x + 30, mY = event.y + 30})
end

function Synthesis:_createPop(itemIndex, event, side)
    if self.mPopWindow then
        InitMiniGameUISystem().destroyWindow(self.mPopWindow)
        self.mPopWindow = nil
    end
    self.mPopWindow = InitMiniGameUISystem().createWindow("SynthesisPop", "_lt", event.x + 3, event.y + 3, 90, 60)
    self.mPopWindow:setZOrder(1)
    local edit_delete = self.mPopWindow:createUI("Edit", "SynthesisPop/Edit/Delete", "_lt", 60, 0, 30, 30)
    edit_delete:setText("0")
    local button_delete = self.mPopWindow:createUI("Button", "SynthesisPop/Button/Delete", "_lt", 0, 0, 60, 30)
    button_delete:setFontSize(20)
    if side == "left" or self.mSelectMode_Right == "物品栏" then
        button_delete:setText("删除")
        button_delete:addEventFunction("onclick", function()
            local count = tonumber(edit_delete:getText())
            if count and count > 0 then
                if side == "left" then
                    Game.singleton():getPlayerManager():getPlayerByID():removeBagItem(self.mBagItems[self.mSelectItemIndex].mSlot, count)
                elseif side == "right" then
                    self.mParameter.mInteractive:removeBagItem(self.mBagItems_Right[itemIndex].mSlot, count)
                end
            end
            InitMiniGameUISystem().destroyWindow(self.mPopWindow)
            self.mPopWindow = nil
        end)
        local edit_translate = self.mPopWindow:createUI("Edit", "SynthesisPop/Edit/Translate", "_lt", 60, 30, 30, 30)
        edit_translate:setText("1")
        local button_translate = self.mPopWindow:createUI("Button", "SynthesisPop/Button/Translate", "_lt", 0, 30, 60, 30)
        button_translate:setText("移动")
        button_translate:setFontSize(20)
        button_translate:addEventFunction("onclick", function()
            local count = tonumber(edit_translate:getText())
            if count and count > 0 then
                if side == "left" then
                    local item = clone(self.mParameter.mPlayer.mBag.mItems[self.mBagItems[itemIndex].mSlot])
                    count = math.min(count, item.mCount)
                    item.mCount = count
                    self.mParameter.mInteractive:addBagItem(item, function(count)
                        local player = Game.singleton():getPlayerManager():getPlayerByID()
                        player:removeBagItem(self.mBagItems[itemIndex].mSlot, count)
                    end)
                elseif side == "right" then
                    local item = self.mParameter.mInteractive:getProperty():cache().mBag.mItems[self.mBagItems_Right[itemIndex].mSlot]
                    count = math.min(count, item.mCount)
                    self.mParameter.mInteractive:removeBagItem(self.mBagItems_Right[itemIndex].mSlot, count, function(count)
                        if count > 0 then
                            local add_item = clone(item)
                            add_item.mCount = count
                            local player = Game.singleton():getPlayerManager():getPlayerByID()
                            player:addBagItem(add_item)
                        end
                    end)
                end
            end
            self.mSelectItemIndex = nil
            InitMiniGameUISystem().destroyWindow(self.mPopWindow)
            self.mPopWindow = nil
        end)
    elseif side == "right" and self.mSelectMode_Right == "制作栏" then
        button_delete:setText("制作")
        button_delete:addEventFunction("onclick", function()
            local count = tonumber(edit_delete:getText())
            if count and count > 0 then
                self.mParameter.mInteractive:useBlueprint(self.mBagItems_Right[itemIndex].mConfigIndex, count)
            end
            InitMiniGameUISystem().destroyWindow(self.mPopWindow)
            self.mPopWindow = nil
        end)
    end
end
