local Layout = {
    [1] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 255 0",
            ["y"] = 2,
            ["parent"] = "__root",
            ["zorder"] = 12,
            ["align"] = "_lt",
            ["x"] = 1,
            ["type"] = "container",
            ["height"] = 1,
            ["name"] = "ingame",
            ["events"] = {
            },
        },
    },
    [2] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 600,
            ["y"] = 120,
            ["clip"] = true,
            ["zorder"] = 12,
            ["align"] = "_lt",
            ["x"] = 100,
            ["type"] = "container",
            ["background_res"] = {hash = "FgCGQ9pm0kY1aSdeuQeXsrKQVSsl", pid = "161997", ext = "png", },
            ["name"] = "物品栏",
            ["height"] = 850,
            ["events"] = {
            },
        },
    },
    [3] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["font_color"] = "255",
            ["parent"] = "物品栏",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["background_res"] = {hash = "FojEfRVKveenjMwSv4yHhbBc7t5b", pid = "187310", ext = "png", },
            ["background_res_on"] = {hash = "FkunYt5wDJUlrSS238vJ4VZ5pI51", pid = "162026", ext = "png", },
            ["x"] = 0,
            ["name"] = "切至物品栏按钮",
            ["align"] = "_lt",
        },
    },
    [4] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["font_color"] = "255 255 255",
            ["parent"] = "物品栏",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["background_res"] = {hash = "Fq8TOjSLQklm_WhL7KMnnjWHRldd", pid = "187311", ext = "png", },
            ["background_res_on"] = {hash = "FvFMWitTRWrZ0jPUfAVMaTmVlW8k", pid = "162027", ext = "png", },
            ["x"] = 400,
            ["name"] = "切至制作栏按钮",
            ["align"] = "_lt",
        },
    },
    [5] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 540,
            ["y"] = 78,
            ["clip"] = true,
            ["parent"] = "物品栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 30,
            ["type"] = "container",
            ["name"] = "物品栏辅助功能",
            ["height"] = 75,
            ["events"] = {
            },
        },
    },
    [6] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 150,
            ["y"] = 10,
            ["font_color"] = "50 50 50",
            ["parent"] = "物品栏辅助功能",
            ["text"] = "查找",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "editbox",
            ["x"] = 0,
            ["name"] = "查找栏",
            ["align"] = "_lt",
        },
    },
    [7] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 160,
            ["type"] = "container",
            ["color"] = "0 0 0",
            ["name"] = "预留4",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [8] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留4",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["color"] = "0 0 0",
            ["font_color"] = "255 255 255",
            ["name"] = "预留按钮4",
            ["align"] = "_lt",
        },
    },
    [9] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 220,
            ["type"] = "container",
            ["name"] = "预留5",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [10] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留5",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["color"] = "0 0 0",
            ["height"] = 50,
            ["name"] = "预留按钮5",
            ["events"] = {
            },
        },
    },
    [11] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 280,
            ["type"] = "container",
            ["name"] = "预留6",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [12] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留6",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮6",
            ["events"] = {
            },
        },
    },
    [13] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 340,
            ["type"] = "container",
            ["name"] = "预留7",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [14] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留7",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮7",
            ["events"] = {
            },
        },
    },
    [15] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 425,
            ["type"] = "container",
            ["name"] = "预留8",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [16] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留8",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮8",
            ["events"] = {
            },
        },
    },
    [17] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 485,
            ["type"] = "container",
            ["name"] = "预留9",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [18] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留9",
            ["text"] = "button",
            ["color"] = "0 0 0",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮9",
            ["events"] = {
            },
        },
    },
    [19] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 540,
            ["color"] = "125 125 125",
            ["y"] = 170,
            ["clip"] = true,
            ["parent"] = "物品栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 30,
            ["type"] = "container",
            ["background_res"] = {hash = "FjWpC1tfDBFZr3tneBOQmycA4aU2", pid = "162025", ext = "png", },
            ["name"] = "物品区",
            ["height"] = 650,
            ["events"] = {
            },
        },
    },
    [20] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["parent"] = "物品区",
            ["zorder"] = 1,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["name"] = "物品",
            ["color"] = "25 125 125",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["background_res_on"] = {hash = "Fmuq3eJUVUDEXBfgutHodyBTqNGD", pid = "161637", ext = "png", },
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [21] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "200 200 200",
            ["y"] = 35,
            ["height"] = 25,
            ["parent"] = "物品",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "物品名称",
            ["x"] = 0,
        },
    },
    [22] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "物品",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [23] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 450,
            ["y"] = 118,
            ["clip"] = true,
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 734,
            ["type"] = "container",
            ["background_res"] = {hash = "FoSn4Hh0qYJoX92dHTFrkHqdlnWS", pid = "162035", ext = "png", },
            ["name"] = "本人",
            ["height"] = 850,
            ["events"] = {
            },
        },
    },
    [24] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["x"] = 20,
            ["parent"] = "本人",
            ["text"] = "本人",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "本人标题",
            ["align"] = "_lt",
        },
    },
    [25] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 80,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 10,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "头部",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [26] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "头部",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "头部物品名称",
            ["x"] = 0,
        },
    },
    [27] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "头部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "头部物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [28] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 180,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 10,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "躯干",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [29] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "躯干",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "躯干物品名称",
            ["x"] = 0,
        },
    },
    [30] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "躯干",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "躯干物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [31] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 280,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 10,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "腿部",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [32] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "腿部",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "腿部物品名称",
            ["x"] = 0,
        },
    },
    [33] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "腿部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "腿部物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [34] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 80,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 350,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "手部",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [35] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "手部",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "手部物品名称",
            ["x"] = 0,
        },
    },
    [36] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "手部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "手部物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [37] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 180,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 350,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "副手",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [38] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "副手",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "副手物品名称",
            ["x"] = 0,
        },
    },
    [39] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "副手",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "副手物品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [40] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 280,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["x"] = 350,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "FlTVnHEY-KISMAAEb3ZrSn_B19id", pid = "161632", ext = "png", },
            ["name"] = "脚部",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [41] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["font_color"] = "255 255 255",
            ["y"] = 65,
            ["height"] = 25,
            ["parent"] = "脚部",
            ["text"] = "text",
            ["zorder"] = 1,
            ["text_format"] = 9,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "脚部物品名称",
            ["x"] = 0,
        },
    },
    [42] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "脚部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "脚部品图片",
            ["height"] = 70,
            ["events"] = {
            },
        },
    },
    [43] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 220,
            ["color"] = "0 0 0",
            ["y"] = 80,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 115,
            ["type"] = "container",
            ["name"] = "角色信息区",
            ["height"] = 300,
            ["events"] = {
            },
        },
    },
    [44] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 220,
            ["y"] = 0,
            ["height"] = 30,
            ["parent"] = "角色信息区",
            ["text"] = "人类",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "人类",
            ["x"] = 0,
        },
    },
    [45] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 220,
            ["y"] = 35,
            ["height"] = 25,
            ["parent"] = "角色信息区",
            ["text"] = "等级：1",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "等级",
            ["x"] = 0,
        },
    },
    [46] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 220,
            ["y"] = 70,
            ["height"] = 25,
            ["parent"] = "角色信息区",
            ["text"] = "人类的部落",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "人类的部落",
            ["x"] = 0,
        },
    },
    [47] = {
        ["type"] = "ui",
        ["params"] = {
            ["font_color"] = "255 255 255",
            ["width"] = 220,
            ["y"] = 100,
            ["height"] = 25,
            ["parent"] = "角色信息区",
            ["text"] = "足爪",
            ["text_format"] = 9,
            ["zorder"] = 1,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "足爪",
            ["x"] = 0,
        },
    },
    [48] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 120,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "天数：1",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 18,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "天数",
            ["align"] = "_lt",
        },
    },
    [49] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 140,
            ["font_color"] = "255 255 255",
            ["parent"] = "角色信息区",
            ["text"] = "时间：  00:00:00",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 20,
            ["name"] = "时间",
            ["align"] = "_lt",
        },
    },
    [50] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 160,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "温度:   13",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 18,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "温度",
            ["align"] = "_lt",
        },
    },
    [51] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 200,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "护甲：0",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 18,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "护甲",
            ["align"] = "_lt",
        },
    },
    [52] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 220,
            ["font_color"] = "255 255 255",
            ["parent"] = "角色信息区",
            ["text"] = "低温抗性： 0",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 20,
            ["name"] = "低温抗性",
            ["align"] = "_lt",
        },
    },
    [53] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 240,
            ["x"] = 20,
            ["parent"] = "角色信息区",
            ["text"] = "高温抗性： 0",
            ["zorder"] = 1,
            ["height"] = 25,
            ["events"] = {
            },
            ["font_size"] = 18,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "高温抗性",
            ["align"] = "_lt",
        },
    },
    [54] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 450,
            ["y"] = 390,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "经验条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [55] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 0 20",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "经验条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "经验动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [56] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 5,
            ["parent"] = "经验条",
            ["text"] = "经验值 : 0.7/5",
            ["zorder"] = 1,
            ["x"] = 10,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["height"] = 30,
            ["name"] = "经验数值",
            ["events"] = {
            },
        },
    },
    [57] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 450,
            ["color"] = "255 25 255",
            ["y"] = 429,
            ["clip"] = true,
            ["parent"] = "本人",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 1,
            ["type"] = "container",
            ["background"] = "",
            ["name"] = "成长条",
            ["height"] = 420,
            ["events"] = {
            },
        },
    },
    [58] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条1",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [59] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条1",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条1图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [60] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条1",
            ["text"] = "生命值",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条1名称",
            ["events"] = {
            },
        },
    },
    [61] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条1",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条1数值",
            ["font_color"] = "255 255 255",
        },
    },
    [62] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条1",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条1动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [63] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 0,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条1按钮",
            ["events"] = {
            },
        },
    },
    [64] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 37,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条2",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [65] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条2",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条2图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [66] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条2",
            ["text"] = "耐力",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条2名称",
            ["events"] = {
            },
        },
    },
    [67] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条2",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条2数值",
            ["font_color"] = "255 255 255",
        },
    },
    [68] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条2",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条2动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [69] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 37,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条2按钮",
            ["events"] = {
            },
        },
    },
    [70] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 74,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条3",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [71] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条3",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条3图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [72] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条3",
            ["text"] = "氧气",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条3名称",
            ["events"] = {
            },
        },
    },
    [73] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条3",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条3数值",
            ["font_color"] = "255 255 255",
        },
    },
    [74] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条3",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条3动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [75] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 74,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条3按钮",
            ["events"] = {
            },
        },
    },
    [76] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 111,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条4",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [77] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条4",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条4图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [78] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条4",
            ["text"] = "食物",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条4名称",
            ["events"] = {
            },
        },
    },
    [79] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条4",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条4数值",
            ["font_color"] = "255 255 255",
        },
    },
    [80] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条4",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条4动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [81] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 111,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条4按钮",
            ["events"] = {
            },
        },
    },
    [82] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 148,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条5",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [83] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条5",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条5图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [84] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条5",
            ["text"] = "水",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条5名称",
            ["events"] = {
            },
        },
    },
    [85] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条5",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条5数值",
            ["font_color"] = "255 255 255",
        },
    },
    [86] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条5",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条5动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [87] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 148,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条5按钮",
            ["events"] = {
            },
        },
    },
    [88] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 185,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条6",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [89] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条6",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条6图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [90] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条6",
            ["text"] = "重量",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条6名称",
            ["events"] = {
            },
        },
    },
    [91] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条6",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条6数值",
            ["font_color"] = "255 255 255",
        },
    },
    [92] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条6",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条6动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [93] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 185,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条6按钮",
            ["events"] = {
            },
        },
    },
    [94] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 222,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条7",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [95] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条7",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条7图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [96] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条7",
            ["text"] = "近战伤害",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条7名称",
            ["events"] = {
            },
        },
    },
    [97] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条7",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条7数值",
            ["font_color"] = "255 255 255",
        },
    },
    [98] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条7",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条7动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [99] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 222,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条7按钮",
            ["events"] = {
            },
        },
    },
    [100] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 259,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条8",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [101] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条8",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条8图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [102] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条8",
            ["text"] = "移动速度",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条8名称",
            ["events"] = {
            },
        },
    },
    [103] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条8",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条8数值",
            ["font_color"] = "255 255 255",
        },
    },
    [104] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条8",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条8动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [105] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 259,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条8按钮",
            ["events"] = {
            },
        },
    },
    [106] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 296,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条9",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [107] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条9",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条9图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [108] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条9",
            ["text"] = "制作技能",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条9名称",
            ["events"] = {
            },
        },
    },
    [109] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条9",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条9数值",
            ["font_color"] = "255 255 255",
        },
    },
    [110] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条9",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条9动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [111] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 296,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条9按钮",
            ["events"] = {
            },
        },
    },
    [112] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 333,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条10",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [113] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条10",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条10图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [114] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条10",
            ["text"] = "抗性",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条10名称",
            ["events"] = {
            },
        },
    },
    [115] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条10",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条10数值",
            ["font_color"] = "255 255 255",
        },
    },
    [116] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条10",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条10动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [117] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 333,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条10按钮",
            ["events"] = {
            },
        },
    },
    [118] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 400,
            ["y"] = 370,
            ["clip"] = true,
            ["parent"] = "成长条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条11",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [119] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "成长条11",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 2,
            ["type"] = "container",
            ["name"] = "成长条11图标",
            ["height"] = 30,
            ["events"] = {
            },
        },
    },
    [120] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 2,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条11",
            ["text"] = "眩晕",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["x"] = 33,
            ["name"] = "成长条11名称",
            ["events"] = {
            },
        },
    },
    [121] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 200,
            ["text_format"] = 18,
            ["y"] = 2,
            ["height"] = 25,
            ["parent"] = "成长条11",
            ["text"] = "100.0 / 100.0",
            ["zorder"] = 1,
            ["width"] = 200,
            ["align"] = "_lt",
            ["font_size"] = 18,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "成长条11数值",
            ["font_color"] = "255 255 255",
        },
    },
    [122] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 25 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "成长条11",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "成长条11动态条",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [123] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 35,
            ["y"] = 370,
            ["font_color"] = "255 255 255",
            ["parent"] = "成长条",
            ["text"] = "+",
            ["zorder"] = 1,
            ["height"] = 35,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 407,
            ["name"] = "成长条11按钮",
            ["events"] = {
            },
        },
    },
    [124] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 600,
            ["y"] = 118,
            ["clip"] = true,
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 1220,
            ["type"] = "container",
            ["background_res"] = {hash = "FqlwiAyYhm4EeZl7hqe3-NbCI55i", pid = "162037", ext = "png", },
            ["name"] = "角色形象",
            ["height"] = 850,
            ["events"] = {
            },
        },
    },
    [125] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 550,
            ["y"] = 26,
            ["clip"] = true,
            ["parent"] = "角色形象",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 25,
            ["type"] = "container",
            ["name"] = "avatar",
            ["height"] = 800,
            ["events"] = {
            },
        },
    },
    [126] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 118,
            ["x"] = 1829,
            ["text"] = "X",
            ["zorder"] = 1,
            ["height"] = 50,
            ["events"] = {
            },
            ["font_size"] = 30,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "close",
            ["align"] = "_lt",
        },
    },
    [127] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 5,
            ["parent"] = "经验条",
            ["text"] = "可用点数 999",
            ["zorder"] = 1,
            ["x"] = 240,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["text_format"] = 6,
            ["height"] = 25,
            ["name"] = "可用点数",
        },
    },
    [128] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "物品",
            ["zorder"] = 3,
            ["align"] = "_lt",
            ["type"] = "container",
            --["background_res"] = {hash="FidoMko_5tD8vgrTuSVpIVNnjF55",pid="161641",ext="png",},
            ["x"] = 2,
            ["name"] = "物品数量标",
            ["height"] = 25,
        },
    },
    [129] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "物品数量标",
            ["zorder"] = 8,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png", },
            ["x"] = 0,
            ["name"] = "物品数量标图",
            ["height"] = 25,
        },
    },
    [130] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "物品数量标",
            ["text"] = "X999",
            ["zorder"] = 10,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "物品数量标值",
        },
    },
    [131] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 80,
            ["y"] = 5,
            ["parent"] = "物品",
            ["text"] = "88",
            ["zorder"] = 10,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["font_color"] = "200 200 200",
            ["type"] = "text",
            ["text_format"] = 10,
            ["x"] = 5,
            ["height"] = 80,
            ["name"] = "物品重量",
        },
    },
    [132] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "物品",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "物品血条",
            ["height"] = 5,
        },
    },
    [133] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "物品血条动态条",
            ["height"] = 6,
        },
    },
    [134] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 0,
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "物品",
            ["font_color"] = "255 255 255",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "button",
            ["width"] = 90,
            ["name"] = "物品按钮",
            ["height"] = 90,
        },
    },
    [135] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "头部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "头部物品血条",
            ["height"] = 5,
        },
    },
    [136] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "头部物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "头部物品血条动态条",
            ["height"] = 6,
        },
    },
    [137] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "躯干",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "躯干物品血条",
            ["height"] = 5,
        },
    },
    [138] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "躯干物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "躯干物品血条动态条",
            ["height"] = 6,
        },
    },
    [139] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "腿部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "腿部物品血条",
            ["height"] = 5,
        },
    },
    [140] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "腿部物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "腿部物品血条动态条",
            ["height"] = 6,
        },
    },
    [141] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "手部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手部物品血条",
            ["height"] = 5,
        },
    },
    [142] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手部物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手部物品血条动态条",
            ["height"] = 6,
        },
    },
    [143] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "副手",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "副手物品血条",
            ["height"] = 5,
        },
    },
    [144] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "副手物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "副手物品血条动态条",
            ["height"] = 6,
        },
    },
    [145] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 84,
            ["clip"] = true,
            ["parent"] = "脚部",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "脚部物品血条",
            ["height"] = 5,
        },
    },
    [146] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "脚部物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "脚部物品血条动态条",
            ["height"] = 6,
        },
    },
    [147] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "头部",
            ["zorder"] = 3,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["background"] = "",
            ["name"] = "头部按钮",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [148] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "躯干",
            ["zorder"] = 3,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["background"] = "",
            ["name"] = "躯干按钮",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [149] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "腿部",
            ["zorder"] = 3,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["background"] = "",
            ["name"] = "腿部按钮",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [150] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手部",
            ["zorder"] = 3,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["background"] = "",
            ["name"] = "手部按钮",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [151] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "副手",
            ["zorder"] = 3,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["background"] = "",
            ["name"] = "副手按钮",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
    [152] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "脚部",
            ["zorder"] = 3,
            ["x"] = 0,
            ["events"] = {
            },
            ["font_size"] = 20,
            ["type"] = "button",
            ["background"] = "",
            ["name"] = "脚部按钮",
            ["height"] = 90,
            ["align"] = "_lt",
        },
    },
}

local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Bag = inherit(Layout, Devil.getTable("Game.Famine.UI.Bag"))

local function _getPropertyColor(scalar, invert)
    if invert then
        if scalar < 0.2 then
            return "0 " .. tostring(math.floor(255 - scalar * 255)) .. " 0"
        elseif scalar < 0.5 then
            return tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " " .. tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor(255 - (scalar + 0.5) * 255)) .. " 0 0"
        end
    else
        if scalar > 0.8 then
            return "0 " .. tostring(math.floor(scalar * 255)) .. " 0"
        elseif scalar > 0.5 then
            return tostring(math.floor((scalar + 0.2) * 255)) .. " " .. tostring(math.floor((scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor((scalar + 0.5) * 255)) .. " 0 0"
        end
    end
end

function Bag:construction(parameter)
    self.mUIs = {}
    for _, cfg in ipairs(Layout) do
        local cfg_cpy = clone(cfg)
        cfg_cpy.params.name = "Bag/" .. cfg_cpy.params.name .. tostring(self)
        cfg_cpy.params.parent = self.mUIs[cfg.params.parent]
        cfg_cpy.params.align = cfg.params.align or "_lt"
        if cfg.params.type == "button" then
            cfg_cpy.params.onclick = function(p)
                self:onClick(cfg.params.name, p)
            end
        end
        local ui = CreateUI(cfg_cpy.params)
        self.mUIs[cfg.params.name] = ui
        if cfg.params.background_res then
            GetResourceImage(
                cfg.params.background_res,
                function(path, err)
                    ui.background = path
                end
        )
        end
    end
    self.mUIs["物品"].visible = false
    
    self.mLeftMode = "物品栏"
    
    self.mMiniScene = CreateMiniScene("Famine/Bag", 512, 512)
    self.mMiniScene:addMiniGameUI({mInternal = self.mUIs["avatar"]()})
    self.mMiniScene:setCameraLookAtPosition(0, 1.5, 10)
    self.mMiniScene:setCameraPosition(0, 0, -3.5)
    
    --self:refresh(parameter)
    Game.singleton():getManipulationManager():pushMode("Base")
end

function Bag:destruction()
    Game.singleton():getManipulationManager():popMode()
    if self.mMiniScene then
        if self.mAvatarEntity then
            self.mAvatarEntity:SetDead(true)
        end
        self.mAvatarEntity = nil
        self.mMiniScene.mScene:Reset()
        DestroyMiniScene("Famine/Bag")
        self.mMiniScene = nil
    end
    if self.mDragUI then
        self.mDragUI:destroy()
        self.mDragUI = nil
    end
    for _, ui in pairs(self.mUIs) do
        ui:destroy()
    end
    self.mUIs = nil
    self.mItemGridView = nil
    self.mGridViewItemUIs = nil
    self.mParameter = nil
    Game.singleton():getUIManager():close("ItemInfo")
    if self.mPopWindow then
        InitMiniGameUISystem().destroyWindow(self.mPopWindow)
        self.mPopWindow = nil
    end
end

function Bag:refresh(parameter)
    self.mParameter = parameter
    self:_refreshLeft(parameter)
    self:_refreshMiddle(parameter)
    self:_refreshRight(parameter)
end

function Bag:_refreshLeft(parameter)
    self.mItemGridView =
        self.mItemGridView or
        CreateUI(
            {
                ext = "TABLE",
                type = "container",
                x = 0,
                y = 0,
                width = Layout[19].params.width,
                height = Layout[19].params.height,
                stride = 5,
                item_width = Layout[20].params.width,
                item_height = Layout[20].params.height,
                margin = 10,
                align = "_lt",
                parent = self.mUIs["物品区"]
            }
    )
    self.mGridViewItemUIs = self.mGridViewItemUIs or {}
    self.mBagItems = {}
    if self.mLeftMode == "物品栏" then
        GetResourceImage(Layout[3].params.background_res_on, function(path, err)
            self.mUIs[Layout[3].params.name].background = path
        end)
        GetResourceImage(Layout[4].params.background_res, function(path, err)
            self.mUIs[Layout[4].params.name].background = path
        end)
        local item_index = 1
        if parameter.mPlayer.mBag.mItems then
            for slot, item in pairs(parameter.mPlayer.mBag.mItems) do
                local item_cfg = Config.Item[item.mConfigIndex]
                local equiped
                if parameter.mPlayer.mEquip then
                    if slot == parameter.mPlayer.mEquip.mHead
                        or slot == parameter.mPlayer.mEquip.mBody
                        or slot == parameter.mPlayer.mEquip.mLeg
                        or slot == parameter.mPlayer.mEquip.mHand
                        or slot == parameter.mPlayer.mEquip.mFoot
                        or slot == parameter.mPlayer.mEquip.mSecondHand
                    then
                        equiped = true
                    end
                end
                if not equiped then
                    local item_ui
                    local item_index2 = item_index
                    self.mGridViewItemUIs[item_index2] = self.mGridViewItemUIs[item_index2] or {}
                    local uis = self.mGridViewItemUIs[item_index2]
                    local exist_item = self.mItemGridView:getItem(item_index2) or self.mItemGridView:addItem(function(parent)
                        for _, cfg in ipairs(Layout) do
                            if cfg.params.name == "物品"
                                or cfg.params.name == "物品名称"
                                or cfg.params.name == "物品重量"
                                or cfg.params.name == "物品数量标"
                                or cfg.params.name == "物品数量标值"
                                or cfg.params.name == "物品血条"
                                or cfg.params.name == "物品血条动态条"
                                or cfg.params.name == "物品按钮"
                                or cfg.params.name == "物品图片"
                            then
                                local cfg_cpy = clone(cfg)
                                cfg_cpy.params.clip = false
                                local is_item
                                if cfg.params.name == "物品" then
                                    cfg_cpy.params.parent = parent
                                    is_item = true
                                elseif cfg.params.parent == "物品" then
                                    cfg_cpy.params.parent = item_ui
                                else
                                    cfg_cpy.params.parent = uis[cfg.params.parent]
                                end
                                cfg_cpy.params.name = cfg.params.name .. "/" .. tostring(item_index2)
                                if cfg.params.type == "button" then
                                    cfg_cpy.params.onclick = function(p)
                                        self:onClickItem(item_index2, p)
                                    end
                                    --cfg_cpy.params.candrag = true
                                    cfg_cpy.params.ondragbegin = function(p)
                                        self:onDragBegin(item_index2, p)
                                    end
                                    cfg_cpy.params.ondragend = function(p)
                                        self:onDragEnd(item_index2, p)
                                    end
                                    cfg_cpy.params.ondragmove = function(p)
                                        self:onDragMove(item_index2, p)
                                    end
                                    cfg_cpy.params.onmouseenter = function(p)
                                        self:onMouseEnter(item_index2, p)
                                    end
                                    cfg_cpy.params.onmouseleave = function(p)
                                        Game.singleton():getUIManager():close("ItemInfo")
                                    end
                                end
                                local ui = CreateUI(cfg_cpy.params)
                                if is_item then
                                    item_ui = ui
                                end
                                
                                uis[cfg.params.name] = ui
                            end
                        end
                    end)
                    local background_res
                    if self.mSelectItemIndex == item_index2 then
                        background_res = Layout[20].params.background_res_on
                    else
                        background_res = Layout[20].params.background_res
                    end
                    GetResourceImage(background_res, function(path)
                        uis["物品"].background = path
                    end)
                    if item_cfg.mIcon then
                        local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
                        if icon_cfg.mFile then
                            uis["物品图片"].background = icon_cfg.mFile
                        elseif icon_cfg.mResource then
                            GetResourceImage(icon_cfg.mResource, function(path, err)
                                uis["物品图片"].background = path
                            end)
                        end
                    else
                        uis["物品图片"].background = ""
                    end
                    uis["物品名称"].text = item_cfg.mName
                    uis["物品重量"].text = tostring(item.mCount * item_cfg.mWeight) .. "kg"
                    uis["物品数量标值"].text = "X" .. tostring(item.mCount)
                    
                    uis["物品重量"].visible = true
                    uis["物品数量标"].visible = true
                    uis["物品数量标值"].visible = true
                    if item.mHP then
                        uis["物品血条动态条"].width = math.floor(item.mHP / item_cfg.mHP * uis["物品血条"].width)
                        uis["物品血条动态条"].color = "0 255 0"
                        uis["物品血条"].visible = true
                        uis["物品血条动态条"].visible = true
                    else
                        uis["物品血条"].visible = false
                        uis["物品血条动态条"].visible = false
                    end
                    
                    uis["物品按钮"].candrag = true
                    
                    self.mBagItems[item_index2] = {mSlot = slot}
                    item_index = item_index + 1
                end
            end
        end
        while self.mItemGridView:getItem(item_index) do
            self.mItemGridView:destroyItem(item_index)
            table.remove(self.mGridViewItemUIs)
        end
    elseif self.mLeftMode == "制作栏" then
        GetResourceImage(Layout[3].params.background_res, function(path, err)
            self.mUIs[Layout[3].params.name].background = path
        end)
        GetResourceImage(Layout[4].params.background_res_on, function(path, err)
            self.mUIs[Layout[4].params.name].background = path
        end)
        local item_index = 1
        if parameter.mPlayer.mBlueprints then
            for slot, item in pairs(parameter.mPlayer.mBlueprints) do
                local item_ui
                local item_index2 = item_index
                self.mGridViewItemUIs[item_index2] = self.mGridViewItemUIs[item_index2] or {}
                local uis = self.mGridViewItemUIs[item_index2]
                local blueprint_cfg = Config.Blueprint[item.mConfigIndex]
                local can_use = true
                if blueprint_cfg.mRequests then
                    can_use = nil
                end
                if can_use then
                    local item_cfg = Utility.arrayGet(Config.Item, function(e) return e.mName == blueprint_cfg.mGenerates.mItems[1].mName end)
                    local exist_item = self.mItemGridView:getItem(item_index2) or self.mItemGridView:addItem(function(parent)
                        for i, cfg in ipairs(Layout) do
                            if cfg.params.name == "物品"
                                or cfg.params.name == "物品名称"
                                or cfg.params.name == "物品重量"
                                or cfg.params.name == "物品数量标"
                                or cfg.params.name == "物品数量标值"
                                or cfg.params.name == "物品血条"
                                or cfg.params.name == "物品血条动态条"
                                or cfg.params.name == "物品按钮"
                                or cfg.params.name == "物品图片"
                            then
                                local cfg_cpy = clone(cfg)
                                cfg_cpy.params.clip = false
                                local is_item
                                if cfg_cpy.params.name == "物品" then
                                    cfg_cpy.params.parent = parent
                                    is_item = true
                                elseif cfg.params.parent == "物品" then
                                    cfg_cpy.params.parent = item_ui
                                else
                                    cfg_cpy.params.parent = uis[cfg.params.parent]
                                end
                                cfg_cpy.params.name = cfg.params.name .. "/" .. tostring(item_index2)
                                if cfg.params.type == "button" then
                                    cfg_cpy.params.onclick = function(p)
                                        self:onClickItem(item_index2, p)
                                    end
                                    cfg_cpy.params.onmouseenter = function(p)
                                        self:onMouseEnter(item_index2, p)
                                    end
                                    cfg_cpy.params.onmouseleave = function(p)
                                        Game.singleton():getUIManager():close("ItemInfo")
                                    end
                                end
                                local ui = CreateUI(cfg_cpy.params)
                                if is_item then
                                    item_ui = ui
                                end
                                
                                uis[cfg.params.name] = ui
                            end
                        end
                    end)
                    local background_res
                    if self.mSelectItemIndex == item_index2 then
                        background_res = Layout[20].params.background_res_on
                    else
                        background_res = Layout[20].params.background_res
                    end
                    GetResourceImage(background_res, function(path)
                        uis["物品"].background = path
                    end)
                    if item_cfg.mIcon then
                        local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
                        if icon_cfg.mFile then
                            uis["物品图片"].background = icon_cfg.mFile
                        elseif icon_cfg.mResource then
                            GetResourceImage(icon_cfg.mResource, function(path, err)
                                uis["物品图片"].background = path or ""
                            end)
                        end
                    else
                        uis["物品图片"].background = ""
                    end
                    uis["物品名称"].text = item_cfg.mName
                    uis["物品重量"].visible = false
                    uis["物品数量标"].visible = false
                    uis["物品数量标值"].visible = false
                    uis["物品血条"].visible = false
                    uis["物品血条动态条"].visible = false
                    
                    if parameter.mPlayer.mCrafting then
                        for _, crafting in pairs(parameter.mPlayer.mCrafting) do
                            if crafting.mSlot == slot then
                                uis["物品数量标"].visible = true
                                uis["物品数量标值"].visible = true
                                uis["物品血条"].visible = true
                                uis["物品血条动态条"].visible = true
                                uis["物品数量标值"].text = "X" .. tostring(crafting.mCount)
                                uis["物品血条动态条"].width = math.floor(crafting.mTime / blueprint_cfg.mCraftingTime * uis["物品血条"].width)
                            end
                        end
                    end
                    uis["物品按钮"].candrag = false
                    
                    self.mBagItems[item_index2] = {mSlot = slot}
                    item_index = item_index + 1
                end
            end
        end
        while self.mItemGridView:getItem(item_index) do
            self.mItemGridView:destroyItem(item_index)
            table.remove(self.mGridViewItemUIs)
        end
    end
end

function Bag:_refreshMiddle(parameter)
    self.mUIs["头部物品名称"].visible = false
    self.mUIs["躯干物品名称"].visible = false
    self.mUIs["腿部物品名称"].visible = false
    self.mUIs["手部物品名称"].visible = false
    self.mUIs["脚部物品名称"].visible = false
    self.mUIs["副手物品名称"].visible = false
    self.mUIs["头部物品图片"].visible = false
    self.mUIs["躯干物品图片"].visible = false
    self.mUIs["腿部物品图片"].visible = false
    self.mUIs["手部物品图片"].visible = false
    self.mUIs["脚部品图片"].visible = false
    self.mUIs["副手物品图片"].visible = false
    self.mUIs["头部物品血条"].visible = false
    self.mUIs["躯干物品血条"].visible = false
    self.mUIs["腿部物品血条"].visible = false
    self.mUIs["脚部物品血条"].visible = false
    self.mUIs["副手物品血条"].visible = false
    self.mUIs["手部物品血条"].visible = false
    if parameter.mPlayer.mEquip then
        if parameter.mPlayer.mEquip.mHead then
            local item = parameter.mPlayer.mBag.mItems[parameter.mPlayer.mEquip.mHead]
            local cfg = Config.Item[item.mConfigIndex]
            self.mUIs["头部物品名称"].visible = true
            self.mUIs["头部物品名称"].text = cfg.mName
            if cfg.mIcon then
                self.mUIs["头部物品图片"].visible = true
                local icon_cfg = cfg.mIcon["m" .. parameter.mPlayer.mSex]
                if icon_cfg.mFile then
                    self.mUIs["头部物品图片"].background = icon_cfg.mFile
                elseif icon_cfg.mResource then
                    GetResourceImage(icon_cfg.mResource, function(path, err)
                        self.mUIs["头部物品图片"].background = path
                    end)
                end
            end
            if cfg.mHP then
                self.mUIs["头部物品血条"].visible = true
                self.mUIs["头部物品血条动态条"].width = math.floor(item.mHP / cfg.mHP * self.mUIs["头部物品血条"].width)
            end
        end
        if parameter.mPlayer.mEquip.mBody then
            local item = parameter.mPlayer.mBag.mItems[parameter.mPlayer.mEquip.mBody]
            local cfg = Config.Item[item.mConfigIndex]
            self.mUIs["躯干物品名称"].visible = true
            self.mUIs["躯干物品名称"].text = cfg.mName
            if cfg.mIcon then
                self.mUIs["躯干物品图片"].visible = true
                local icon_cfg = cfg.mIcon["m" .. parameter.mPlayer.mSex]
                if icon_cfg.mFile then
                    self.mUIs["躯干物品图片"].background = icon_cfg.mFile
                elseif icon_cfg.mResource then
                    GetResourceImage(icon_cfg.mResource, function(path, err)
                        self.mUIs["躯干物品图片"].background = path
                    end)
                end
            end
            if cfg.mHP then
                self.mUIs["躯干物品血条"].visible = true
                self.mUIs["躯干物品血条动态条"].width = math.floor(item.mHP / cfg.mHP * self.mUIs["躯干物品血条"].width)
            end
        end
        if parameter.mPlayer.mEquip.mLeg then
            local item = parameter.mPlayer.mBag.mItems[parameter.mPlayer.mEquip.mLeg]
            local cfg = Config.Item[item.mConfigIndex]
            self.mUIs["腿部物品名称"].visible = true
            self.mUIs["腿部物品名称"].text = cfg.mName
            if cfg.mIcon then
                self.mUIs["腿部物品图片"].visible = true
                local icon_cfg = cfg.mIcon["m" .. parameter.mPlayer.mSex]
                if icon_cfg.mFile then
                    self.mUIs["腿部物品图片"].background = icon_cfg.mFile
                elseif icon_cfg.mResource then
                    GetResourceImage(icon_cfg.mResource, function(path, err)
                        self.mUIs["腿部物品图片"].background = path
                    end)
                end
            end
            if cfg.mHP then
                self.mUIs["腿部物品血条"].visible = true
                self.mUIs["腿部物品血条动态条"].width = math.floor(item.mHP / cfg.mHP * self.mUIs["腿部物品血条"].width)
            end
        end
        if parameter.mPlayer.mEquip.mHand then
            local item = parameter.mPlayer.mBag.mItems[parameter.mPlayer.mEquip.mHand]
            local cfg = Config.Item[item.mConfigIndex]
            self.mUIs["手部物品名称"].visible = true
            self.mUIs["手部物品名称"].text = cfg.mName
            if cfg.mIcon then
                self.mUIs["手部物品图片"].visible = true
                local icon_cfg = cfg.mIcon["m" .. parameter.mPlayer.mSex]
                if icon_cfg.mFile then
                    self.mUIs["手部物品图片"].background = icon_cfg.mFile
                elseif icon_cfg.mResource then
                    GetResourceImage(icon_cfg.mResource, function(path, err)
                        self.mUIs["手部物品图片"].background = path
                    end)
                end
            end
            if cfg.mHP then
                self.mUIs["手部物品血条"].visible = true
                self.mUIs["手部物品血条动态条"].width = math.floor(item.mHP / cfg.mHP * self.mUIs["手部物品血条"].width)
            end
        end
        if parameter.mPlayer.mEquip.mFoot then
            local item = parameter.mPlayer.mBag.mItems[parameter.mPlayer.mEquip.mFoot]
            local cfg = Config.Item[item.mConfigIndex]
            self.mUIs["脚部物品名称"].visible = true
            self.mUIs["脚部物品名称"].text = cfg.mName
            if cfg.mIcon then
                self.mUIs["脚部品图片"].visible = true
                local icon_cfg = cfg.mIcon["m" .. parameter.mPlayer.mSex]
                if icon_cfg.mFile then
                    self.mUIs["脚部品图片"].background = icon_cfg.mFile
                elseif icon_cfg.mResource then
                    GetResourceImage(icon_cfg.mResource, function(path, err)
                        self.mUIs["脚部品图片"].background = path
                    end)
                end
            end
            if cfg.mHP then
                self.mUIs["脚部物品血条"].visible = true
                self.mUIs["脚部物品血条动态条"].width = math.floor(item.mHP / cfg.mHP * self.mUIs["脚部物品血条"].width)
            end
        end
    end
    if parameter.mTime then
        local day = math.floor(parameter.mTime / 60 / 24) + 1
        local month = math.floor(parameter.mTime / 60 / 24 / 30) % 12 + 1
        local year = math.floor(parameter.mTime / 60 / 24 / 30 / 12) + 666
        self.mUIs["天数"].text = "你妹元" .. tostring(year) .. "年" .. tostring(month) .. "月" .. tostring(day) .. "日"
        self.mUIs["时间"].text = tostring(math.floor(parameter.mTime / 60) % 24) .. "时" .. tostring(math.floor(parameter.mTime) % 60) .. "分"
        self.mUIs["温度"].text = "温度:" .. tostring(Utility.processFloat(GameUtility.calcTemperature(parameter.mTime), 1)) .. "℃"
    end
    if parameter.mPlayer then
        if parameter.mPlayer.mLevel then
            self.mUIs["等级"].text = "等级:" .. parameter.mPlayer.mLevel
        end
        if parameter.mPlayer.mExp and parameter.mPlayer.mLevel then
            local scalar = parameter.mPlayer.mExp / GameUtility.calcPlayerLevelUpExp(parameter.mPlayer.mLevel)
            self.mUIs["经验数值"].text = "经验值:" .. tostring(Utility.processFloat(parameter.mPlayer.mExp, 1)) .. "/" .. tostring(GameUtility.calcPlayerLevelUpExp(parameter.mPlayer.mLevel))
            self.mUIs["经验动态条"].width = math.floor(Layout[54].params.width * scalar)
            self.mUIs["经验动态条"].color = _getPropertyColor(scalar)
        end
        if parameter.mPlayer.mHPLevel and parameter.mPlayer.mHP then
            local scalar = parameter.mPlayer.mHP / GameUtility.calcPlayerHP(parameter.mPlayer.mHPLevel)
            self.mUIs["成长条1数值"].text = tostring(Utility.processFloat(parameter.mPlayer.mHP, 1)) .. "/" .. tostring(GameUtility.calcPlayerHP(parameter.mPlayer.mHPLevel))
            self.mUIs["成长条1动态条"].width = math.floor(Layout[58].params.width * scalar)
            self.mUIs["成长条1动态条"].color = _getPropertyColor(scalar)
        end
        if parameter.mPlayer.mStaminaLevel and parameter.mPlayer.mStamina then
            local scalar = parameter.mPlayer.mStamina / GameUtility.calcPlayerStamina(parameter.mPlayer.mStaminaLevel)
            self.mUIs["成长条2动态条"].color = _getPropertyColor(scalar)
            self.mUIs["成长条2动态条"].width = math.floor(Layout[58].params.width * scalar)
            self.mUIs["成长条2数值"].text = tostring(Utility.processFloat(parameter.mPlayer.mStamina, 1)) .. "/" .. tostring(GameUtility.calcPlayerStamina(parameter.mPlayer.mStaminaLevel))
        end
        if parameter.mPlayer.mOxygenLevel and parameter.mPlayer.mOxygen then
            local scalar = parameter.mPlayer.mOxygen / GameUtility.calcPlayerOxygen(parameter.mPlayer.mOxygenLevel)
            self.mUIs["成长条3动态条"].color = _getPropertyColor(scalar)
            self.mUIs["成长条3动态条"].width = math.floor(Layout[58].params.width * scalar)
            self.mUIs["成长条3数值"].text = tostring(Utility.processFloat(parameter.mPlayer.mOxygen, 1)) .. "/" .. tostring(GameUtility.calcPlayerOxygen(parameter.mPlayer.mOxygenLevel))
        end
        if parameter.mPlayer.mFoodLevel and parameter.mPlayer.mFood then
            local scalar = parameter.mPlayer.mFood / GameUtility.calcPlayerFood(parameter.mPlayer.mFoodLevel)
            self.mUIs["成长条4动态条"].color = _getPropertyColor(scalar)
            self.mUIs["成长条4动态条"].width = math.floor(Layout[58].params.width * scalar)
            self.mUIs["成长条4数值"].text = tostring(Utility.processFloat(parameter.mPlayer.mFood, 1)) .. "/" .. tostring(GameUtility.calcPlayerFood(parameter.mPlayer.mFoodLevel))
        end
        if parameter.mPlayer.mWaterLevel and parameter.mPlayer.mWater then
            local scalar = parameter.mPlayer.mWater / GameUtility.calcPlayerWater(parameter.mPlayer.mWaterLevel)
            self.mUIs["成长条5动态条"].color = _getPropertyColor(scalar)
            self.mUIs["成长条5动态条"].width = math.floor(Layout[58].params.width * scalar)
            self.mUIs["成长条5数值"].text = tostring(Utility.processFloat(parameter.mPlayer.mWater, 1)) .. "/" .. tostring(GameUtility.calcPlayerWater(parameter.mPlayer.mWaterLevel))
        end
        if parameter.mPlayer.mWeightLevel and parameter.mPlayer.mWeight then
            local scalar = parameter.mPlayer.mWeight / GameUtility.calcPlayerWeight(parameter.mPlayer.mWeightLevel)
            self.mUIs["成长条6动态条"].color = _getPropertyColor(scalar, true)
            self.mUIs["成长条6动态条"].width = math.floor(Layout[58].params.width * scalar)
            self.mUIs["成长条6数值"].text = tostring(Utility.processFloat(parameter.mPlayer.mWeight, 1)) .. "/" .. tostring(GameUtility.calcPlayerWeight(parameter.mPlayer.mWeightLevel))
        end
        if parameter.mPlayer.mMeleeDamageLevel then
            self.mUIs["成长条7数值"].text = tostring(Utility.processFloat(GameUtility.calcPlayerMeleeDamage(parameter.mPlayer.mMeleeDamageLevel) * 100, 1)) .. "%"
        end
        if parameter.mPlayer.mMovementSpeedLevel then
            self.mUIs["成长条8数值"].text = tostring(Utility.processFloat(GameUtility.calcPlayerMovementSpeed(parameter.mPlayer.mMovementSpeedLevel) * 100, 1)) .. "%"
        end
        if parameter.mPlayer.mCraftingSpeedLevel then
            self.mUIs["成长条9数值"].text = tostring(Utility.processFloat(GameUtility.calcPlayerCraftingSpeed(parameter.mPlayer.mCraftingSpeedLevel) * 100, 1)) .. "%"
        end
        if parameter.mPlayer.mFortitudeLevel then
            local value = GameUtility.calcPlayerFortitude(parameter.mPlayer.mFortitudeLevel)
            self.mUIs["成长条10数值"].text = tostring(Utility.processFloat(value, 1))
        end
        if parameter.mPlayer.mFaint then
            local scalar = parameter.mPlayer.mFaint / 100
            self.mUIs["成长条11数值"].text = tostring(Utility.processFloat(parameter.mPlayer.mFaint, 0.1)) .. "/100"
            self.mUIs["成长条11动态条"].color = _getPropertyColor(scalar, true)
            self.mUIs["成长条11动态条"].width = math.floor(Layout[58].params.width * scalar)
        end
        if parameter.mPlayer.mPropertyPoint and parameter.mPlayer.mPropertyPoint > 0 then
            self.mUIs["可用点数"].visible = true
            self.mUIs["可用点数"].text = "可用点数:" .. tostring(parameter.mPlayer.mPropertyPoint)
            for i = 1, 11 do
                self.mUIs["成长条" .. tostring(i) .. "按钮"].visible = true
            end
        else
            self.mUIs["可用点数"].visible = false
            for i = 1, 11 do
                self.mUIs["成长条" .. tostring(i) .. "按钮"].visible = false
            end
        end
        
        self.mUIs["护甲"].text = "护甲：" .. tostring(parameter.mPlayer.mDefense or 0)
        self.mUIs["高温抗性"].text = "高温抗性：" .. tostring(Utility.processFloat(parameter.mPlayer.mFortitude.mHot, 1))
        self.mUIs["低温抗性"].text = "低温抗性：" .. tostring(Utility.processFloat(parameter.mPlayer.mFortitude.mCold, 1))
    end
end

function Bag:_refreshRight(parameter)
    if not self.mAvatarEntity then
        self.mAvatarEntity =
            CreateNPC({bx = 0, by = 0, bz = 0, item_id = 10062, can_random_move = false, mDisableSync = true})
        self.mAvatarEntity._super.SetMainAssetPath(self.mAvatarEntity, "")
        self.mAvatarEntity._super.SetMainAssetPath(self.mAvatarEntity, Config.Default.Avatar.Head.mModel["m" .. parameter.mPlayer.mSex].mFile)
        self.mAvatarBodyModel = AddCustomAvatarComponent(Config.Default.Avatar.Body.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        self.mAvatarWaistModel = AddCustomAvatarComponent(Config.Default.Avatar.Body.mWaist.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        self.mAvatarLegModel = AddCustomAvatarComponent(Config.Default.Avatar.Leg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        self.mAvatarHandModel = AddCustomAvatarComponent(Config.Default.Avatar.Hand.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        self.mAvatarFootModel = AddCustomAvatarComponent(Config.Default.Avatar.Foot.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        self.mAvatarEntity:SetPosition(0, -0.5, 0)
        self.mAvatarEntity:SetFacing(0.8)
        self.mMiniScene:addEntity(self.mAvatarEntity)
    end
    if self.mAvatarHead ~= parameter.mPlayer.mEquip.mHead then
        self.mAvatarHead = parameter.mPlayer.mEquip.mHead
        local cfg
        if self.mAvatarHead then
            cfg = Config.Item[parameter.mPlayer.mBag.mItems[self.mAvatarHead].mConfigIndex]
        else
            cfg = Config.Default.Avatar.Head
        end
        self.mAvatarEntity._super.SetMainAssetPath(self.mAvatarEntity, "")
        self.mAvatarEntity._super.SetMainAssetPath(self.mAvatarEntity, cfg.mModel["m" .. parameter.mPlayer.mSex].mFile)
        if cfg.mTextures then
            local texture = cfg.mTextures["m" .. parameter.mPlayer.mSex][1]
            if texture then
                if texture.mFile then
                    SetReplaceableTexture(self.mAvatarEntity, texture.mFile)
                elseif texture.mResource then
                    GetResourceImage(texture.mResource, function(path, err)
                        SetReplaceableTexture(self.mAvatarEntity, path)
                    end)
                end
            end
        end
    end
    if self.mAvatarBody ~= parameter.mPlayer.mEquip.mBody then
        RemoveCustomAvatarComponent(self.mAvatarBodyModel)
        self.mAvatarBody = parameter.mPlayer.mEquip.mBody
        local cfg
        if self.mAvatarBody then
            cfg = Config.Item[parameter.mPlayer.mBag.mItems[self.mAvatarBody].mConfigIndex]
        else
            cfg = Config.Default.Avatar.Body
        end
        local texture = {}
        if cfg.mTextures then
            texture = cfg.mTextures["m" .. parameter.mPlayer.mSex][1]
        end
        if texture.mFile then
            self.mAvatarBodyModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, texture.mFile)
        elseif texture.mResource then
            GetResourceImage(texture.mResource, function(path, err)
                self.mAvatarBodyModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, path)
            end)
        else
            self.mAvatarBodyModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        end
        
        if self.mAvatarWaistModel then
            RemoveCustomAvatarComponent(self.mAvatarWaistModel)
            self.mAvatarWaistModel = nil
        end
        if cfg.mWaist then
            local cfg = cfg.mWaist
            local texture = {}
            if cfg.mTextures then
                texture = cfg.mTextures["m" .. parameter.mPlayer.mSex][1]
            end
            if texture.mFile then
                self.mAvatarWaistModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, texture.mFile)
            elseif texture.mResource then
                GetResourceImage(texture.mResource, function(path, err)
                    self.mAvatarWaistModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, path)
                end)
            else
                self.mAvatarWaistModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
            end
        end
    end
    if self.mAvatarLeg ~= parameter.mPlayer.mEquip.mLeg then
        RemoveCustomAvatarComponent(self.mAvatarLegModel)
        self.mAvatarLeg = parameter.mPlayer.mEquip.mLeg
        local cfg
        if self.mAvatarLeg then
            cfg = Config.Item[parameter.mPlayer.mBag.mItems[self.mAvatarLeg].mConfigIndex]
        else
            cfg = Config.Default.Avatar.Leg
        end
        local texture = {}
        if cfg.mTextures then
            texture = cfg.mTextures["m" .. parameter.mPlayer.mSex][1]
        end
        if texture.mFile then
            self.mAvatarLegModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, texture.mFile)
        elseif texture.mResource then
            GetResourceImage(texture.mResource, function(path, err)
                self.mAvatarLegModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, path)
            end)
        else
            self.mAvatarLegModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        end
    end
    if self.mAvatarHand ~= parameter.mPlayer.mEquip.mHand then
        RemoveCustomAvatarComponent(self.mAvatarHandModel)
        self.mAvatarHand = parameter.mPlayer.mEquip.mHand
        local cfg
        if self.mAvatarHand then
            cfg = Config.Item[parameter.mPlayer.mBag.mItems[self.mAvatarHand].mConfigIndex]
        else
            cfg = Config.Default.Avatar.Hand
        end
        local texture = {}
        if cfg.mTextures then
            texture = cfg.mTextures["m" .. parameter.mPlayer.mSex][1]
        end
        if texture.mFile then
            self.mAvatarHandModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, texture.mFile)
        elseif texture.mResource then
            GetResourceImage(texture.mResource, function(path, err)
                self.mAvatarHandModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, path)
            end)
        else
            self.mAvatarHandModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        end
    end
    if self.mAvatarFoot ~= parameter.mPlayer.mEquip.mFoot then
        RemoveCustomAvatarComponent(self.mAvatarFootModel)
        self.mAvatarFoot = parameter.mPlayer.mEquip.mFoot
        local cfg
        if self.mAvatarFoot then
            cfg = Config.Item[parameter.mPlayer.mBag.mItems[self.mAvatarFoot].mConfigIndex]
        else
            cfg = Config.Default.Avatar.Foot
        end
        local texture = {}
        if cfg.mTextures then
            texture = cfg.mTextures["m" .. parameter.mPlayer.mSex][1]
        end
        if texture.mFile then
            self.mAvatarFootModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, texture.mFile)
        elseif texture.mResource then
            GetResourceImage(texture.mResource, function(path, err)
                self.mAvatarFootModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity, path)
            end)
        else
            self.mAvatarFootModel = AddCustomAvatarComponent(cfg.mModel["m" .. parameter.mPlayer.mSex].mFile, self.mAvatarEntity)
        end
    end
    if self.mAvatarWeapon ~= parameter.mPlayer.mEquip.mWeapon then
        if self.mAvatarWeaponModel then
            RemoveCustomAvatarComponent(self.mAvatarWeaponModel)
            self.mAvatarWeaponModel = nil
        end
        self.mAvatarWeapon = parameter.mPlayer.mEquip.mWeapon
        if self.mAvatarWeapon then
            local cfg = Config.Item[parameter.mPlayer.mBag.mItems[self.mAvatarWeapon].mConfigIndex]
            local texture = {}
            if cfg.mTextures then
                texture = cfg.mTextures[1]
            end
            if texture.mFile then
                self.mAvatarWeaponModel = AddCustomAvatarComponent(cfg.mModel.mFile, self.mAvatarEntity, texture.mFile)
            elseif texture.mResource then
                GetResourceImage(texture.mResource, function(path, err)
                    self.mAvatarWeaponModel = AddCustomAvatarComponent(cfg.mModel.mFile, self.mAvatarEntity, path)
                end)
            else
                self.mAvatarWeaponModel = AddCustomAvatarComponent(cfg.mModel.mFile, self.mAvatarEntity)
            end
        end
    end
end

function Bag:onClick(uiName, event)
    Game.singleton():getUIManager():close("ItemInfo")
    if self.mPopWindow then
        InitMiniGameUISystem().destroyWindow(self.mPopWindow)
        self.mPopWindow = nil
    end
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    local player_property = player:getProperty()
    local player_local_property = player:getLocalProperty()
    if uiName == "close" then
        Game.singleton():getUIManager():close("Bag")
    elseif uiName == "切至物品栏按钮" then
        self.mLeftMode = "物品栏"
        self.mSelectItemIndex = nil
    elseif uiName == "切至制作栏按钮" then
        self.mLeftMode = "制作栏"
        self.mSelectItemIndex = nil
    elseif uiName == "成长条1按钮" then
        player_local_property.mHPLevel = player_local_property.mHPLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条2按钮" then
        player_local_property.mStaminaLevel = player_local_property.mStaminaLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条3按钮" then
        player_local_property.mOxygenLevel = player_local_property.mOxygenLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条4按钮" then
        player_local_property.mFoodLevel = player_local_property.mFoodLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条5按钮" then
        player_local_property.mWaterLevel = player_local_property.mWaterLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条6按钮" then
        player_local_property.mWeightLevel = player_local_property.mWeightLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条7按钮" then
        player_local_property.mMeleeDamageLevel = player_local_property.mMeleeDamageLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条8按钮" then
        player_local_property.mMovementSpeedLevel = player_local_property.mMovementSpeedLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条9按钮" then
        player_local_property.mCraftingSpeedLevel = player_local_property.mCraftingSpeedLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "成长条10按钮" then
        player_local_property.mFortitudeLevel = player_local_property.mFortitudeLevel + 1
        player_local_property.mPropertyPoint = player_local_property.mPropertyPoint - 1
    elseif uiName == "头部按钮" then
        if event.button == "right" and player_local_property.mEquip.mHead then
            player:unequip("Head")
        end
    elseif uiName == "躯干按钮" then
        if event.button == "right" and player_local_property.mEquip.mBody then
            player:unequip("Body")
        end
    elseif uiName == "腿部按钮" then
        if event.button == "right" and player_local_property.mEquip.mLeg then
            player:unequip("Leg")
        end
    elseif uiName == "手部按钮" then
        if event.button == "right" and player_local_property.mEquip.mHand then
            player:unequip("Hand")
        end
    elseif uiName == "脚部按钮" then
        if event.button == "right" and player_local_property.mEquip.mFoot then
            player:unequip("Foot")
        end
    elseif uiName == "副手按钮" then
        if event.button == "right" and player_local_property.mEquip.mSecondHand then
            player:unequip("SecondHand")
        end
    else
        echo("devilwalk", uiName)
    end
end

function Bag:onClickItem(itemIndex, event)
    self.mSelectItemIndex = itemIndex
    if self.mSelectItemIndex then
        if event.button == "right" then
            if self.mLeftMode == "物品栏" then
                Game.singleton():getPlayerManager():getPlayerByID():useItem(self.mBagItems[self.mSelectItemIndex].mSlot)
            elseif self.mLeftMode == "制作栏" then
                Game.singleton():getPlayerManager():getPlayerByID():useBlueprint(self.mBagItems[self.mSelectItemIndex].mSlot)
            end
        elseif event.button == "left" then
            if self.mPopWindow then
                InitMiniGameUISystem().destroyWindow(self.mPopWindow)
                self.mPopWindow = nil
            end
            if self.mLeftMode == "物品栏" then
                local bag_item = self.mParameter.mPlayer.mBag.mItems[self.mBagItems[self.mSelectItemIndex].mSlot]
                local item_cfg = Config.Item[bag_item.mConfigIndex]
                self.mPopWindow = InitMiniGameUISystem().createWindow("BagPop", "_lt", event.x + 3, event.y + 3, 90, 60)
                self.mPopWindow:setZOrder(1)
                local edit_delete = self.mPopWindow:createUI("Edit", "BagPop/Edit/Delete", "_lt", 60, 0, 30, 30)
                edit_delete:setText("0")
                local button_delete = self.mPopWindow:createUI("Button", "BagPop/Button/Delete", "_lt", 0, 0, 60, 30)
                button_delete:setText("删除")
                button_delete:setFontSize(20)
                button_delete:addEventFunction("onclick", function()
                    local count = tonumber(edit_delete:getText())
                    if count and count > 0 then
                        Game.singleton():getPlayerManager():getPlayerByID():removeBagItem(self.mBagItems[self.mSelectItemIndex].mSlot, count)
                    end
                    InitMiniGameUISystem().destroyWindow(self.mPopWindow)
                    self.mPopWindow = nil
                end)
                if item_cfg.mUse then
                    local edit_use = self.mPopWindow:createUI("Edit", "BagPop/Edit/Use", "_lt", 60, 30, 30, 30)
                    edit_use:setText("1")
                    local button_use = self.mPopWindow:createUI("Button", "BagPop/Button/Use", "_lt", 0, 30, 60, 30)
                    button_use:setText("使用")
                    button_use:setFontSize(20)
                    button_use:addEventFunction("onclick", function()
                        local count = tonumber(edit_use:getText())
                        if count and count > 0 then
                            Game.singleton():getPlayerManager():getPlayerByID():useItem(self.mBagItems[self.mSelectItemIndex].mSlot, count)
                        end
                        InitMiniGameUISystem().destroyWindow(self.mPopWindow)
                        self.mPopWindow = nil
                    end)
                end
            end
        end
    end
end

function Bag:onDragBegin(itemIndex, event)
    local item = self.mParameter.mPlayer.mBag.mItems[self.mBagItems[itemIndex].mSlot]
    if item then
        local item_cfg = Config.Item[item.mConfigIndex]
        local icon_cfg = item_cfg.mIcon["m" .. self.mParameter.mPlayer.mSex] or item_cfg.mIcon
        if icon_cfg.mFile then
            self.mDragUI = CreateUI({type = "container", x = event.x - 45, y = event.y - 93, width = 90, height = 90, background = icon_cfg.mFile, align = "_lt", zorder = 100})
        elseif icon_cfg.mResource then
            GetResourceImage(icon_cfg.mResource, function(path, err)
                self.mDragUI = CreateUI({type = "container", x = event.x - 45, y = event.y - 93, width = 90, height = 90, background = path, align = "_lt", zorder = 100})
            end)
        end
        if item_cfg.mUse
            or Utility.arrayContain(item_cfg.mTypes, "建筑")
            or Utility.arrayContain(item_cfg.mTypes, "武器")
        then
            Game.singleton():getUIManager():show("InGame", 1, {mHighLightHand = true})
        end
        Game.singleton():getUIManager():close("ItemInfo")
        if self.mPopWindow then
            InitMiniGameUISystem().destroyWindow(self.mPopWindow)
            self.mPopWindow = nil
        end
    end
end

function Bag:onDragEnd(itemIndex, event)
    if self.mDragUI then
        self.mDragUI:destroy()
        self.mDragUI = nil
    end
    Game.singleton():getUIManager():show("InGame")
    local item = self.mParameter.mPlayer.mBag.mItems[self.mBagItems[itemIndex].mSlot]
    if not item then
        return
    end
    local item_cfg = Config.Item[item.mConfigIndex]
    if item_cfg.mUse
        or Utility.arrayContain(item_cfg.mTypes, "建筑")
        or Utility.arrayContain(item_cfg.mTypes, "武器")
    then
        for i = 1, 10 do
            if string.find(event.dragging_dst.name, "InGame/手持栏图标上盖框" .. tostring(i)) then
                Game.singleton():getPlayerManager():getPlayerByID():setHandItem(i, self.mBagItems[itemIndex].mSlot)
            end
        end
    end
end

function Bag:onDragMove(itemIndex, event)
    if self.mDragUI then
        self.mDragUI.x = event.x - 45
        self.mDragUI.y = event.y - 91
    end
end

function Bag:onMouseEnter(itemIndex, event)
    if self.mLeftMode == "物品栏" then
        Game.singleton():getUIManager():show("ItemInfo", 1, {mBagSlot = self.mBagItems[itemIndex].mSlot, mX = event.x + 30, mY = event.y + 30})
    elseif self.mLeftMode == "制作栏" then
        Game.singleton():getUIManager():show("ItemInfo", 1, {mBlueprintSlot = self.mBagItems[itemIndex].mSlot, mX = event.x + 30, mY = event.y + 30})
    end
end
