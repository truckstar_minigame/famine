local Layout = {
    [1] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 255 0",
            ["y"] = 1,
            ["parent"] = "__root",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 1,
            ["type"] = "container",
            ["height"] = 1,
            ["name"] = "ingame",
            ["events"] = {
            },
        },
    },
    [2] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1720,
            ["y"] = 100,
            ["clip"] = true,
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 100,
            ["type"] = "container",
            ["background_res"] = {hash="Fq-lK6px_KgNzBjrYBbUXT23-a_c",pid="177941",ext="png",},
            ["name"] = "技能栏",
            ["height"] = 850,
            ["events"] = {
            },
        },
    },
    [3] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 20,
            ["font_color"] = "255 255 255",
            ["parent"] = "技能栏",
            ["text"] = "技能栏",
            ["zorder"] = 1,
            ["height"] = 50,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["x"] = 0,
            ["name"] = "技能栏标题",
            ["events"] = {
            },
        },
    },
    [4] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 500,
            ["y"] = 6,
            ["clip"] = true,
            ["parent"] = "技能栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 1146,
            ["type"] = "container",
            ["background"] = "",
            ["name"] = "技能栏辅助功能",
            ["height"] = 75,
            ["events"] = {
            },
        },
    },
    [5] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 150,
            ["y"] = 10,
            ["font_color"] = "125 125 125",
            ["parent"] = "技能栏辅助功能",
            ["text"] = "查找",
            ["zorder"] = 1,
            ["height"] = 50,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "editbox",
            ["x"] = 0,
            ["name"] = "查找栏",
            ["events"] = {
            },
        },
    },
    [6] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "技能栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 160,
            ["type"] = "container",
            ["name"] = "预留4",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [7] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留4",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 50,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "预留按钮4",
            ["events"] = {
            },
        },
    },
    [8] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "技能栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 220,
            ["type"] = "container",
            ["name"] = "预留5",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [9] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留5",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮5",
            ["events"] = {
            },
        },
    },
    [10] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "技能栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 280,
            ["type"] = "container",
            ["name"] = "预留6",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [11] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留6",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮6",
            ["events"] = {
            },
        },
    },
    [12] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "技能栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 340,
            ["type"] = "container",
            ["name"] = "预留7",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [13] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留7",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮7",
            ["events"] = {
            },
        },
    },
    [14] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "技能栏辅助功能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 400,
            ["type"] = "container",
            ["name"] = "预留8",
            ["height"] = 50,
            ["events"] = {
            },
        },
    },
    [15] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "预留8",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "button",
            ["height"] = 50,
            ["name"] = "预留按钮8",
            ["events"] = {
            },
        },
    },
    [16] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1700,
            ["y"] = 90,
            ["clip"] = true,
            ["parent"] = "技能栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background"] = "",
            ["name"] = "技能区",
            ["height"] = 650,
            ["events"] = {
            },
        },
    },
    [22] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 145,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "技能区",
            ["zorder"] = 1,
            ["x"] = 151,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "container",
            ["name"] = "技能",
            ["height"] = 145,
            ["events"] = {
            },
        },
    },
    -- 技能框每个大小150*150像素，每行排9个，列间隔为0。行与行之间的间隔为10像素
    [23] = {
        ["type"] = "ui",
        ["params"] = {
            ["x"] = 0,
            ["text_format"] = 9,
            ["color"] = "100 100 100",
            ["y"] = 100,
            ["height"] = 50,
            ["parent"] = "技能",
            ["text"] = "text",
            ["zorder"] = 1,
            ["width"] = 145,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["events"] = {
            },
            ["name"] = "技能名称",
            ["font_color"] = "25 25 25",
        },
    },
    [24] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 145,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "技能",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "技能图片",
            ["height"] = 145,
            ["events"] = {
            },
        },
    },
    -- 技能序号框每个大小150*150像素，每行排9个，列间隔为0。行与行之间的间隔为10像素
    [20] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 150,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "技能区",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background"] = "",
            ["name"] = "技能序号框",
            ["height"] = 150,
            ["events"] = {
            },
        },
    },
    [21] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 150,
            ["font_color"] = "255 255 255",
            ["y"] = 0,
            ["height"] = 150,
            ["parent"] = "技能序号框",
            ["text"] = "1",
            ["zorder"] = 1,
            ["text_format"] = 5,
            ["background"] = "",
            ["events"] = {
            },
            ["font_size"] = 40,
            ["type"] = "text",
            ["align"] = "_lt",
            ["name"] = "技能序号",
            ["x"] = 0,
        },
    },
    [17] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1000,
            ["y"] = 782,
            ["clip"] = true,
            ["color"] = "0 0 0",
            ["parent"] = "技能栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 8,
            ["type"] = "container",
            ["name"] = "可用点数",
            ["height"] = 55,
            ["events"] = {
            },
        },
    },
    [18] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1000,
            ["y"] = 10,
            ["parent"] = "可用点数",
            ["text"] = "可用点数: 0",
            ["zorder"] = 1,
            ["x"] = 10,
            ["events"] = {
            },
            ["font_size"] = 25,
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 35,
            ["name"] = "可用点数值",
            ["align"] = "_lt",
        },
    },
    [19] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["y"] = 18,
            ["x"] = 1659,
            ["parent"] = "技能栏",
            ["text"] = "",
            ["zorder"] = 1,
            ["height"] = 50,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "button",
            ["background_res"] = {hash="FlVILgIA0YaGsX8GOKyTqFp3YZc8",pid="177942",ext="png",},
            ["font_color"] = "255 255 255",
            ["name"] = "close",
            ["events"] = {
            },
        },
    },
}

local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Ability = inherit(Layout, Devil.getTable("Game.Famine.UI.Ability"))

local function _getPropertyColor(scalar, invert)
    if invert then
        if scalar < 0.2 then
            return "0 " .. tostring(math.floor(255 - scalar * 255)) .. " 0"
        elseif scalar < 0.5 then
            return tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " " .. tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor(255 - (scalar + 0.5) * 255)) .. " 0 0"
        end
    else
        if scalar > 0.8 then
            return "0 " .. tostring(math.floor(scalar * 255)) .. " 0"
        elseif scalar > 0.5 then
            return tostring(math.floor((scalar + 0.2) * 255)) .. " " .. tostring(math.floor((scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor((scalar + 0.5) * 255)) .. " 0 0"
        end
    end
end

function Ability:construction(parameter)
    self.mUIs = {}
    for _, cfg in ipairs(Layout) do
        local cfg_cpy = clone(cfg)
        cfg_cpy.params.name = "Ability/" .. cfg_cpy.params.name .. tostring(self)
        cfg_cpy.params.parent = self.mUIs[cfg.params.parent]
        cfg_cpy.params.align = cfg.params.align or "_lt"
        if cfg.params.type == "button" then
            cfg_cpy.params.onclick = function(p)
                self:onClick(cfg.params.name, p)
            end
        end
        local ui = CreateUI(cfg_cpy.params)
        self.mUIs[cfg.params.name] = ui
        if cfg.params.background_res then
            GetResourceImage(
                cfg.params.background_res,
                function(path, err)
                    ui.background = path
                end
        )
        end
    end
    
    self.mUIs["技能序号框"].visible = false
    self.mUIs["技能"].visible = false
    
    self.mLineItemGridView = CreateUI(
        {
            ext = "TABLE",
            type = "container",
            x = Layout[20].params.x,
            y = Layout[20].params.y,
            width = Layout[16].params.width,
            height = Layout[16].params.height,
            stride = 1,
            item_width = Layout[16].params.width,
            item_height = Layout[20].params.height,
            margin = 10,
            align = "_lt",
            parent = self.mUIs[Layout[16].params.name]
        }
    )
    self.mBlueprintsUIs = {}
    
    Game.singleton():getManipulationManager():pushMode("Base")
end

function Ability:destruction()
    Game.singleton():getManipulationManager():popMode()
    if self.mDragUI then
        self.mDragUI:destroy()
        self.mDragUI = nil
    end
    for _, ui in pairs(self.mUIs) do
        ui:destroy()
    end
    self.mUIs = nil
    self.mLineItemGridView = nil
    self.mBlueprintsUIs = nil
    Game.singleton():getUIManager():close("ItemInfo")
end

function Ability:refresh(parameter)
    for i = 1, parameter.mPlayer.mLevel do
        local level = i
        local blueprint_cfg_indices = Utility.arrayIndex(Config.Blueprint, function(e) return e.mLevel == level end, true)
        if blueprint_cfg_indices and not self.mBlueprintsUIs[blueprint_cfg_indices[1]] then
            self.mLineItemGridView:addItem(function(parent)
                local container_ui
                for j = 20, 21 do
                    local cfg = Layout[j]
                    local cfg_cpy = clone(cfg)
                    cfg_cpy.params.name = "Ability/" .. cfg.params.name .. "/" .. tostring(level) .. "/" .. tostring(self)
                    if cfg.params.parent == Layout[16].params.name then
                        cfg_cpy.params.parent = parent
                    else
                        cfg_cpy.params.parent = container_ui
                    end
                    cfg_cpy.params.align = cfg.params.align or "_lt"
                    local ui = CreateUI(cfg_cpy.params)
                    if cfg.params.background_res then
                        GetResourceImage(
                            cfg.params.background_res,
                            function(path, err)
                                ui.background = path
                            end
                    )
                    end
                    
                    if cfg.params.parent == Layout[16].params.name then
                        container_ui = ui
                    end
                    
                    if j == 21 then
                        ui.text = tostring(level)
                    end
                end
                
                for k, index in ipairs(blueprint_cfg_indices) do
                    local blueprint = Config.Blueprint[index]
                    local item_cfg = Utility.arrayGet(Config.Item, function(e) return e.mName == blueprint.mGenerates.mItems[1].mName end)
                    container_ui = nil
                    for j = 22, 24 do
                        local cfg = Layout[j]
                        local cfg_cpy = clone(cfg)
                        cfg_cpy.params.name = "Ability/" .. cfg_cpy.params.name .. "/" .. tostring(level) .. "/" .. tostring(k) .. tostring(self)
                        if cfg_cpy.params.parent == Layout[16].params.name then
                            cfg_cpy.params.parent = parent
                            cfg_cpy.params.x = cfg_cpy.params.x + (k - 1) * 150
                        else
                            cfg_cpy.params.parent = container_ui
                        end
                        cfg_cpy.params.align = cfg.params.align or "_lt"
                        local ui = CreateUI(cfg_cpy.params)
                        if cfg.params.background_res then
                            GetResourceImage(
                                cfg.params.background_res,
                                function(path, err)
                                    ui.background = path
                                end
                        )
                        end
                        
                        if cfg_cpy.params.parent == parent then
                            container_ui = ui
                        end
                        
                        if j == 23 then
                            ui.font_size = 18
                            ui.font_color = "255 0 0"
                            ui.text = blueprint.mGenerates.mItems[1].mName .. "\n需求点数:" .. tostring(blueprint.mPoint)
                        elseif j == 24 then
                            if item_cfg.mIcon then
                                local icon_cfg = item_cfg.mIcon["m" .. Game.singleton():getPlayerManager():getPlayerByID():getProperty():cache().mSex] or item_cfg.mIcon
                                if icon_cfg.mFile then
                                    ui.background = icon_cfg.mFile
                                elseif icon_cfg.mResource then
                                    GetResourceImage(icon_cfg.mResource, function(path, err)
                                        ui.background = path
                                    end)
                                end
                            end
                        end
                        
                        self.mBlueprintsUIs[index] = self.mBlueprintsUIs[index] or {}
                        self.mBlueprintsUIs[index][cfg.params.name] = ui
                    end
                    local j = 1, 1 do
                        local cfg = Layout[24]
                        local cfg_cpy = clone(cfg)
                        cfg_cpy.params.x = 0
                        cfg_cpy.params.y = 0
                        cfg_cpy.params.name = "Ability/Studied/" .. tostring(level) .. "/" .. tostring(k) .. tostring(self)
                        cfg_cpy.params.parent = self.mBlueprintsUIs[index][Layout[24].params.name]
                        cfg_cpy.params.align = cfg.params.align or "_lt"
                        local ui = CreateUI(cfg_cpy.params)
                        self.mBlueprintsUIs[index]["Studied"] = ui
                        local icon_cfg = Utility.arrayGet(Config.Common, function(e) return e.mName == "未学会" end)
                        if icon_cfg then
                            icon_cfg = icon_cfg.mIcon
                            if icon_cfg.mFile then
                                ui.background = icon_cfg.mFile
                            elseif icon_cfg.mResource then
                                GetResourceImage(icon_cfg.mResource, function(path, err)
                                    ui.background = path
                                end)
                            end
                        end
                    end
                    local j = 1, 1 do
                        local cfg = Layout[24]
                        local cfg_cpy = clone(cfg)
                        cfg_cpy.params.x = 0
                        cfg_cpy.params.y = 0
                        cfg_cpy.params.name = "Ability/Button/" .. tostring(level) .. "/" .. tostring(k) .. tostring(self)
                        cfg_cpy.params.parent = self.mBlueprintsUIs[index]["Studied"]
                        cfg_cpy.params.align = cfg.params.align or "_lt"
                        cfg_cpy.params.type = "button"
                        cfg_cpy.params.background = ""
                        cfg_cpy.params.onclick = function(p)
                            self:onClickItem(index, p)
                        end
                        local ui = CreateUI(cfg_cpy.params)
                        self.mBlueprintsUIs[index]["Button"] = ui
                    end
                end
            end)
        end
    end
    local icon_cfg = Utility.arrayGet(Config.Common, function(e) return e.mName == "已学会" end)
    if icon_cfg then
        icon_cfg = icon_cfg.mIcon
        for _, blueprint in pairs(parameter.mPlayer.mBlueprints) do         
            if icon_cfg.mFile then
                self.mBlueprintsUIs[blueprint.mConfigIndex]["Studied"].background = icon_cfg.mFile
            elseif icon_cfg.mResource then
                GetResourceImage(icon_cfg.mResource, function(path, err)
                    if self.mBlueprintsUIs and self.mBlueprintsUIs[blueprint.mConfigIndex] and self.mBlueprintsUIs[blueprint.mConfigIndex]["Studied"] then
                        self.mBlueprintsUIs[blueprint.mConfigIndex]["Studied"].background = path
                    end
                end)
            end
            self.mBlueprintsUIs[blueprint.mConfigIndex][Layout[23].params.name].text = Config.Blueprint[blueprint.mConfigIndex].mGenerates.mItems[1].mName
            self.mBlueprintsUIs[blueprint.mConfigIndex][Layout[23].params.name].font_size = 30
            self.mBlueprintsUIs[blueprint.mConfigIndex][Layout[23].params.name].font_color = "0 255 0"
        end
    end
    self.mUIs["可用点数值"].text = "可用点数：" .. tostring(parameter.mPlayer.mBlueprintPoint) .. "(鼠标右键点击技能图标进行学习)"
end

function Ability:onClick(uiName)
    if uiName == "close" then
        Game.singleton():getUIManager():close("Ability")
    end
end

function Ability:onClickItem(blueprintConfigIndex, event)
    if event.button == "right" then
        Game.singleton():getPlayerManager():getPlayerByID():studyBlueprint(blueprintConfigIndex)
    end
end
