local Layout = {
    [1] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1,
            ["color"] = "255 255 0",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "__root",
            ["zorder"] = 11,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["height"] = 1,
            ["name"] = "ingame",
            ["events"] = {}
        }
    },
    [2] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 630,
            ["color"] = "255 25 255",
            ["y"] = 990,
            ["clip"] = true,
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["background_res"] = {hash = "FgCGQ9pm0kY1aSdeuQeXsrKQVSsl", pid = "161997", ext = "png"},
            ["name"] = "顶部菜单栏",
            ["height"] = 80,
            ["events"] = {}
        }
    },
    [3] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "物品栏\13",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [4] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["x"] = 0,
            ["y"] = 0,
            ["background"] = "Texture/Aries/Creator/Theme/GameCommonIcon_32bits.png;390 179 24 10",
            ["parent"] = "物品栏\13",
            ["text"] = "物品",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "物品栏按钮",
            ["align"] = "_lt"
        }
    },
    [5] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 75,
            ["type"] = "container",
            ["name"] = "技能栏",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [6] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "技能栏",
            ["text"] = "技能",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "技能栏按钮",
            ["align"] = "_lt"
        }
    },
    [7] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 150,
            ["type"] = "container",
            ["name"] = "预留1",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [8] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留1",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "预留1按钮",
            ["align"] = "_lt"
        }
    },
    [9] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 225,
            ["type"] = "container",
            ["name"] = "预留2",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [10] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留2",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "预留2按钮",
            ["align"] = "_lt"
        }
    },
    [11] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 300,
            ["type"] = "container",
            ["name"] = "预留3",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [12] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留3",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "预留3按钮",
            ["align"] = "_lt"
        }
    },
    [13] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 375,
            ["type"] = "container",
            ["name"] = "预留4",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [14] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留4",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "预留4按钮",
            ["align"] = "_lt"
        }
    },
    [15] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 450,
            ["type"] = "container",
            ["name"] = "预留5",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [16] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留5",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "预留5按钮",
            ["align"] = "_lt"
        }
    },
    [17] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "顶部菜单栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 525,
            ["type"] = "container",
            ["name"] = "预留6",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [18] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 75,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "预留6",
            ["text"] = "预留",
            ["zorder"] = 1,
            ["height"] = 75,
            ["events"] = {},
            ["font_size"] = 25,
            ["type"] = "button",
            ["font_color"] = "255 255 255",
            ["name"] = "预留6按钮",
            ["align"] = "_lt"
        }
    },
    [19] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 100,
            ["y"] = 10,
            ["clip"] = true,
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "左侧边栏",
            ["height"] = 860,
            ["events"] = {},
            ["background"] = ""
        }
    },
    [20] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 10,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标1",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [21] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标1",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标1数值",
            ["x"] = 0
        }
    },
    [22] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 100,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标2",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [23] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标2",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标2数值",
            ["x"] = 0
        }
    },
    [24] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 190,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标3",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [25] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标3",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标3数值",
            ["x"] = 0
        }
    },
    [26] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 280,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标4",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [27] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标4",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标4数值",
            ["x"] = 0
        }
    },
    [28] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 370,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标5",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [29] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标5",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标5数值",
            ["x"] = 0
        }
    },
    [30] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 460,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标6",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [31] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标6",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标6数值",
            ["x"] = 0
        }
    },
    [32] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 550,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标7",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [33] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标7",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标7数值",
            ["x"] = 0
        }
    },
    [34] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 640,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标8",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [35] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标8",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标8数值",
            ["x"] = 0
        }
    },
    [36] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 730,
            ["clip"] = true,
            ["parent"] = "左侧边栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 10,
            ["type"] = "container",
            ["name"] = "侧边图标9",
            ["height"] = 90,
            ["color"] = "0 0 0",
            ["events"] = {}
        }
    },
    [37] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["color"] = "",
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "侧边图标9",
            ["text_format"] = 5,
            ["events"] = {},
            ["font_size"] = 18,
            ["clip"] = true,
            ["text"] = "100%",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 90,
            ["name"] = "侧边图标9数值",
            ["x"] = 0
        }
    },
    [38] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1550,
            -- ["color"] = "255 0 0",
            ["y"] = 990,
            ["clip"] = true,
            ["zorder"] = 13,
            ["align"] = "_lt",
            ["x"] = 630,
            ["type"] = "container",
            ["background"] = "",
            ["background_res"] = {hash = "FoqpuqA8wtJJBgE5xCyiZLRG0VK-", pid = "177947", ext = "png"},
            ["name"] = "手持栏",
            ["height"] = 90,
            ["events"] = {}
        }
    },
    [39] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 15,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏1",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [40] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框1",
            ["text"] = "手持1",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏1按钮",
            ["align"] = "_lt"
        }
    },
    [41] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 75,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏2",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [42] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框2",
            ["text"] = "手持2",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏2按钮",
            ["align"] = "_lt"
        }
    },
    [43] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 150,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏3",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [44] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框3",
            ["text"] = "手持3",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏3按钮",
            ["align"] = "_lt"
        }
    },
    [45] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 225,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏4",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [46] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框4",
            ["text"] = "手持4",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏4按钮",
            ["align"] = "_lt"
        }
    },
    [47] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 300,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏5",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [48] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框5",
            ["text"] = "手持5",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏5按钮",
            ["align"] = "_lt"
        }
    },
    [49] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 375,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏6",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [50] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框6",
            ["text"] = "手持6",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏6按钮",
            ["align"] = "_lt"
        }
    },
    [51] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 450,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏7",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [52] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框7",
            ["text"] = "手持7",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏7按钮",
            ["align"] = "_lt"
        }
    },
    [53] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 525,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏8",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [54] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框8",
            ["text"] = "手持8",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏8按钮",
            ["align"] = "_lt"
        }
    },
    [55] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 600,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏9",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [56] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框9",
            ["text"] = "手持9",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏9按钮",
            ["align"] = "_lt"
        }
    },
    [57] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 675,
            ["type"] = "container",
            ["background_res"] = {hash = "FtDZhs6lOWzm1VQDLwMqjNF8Zpp9", pid = "180090", ext = "png", },
            ["name"] = "手持栏10",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [58] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["x"] = 0,
            ["parent"] = "手持栏图标上盖框10",
            ["text"] = "手持10",
            ["zorder"] = 18,
            ["height"] = 70,
            ["events"] = {},
            ["font_size"] = 18,
            ["type"] = "button",
            ["background"] = "",
            ["font_color"] = "255 255 255",
            ["name"] = "手持栏10按钮",
            ["align"] = "_lt"
        }
    },
    [59] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 300,
            ["y"] = 0,
            ["clip"] = true,
            ["text"] = "",
            ["zorder"] = 11,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "container",
            ["background_res"] = {hash = "Fkd9pmK-4c0Exc1vgm9SKYhkxONu", pid = "161648", ext = "png"},
            ["x"] = 0,
            ["name"] = "天气",
            ["height"] = 200
        }
    },
    [60] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 300,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "天气",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "时间",
            ["height"] = 50
        }
    },
    [61] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 5,
            ["parent"] = "时间",
            ["text"] = "08:08",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["x"] = 200,
            ["height"] = 35,
            ["name"] = "时间值"
        }
    },
    [62] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 5,
            ["parent"] = "时间",
            ["text"] = "天数:9999天",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["x"] = 10,
            ["height"] = 35,
            ["name"] = "经过天数"
        }
    },
    [63] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 120,
            ["y"] = 60,
            ["clip"] = true,
            ["parent"] = "天气",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FtibWrCnsa1RlET9G4w_1gaeZ89d", pid = "161646", ext = "png"},
            ["background_rain_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["background_snow_res"] = {hash = "FmESz8XKXcIBiVO-v_MXLmEQfo1q", pid = "161647", ext = "png"},
            ["x"] = 160,
            ["name"] = "天气图标",
            ["height"] = 120
        }
    },
    [64] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 120,
            ["y"] = 0,
            ["parent"] = "天气图标",
            ["text"] = "晴",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 40,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["text_format"] = 9,
            ["height"] = 120,
            ["name"] = "天气说明文字"
        }
    },
    [65] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 50,
            ["parent"] = "天气",
            ["text"] = "85 ℃",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 50,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["x"] = 10,
            ["height"] = 66,
            ["name"] = "气温"
        }
    },
    [66] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 110,
            ["parent"] = "天气",
            ["text"] = "水下",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["x"] = 10,
            ["height"] = 22,
            ["name"] = "位置"
        }
    },
    [67] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 130,
            ["parent"] = "天气",
            ["text"] = "天气预留1",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["x"] = 10,
            ["height"] = 50,
            ["name"] = "天气预留1"
        }
    },
    [68] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 150,
            ["parent"] = "天气",
            ["text"] = "天气预留2",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 20,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["x"] = 10,
            ["height"] = 25,
            ["name"] = "天气预留2"
        }
    },
    [69] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 550,
            ["y"] = 670,
            ["clip"] = true,
            ["zorder"] = 14,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background"] = "",
            -- ["background_res"] = {hash="FoqpuqA8wtJJBgE5xCyiZLRG0VK-",pid="177947",ext="png",},
            ["x"] = 1380,
            ["name"] = "异常状态",
            ["height"] = 400
        }
    },
    [70] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 220,
            ["clip"] = true,
            ["parent"] = "异常状态",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FmizKhpfSg4I_W9l3B4y0lQu97W7", pid = "180033", ext = "png"},
            -- ["background_error1_res"] = {hash="FlTVnHEY-KISMAAEb3ZrSn_B19id",pid="161632",ext="png",},
            -- ["background_error2_res"] = {hash="FlTVnHEY-KISMAAEb3ZrSn_B19id",pid="161632",ext="png",},
            -- ["background_error3_res"] = {hash="FlTVnHEY-KISMAAEb3ZrSn_B19id",pid="161632",ext="png",},
            -- ["background_error4_res"] = {hash="FlTVnHEY-KISMAAEb3ZrSn_B19id",pid="161632",ext="png",},
            -- ["background_error5_res"] = {hash="FlTVnHEY-KISMAAEb3ZrSn_B19id",pid="161632",ext="png",},
            -- ["background_error6_res"] = {hash="FlTVnHEY-KISMAAEb3ZrSn_B19id",pid="161632",ext="png",},
            -- ["background_error7_res"] = {hash="FlTVnHEY-KISMAAEb3ZrSn_B19id",pid="161632",ext="png",},
            -- 要么你照这个格式写？
            ["x"] = 1,
            ["name"] = "异常状态1",
            ["height"] = 175
        }
    },
    [71] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态1",
            ["color"] = "10 10 10",
            ["text"] = "异常状态",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "0 0 0",
            ["text_format"] = 5,
            ["height"] = 175,
            ["name"] = "异常状态1名称"
        }
    },
    [72] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 220,
            ["clip"] = true,
            ["parent"] = "异常状态",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FmizKhpfSg4I_W9l3B4y0lQu97W7", pid = "180033", ext = "png"},
            ["x"] = 188,
            ["name"] = "异常状态2",
            ["height"] = 175
        }
    },
    [73] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["color"] = "10 10 10",
            ["parent"] = "异常状态2",
            ["text"] = "异常状态",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["text_format"] = 5,
            ["height"] = 175,
            ["name"] = "异常状态2名称"
        }
    },
    [74] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 220,
            ["clip"] = true,
            ["parent"] = "异常状态",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FmizKhpfSg4I_W9l3B4y0lQu97W7", pid = "180033", ext = "png"},
            ["x"] = 375,
            ["name"] = "异常状态3",
            ["height"] = 175
        }
    },
    [75] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态3",
            ["text"] = "异常状态",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["text_format"] = 5,
            ["height"] = 175,
            ["name"] = "异常状态3名称"
        }
    },
    [76] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 200,
            ["parent"] = "异常状态",
            ["text"] = "text",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            -- ["background_res"] = {hash="FoqpuqA8wtJJBgE5xCyiZLRG0VK-",pid="177947",ext="png",},
            ["text_format"] = 8,
            ["height"] = 20,
            ["name"] = "异常状态描述1"
        }
    },
    [77] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 200,
            ["parent"] = "异常状态",
            ["text"] = "text",
            ["zorder"] = 1,
            ["x"] = 120,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            -- ["background_res"] = {hash="FoqpuqA8wtJJBgE5xCyiZLRG0VK-",pid="177947",ext="png",},
            ["text_format"] = 8,
            ["height"] = 20,
            ["name"] = "异常状态描述2"
        }
    },
    [78] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 200,
            ["parent"] = "异常状态",
            ["text"] = "text",
            ["zorder"] = 1,
            ["x"] = 240,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            -- ["background_res"] = {hash="FoqpuqA8wtJJBgE5xCyiZLRG0VK-",pid="177947",ext="png",},
            ["text_format"] = 8,
            ["height"] = 20,
            ["name"] = "异常状态描述3"
        }
    },
    [79] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏1",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏1物品数量标",
            ["height"] = 25
        }
    },
    [80] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏1物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏1物品数量标图",
            ["height"] = 22
        }
    },
    [81] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏1物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏1物品数量标值"
        -- 类似你方舟中有些东西没有数量标，只有一个icon，这个81号UI和80号UI不会同时出现
        }
    },
    [82] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏1",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏1物品重量"
        }
    },
    [83] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏1",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏1物品血条",
            ["height"] = 5
        }
    },
    [84] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏1物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏1物品血条动态条",
            ["height"] = 6
        }
    },
    [85] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏2",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏2物品数量标",
            ["height"] = 25
        }
    },
    [86] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏2物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏2物品数量标图",
            ["height"] = 22
        }
    },
    [87] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏2物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏2物品数量标值"
        }
    },
    [88] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏2",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏2物品重量"
        }
    },
    [89] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏2",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏2物品血条",
            ["height"] = 5
        }
    },
    [90] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏2物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏2物品血条动态条",
            ["height"] = 6
        }
    },
    [91] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏3",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏3物品数量标",
            ["height"] = 25
        }
    },
    [92] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏3物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏3物品数量标图",
            ["height"] = 22
        }
    },
    [93] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏3物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏3物品数量标值"
        }
    },
    [94] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏3",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏3物品重量"
        }
    },
    [95] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏3",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏3物品血条",
            ["height"] = 5
        }
    },
    [96] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏3物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏3物品血条动态条",
            ["height"] = 6
        }
    },
    [97] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏4",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏4物品数量标",
            ["height"] = 25
        }
    },
    [98] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏4物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏4物品数量标图",
            ["height"] = 22
        }
    },
    [99] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏4物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏4物品数量标值"
        }
    },
    [100] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏4",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏4物品重量"
        }
    },
    [101] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏4",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏4物品血条",
            ["height"] = 5
        }
    },
    [102] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏4物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏4物品血条动态条",
            ["height"] = 6
        }
    },
    [103] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏5",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏5物品数量标",
            ["height"] = 25
        }
    },
    [104] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏5物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏5物品数量标图",
            ["height"] = 22
        }
    },
    [105] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏5物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏5物品数量标值"
        }
    },
    [106] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏5",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏5物品重量"
        }
    },
    [107] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏5",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏5物品血条",
            ["height"] = 5
        }
    },
    [108] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏5物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏5物品血条动态条",
            ["height"] = 6
        }
    },
    [109] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏6",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏6物品数量标",
            ["height"] = 25
        }
    },
    [110] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏6物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏6物品数量标图",
            ["height"] = 22
        }
    },
    [111] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏6物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏6物品数量标值"
        }
    },
    [112] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏6",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏6物品重量"
        }
    },
    [113] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏6",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏6物品血条",
            ["height"] = 5
        }
    },
    [114] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏6物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏6物品血条动态条",
            ["height"] = 6
        }
    },
    [115] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏7",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏7物品数量标",
            ["height"] = 25
        }
    },
    [116] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏7物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏7物品数量标图",
            ["height"] = 22
        }
    },
    [117] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏7物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏7物品数量标值"
        }
    },
    [118] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏7",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏7物品重量"
        }
    },
    [119] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏7",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏7物品血条",
            ["height"] = 5
        }
    },
    [120] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏7物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏7物品血条动态条",
            ["height"] = 6
        }
    },
    [121] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏8",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏8物品数量标",
            ["height"] = 25
        }
    },
    [122] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏8物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏8物品数量标图",
            ["height"] = 22
        }
    },
    [123] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏8物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏8物品数量标值"
        }
    },
    [124] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏8",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏8物品重量"
        }
    },
    [125] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏8",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏8物品血条",
            ["height"] = 5
        }
    },
    [126] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏8物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏8物品血条动态条",
            ["height"] = 6
        }
    },
    [127] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏9",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏9物品数量标",
            ["height"] = 25
        }
    },
    [128] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏9物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏9物品数量标图",
            ["height"] = 22
        }
    },
    [129] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏9物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏9物品数量标值"
        }
    },
    [130] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏9",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏9物品重量"
        }
    },
    [131] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏9",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏9物品血条",
            ["height"] = 5
        }
    },
    [132] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏9物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏9物品血条动态条",
            ["height"] = 6
        }
    },
    [133] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏10",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏10物品数量标",
            ["height"] = 25
        }
    },
    [134] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 2,
            ["clip"] = true,
            ["parent"] = "手持栏10物品数量标",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FidoMko_5tD8vgrTuSVpIVNnjF55", pid = "161641", ext = "png"},
            ["x"] = 2,
            ["name"] = "手持栏10物品数量标图",
            ["height"] = 22
        }
    },
    [135] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 25,
            ["y"] = 0,
            ["parent"] = "手持栏10物品数量标",
            ["text"] = "X999",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["font_color"] = "0 0 255",
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 25,
            ["name"] = "手持栏10物品数量标值"
        }
    },
    [136] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 30,
            ["y"] = 52,
            ["parent"] = "手持栏10",
            ["text"] = "9kg",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 8,
            ["type"] = "text",
            ["x"] = 50,
            ["height"] = 50,
            ["name"] = "手持栏10物品重量"
        }
    },
    [137] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 64,
            ["clip"] = true,
            ["parent"] = "手持栏10",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏10物品血条",
            ["height"] = 5
        }
    },
    [138] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 50,
            ["color"] = "50 100 25",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏10物品血条",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "手持栏10物品血条动态条",
            ["height"] = 6
        }
    },
    [139] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 20,
            ["clip"] = true,
            ["parent"] = "异常状态",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FmizKhpfSg4I_W9l3B4y0lQu97W7", pid = "180033", ext = "png"},
            ["x"] = 1,
            ["name"] = "异常状态4",
            ["height"] = 175
        }
    },
    [140] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态4",
            ["text"] = "异常状态",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["text_format"] = 5,
            ["height"] = 175,
            ["name"] = "异常状态4名称"
        }
    },
    [141] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 20,
            ["clip"] = true,
            ["parent"] = "异常状态",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FmizKhpfSg4I_W9l3B4y0lQu97W7", pid = "180033", ext = "png"},
            ["x"] = 188,
            ["name"] = "异常状态5",
            ["height"] = 175
        }
    },
    [142] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态5",
            ["text"] = "异常状态",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["text_format"] = 5,
            ["height"] = 175,
            ["name"] = "异常状态5名称"
        }
    },
    [143] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 20,
            ["clip"] = true,
            ["parent"] = "异常状态",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash = "FmizKhpfSg4I_W9l3B4y0lQu97W7", pid = "180033", ext = "png"},
            ["x"] = 375,
            ["name"] = "异常状态6",
            ["height"] = 175
        }
    },
    [144] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态6",
            ["text"] = "异常状态",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 30,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["text_format"] = 5,
            ["height"] = 175,
            ["name"] = "异常状态6名称"
        }
    },
    [145] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态",
            ["text"] = "text",
            ["zorder"] = 1,
            ["x"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["type"] = "text",
            -- ["background_res"] = {hash="FoqpuqA8wtJJBgE5xCyiZLRG0VK-",pid="177947",ext="png",},
            ["font_color"] = "255 255 255",
            ["text_format"] = 8,
            ["height"] = 20,
            ["name"] = "异常状态描述4"
        }
    },
    [146] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态",
            ["text"] = "text",
            ["zorder"] = 1,
            ["x"] = 188,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["type"] = "text",
            -- ["background_res"] = {hash="FoqpuqA8wtJJBgE5xCyiZLRG0VK-",pid="177947",ext="png",},
            ["font_color"] = "255 255 255",
            ["text_format"] = 8,
            ["height"] = 20,
            ["name"] = "异常状态描述5"
        }
    },
    [147] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 175,
            ["y"] = 0,
            ["parent"] = "异常状态",
            ["text"] = "text",
            ["zorder"] = 1,
            ["x"] = 375,
            ["align"] = "_lt",
            ["font_size"] = 10,
            ["type"] = "text",
            -- ["background_res"] = {hash="FoqpuqA8wtJJBgE5xCyiZLRG0VK-",pid="177947",ext="png",},
            ["font_color"] = "255 255 255",
            ["text_format"] = 8,
            ["height"] = 20,
            ["name"] = "异常状态描述6"
        }
    },
    [148] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏1",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框1",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [149] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏2",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框2",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [150] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏3",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框3",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [151] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏4",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框4",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [152] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏5",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框5",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [153] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏6",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框6",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [154] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏7",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框7",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [155] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏8",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框8",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [156] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏9",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框9",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [157] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 70,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "手持栏10",
            ["zorder"] = 17,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = {hash = "Fj4Ut6tBW4ODbXkKcvIVxnPm7gwr", pid = "180137", ext = "png", },
            ["background_res_on"] = {hash = "FkVVCBC_wInKwGjAcXEdJMD66alm", pid = "180138", ext = "png", },
            ["name"] = "手持栏图标上盖框10",
            ["height"] = 70,
            ["events"] = {}
        }
    },
    [158] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 800,
            ["color"] = "",
            ["y"] = -15,
            ["background"] = "",
            ["parent"] = "手持栏",
            ["text_format"] = 4,
            ["events"] = {},
            ["font_size"] = 12,
            ["clip"] = true,
            ["text"] = "数字快捷键1   数字快捷键2   数字快捷键3   数字快捷键4  数字快捷键5   数字快捷键6   数字快捷键7   数字快捷键8  数字快捷键9   数字快捷键0",
            ["zorder"] = 20,
            ["align"] = "_lt",
            ["font_color"] = "255 255 255",
            ["type"] = "text",
            ["height"] = 20,
            ["name"] = "手持栏数字键提示",
            ["x"] = 0
        }
    }
}

local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Game = Devil.getTable("Game.Famine.Client_Game")

local InGame = inherit(Layout, Devil.getTable("Game.Famine.UI.InGame"))

local function _getPropertyColor(scalar, invert)
    if invert then
        if scalar < 0.2 then
            return "0 " .. tostring(math.floor(255 - scalar * 255)) .. " 0"
        elseif scalar < 0.5 then
            return tostring(math.floor(255 - (scalar + 0.2) * 255)) ..
                " " .. tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor(255 - (scalar + 0.5) * 255)) .. " 0 0"
        end
    else
        if scalar > 0.8 then
            return "0 " .. tostring(math.floor(scalar * 255)) .. " 0"
        elseif scalar > 0.5 then
            return tostring(math.floor((scalar + 0.2) * 255)) ..
                " " .. tostring(math.floor((scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor((scalar + 0.5) * 255)) .. " 0 0"
        end
    end
end

function InGame:construction(parameter)
    self.mUIs = {}
    for _, cfg in ipairs(Layout) do
        local cfg_cpy = clone(cfg)
        cfg_cpy.params.name = "InGame/" .. cfg_cpy.params.name
        cfg_cpy.params.parent = self.mUIs[cfg.params.parent]
        cfg_cpy.params.align = cfg.params.align or "_lt"
        if cfg.params.type == "button" then
            cfg_cpy.params.onclick = function()
                self:onClick(cfg.params.name)
            end
        end
        local ui = CreateUI(cfg_cpy.params)
        self.mUIs[cfg.params.name] = ui
        if cfg.params.background_res then
            GetResourceImage(
                cfg.params.background_res,
                function(path, err)
                    ui.background = path
                end
        )
        end
    end
    self:refresh(parameter)
end

function InGame:destruction()
    for _, ui in pairs(self.mUIs) do
        ui:destroy()
    end
    self.mUIs = nil
end

function InGame:refresh(parameter)
    local temporature = 20
    if parameter.mTime then
        temporature = GameUtility.calcTemperature(parameter.mTime)
        local day_time = GameUtility.calcTime(parameter.mTime)
        local day = day_time.mDay + 1
        local month = day_time.mMonth + 1
        local year = day_time.mYear
        self.mUIs["经过天数"].text = tostring(year) .. "年" .. tostring(month) .. "月" .. tostring(day) .. "日"
        self.mUIs["时间值"].text =
            tostring(math.floor(parameter.mTime / 60) % 24) .. ":" .. tostring(math.floor(parameter.mTime) % 60)
        self.mUIs["气温"].text = tostring(Utility.processFloat(temporature, 1)) .. "℃"
        self.mUIs["天气说明文字"].text = parameter.mWeather
        if parameter.mWeather == "晴" then
            GetResourceImage(
                Layout[63].params.background_res,
                function(path, err)
                    self.mUIs["天气图标"].background = path
                end
        )
        elseif parameter.mWeather == "雨" then
            GetResourceImage(
                Layout[63].params.background_rain_res,
                function(path, err)
                    self.mUIs["天气图标"].background = path
                end
        )
        elseif parameter.mWeather == "雪" then
            GetResourceImage(
                Layout[63].params.background_snow_res,
                function(path, err)
                    self.mUIs["天气图标"].background = path
                end
        )
        end
        
        self.mUIs["位置"].visible = false
        self.mUIs["天气预留1"].visible = false
        self.mUIs["天气预留2"].visible = false
    end
    if parameter.mPlayer then
        if parameter.mPlayer.mHPLevel and parameter.mPlayer.mHP then
            local scalar = parameter.mPlayer.mHP / GameUtility.calcPlayerHP(parameter.mPlayer.mHPLevel)
            self.mUIs["侧边图标9"].color = _getPropertyColor(scalar)
            self.mUIs["侧边图标9数值"].font_size = 20
            self.mUIs["侧边图标9数值"].text =
                "生命\n" ..
                tostring(math.floor(parameter.mPlayer.mHP)) ..
                "/" .. tostring(GameUtility.calcPlayerHP(parameter.mPlayer.mHPLevel))
        end
        if parameter.mPlayer.mStaminaLevel and parameter.mPlayer.mStamina then
            local scalar = parameter.mPlayer.mStamina / GameUtility.calcPlayerStamina(parameter.mPlayer.mStaminaLevel)
            self.mUIs["侧边图标8"].color = _getPropertyColor(scalar)
            self.mUIs["侧边图标8数值"].font_size = 20
            self.mUIs["侧边图标8数值"].text =
                "耐力\n" ..
                tostring(math.floor(parameter.mPlayer.mStamina)) ..
                "/" .. tostring(GameUtility.calcPlayerStamina(parameter.mPlayer.mStaminaLevel))
        end
        if parameter.mPlayer.mFoodLevel and parameter.mPlayer.mFood then
            local scalar = parameter.mPlayer.mFood / GameUtility.calcPlayerFood(parameter.mPlayer.mFoodLevel)
            self.mUIs["侧边图标7"].color = _getPropertyColor(scalar)
            self.mUIs["侧边图标7数值"].font_size = 20
            self.mUIs["侧边图标7数值"].text =
                "食物\n" ..
                tostring(math.floor(parameter.mPlayer.mFood)) ..
                "/" .. tostring(GameUtility.calcPlayerFood(parameter.mPlayer.mFoodLevel))
        end
        if parameter.mPlayer.mWaterLevel and parameter.mPlayer.mWater then
            local scalar = parameter.mPlayer.mWater / GameUtility.calcPlayerWater(parameter.mPlayer.mWaterLevel)
            self.mUIs["侧边图标6"].color = _getPropertyColor(scalar)
            self.mUIs["侧边图标6数值"].font_size = 20
            self.mUIs["侧边图标6数值"].text =
                "水\n" ..
                tostring(math.floor(parameter.mPlayer.mWater)) ..
                "/" .. tostring(GameUtility.calcPlayerWater(parameter.mPlayer.mWaterLevel))
        end
        if parameter.mPlayer.mWeightLevel and parameter.mPlayer.mWeight then
            local scalar = parameter.mPlayer.mWeight / GameUtility.calcPlayerWeight(parameter.mPlayer.mWeightLevel)
            self.mUIs["侧边图标5"].color = _getPropertyColor(scalar, true)
            self.mUIs["侧边图标5数值"].font_size = 20
            self.mUIs["侧边图标5数值"].text =
                "重量\n" ..
                tostring(math.floor(parameter.mPlayer.mWeight)) ..
                "/" .. tostring(GameUtility.calcPlayerWeight(parameter.mPlayer.mWeightLevel))
        end
        if parameter.mPlayer.mLevel and parameter.mPlayer.mExp then
            local scalar = parameter.mPlayer.mExp / GameUtility.calcPlayerLevelUpExp(parameter.mPlayer.mLevel)
            self.mUIs["侧边图标4"].color = _getPropertyColor(scalar)
            self.mUIs["侧边图标4数值"].font_size = 20
            self.mUIs["侧边图标4数值"].text =
                "等级" ..
                tostring(parameter.mPlayer.mLevel) ..
                "经验\n" ..
                tostring(math.floor(parameter.mPlayer.mExp)) ..
                "/" .. tostring(GameUtility.calcPlayerLevelUpExp(parameter.mPlayer.mLevel))
        end
        self.mUIs["侧边图标3"].visible = false
        self.mUIs["侧边图标2"].visible = false
        self.mUIs["侧边图标1"].visible = false
        local exception_index = 1
        if temporature + parameter.mPlayer.mFortitude.mCold * 0.1 < 0 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "冻僵"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "冻僵"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "生命降低,食物快速降低"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        elseif temporature + parameter.mPlayer.mFortitude.mCold * 0.1 < 20 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "寒冷"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "寒冷"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "食物快速降低"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        elseif temporature - parameter.mPlayer.mFortitude.mHot * 0.1 > 50 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "烧焦"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "烧焦"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "生命降低,水分快速降低"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        elseif temporature - parameter.mPlayer.mFortitude.mHot * 0.1 > 30 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "炎热"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "炎热"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "水分快速降低"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        end
        if parameter.mPlayer.mHP / GameUtility.calcPlayerHP(parameter.mPlayer.mHPLevel) < 0.2 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "骨折"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "骨折"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "移速大幅降低,无法跳跃"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        end
        if parameter.mPlayer.mFood == 0 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "饥饿"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "饥饿"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "生命降低"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        end
        if parameter.mPlayer.mWater == 0 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "口渴"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "口渴"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "晕眩升高"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        end
        if parameter.mPlayer.mWeight / GameUtility.calcPlayerWeight(parameter.mPlayer.mWeightLevel) > 0.8 then
            self.mUIs["异常状态" .. tostring(exception_index)].visible = true
            local cfg =
                Utility.arrayGet(
                    Config.Buff,
                    function(e)
                        return e.mName == "超重"
                    end
            )
            if cfg.mIcon then
                if cfg.mIcon.mFile then
                    self.mUIs["异常状态" .. tostring(exception_index)].background = cfg.mIcon.mFile
                elseif cfg.mIcon.mResource then
                    GetResourceImage(
                        cfg.mIcon.mResource,
                        function(path, err)
                            self.mUIs["异常状态" .. tostring(exception_index)].background = path
                        end
                )
                end
            end
            self.mUIs["异常状态" .. tostring(exception_index) .. "名称"].text = "超重"
            self.mUIs["异常状态描述" .. tostring(exception_index)].visible = true
            self.mUIs["异常状态描述" .. tostring(exception_index)].text = "移速大幅降低,无法跳跃"
            self.mUIs["异常状态描述" .. tostring(exception_index)].font_size = 15
            exception_index = exception_index + 1
        end
        for i = exception_index, 6 do
            self.mUIs["异常状态" .. tostring(i)].visible = false
            self.mUIs["异常状态描述" .. tostring(i)].visible = false
        end
        
        for i = 1, 10 do
            if parameter.mHighLightHand
                or (parameter.mPlayer and parameter.mPlayer.mHandItems and parameter.mPlayer.mEquip and parameter.mPlayer.mHandItems[i] and parameter.mPlayer.mHandItems[i].mBagSlot == parameter.mPlayer.mEquip.mWeapon)
                or (parameter.mPlayer and parameter.mPlayer.mHandItems and parameter.mPlayer.mEquip and parameter.mPlayer.mHandItems[i] and parameter.mPlayer.mHandItems[i].mBagSlot == parameter.mPlayer.mPlaceItem)
            then
                GetResourceImage(
                    Layout[157].params.background_res_on,
                    function(path, err)
                        self.mUIs["手持栏图标上盖框" .. tostring(i)].background = path
                    end
            )
            else
                GetResourceImage(
                    Layout[157].params.background_res,
                    function(path, err)
                        self.mUIs["手持栏图标上盖框" .. tostring(i)].background = path
                    end
            )
            end
            local item = parameter.mPlayer.mHandItems[i]
            if item then
                self.mUIs["手持栏" .. tostring(i) .. "物品血条"].visible = true
                self.mUIs["手持栏" .. tostring(i) .. "物品血条动态条"].visible = true
                self.mUIs["手持栏" .. tostring(i) .. "物品数量标"].visible = true
                self.mUIs["手持栏" .. tostring(i) .. "物品重量"].visible = true
                self.mUIs["手持栏" .. tostring(i) .. "物品数量标图"].visible = false
                self.mUIs["手持栏" .. tostring(i) .. "按钮"].visible = true
                
                local bag_item = parameter.mPlayer.mBag.mItems[item.mBagSlot]
                local item_cfg = Config.Item[bag_item.mConfigIndex]
                if item_cfg.mIcon then
                    local icon = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
                    if icon.mFile then
                        self.mUIs["手持栏" .. tostring(i)].background = icon.mFile
                    elseif icon.mResource then
                        GetResourceImage(
                            icon.mResource,
                            function(path, err)
                                self.mUIs["手持栏" .. tostring(i)].background = path
                            end
                    )
                    end
                end
                if bag_item.mHP then
                    self.mUIs["手持栏" .. tostring(i) .. "物品血条"].visible = true
                    self.mUIs["手持栏" .. tostring(i) .. "物品血条动态条"].visible = true
                    self.mUIs["手持栏" .. tostring(i) .. "物品血条动态条"].width =
                        math.floor(bag_item.mHP / item_cfg.mHP * Layout[108].params.width)
                    self.mUIs["手持栏" .. tostring(i) .. "物品血条动态条"].color = "0 255 0"
                else
                    self.mUIs["手持栏" .. tostring(i) .. "物品血条"].visible = false
                    self.mUIs["手持栏" .. tostring(i) .. "物品血条动态条"].visible = false
                end
                self.mUIs["手持栏" .. tostring(i) .. "物品数量标值"].text = "X" .. tostring(bag_item.mCount)
                self.mUIs["手持栏" .. tostring(i) .. "物品重量"].text = tostring(bag_item.mCount * item_cfg.mWeight) .. "kg"
                self.mUIs["手持栏" .. tostring(i) .. "按钮"].text = item_cfg.mName
            else
                GetResourceImage(
                    Layout[39].params.background_res,
                    function(path, err)
                        self.mUIs["手持栏" .. tostring(i)].background = path
                    end
                )
                self.mUIs["手持栏" .. tostring(i) .. "物品血条"].visible = false
                self.mUIs["手持栏" .. tostring(i) .. "物品血条动态条"].visible = false
                self.mUIs["手持栏" .. tostring(i) .. "物品数量标"].visible = false
                self.mUIs["手持栏" .. tostring(i) .. "物品重量"].visible = false
                self.mUIs["手持栏" .. tostring(i) .. "按钮"].visible = false
                self.mUIs["手持栏" .. tostring(i) .. "物品数量标图"].visible = false
            end
        end
    end
end

function InGame:onClick(uiName)
    if uiName == "物品栏按钮" then
        Game.singleton():getUIManager():show("Bag")
    elseif uiName == "技能栏按钮" then
        Game.singleton():getUIManager():show("Ability")
    elseif string.find(uiName, "手持栏") then
        for i = 1, 10 do
            if uiName == "手持栏" .. tostring(i) .. "按钮" then
                local player = Game.singleton():getPlayerManager():getPlayerByID()
                if player:getLocalProperty().mHandItems[i] then
                    player:useItem(player:getLocalProperty().mHandItems[i].mBagSlot)
                end
                break
            end
        end
    else
        echo("devilwalk", uiName)
    end
end
