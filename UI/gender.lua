local Layout = {
    [1] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 800,
            ["y"] = 243,
            ["parent"] = "遮罩",
            ["text"] = "",
            ["zorder"] = 12,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 537,
            ["name"] = "男女",
            ["height"] = 600,
        },
    },
    [2] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 250,
            ["y"] = 100,
            ["clip"] = true,
            ["parent"] = "男女",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash="Fjw166CwtVPR5eSCxrLpB8wIBY6_",pid="141993",ext="png",},
            ["background_res_on"] = {hash="FnKHGp-szsGF1BVnZDk0Vcz4PEUC",pid="141994",ext="png",},
            ["x"] = 100,
            ["name"] = "男女性别选择",
            ["height"] = 350,
        },
    },
    [3] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 250,
            ["y"] = 300,
            ["parent"] = "男女性别选择",
            ["text"] = "男  性",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 30,
            ["name"] = "男性",
        },
    },
    [4] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 250,
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "男女性别选择",
            ["text"] = "",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["type"] = "button",
            ["x"] = 0,
            ["height"] = 350,
            ["name"] = "选择男性按钮",
        },
    },
    [5] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 250,
            ["y"] = 100,
            ["clip"] = true,
            ["parent"] = "男女",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["background_res"] = {hash="Ft0ulWJy9oRZB2Yd7lv1OSjM44p6",pid="141996",ext="png",},
            ["background_res_on"] = {hash="Fig5CiSERb59XDc4XnJZIUBhUCoK",pid="141997",ext="png",},
            ["x"] = 450,
            ["name"] = "男女性别选择2",
            ["height"] = 350,
        },
    },
    [6] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 250,
            ["y"] = 300,
            ["parent"] = "男女性别选择2",
            ["text"] = "女  性",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 30,
            ["name"] = "女性",
        },
    },
    [7] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 250,
            ["y"] = 0,
            ["background"] = "",
            ["parent"] = "男女性别选择2",
            ["text"] = "",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "button",
            ["x"] = 0,
            ["height"] = 350,
            ["name"] = "选择女性按钮",
        },
    },
    [8] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 800,
            ["y"] = 10,
            ["parent"] = "男女",
            ["text"] = "请选择性别",
            ["zorder"] = 1,
            ["x"] = 0,
            ["align"] = "_lt",
            ["font_size"] = 35,
            ["type"] = "text",
            ["text_format"] = 5,
            ["height"] = 40,
            ["name"] = "标题",
        },
    },
    [9] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 160,
            ["y"] = 500,
            ["parent"] = "男女",
            ["text"] = "确定",
            ["zorder"] = 2,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "button",
            ["x"] = 320,
            ["height"] = 50,
            ["name"] = "确定按钮",
        },
    },
    [10] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 40,
            ["y"] = 20,
            ["parent"] = "男女",
            ["text"] = "button",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "button",
            ["x"] = 740,
            ["height"] = 40,
            ["name"] = "关闭窗口",
        },
    },
    [11] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 1920,
            ["y"] = 0,
            ["parent"] = "root",
            ["text"] = "",
            ["zorder"] = 11,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 0,
            ["name"] = "遮罩",
            ["height"] = 1080,
        },
    },
}
local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Game = Devil.getTable("Game.Famine.Client_Game")

local Gender = inherit(Layout, Devil.getTable("Game.Famine.UI.Gender"))

function Gender:construction(parameter)
    self.mCallback = parameter.mCallback
    self.mUIs = {}
    for _, cfg in ipairs(Layout) do
        local cfg_cpy = clone(cfg)
        cfg_cpy.params.name = "Gender/" .. cfg_cpy.params.name .. tostring(self)
        cfg_cpy.params.parent = self.mUIs[cfg.params.parent]
        cfg_cpy.params.align = cfg.params.align or "_lt"
        if cfg.params.type == "button" then
            cfg_cpy.params.onclick = function()
                self:onClick(cfg.params.name)
            end
        end
        local ui = CreateUI(cfg_cpy.params)
        self.mUIs[cfg.params.name] = ui
        if cfg.params.background_res then
            GetResourceImage(
                cfg.params.background_res,
                function(path, err)
                    ui.background = path
                end
        )
        end
    end
    self.mSex = "Male"
    self:refresh(parameter)
end

function Gender:destruction()
    for _,ui in pairs(self.mUIs) do
        ui:destroy()
    end
    self.mUIs = nil
end

function Gender:refresh(parameter)
    if self.mSex == "Male" then
        GetResourceImage(Layout[2].params.background_res_on,
            function(path,err)
                self.mUIs["男女性别选择"].background = path
            end)
        GetResourceImage(Layout[5].params.background_res,
            function(path,err)
                self.mUIs["男女性别选择2"].background = path
            end)
    elseif self.mSex == "Female" then
        GetResourceImage(Layout[5].params.background_res_on,
            function(path,err)
                self.mUIs["男女性别选择2"].background = path
            end)
        GetResourceImage(Layout[2].params.background_res,
            function(path,err)
                self.mUIs["男女性别选择"].background = path
            end)
    end
end

function Gender:onClick(uiName)
    if uiName == "选择女性按钮" then
        self.mSex = "Female"
    elseif uiName == "选择男性按钮" then
        self.mSex = "Male"
    elseif uiName == "确定按钮" then
        self.mCallback(self.mSex)
        Game.singleton():getUIManager():close("Gender")
    end
end