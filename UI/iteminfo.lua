local Layout = {
    [1] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 800,
            ["y"] = 322,
            ["parent"] = "__root",
            ["zorder"] = 20,
            ["align"] = "_lt",
            ["events"] = {
            },
            ["type"] = "container",
            ["background"] = "",
            ["name"] = "物品详情",
            ["height"] = 400,
            ["x"] = 485,
        },
    },
    [2] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 550,
            ["color"] = "75 75 75",
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "物品详情",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["background_res"] = "",
            ["name"] = "物品详情框",
            ["height"] = 200,
            ["events"] = {
            },
        },
    },
    [3] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 140,
            ["y"] = 10,
            ["x"] = 10,
            ["parent"] = "物品详情框",
            ["text"] = "详情内容1",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "顶部物品详情内容1",
            ["events"] = {
            },
        },
    },
    [4] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 140,
            ["y"] = 0,
            ["x"] = 141,
            ["parent"] = "物品详情框",
            ["text"] = "详情内容2",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "顶部物品详情内容2",
            ["events"] = {
            },
        },
    },
    [5] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 140,
            ["y"] = 0,
            ["x"] = 280,
            ["parent"] = "物品详情框",
            ["text"] = "详情内容3",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "顶部物品详情内容3",
            ["events"] = {
            },
        },
    },
    [6] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 140,
            ["y"] = 0,
            ["x"] = 420,
            ["parent"] = "物品详情框",
            ["text"] = "详情内容4",
            ["zorder"] = 1,
            ["height"] = 30,
            ["align"] = "_lt",
            ["font_size"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "顶部物品详情内容4",
            ["events"] = {
            },
        },
    },
    [7] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 90,
            ["y"] = 52,
            ["clip"] = true,
            ["parent"] = "物品详情框",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 21,
            ["type"] = "container",
            ["name"] = "物品详情图片",
            ["height"] = 90,
            ["events"] = {
            },
        },
    },
    [8] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["y"] = 50,
            ["parent"] = "物品详情框",
            ["text"] = "物品详情介绍",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 120,
            ["type"] = "text",
            ["height"] = 50,
            ["name"] = "物品详情介绍",
            ["events"] = {
            },
        },
    },
    [9] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["color"] = "255 255 75",
            ["y"] = 105,
            ["parent"] = "物品详情框",
            ["text"] = "物品详情介绍辅助信息1",
            ["zorder"] = 1,
            ["font_color"] = "255 255 155",
            ["align"] = "_lt",
            ["height"] = 25,
            ["type"] = "text",
            ["x"] = 120,
            ["name"] = "物品详情介绍辅助信息1",
            ["events"] = {
            },
        },
    },
    [10] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["y"] = 125,
            ["parent"] = "物品详情框",
            ["text"] = "物品详情介绍辅助信息2",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 120,
            ["type"] = "text",
            ["height"] = 25,
            ["name"] = "物品详情介绍辅助信息2",
            ["events"] = {
            },
        },
    },
    [11] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["y"] = 145,
            ["parent"] = "物品详情框",
            ["text"] = "物品详情介绍辅助信息3",
            ["zorder"] = 1,
            ["x"] = 120,
            ["align"] = "_lt",
            ["height"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 155",
            ["name"] = "物品详情介绍辅助信息3",
            ["events"] = {
            },
        },
    },
    [12] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["y"] = 165,
            ["parent"] = "物品详情框",
            ["text"] = "物品详情介绍辅助信息4",
            ["zorder"] = 1,
            ["x"] = 120,
            ["align"] = "_lt",
            ["height"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 0 0",
            ["name"] = "物品详情介绍辅助信息4",
            ["events"] = {
            },
        },
    },
    [13] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 550,
            ["color"] = "75 75 75",
            ["y"] = 200,
            ["clip"] = true,
            ["parent"] = "物品详情",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "物品详情拓展栏1",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [14] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["y"] = 10,
            ["parent"] = "物品详情拓展栏1",
            ["text"] = "物品详情拓展1",
            ["zorder"] = 1,
            ["x"] = 10,
            ["align"] = "_lt",
            ["height"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "物品详情拓展1",
            ["events"] = {
            },
        },
    },
    [15] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 550,
            ["color"] = "75 75 75",
            ["y"] = 235,
            ["clip"] = true,
            ["parent"] = "物品详情",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "物品详情拓展栏2",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [16] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["y"] = 10,
            ["parent"] = "物品详情拓展栏2",
            ["text"] = "物品详情拓展2",
            ["zorder"] = 1,
            ["x"] = 10,
            ["align"] = "_lt",
            ["height"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "物品详情拓展2",
            ["events"] = {
            },
        },
    },
    [17] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 550,
            ["color"] = "75 75 75",
            ["y"] = 270,
            ["clip"] = true,
            ["parent"] = "物品详情",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["x"] = 0,
            ["type"] = "container",
            ["name"] = "物品详情拓展栏3",
            ["height"] = 35,
            ["events"] = {
            },
        },
    },
    [18] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 420,
            ["y"] = 10,
            ["parent"] = "物品详情拓展栏3",
            ["text"] = "物品详情拓展3",
            ["zorder"] = 1,
            ["x"] = 10,
            ["align"] = "_lt",
            ["height"] = 25,
            ["type"] = "text",
            ["font_color"] = "255 255 255",
            ["name"] = "物品详情拓展3",
            ["events"] = {
            },
        },
    },
    [19] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 250,
            ["y"] = 0,
            ["clip"] = true,
            ["parent"] = "物品详情",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 551,
            ["name"] = "需要素材框",
            ["height"] = 305,
        },
    },
    [20] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 0,
            ["parent"] = "需要素材框",
            ["text"] = "所需素材",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 22,
            ["type"] = "text",
            ["x"] = 5,
            ["height"] = 35,
            ["name"] = "所需素材标题栏",
        },
    },
    [21] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 22,
            ["y"] = 36,
            ["clip"] = true,
            ["parent"] = "需要素材框",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["type"] = "container",
            ["x"] = 7,
            ["name"] = "所需素材图标",
            ["height"] = 22,
        },
    },
    -- 每一行需要的素材间隔25像素
    [22] = {
        ["type"] = "ui",
        ["params"] = {
            ["width"] = 200,
            ["y"] = 35,
            ["parent"] = "需要素材框",
            ["text"] = "text",
            ["zorder"] = 1,
            ["align"] = "_lt",
            ["font_size"] = 15,
            ["type"] = "text",
            ["x"] = 35,
            ["height"] = 25,
            ["name"] = "所需素材名称数量",
        },
    },
}
local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local ItemInfo = inherit(Layout, Devil.getTable("Game.Famine.UI.ItemInfo"))

local function _getPropertyColor(scalar, invert)
    if invert then
        if scalar < 0.2 then
            return "0 " .. tostring(math.floor(255 - scalar * 255)) .. " 0"
        elseif scalar < 0.5 then
            return tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " " .. tostring(math.floor(255 - (scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor(255 - (scalar + 0.5) * 255)) .. " 0 0"
        end
    else
        if scalar > 0.8 then
            return "0 " .. tostring(math.floor(scalar * 255)) .. " 0"
        elseif scalar > 0.5 then
            return tostring(math.floor((scalar + 0.2) * 255)) .. " " .. tostring(math.floor((scalar + 0.2) * 255)) .. " 0"
        else
            return tostring(math.floor((scalar + 0.5) * 255)) .. " 0 0"
        end
    end
end

function ItemInfo:construction(parameter)
    self.mUIs = {}
    for _, cfg in ipairs(Layout) do
        local cfg_cpy = clone(cfg)
        cfg_cpy.params.name = "ItemInfo/" .. cfg_cpy.params.name .. tostring(self)
        cfg_cpy.params.parent = self.mUIs[cfg.params.parent]
        cfg_cpy.params.align = cfg.params.align or "_lt"
        if cfg.params.type == "button" then
            cfg_cpy.params.onclick = function()
                self:onClick(cfg.params.name)
            end
        end
        local ui = CreateUI(cfg_cpy.params)
        self.mUIs[cfg.params.name] = ui
        if cfg.params.background_res then
            GetResourceImage(
                cfg.params.background_res,
                function(path, err)
                    ui.background = path
                end
        )
        end
    end
    
    for i = 1, 9 do
        for j = 21, 22 do
            local cfg = Layout[j]
            local cfg_cpy = clone(cfg)
            cfg_cpy.params.name = "ItemInfo/" .. cfg_cpy.params.name .. tostring(i) .. tostring(self)
            cfg_cpy.params.parent = self.mUIs[cfg.params.parent]
            cfg_cpy.params.align = cfg.params.align or "_lt"
            cfg_cpy.params.y = cfg_cpy.params.y + 25 * i
            if cfg.params.type == "button" then
                cfg_cpy.params.onclick = function()
                    self:onClick(cfg.params.name .. tostring(i))
                end
            end
            local ui = CreateUI(cfg_cpy.params)
            self.mUIs[cfg.params.name .. tostring(i)] = ui
            if cfg.params.background_res then
                GetResourceImage(
                    cfg.params.background_res,
                    function(path, err)
                        ui.background = path
                    end
            )
            end
        end
    end
    
    Game.singleton():getManipulationManager():pushMode("Base")
end

function ItemInfo:destruction()
    Game.singleton():getManipulationManager():popMode()
    for _, ui in pairs(self.mUIs) do
        ui:destroy()
    end
    self.mUIs = nil
end

function ItemInfo:refresh(parameter)
    if parameter.mX then
        self.mUIs["物品详情"].x = parameter.mX
    end
    if parameter.mY then
        self.mUIs["物品详情"].y = parameter.mY
    end
    if parameter.mBagSlot then
        self.mUIs["需要素材框"].visible = false
        local item = parameter.mPlayer.mBag.mItems[parameter.mBagSlot]
        if not item then
            return
        end
        local item_cfg = Config.Item[item.mConfigIndex]
        self.mUIs["顶部物品详情内容1"].text = item_cfg.mName
        self.mUIs["顶部物品详情内容2"].visible = false
        self.mUIs["顶部物品详情内容3"].visible = false
        self.mUIs["顶部物品详情内容4"].visible = false
        if item_cfg.mIcon then
            local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
            if icon_cfg.mFile then
                self.mUIs["物品详情图片"].background = icon_cfg.mFile
            elseif icon_cfg.mResource then
                GetResourceImage(icon_cfg.mResource, function(path, err)
                    if self.mUIs then
                        self.mUIs["物品详情图片"].background = path
                    end
                end)
            end
        end
        self.mUIs["物品详情介绍"].text = item_cfg.mDescription or ""
        local text = "重量：" .. item_cfg.mWeight
        if item_cfg.mHP then
            text = text .. "                                    耐久：" .. tostring(Utility.processFloat(item.mHP, 1)) .. "/" .. tostring(item_cfg.mHP)
        end
        self.mUIs["物品详情介绍辅助信息1"].visible = false
        self.mUIs["物品详情介绍辅助信息2"].visible = false
        self.mUIs["物品详情介绍辅助信息3"].visible = false
        self.mUIs["物品详情介绍辅助信息4"].visible = false
        self.mUIs["物品详情拓展1"].text = text
        self.mUIs["物品详情拓展2"].visible = false
        self.mUIs["物品详情拓展3"].visible = false
    elseif parameter.mBlueprintSlot then
        local item = parameter.mPlayer.mBlueprints[parameter.mBlueprintSlot]
        if not item then
            return
        end
        local blueprint_cfg = Config.Blueprint[item.mConfigIndex]
        local item_cfg = Utility.arrayGet(Config.Item, function(e) return e.mName == blueprint_cfg.mGenerates.mItems[1].mName end)
        self.mUIs["顶部物品详情内容1"].text = item_cfg.mName
        self.mUIs["顶部物品详情内容2"].visible = false
        self.mUIs["顶部物品详情内容3"].visible = false
        self.mUIs["顶部物品详情内容4"].visible = false
        if item_cfg.mIcon then
            local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
            if icon_cfg.mFile then
                self.mUIs["物品详情图片"].background = icon_cfg.mFile
            elseif icon_cfg.mResource then
                GetResourceImage(icon_cfg.mResource, function(path, err)
                    if self.mUIs then
                        self.mUIs["物品详情图片"].background = path
                    end
                end)
            end
        end
        self.mUIs["物品详情介绍"].text = item_cfg.mDescription or ""
        local text = "重量：" .. item_cfg.mWeight
        if item_cfg.mHP then
            text = text .. "                                    耐久：" .. tostring(item_cfg.mHP) .. "/" .. tostring(item_cfg.mHP)
        end
        self.mUIs["物品详情介绍辅助信息1"].visible = false
        self.mUIs["物品详情介绍辅助信息2"].visible = false
        self.mUIs["物品详情介绍辅助信息3"].visible = false
        self.mUIs["物品详情介绍辅助信息4"].visible = false
        self.mUIs["物品详情拓展1"].text = text
        self.mUIs["物品详情拓展2"].visible = false
        self.mUIs["物品详情拓展3"].visible = false
        self.mUIs["需要素材框"].visible = true
        for i = 1, 10 do
            local ui_name_1, ui_name_2
            if i == 1 then
                ui_name_1 = "所需素材图标"
                ui_name_2 = "所需素材名称数量"
            else
                ui_name_1 = "所需素材图标" .. tostring(i - 1)
                ui_name_2 = "所需素材名称数量" .. tostring(i - 1)
            end
            local res = blueprint_cfg.mResources[i]
            if res then
                self.mUIs[ui_name_1].visible = true
                self.mUIs[ui_name_2].visible = true
                local item_cfg = Utility.arrayGet(Config.Item, function(e) return e.mName == res.mName end)
                if item_cfg.mIcon then
                    local icon_cfg = item_cfg.mIcon["m" .. parameter.mPlayer.mSex] or item_cfg.mIcon
                    if icon_cfg.mFile then
                        self.mUIs[ui_name_1].background = icon_cfg.mFile
                    elseif icon_cfg.mResource then
                        GetResourceImage(icon_cfg.mResource, function(path, err)
                            if self.mUIs then
                                self.mUIs[ui_name_1].background = path
                            end
                        end)
                    end
                end
                self.mUIs[ui_name_2].text = res.mName .. "：" .. tostring(res.mCount)
                local bag_item = Utility.arrayGet(parameter.mPlayer.mBag.mItems,function(e)return Config.Item[e.mConfigIndex].mName == res.mName end)
                if bag_item and bag_item.mCount >= res.mCount then
                    self.mUIs[ui_name_2].font_color = "0 255 0"
                else
                    self.mUIs[ui_name_2].font_color = "255 0 0"
                end
            else
                self.mUIs[ui_name_1].visible = false
                self.mUIs[ui_name_2].visible = false
            end
        end
    end
end

function ItemInfo:onClick(uiName)
end
