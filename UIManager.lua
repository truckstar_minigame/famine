local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")

local UIManager = Devil.getTable("Game.Famine.UIManager")
function UIManager:construction(parameter)
	self.mUIs = {}
	self.mProperty = {}
end

function UIManager:destruction()
	for _,ui in pairs(self.mUIs) do
		delete(ui.mInstance)
	end
	self.mUIs = nil
	self.mProperty = nil
end

function UIManager:update(deltaTime)
	self:_updateProperty(deltaTime)
	for _,ui in pairs(self.mUIs) do
		ui.mInstance:refresh(self.mProperty[ui.mPropertyName][ui.mKey])
	end
end

function UIManager:show(uiName,key,parameter)
	key = key or 1
	local property_name = "m" .. uiName
	self.mProperty[property_name] = self.mProperty[property_name] or {}
	self.mProperty[property_name][key] = parameter or {}
	local ui = Utility.arrayGet(self.mUIs,function(ui)
			return ui.mPropertyName == property_name and ui.mKey == key
		end)
	if not ui then
		local inst = new(UI[uiName],self.mProperty[property_name][key])
		self.mUIs[#self.mUIs + 1] = {mInstance = inst,mPropertyName = property_name,mKey = key}
	end
end

function UIManager:close(uiName,key)
	key = key or 1
	local property_name = "m" .. uiName
	if self.mProperty[property_name] then
		self.mProperty[property_name][key] = nil
	end
	Utility.arrayRemove(self.mUIs,function(ui)
			return ui.mPropertyName == property_name and ui.mKey == key
		end,
		function(ui)
			delete(ui.mInstance)
		end)
end

function UIManager:get(uiName,key)
	key = key or 1
	local property_name = "m" .. uiName
	return Utility.arrayGet(self.mUIs,function(ui)
			return ui.mPropertyName == property_name and ui.mKey == key
		end).mInstance
end

function UIManager:_updateProperty(deltaTime)
	local property = self.mProperty.mInGame
	if property and property[1] then
		property = property[1]
		property.mTime = Game.singleton():getTime()
		if Game.singleton():getProperty():cache().mWeather == Game.EWeather.Sun then
			property.mWeather = "晴"
		elseif Game.singleton():getProperty():cache().mWeather == Game.EWeather.Rain then
			if property.mTime and GameUtility.calcTemperature(property.mTime) <= 0 then
				property.mWeather = "雪"
			else
				property.mWeather = "雨"
			end
		end
		if Game.singleton():getPlayerManager():getPlayerByID() then
			property.mPlayer = clone(Game.singleton():getPlayerManager():getPlayerByID():getLocalProperty())
			property.mPlayer.mWeight = Game.singleton():getPlayerManager():getPlayerByID():getWeight()
			property.mPlayer.mSex = Game.singleton():getPlayerManager():getPlayerByID():getProperty():cache().mSex
		end    
	end
	local property = self.mProperty.mBag
	if property and property[1] then
		property = property[1]
		property.mTime = Game.singleton():getTime()
		if Game.singleton():getProperty():cache().mWeather == Game.EWeather.Sun then
			property.mWeather = "晴"
		elseif Game.singleton():getProperty():cache().mWeather == Game.EWeather.Rain then
			if property.mTime and GameUtility.calcTemperature(property.mTime) <= 0 then
				property.mWeather = "雪"
			else
				property.mWeather = "雨"
			end
		end
		if Game.singleton():getPlayerManager():getPlayerByID() then
			property.mPlayer = clone(Game.singleton():getPlayerManager():getPlayerByID():getLocalProperty())
			property.mPlayer.mWeight = Game.singleton():getPlayerManager():getPlayerByID():getWeight()
			property.mPlayer.mSex = Game.singleton():getPlayerManager():getPlayerByID():getProperty():cache().mSex
		end    
	end
	local property = self.mProperty.mItemInfo
	if property and property[1] then
		property = property[1]
		if Game.singleton():getPlayerManager():getPlayerByID() then
			property.mPlayer = clone(Game.singleton():getPlayerManager():getPlayerByID():getLocalProperty())
			property.mPlayer.mSex = Game.singleton():getPlayerManager():getPlayerByID():getProperty():cache().mSex
		end
	end
	local property = self.mProperty.mSynthesis
	if property and property[1] then
		property = property[1]
		property.mTime = Game.singleton():getTime()
		if Game.singleton():getProperty():cache().mWeather == Game.EWeather.Sun then
			property.mWeather = "晴"
		elseif Game.singleton():getProperty():cache().mWeather == Game.EWeather.Rain then
			if property.mTime and GameUtility.calcTemperature(property.mTime) <= 0 then
				property.mWeather = "雪"
			else
				property.mWeather = "雨"
			end
		end
		if Game.singleton():getPlayerManager():getPlayerByID() then
			property.mPlayer = clone(Game.singleton():getPlayerManager():getPlayerByID():getLocalProperty())
			property.mPlayer.mWeight = Game.singleton():getPlayerManager():getPlayerByID():getWeight()
			property.mPlayer.mSex = Game.singleton():getPlayerManager():getPlayerByID():getProperty():cache().mSex
		end    
	end
	local property = self.mProperty.mAbility
	if property and property[1] then
		property = property[1]
		property.mTime = Game.singleton():getTime()
		if Game.singleton():getProperty():cache().mWeather == Game.EWeather.Sun then
			property.mWeather = "晴"
		elseif Game.singleton():getProperty():cache().mWeather == Game.EWeather.Rain then
			if property.mTime and GameUtility.calcTemperature(property.mTime) <= 0 then
				property.mWeather = "雪"
			else
				property.mWeather = "雨"
			end
		end
		if Game.singleton():getPlayerManager():getPlayerByID() then
			property.mPlayer = clone(Game.singleton():getPlayerManager():getPlayerByID():getLocalProperty())
			property.mPlayer.mWeight = Game.singleton():getPlayerManager():getPlayerByID():getWeight()
			property.mPlayer.mSex = Game.singleton():getPlayerManager():getPlayerByID():getProperty():cache().mSex
		end    
	end
end