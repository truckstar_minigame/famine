local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local UI = Devil.getTable("Game.Famine.UI")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local SceneManager = Devil.getTable("Game.Famine.Client_GameSceneManager")

local BaseMode = Devil.getTable("Game.Famine.ManipulationManager.Base")
local HomeMode = inherit(BaseMode, Devil.getTable("Game.Famine.ManipulationManager.Home"))
local GameMode = inherit(BaseMode, Devil.getTable("Game.Famine.ManipulationManager.Game"))
local BuildMode = inherit(BaseMode, Devil.getTable("Game.Famine.ManipulationManager.Build"))
local ManipulationManager = Devil.getTable("Game.Famine.ManipulationManager")
--------------------------------------------------------------------------------------------------------------------------
function BaseMode:construction(parameter)
    self.mKeyPresseds = {}
    self.mMousePresseds = {}
end

function BaseMode:destruction(parameter)
    self.mKeyPresseds = nil
    self.mMousePresseds = nil
end

function BaseMode:update(parameter)
end

function BaseMode:processInputEvent(event)
    if event.event_type == "keyPressEvent" then
        if not self.mKeyPresseds[event.keyname] then
            self.mKeyPresseds[event.keyname] = {}
            self:_keyPress(event)
        end
    elseif event.event_type == "keyReleaseEvent" then
        self:_keyRelease(event)
        self.mKeyPresseds[event.keyname] = nil
    elseif event.event_type == "mousePressEvent" then
        self.mMousePresseds[event.mouse_button] = clone(event)
        self.mMousePresseds[event.mouse_button].mTimer = new(Utility.Timer)
        self:_mousePress(event)
    elseif event.event_type == "mouseReleaseEvent" then
        self:_mouseRelease(event)
        if self.mMousePresseds[event.mouse_button] and self.mMousePresseds[event.mouse_button].x == event.x
            and self.mMousePresseds[event.mouse_button].y == event.y
            and self.mMousePresseds[event.mouse_button].mTimer:total() <= 0.5
        then
            self:_mouseClick(event)
        end
        if self.mIsDelete then
            return
        end
        if self.mMousePresseds[event.mouse_button] then
            delete(self.mMousePresseds[event.mouse_button].mTimer)
        end
        self.mMousePresseds[event.mouse_button] = nil
    elseif event.event_type == "mouseMoveEvent" then
        self:_mouseMove(event)
    elseif event.event_type == "mouseWheelEvent" then
        self:_mouseWheel(event)
    end
end

function BaseMode:_keyPress(event)
end

function BaseMode:_keyRelease(event)
end

function BaseMode:_mousePress(event)
end

function BaseMode:_mouseRelease(event)
end

function BaseMode:_mouseMove(event)
end

function BaseMode:_mouseWheel(event)
end

function BaseMode:_mouseClick(event)
end
------------------------------------------------------------------------------------------------------------------------
function BuildMode:construction(parameter)
    self.mTempSelectBoxTrue = {}
    self.mTempSelectBoxFalse = {}
    self.mFacing = 0
    self.mBulletScreen = new(Utility.BulletScreen, {mFontColour = "0 0 255", mX = 0, mY = 0, mTextFormat = 5, mText = "鼠标左键放置\n鼠标滚轮旋转\nESC取消\n绿色框表示可以放置\n红色框表示不能放置"})
end

function BuildMode:destruction(parameter)
    for _, block in pairs(self.mTempSelectBoxTrue) do
        ShowSelectBox(block[1], block[2], block[3], false, 6)
    end
    for _, block in pairs(self.mTempSelectBoxFalse) do
        ShowSelectBox(block[1], block[2], block[3], false, 3)
    end
    self.mTempSelectBoxTrue = nil
    self.mTempSelectBoxFalse = nil
    if self.mTempEntity then
        self.mTempEntity:SetDead(true)
    end
    self.mTempEntity = nil
    self.mItemConfig = nil
    self.mBuildingConfig = nil
    self.mFacing = nil
    delete(self.mBulletScreen)
    self.mBulletScreen = nil
end

function BuildMode:setItem(cfg, callback)
    self.mItemConfig = cfg
    self.mBuildingConfig = Utility.arrayGet(Config.Building,function(e)return e.mName == self.mItemConfig.mName end)
    self.mCallback = callback
    if self.mTempEntity then
        self.mTempEntity:SetDead(true)
    end
    self.mTempEntity = nil
    self.mFacing = 0
end

function BuildMode:_mouseMove(event)
    for _, block in pairs(self.mTempSelectBoxTrue) do
        ShowSelectBox(block[1], block[2], block[3], false, 6)
    end
    self.mTempSelectBoxTrue = {}
    for _, block in pairs(self.mTempSelectBoxFalse) do
        ShowSelectBox(block[1], block[2], block[3], false, 3)
    end
    self.mTempSelectBoxFalse = {}
    if not self.mItemConfig then
        return
    end
    local pick = Pick()
    if pick.blockX and pick.blockY and pick.blockZ
        and GetBlockId(pick.blockX, pick.blockY, pick.blockZ) ~= 2217
    then
        local scene = Game.singleton():getPlayerManager():getPlayerByID():getScene()
        local building_pivot = {pick.blockX, pick.blockY + 1, pick.blockZ}
        if Utility.arrayContain(self.mItemConfig.mTypes, "放置物") then
            local torchData = {
                [5] = {x = 0, y = 1, z = 0, data = 5},
                [4] = {x = 0, y = -1, z = 0, data = 6},
                [3] = {x = 0, y = 0, z = 1, data = 2},
                [2] = {x = 0, y = 0, z = -1, data = 4},
                [1] = {x = 1, y = 0, z = 0, data = 3},
                [0] = {x = -1, y = 0, z = 0, data = 1}
            }
            local data = torchData[pick.side]
            building_pivot = {pick.blockX + data.x, pick.blockY + data.y, pick.blockZ + data.z}
        end
        local building_aabb = GameUtility.calcBuildingBound(building_pivot, self.mBuildingConfig.mBlockSize)
        building_aabb:rotationAroundYAxis(self.mFacing)
        local blocks = building_aabb:blocks()
        if scene and (scene:getType() ~= SceneManager.EScene.PlayerHome or scene == Game.singleton():getSceneManager():getPlayerHome()) and scene:canBuild({mBound = building_aabb, mConfig = self.mBuildingConfig}) then
            for _, block in pairs(blocks) do
                self.mTempSelectBoxTrue[#self.mTempSelectBoxTrue + 1] = block
            end
            if self.mBuildingConfig.mModel then
                if not self.mTempEntity then
                    if self.mBuildingConfig.mModel.mFile then
                        self.mTempEntity = self.mTempEntity or CreateEntity(building_pivot[1], building_pivot[2], building_pivot[3], self.mBuildingConfig.mModel.mFile)
                        if self.mBuildingConfig.mTextures then
                            for material_id, tex in pairs(self.mBuildingConfig.mTextures) do
                                material_id = material_id - 1
                                if tex.mFile then
                                    SetReplaceableTexture(self.mTempEntity, tex.mFile, material_id)
                                elseif tex.mResource then
                                    GetResourceImage(tex.mResource, function(path)
                                        if self.mTempEntity then
                                            SetReplaceableTexture(self.mTempEntity, path, material_id)
                                        end
                                    end)
                                end
                            end
                        end
                    elseif self.mBuildingConfig.mModel.mResource then
                        GetResourceModel(self.mBuildingConfig.mModel.mResource, function(path, err)
                            self.mTempEntity = self.mTempEntity or CreateEntity(building_pivot[1], building_pivot[2], building_pivot[3], path)
                            if self.mBuildingConfig.mTextures then
                                for material_id, tex in pairs(self.mBuildingConfig.mTextures) do
                                    material_id = material_id - 1
                                    if tex.mFile then
                                        SetReplaceableTexture(self.mTempEntity, tex.mFile, material_id)
                                    elseif tex.mResource then
                                        GetResourceImage(tex.mResource, function(path)
                                            if self.mTempEntity then
                                                SetReplaceableTexture(self.mTempEntity, path, material_id)
                                            end
                                        end)
                                    end
                                end
                            end
                        end)
                    end
                end
                if self.mTempEntity then
                    self.mTempEntity:SetBlockPos(building_pivot[1], building_pivot[2], building_pivot[3])
                    self.mTempEntity:SetFacing(self.mFacing)
                    self.mTempEntity:SetScaling(self.mBuildingConfig.mModel.mScaling or 1)
                end
            end
        else
            for _, block in pairs(blocks) do
                self.mTempSelectBoxFalse[#self.mTempSelectBoxFalse + 1] = block
            end
            if self.mTempEntity then
                self.mTempEntity:SetDead(true)
            end
            self.mTempEntity = nil
        end
    end
    for _, block in pairs(self.mTempSelectBoxTrue) do
        ShowSelectBox(block[1], block[2], block[3], true, 6)
    end
    for _, block in pairs(self.mTempSelectBoxFalse) do
        ShowSelectBox(block[1], block[2], block[3], true, 3)
    end
end

function BuildMode:_mousePress(event)
    if event.mouse_button == "left" then
        local pick = Pick()
        if pick.blockX and pick.blockY and pick.blockZ
            and GetBlockId(pick.blockX, pick.blockY, pick.blockZ) ~= 2217
        then
            local scene = Game.singleton():getPlayerManager():getPlayerByID():getScene()
            local building_pivot = {pick.blockX, pick.blockY + 1, pick.blockZ}
            local block_dir = nil
            if Utility.arrayContain(self.mItemConfig.mTypes, "放置物") then
                local torchData = {
                    [5] = {x = 0, y = 1, z = 0, data = 5},
                    [4] = {x = 0, y = -1, z = 0, data = 6},
                    [3] = {x = 0, y = 0, z = 1, data = 2},
                    [2] = {x = 0, y = 0, z = -1, data = 4},
                    [1] = {x = 1, y = 0, z = 0, data = 3},
                    [0] = {x = -1, y = 0, z = 0, data = 1}
                }
                local data = torchData[pick.side]
                building_pivot = {pick.blockX + data.x, pick.blockY + data.y, pick.blockZ + data.z}
                block_dir = data.data
            end
            local building_aabb = GameUtility.calcBuildingBound(building_pivot, self.mBuildingConfig.mBlockSize)
            building_aabb:rotationAroundYAxis(self.mFacing)
            local blocks = building_aabb:blocks()
            if scene and (scene:getType() ~= SceneManager.EScene.PlayerHome or scene == Game.singleton():getSceneManager():getPlayerHome()) and scene:canBuild({mBound = building_aabb, mConfig = self.mBuildingConfig}) then
                for _, block in pairs(blocks) do
                    self.mTempSelectBoxTrue[#self.mTempSelectBoxTrue + 1] = block
                end
                self.mCallback({mPivot = building_pivot, mFacing = self.mFacing, mBlockDirection = block_dir})
            end
        end
    end
end

function BuildMode:_mouseWheel(event)
    if event.mouse_wheel > 0 then
        self.mFacing = self.mFacing + 1.57
    elseif event.mouse_wheel < 0 then
        self.mFacing = self.mFacing - 1.57
    end
    while self.mFacing > 3.14 do
        self.mFacing = self.mFacing - 6.28
    end
    while self.mFacing < -3.14 do
        self.mFacing = self.mFacing + 6.28
    end
end

function BuildMode:_keyPress(event)
    if event.keyname == "DIK_ESCAPE" then
        self.mCallback()
    end
end
------------------------------------------------------------------------------------------------------------------------
function GameMode:construction(parameter)
end

function GameMode:destruction()
    delete(self.mBulletScreen)
    self.mBulletScreen = nil
end

function GameMode:update(deltaTime)
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    if not player then
        return
    end
    local scene = player:getScene()
    self.mBulletScreenTimer = self.mBulletScreenTimer or new(Utility.Timer)
    if self.mBulletScreenTimer:total() > math.random(30, 60) then
        --Framework.singleton():getBulletScreenManager():create({mText = "按住Shift和W奔跑\n注意你的各个状态，不要挨饿\n只有在自己的基地和公共的区域才可以建造\n只有在自己的基地建造的建筑才会被保存", mSpeed = 400, mLocalOnly = true})
        delete(self.mBulletScreenTimer)
        self.mBulletScreenTimer = nil
    end
    if self.mPressedParameters then
        if self.mPressedParameters.mTimer:total() >= 1 then
            if self.mPressedParameters.mBuildings then
                if self.mPopWindow then
                    InitMiniGameUISystem().destroyWindow(self.mPopWindow)
                    self.mPopWindow = nil
                end
                self.mPopWindow = InitMiniGameUISystem().createWindow("GameModePop", "_lt", self.mPressedParameters.mEvent.x + 3, self.mPressedParameters.mEvent.y + 3, 300, 120)
                self.mPopWindow:setZOrder(1)
                local button_delete = self.mPopWindow:createUI("Button", "GameModePop/Button/Delete", "_lt", 0, 0, 300, 60)
                button_delete:setText("删除")
                button_delete:setFontSize(50)
                local buildings = self.mPressedParameters.mBuildings
                button_delete:addEventFunction("onclick", function()
                    for _, building in pairs(buildings) do
                        if not building.mIsDelete then
                            building:getScene():destroyBuilding(building:getID())
                        end
                    end
                    InitMiniGameUISystem().destroyWindow(self.mPopWindow)
                    self.mPopWindow = nil
                end)
                local button_cancel = self.mPopWindow:createUI("Button", "GameModePop/Button/Cancel", "_lt", 0, 60, 300, 60)
                button_cancel:setText("取消")
                button_cancel:setFontSize(50)
                button_cancel:addEventFunction("onclick", function()
                    InitMiniGameUISystem().destroyWindow(self.mPopWindow)
                    self.mPopWindow = nil
                end)
            end
            delete(self.mPressedParameters.mTimer)
            self.mPressedParameters = nil
        end
    end
end

function GameMode:_mouseMove(event)
    delete(self.mBulletScreen)
    self.mBulletScreen = nil
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    if not player then
        return
    end
    local scene = player:getScene()
    if not scene then
        return
    end
    local pick = Pick()
    local intersect_block_pos
    if pick.entity then
        local x, y, z = pick.entity:GetBlockPos()
        intersect_block_pos = {x, y + 1, z}
    elseif pick.blockX and pick.blockY and pick.blockZ then
        intersect_block_pos = {pick.blockX, pick.blockY, pick.blockZ}
    end
    if intersect_block_pos then
        local buildings = scene:intersectBuildingPoint(intersect_block_pos)
        if buildings then
            if not scene:getPlayerID() or scene:getPlayerID() == player:getID() then
                self.mBulletScreen = new(Utility.BulletScreen, {mX = event.x - 300, mY = event.y - 100, mWidth = 600, mHeight = 100, mFontSize = 45, mTextFormat = 5, mFontColour = "0 0 255", mText = "靠近点击鼠标左键进行交互\n长按左键删除"})
            end
        elseif scene:canMine() then
            local resource_cfg
            if pick.entity then
                local monster = Game.singleton():getMonsterManager():getMonsterByEntityID(pick.entity.entityId)
                if monster and monster:getProperty():cache().mHP and monster:getProperty():cache().mHP <= 0 then
                    resource_cfg = Utility.arrayGet(Config.ResourceSource, function(e) return e.mMonsterName == monster:getConfig().mName end)
                end
            else
                local block_id = GetBlockId(pick.blockX, pick.blockY, pick.blockZ)
                resource_cfg = Utility.arrayGet(Config.ResourceSource, function(e) return e.mBlockID == block_id end)
            end
            if resource_cfg then
                local x, y, z = player:getEntity():GetBlockPos()
                local weapon_config_index
                if player:getLocalProperty().mEquip.mWeapon then
                    weapon_config_index = player:getLocalProperty().mBag.mItems[player:getLocalProperty().mEquip.mWeapon].mConfigIndex
                    local weapon_cfg = Config.Item[weapon_config_index]
                    if resource_cfg.mSupportTools and not Utility.arrayGet(resource_cfg.mSupportTools, function(e) return e.mName == weapon_cfg.mName end) and not Utility.arrayIntersect(resource_cfg.mSupportTools, weapon_cfg.mTypes, function(t, e) return e.mType == t end) then
                        self.mBulletScreen = new(Utility.BulletScreen, {mX = event.x - 250, mY = event.y - 50, mWidth = 500, mHeight = 50, mFontSize = 40, mTextFormat = 5, mFontColour = "255 0 0", mText = "当前工具无法采集该资源"})
                        return
                    end
                end
                if not weapon_config_index and resource_cfg.mSupportTools and not Utility.arrayGet(resource_cfg.mSupportTools, function(e) return e.mName == "手" end) then
                    self.mBulletScreen = new(Utility.BulletScreen, {mX = event.x - 200, mY = event.y - 50, mWidth = 400, mHeight = 50, mFontSize = 40, mTextFormat = 5, mFontColour = "255 0 0", mText = "空手无法采集该资源"})
                    return
                end
                if (vector3d:new(x, y, z) - vector3d:new(pick.blockX, pick.blockY, pick.blockZ)):length() > 1.75 then
                    self.mBulletScreen = new(Utility.BulletScreen, {mX = event.x - 150, mY = event.y - 50, mWidth = 300, mHeight = 50, mFontSize = 40, mTextFormat = 5, mFontColour = "255 0 0", mText = "距离太远"})
                end
            end
        end
    end
end

function GameMode:_mousePress(event)
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    if not player then
        return
    end
    local scene = Game.singleton():getPlayerManager():getPlayerByID():getScene()
    if event.mouse_button == "left" then
        local pick = Pick()
        if pick.blockX and pick.blockY and pick.blockZ then
            if player.mLocalProperty.mPlaceItem then
                local torchData = {
                    [5] = {x = 0, y = 1, z = 0, data = 5},
                    [4] = {x = 0, y = -1, z = 0, data = 6},
                    [3] = {x = 0, y = 0, z = 1, data = 2},
                    [2] = {x = 0, y = 0, z = -1, data = 4},
                    [1] = {x = 1, y = 0, z = 0, data = 3},
                    [0] = {x = -1, y = 0, z = 0, data = 1}
                }
                local data = torchData[pick.side]
                local x_new, y_new, z_new = pick.blockX + data.x, pick.blockY + data.y, pick.blockZ + data.z
                player:placeItem(vector3d:new(x_new, y_new, z_new))
            else
                if scene then
                    if scene:getType() ~= SceneManager.EScene.PlayerHome
                        or scene == Game.singleton():getSceneManager():getPlayerHome()
                    then
                        local pick = Pick()
                        local intersect_block_pos
                        if pick.entity then
                            local x, y, z = pick.entity:GetBlockPos()
                            intersect_block_pos = {x, y + 1, z}
                        elseif pick.blockX and pick.blockY and pick.blockZ then
                            intersect_block_pos = {pick.blockX, pick.blockY, pick.blockZ}
                        end
                        if intersect_block_pos then
                            local buildings = scene:intersectBuildingPoint(intersect_block_pos,true)
                            if buildings then
                                self.mPressedParameters = {mBuildings = buildings, mTimer = new(Utility.Timer), mEvent = event}
                            end
                        end
                    end
                    if scene:canMine() then
                        local resource_cfg
                        local mining_parameter = {}
                        if pick.entity then
                            local monster = Game.singleton():getMonsterManager():getMonsterByEntityID(pick.entity.entityId)
                            if monster and monster:getProperty():cache().mHP and monster:getProperty():cache().mHP <= 0 then
                                resource_cfg = Utility.arrayGet(Config.ResourceSource, function(e) return e.mMonsterName == monster:getConfig().mName end)
                                mining_parameter.mMonster = monster
                            end
                        else
                            local block_id = GetBlockId(pick.blockX, pick.blockY, pick.blockZ)
                            resource_cfg = Utility.arrayGet(Config.ResourceSource, function(e) return e.mBlockID == block_id end)
                            mining_parameter.mBlock = {pick.blockX, pick.blockY, pick.blockZ}
                        end
                        if resource_cfg then
                            local x, y, z = player:getEntity():GetBlockPos()
                            if (vector3d:new(x, y, z) - vector3d:new(pick.blockX, pick.blockY, pick.blockZ)):length() > 1.75 then
                                return
                            end
                            local weapon_config_index
                            if player:getLocalProperty().mEquip.mWeapon then
                                weapon_config_index = player:getLocalProperty().mBag.mItems[player:getLocalProperty().mEquip.mWeapon].mConfigIndex
                                local weapon_cfg = Config.Item[weapon_config_index]
                                if resource_cfg.mSupportTools and not Utility.arrayGet(resource_cfg.mSupportTools, function(e) return e.mName == weapon_cfg.mName end) and not Utility.arrayIntersect(resource_cfg.mSupportTools, weapon_cfg.mTypes, function(t, e) return e.mType == t end) then
                                    return
                                end
                            end
                            if not weapon_config_index and resource_cfg.mSupportTools and not Utility.arrayGet(resource_cfg.mSupportTools, function(e) return e.mName == "手" end) then
                                return
                            end
                            if not weapon_config_index then
                                --Framework.singleton():getBulletScreenManager():create({mText = "空手采集资源效率低下，请尽快使用工具采集", mSpeed = 400, mLocalOnly = true})
                            end
                            player:startMining(mining_parameter)
                        else
                            Framework.singleton():getCommandQueueManager():post(new(Command_Callback, {mDebug = "NotResourceUI", mExecutingCallback = function(command)
                                command.mTimer = command.mTimer or new(Utility.Timer)
                                command.mUI = command.mUI or new(Utility.BulletScreen, {mX = event.x - 150, mY = event.y - 50, mWidth = 300, mHeight = 50, mFontSize = 40, mTextFormat = 5, mFontColour = "255 0 0", mText = "这个不是资源"})
                                if command.mTimer:total() > 1 then
                                    delete(command.mTimer)
                                    command.mTimer = nil
                                    delete(command.mUI)
                                    command.mUI = nil
                                    command.mState = Command.EState.Finish
                                end
                            end}))
                        end
                    end
                end
            end
        end
    end
end

function GameMode:_mouseRelease(event)
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    if not player then
        return
    end
    local scene = Game.singleton():getPlayerManager():getPlayerByID():getScene()
    if event.mouse_button == "left" then
        if self.mPressedParameters then
            delete(self.mPressedParameters.mTimer)
            self.mPressedParameters = nil
        end
        if scene and scene:canMine() then
            player:stopMining()
        end
    end
end

function GameMode:_mouseClick(event)
    local scene = Game.singleton():getPlayerManager():getPlayerByID():getScene()
    if not scene then
        return
    end
    if event.mouse_button == "left" then
        local pick = Pick()
        local intersect_block_pos
        if pick.entity then
            local x, y, z = pick.entity:GetBlockPos()
            intersect_block_pos = {x, y + 1, z}
        elseif pick.blockX and pick.blockY and pick.blockZ then
            intersect_block_pos = {pick.blockX, pick.blockY, pick.blockZ}
        end
        if intersect_block_pos then
            local buildings = scene:intersectBuildingPoint(intersect_block_pos,true)
            if buildings then
                for _, building in pairs(buildings) do
                    building:onInteractive()
                end
            end
        end
    end
end

function GameMode:_keyPress(event)
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    if not player then
        return
    end
    for i = 0, 9 do
        if event.keyname == "DIK_" .. tostring(i) then
            if player:getLocalProperty().mHandItems[i] then
                player:useItem(player:getLocalProperty().mHandItems[i].mBagSlot)
            end
            break
        end
    end
    if event.keyname == "DIK_LSHIFT" or event.keyname == "DIK_RSHIFT" then
        player:setMoveState("Run")
    elseif event.keyname == "DIK_G" then
        player:startMeleeAttacking()
    end
end

function GameMode:_keyRelease(event)
    local player = Game.singleton():getPlayerManager():getPlayerByID()
    if not player then
        return
    end
    if event.keyname == "DIK_LSHIFT" or event.keyname == "DIK_RSHIFT" then
        player:setMoveState("Walk")
    elseif event.keyname == "DIK_G" then
        player:stopMeleeAttacking()
    end
end
------------------------------------------------------------------------------------------------------------------------
function ManipulationManager:construction(parameter)
    self.mModeStack = {}
    self.mMode = nil
    Framework.singleton():getInputManager():addListener(self, ManipulationManager._onInputEvent, self)
end

function ManipulationManager:destruction()
    Framework.singleton():getInputManager():removeListener(self)
    self.mModeStack = nil
    delete(self.mMode)
    self.mMode = nil
end

function ManipulationManager:update(deltaTime)
    if self.mMode then
        self.mMode:update(deltaTime)
    end
end

function ManipulationManager:pushMode(mode)
    self.mModeStack[#self.mModeStack + 1] = mode
    delete(self.mMode)
    self.mMode = nil
    self.mMode = self:_createMode(self.mModeStack[#self.mModeStack])
end

function ManipulationManager:popMode()
    table.remove(self.mModeStack)
    delete(self.mMode)
    self.mMode = nil
    self.mMode = self:_createMode(self.mModeStack[#self.mModeStack])
end

function ManipulationManager:getManipulator()
    return self.mMode
end

function ManipulationManager:_onInputEvent(event)
    if self.mMode then
        self.mMode:processInputEvent(event)
    end
end

function ManipulationManager:_createMode(mode)
    if mode then
        return new(ManipulationManager[mode])
    else
        return new(BaseMode)
    end
end
