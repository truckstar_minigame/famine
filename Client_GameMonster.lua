local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Monster = inherit(Devil.Framework.ClientObjectBase, Devil.getTable("Game.Famine.Client_GameMonster"))
function Monster:construction(parameter)
    self.mConfigIndex = parameter.mConfigIndex
    self.mProperty = new(Devil.Game.Famine.GameMonsterProperty, {mID = self.mID})
    
    self.mProperty:addPropertyListener("mEntityHostKey", self, function(_, value)
        self:_updateBloodUI()
        self:_updateNameUI()
    end)
    self.mProperty:addPropertyListener("mHP", self, function(_, value)
        self:_updateBloodUI()
        if value <= 0 then
            self:_updateNameUI()
        end
    end)
    self.mProperty:addPropertyListener("mState", self, function(_, value)
        self:_updateNameUI()
    end)
end

function Monster:destruction()
    self.mProperty:removePropertyListener("mEntityHostKey", self)
    self.mProperty:removePropertyListener("mHP", self)
    self.mProperty:removePropertyListener("mState", self)
    if self.mBloodUI then
        self.mBloodUI:destroy()
    end
    self.mBloodUI = nil
    if self.mNameUI then
        self.mNameUI:destroy()
    end
    self.mNameUI = nil
    delete(self.mProperty)
    self.mProperty = nil
    self.mConfigIndex = nil
end

function Monster:getProperty()
    return self.mProperty
end

function Monster:getID()
    return self.mID
end

function Monster:getConfig()
    return Config.Monster[self.mConfigIndex]
end

function Monster:getEntity()
    if self.mProperty:cache().mEntityHostKey then
        local entity = Framework.singleton():getEntityCustomManager():getEntityByHostKey(self.mProperty:cache().mEntityHostKey)
        if entity then
            return entity.mEntity
        end
    end
end

function Monster:update(deltaTime)
end

function Monster:onHit(player)
    if not self.mProperty:cache().mHP or self.mProperty:cache().mHP <= 0 then
        return
    end
    local attack = {mDamage = 1}
    if player:getLocalProperty().mEquip and player:getLocalProperty().mEquip.mWeapon then
        local item_cfg =
            Config.Item[player:getLocalProperty().mBag.mItems[player:getLocalProperty().mEquip.mWeapon].mConfigIndex]
        attack = item_cfg.mAttack or attack
    end
    self:sendToHost("OnHit", {mPlayerID = player:getID(), mDamage = attack.mDamage})
    Framework.singleton():getCommandQueueManager():post(new(Command_Callback, {mDebug = "Monster:onHit", mExecutingCallback = function(command)
        command.mTimer = command.mTimer or new(Utility.Timer)
        if not command.mUI and self:getEntity() and self:getEntity():GetInnerObject() and self:getEntity().entityId and GetEntityHeadOnObject(self:getEntity().entityId, "OnHit/" .. tostring(self.mID)) then
            command.mUI = GetEntityHeadOnObject(self:getEntity().entityId, "OnHit/" .. tostring(self.mID)):createChild(
                {
                    ui_name = "onhit",
                    type = "text",
                    font_color = "255 0 0",
                    align = "_ct",
                    y = -120,
                    x = -150,
                    height = 20,
                    width = 200,
                    visible = true,
                    font_size = 15,
                    text_format = 5,
                    text = "HP - " .. tostring(attack.mDamage)
                }
        )
        end
        if command.mUI then
            command.mUI.y = -math.floor(120 + command.mTimer:total() * 10)
        end
        if command.mTimer:total() >= 2 then
            delete(command.mTimer)
            command.mTimer = nil
            if command.mUI then
                command.mUI:destroy()
                command.mUI = nil
            end
            command.mState = Command.EState.Finish
        end
    end}))
end

function Monster:_getSendKey()
    return "GameMonster/" .. tostring(self.mID)
end

function Monster:_updateNameUI()
    if self:getEntity() and self:getEntity():GetInnerObject() and self:getEntity().entityId and GetEntityHeadOnObject(self:getEntity().entityId, "Name/" .. tostring(self.mID)) then
        local f_c = "0 255 0"
        if self:getConfig().mType == "中立" then
            f_c = "255 255 0"
        elseif self:getConfig().mType == "危险" then
            f_c = "255 0 0"
        end
        self.mNameUI = self.mNameUI or
            GetEntityHeadOnObject(self:getEntity().entityId, "Name/" .. tostring(self.mID)):createChild(
                {
                    ui_name = "name",
                    type = "text",
                    font_color = f_c,
                    align = "_ct",
                    y = -80,
                    x = -150,
                    height = 20,
                    width = 200,
                    visible = true,
                    font_size = 15,
                    text_format = 5,
                    text = self:getConfig().mName
                }
        )
        if self.mProperty:cache().mHP and self.mProperty:cache().mHP <= 0 then
            self.mNameUI.font_color = "0 255 0"
            self.mNameUI.text = self:getConfig().mName .. "(死亡)"
        elseif self.mProperty:cache().mState == "Peace" then
            self.mNameUI.font_color = "0 255 0"
            self.mNameUI.text = self:getConfig().mName .. "(和平)"
        elseif self.mProperty:cache().mState == "Scared" then
            self.mNameUI.font_color = "0 255 0"
            self.mNameUI.text = self:getConfig().mName .. "(害怕)"
        elseif self.mProperty:cache().mState == "Angry" then
            self.mNameUI.font_color = "255 0 0"
            self.mNameUI.text = self:getConfig().mName .. "(愤怒)"
        elseif self.mProperty:cache().mState == "Crazy" then
            self.mNameUI.font_color = "0 0 0"
            self.mNameUI.text = self:getConfig().mName .. "(发狂)"
        end
    else
        if self.mNameUI then
            self.mNameUI:destroy()
        end
        self.mNameUI = nil
    end
end

function Monster:_updateBloodUI()
    if self.mProperty:cache().mHP and self.mProperty:cache().mHP <= 0 then
        if self.mBloodUI then
            self.mBloodUI:destroy()
        end
        self.mBloodUI = nil
        return
    end
    if self:getEntity() and self:getEntity():GetInnerObject() and self:getEntity().entityId and GetEntityHeadOnObject(self:getEntity().entityId, "Blood/" .. tostring(self.mID)) then
        self.mBloodUI = self.mBloodUI or
            GetEntityHeadOnObject(self:getEntity().entityId, "Blood/" .. tostring(self.mID)):createChild(
                {
                    ui_name = "background",
                    type = "container",
                    color = "0 255 0",
                    align = "_ct",
                    y = -100,
                    x = -150,
                    height = 20,
                    width = 200,
                    visible = true
                }
    )
    else
        if self.mBloodUI then
            self.mBloodUI:destroy()
        end
        self.mBloodUI = nil
    end
    if self.mProperty:cache().mHP and self.mBloodUI then
        self.mBloodUI.width = 200 * self.mProperty:cache().mHP / self:getConfig().mHP
    end
end
