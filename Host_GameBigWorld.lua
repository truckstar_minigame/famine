local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Host_Game")
local Scene = Devil.getTable("Game.Famine.Host_GameNatureScene")
local Building = Devil.getTable("Game.Famine.Host_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")

local BigWorld = inherit(Scene, Devil.getTable("Game.Famine.Host_GameBigWorld"))

function BigWorld.singleton()
    return BigWorld.msInstance
end

function BigWorld:construction(parameter)
    BigWorld.msInstance = self
    self.mZeroPosition = parameter.mZeroPosition
    self.mStartChunk = {math.floor((self.mZeroPosition[1] + 0.5) / 16), math.floor((self.mZeroPosition[3] + 0.5) / 16)}
    self.mBound = new(Utility.BlockAABB, {mPivot = {102400000 + self.mZeroPosition[1], self.mZeroPosition[2], 102400000 + self.mZeroPosition[3]}, mVolume = {204800001, 200, 204800001}})
    if Config.mDebug then
        for x = self.mBound:min()[1], self.mBound:min()[1] + 512 do
            for z = self.mBound:min()[3], self.mBound:min()[3] + 512 do
                for y = self.mBound:min()[2], 250 do
                    local entities = GetEntitiesInBlock(x, y, z)
                    if entities then
                        for k, v in pairs(entities) do
                            local p = GetEntityById(k.entityId)
                            if p and (p:GetType() ~= "PlayerMP" and p:GetType() ~= "Player") then
                                p:SetDead(true)
                            end
                        end
                    end
                    SetBlock(x, y, z, 0)
                end
            end
        end
    end
    for x = 0, 16 do
        for z = 0, 16 do
            self:_asyncGenerateChunk(x, z)
        end
    end
end

function BigWorld:destruction()
    self.mZeroPosition = nil
    self.mStartChunk = nil
    BigWorld.msInstance = nil
    if Config.mDebug then
        for x = self.mBound:min()[1], self.mBound:min()[1] + 512 do
            for z = self.mBound:min()[3], self.mBound:min()[3] + 512 do
                for y = self.mBound:min()[2], 250 do
                    local entities = GetEntitiesInBlock(x, y, z)
                    if entities then
                        for k, v in pairs(entities) do
                            local p = GetEntityById(k.entityId)
                            if p and (p:GetType() ~= "PlayerMP" and p:GetType() ~= "Player") then
                                p:SetDead(true)
                            end
                        end
                    end
                    SetBlock(x, y, z, 0)
                end
            end
        end
        delete(self.mBound)
        self.mBound = nil
    end
end
