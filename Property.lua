local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local GameProperty = inherit(Devil.getTable("Framework.PropertyBase"),Devil.getTable("Game.Famine.GameProperty"))
function GameProperty:construction(parameter)
end

function GameProperty:destruction()
end

function GameProperty:_getLockKey(propertyName)
    return "GameProperty/" .. propertyName
end

local GameSceneProperty = inherit(Devil.getTable("Framework.PropertyBase"),Devil.getTable("Game.Famine.GameSceneProperty"))
function GameSceneProperty:construction(parameter)
end

function GameSceneProperty:destruction()
end

function GameSceneProperty:_getLockKey(propertyName)
    return "GameSceneProperty/" .. tostring(self.mID) .. "/" .. propertyName
end

local GamePlayerProperty = inherit(Devil.getTable("Framework.PropertyBase"),Devil.getTable("Game.Famine.GamePlayerProperty"))
function GamePlayerProperty:construction(parameter)
    self.mID = parameter.mID
end

function GamePlayerProperty:destruction()
end

function GamePlayerProperty:_getLockKey(propertyName)
    return "GamePlayerProperty/" .. tostring(self.mID) .. "/" .. propertyName
end

local GameBuildingProperty = inherit(Devil.getTable("Framework.PropertyBase"),Devil.getTable("Game.Famine.GameBuildingProperty"))
function GameBuildingProperty:construction(parameter)
    self.mID = parameter.mID
end

function GameBuildingProperty:destruction()
end

function GameBuildingProperty:_getLockKey(propertyName)
    return "GameBuildingProperty/" .. tostring(self.mID) .. "/" .. propertyName
end

local GameMonsterProperty = inherit(Devil.getTable("Framework.PropertyBase"),Devil.getTable("Game.Famine.GameMonsterProperty"))
function GameMonsterProperty:construction(parameter)
    self.mID = parameter.mID
end

function GameMonsterProperty:destruction()
end

function GameMonsterProperty:_getLockKey(propertyName)
    return "GameMonsterProperty/" .. tostring(self.mID) .. "/" .. propertyName
end
