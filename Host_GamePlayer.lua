local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Game = Devil.getTable("Game.Famine.Host_Game")
local SceneManager = Devil.getTable("Game.Famine.Host_GameSceneManager")
local Player = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_GamePlayer"))
function Player:construction(parameter)
    echo("devilwalk", "Host_GamePlayer:construction:self.mID:" .. tostring(self.mID))
    self.mProperty = new(Devil.Game.Famine.GamePlayerProperty, {mID = self.mID})
    self.mHome =
        Game.singleton():getSceneManager():createScene({mType = SceneManager.EScene.PlayerHome, mPlayerID = self.mID})

    self.mProperty:addPropertyListener(
        "mSex",
        self,
        function()
        end
    )
end

function Player:destruction()
    self.mProperty:removePropertyListener("mSex", self)
    self.mProperty:safeWrite("mSex")
    self.mProperty:safeWrite("mEquip")
    delete(self.mProperty)
    self.mProperty = nil
    Game.singleton():getSceneManager():destroyScene(self.mHome)
    self.mHome = nil
end

function Player:getID()
    return self.mID
end

function Player:getEntity()
    return GetEntityById(self.mID)
end

function Player:getProperty()
    return self.mProperty
end

function Player:getScene()
    if self:getEntity() then
        local x,y,z = self:getEntity():GetBlockPos()
        return Game.singleton():getSceneManager():getSceneByPosition({x,y,z})
    end
end

function Player:update(deltaTime)
    if not self.mProperty:cache().mSex then
        return
    end
    local x, y, z = self:getEntity():GetBlockPos()
    if GetBlockId(x, y, z) == 200 then
        if not self.mTravel then
            self.mTravel = true
            if not self:_enterNextScene() then
                self.mTravel = nil
            end
        end
    end
    if self.mMining then
        if self.mMining.mTime >= 1 then
            local damage = 1
            if self.mMining.mToolConfigIndex then
                local tool_cfg = Config.Item[self.mMining.mToolConfigIndex]
                damage = damage * tool_cfg.mCollect.mDamage
            end
            local resource_cfg_index
            if self.mMining.mBlockPosition then
                local block_id =
                    GetBlockId(
                    self.mMining.mBlockPosition[1],
                    self.mMining.mBlockPosition[2],
                    self.mMining.mBlockPosition[3]
                )
                resource_cfg_index =
                    Utility.arrayIndex(
                    Config.ResourceSource,
                    function(e)
                        return e.mBlockID == block_id
                    end
                )
            elseif self.mMining.mMonsterID then
                local monster = Game.singleton():getMonsterManager():getMonsterByID(self.mMining.mMonsterID)
                if monster then
                    resource_cfg_index =
                        Utility.arrayIndex(
                        Config.ResourceSource,
                        function(e)
                            return e.mMonsterName == monster:getConfig().mName
                        end
                    )
                end
            end
            if resource_cfg_index then
                self:sendToClient(
                    self.mID,
                    "MinedResource",
                    {mConfigIndex = resource_cfg_index, mToolConfigIndex = self.mMining.mToolConfigIndex}
                )
            end
            if Game.singleton():getResourceManager():mining({mBlockPosition = self.mMining.mBlockPosition, mDamage = damage, mMonsterID = self.mMining.mMonsterID}) then
                self.mMining = nil
            else
                self.mMining.mTime = 0
            end
        else
            self.mMining.mTime = self.mMining.mTime + deltaTime
        end
    end
end

function Player:killMonster(monster)
    local monster_cfg = monster:getConfig()
    if monster_cfg.mExp then
        self:sendToClient(self.mID,"AddExp",{mExp = monster_cfg.mExp})
    end
end

function Player:onHitByMonster(monster)
    local monster_cfg = monster:getConfig()
    self:sendToClient(self.mID,"HitByMonster",{mConfigIndex = monster.mConfigIndex})
end

function Player:_getSendKey()
    return "GamePlayer/" .. tostring(self.mID)
end

function Player:_receiveMessage(parameter)
    if parameter.mMessage == "StartMining" then
        self.mMining = {
            mBlockPosition = parameter.mParameter.mBlockPosition,
            mMonsterID = parameter.mParameter.mMonsterID,
            mToolConfigIndex = parameter.mParameter.mToolConfigIndex,
            mTime = 0
        }
    elseif parameter.mMessage == "StopMining" then
        if self.mMining then
            self.mMining.mBlockPosition = nil
            self.mMining.mMonsterID = nil
            self.mMining.mToolConfigIndex = nil
            self.mMining.mTime = nil
            self.mMining = nil
        end
    elseif parameter.mMessage == "PlaceItem" then
        local result =
            not Utility.hasBlock(
            parameter.mParameter.mBlockPosition[1],
            parameter.mParameter.mBlockPosition[2],
            parameter.mParameter.mBlockPosition[3]
        )
        if result then
            local item_cfg = Config.Item[parameter.mParameter.mItemConfigIndex]
            if item_cfg.mBlockID then
                Utility.setBlock(
                    parameter.mParameter.mBlockPosition[1],
                    parameter.mParameter.mBlockPosition[2],
                    parameter.mParameter.mBlockPosition[3],
                    item_cfg.mBlockID
                )
            end
        end
        self:responseToClient(
            parameter.mFrom,
            "PlaceItem",
            {mResult = result, mResponseCallbackKey = parameter.mParameter.mResponseCallbackKey}
        )
    elseif parameter.mMessage == "Dead" then
        local id = self.mID
        Game.singleton():getPlayerManager():_destroyPlayer(id)
        self.mHome = Game.singleton():getPlayerManager():_createPlayer(id)
    end
end

function Player:_enterNextScene()
    local x, y, z = self:getEntity():GetBlockPos()
    local block_pos = vector3d:new(x, y, z)
    local current_scene = Game.singleton():getSceneManager():getSceneByPosition(block_pos)
    if current_scene then
        local next_scene = current_scene:getNextScene(block_pos)
        if next_scene == "PlayerHome" then
            next_scene = Game.singleton():getSceneManager():getPlayerHome(self.mID)
        end
        if next_scene then
            next_scene:getTravelPosition(
                next_scene:getTravelIndex(current_scene),
                function(travelPosition)
                    SetEntityBlockPos(self.mID, travelPosition[1], travelPosition[2], travelPosition[3])
                    self.mTravel = nil
                end
            )
            return true
        end
    end
end
