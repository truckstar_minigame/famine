local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")

local Building = inherit(Devil.Framework.ClientObjectBase, Devil.getTable("Game.Famine.Client_GameBuilding"))
function Building:construction(parameter)
    self.mItemConfigIndex = parameter.mItemConfigIndex
    self.mPivot = parameter.mPivot
    self.mFacing = parameter.mFacing
    self.mParentID = parameter.mParentID
    self.mProperty = new(Devil.Game.Famine.GameBuildingProperty, {mID = tostring(self.mPlayerID) .. "/" .. tostring(self.mID)})
    
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if building_cfg.mBag then
        self.mProperty:addPropertyListener("mBag", self, function(_, value)
            self.mLocalBag = value
        end)
    end
    if building_cfg.mEnable then
        self.mProperty:addPropertyListener("mEnable", self, function(_, value)
            end)
    end
    self.mProperty:addPropertyListener("mEntityHostKey", self, function(_, value)
        self.mEntityClientKey = Framework.singleton():getEntityCustomManager():getEntityByHostKey(value).mClientKey
    end)
    self.mProperty:addPropertyListener("mCrafting", self, function(_, value)
        self.mLocalCrafting = value
    end)
end

function Building:destruction()
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if building_cfg.mBag then
        self.mProperty:removePropertyListener("mBag", self)
        self.mLocalBag = nil
    end
    if building_cfg.mEnable then
        self.mProperty:removePropertyListener("mEnable", self)
    end
    self.mProperty:removePropertyListener("mEntityHostKey", self)
    self.mProperty:removePropertyListener("mCrafting", self)
    self.mLocalCrafting = nil
    delete(self.mProperty)
    self.mProperty = nil
    self.mEntityClientKey = nil
    delete(self.mBound)
    self.mBound = nil
    self.mParentID = nil
    if building_cfg.mFortitude then
        local player = Game.singleton():getPlayerManager():getPlayerByID()
        if player then
            player:getLocalProperty().mFortitudeAdditions = player:getLocalProperty().mFortitudeAdditions or {}
            player:getLocalProperty().mFortitudeAdditions[tostring(self)] = nil
        end
    end
end

function Building:update(deltaTime)
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if building_cfg.mFortitude then
        local player = Game.singleton():getPlayerManager():getPlayerByID()
        if player then
            player:getLocalProperty().mFortitudeAdditions = player:getLocalProperty().mFortitudeAdditions or {}
            if not building_cfg.mFortitude.mNeedEnable or self.mProperty:cache().mEnable then
                local bound
                if building_cfg.mFortitude.mBlockSize then
                    self.mFortitudeBound = self.mFortitudeBound or GameUtility.calcBuildingBound(self.mPivot, building_cfg.mFortitude.mBlockSize)
                    bound = self.mFortitudeBound
                else
                    bound = self:getBound()
                end
                local x, y, z = GetPlayer():GetBlockPos()
                if bound:containPoint(vector3d:new(x, y, z)) then
                    local hot = building_cfg.mFortitude.mHot
                    local cold = building_cfg.mFortitude.mCold
                    if building_cfg.mFortitude.mAttenuation then
                        local len = (vector3d:new(x, y, z) - vector3d:new(self.mPivot)):length()
                        local half_radius = math.floor(building_cfg.mFortitude.mBlockSize[1] * 0.5)
                        hot = hot * (half_radius - len + 1) / half_radius
                        cold = cold * (half_radius - len + 1) / half_radius
                    end
                    player:getLocalProperty().mFortitudeAdditions[tostring(self)] = {mHot = hot, mCold = cold}
                    return
                end
            end
            player:getLocalProperty().mFortitudeAdditions[tostring(self)] = nil
        end
    end
    if self.mLocalBag then
        for _, bag_item in pairs(self.mLocalBag.mItems) do
            local item_cfg = Config.Item[bag_item.mConfigIndex]
            if item_cfg.mHPSubtractByTime then
                local hp_sub = deltaTime * item_cfg.mHPSubtractByTime
                if building_cfg.mKeepFresh then
                    if not building_cfg.mKeepFresh.mNeedEnable or self.mProperty:cache().mEnable then
                        hp_sub = hp_sub * building_cfg.mKeepFresh.mValue
                    end
                end
                bag_item.mHP = bag_item.mHP - hp_sub
            end
        end
    end
    if self.mLocalCrafting and self.mLocalCrafting[1] then
        local crafting = self.mLocalCrafting[1]
        crafting.mTime =
            crafting.mTime + deltaTime * crafting.mSpeed
    end
end

function Building:getItemConfig()
    return Config.Item[self.mItemConfigIndex]
end

function Building:getConfig()
    return Utility.arrayGet(Config.Building, function(e) return e.mName == self:getItemConfig().mName end)
end

function Building:getBound()
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if not self.mBound then
        self.mBound = GameUtility.calcBuildingBound(self.mPivot, building_cfg.mBlockSize)
        self.mBound:rotationAroundYAxis(self.mFacing)
    end
    return self.mBound
end

function Building:canBuild(parameter)
    if self:getBound():intersectAABB(parameter.mBound) then
        return false
    else
        return "NotCare"
    end
end

function Building:getProperty()
    return self.mProperty
end

function Building:getID()
    return self.mID
end

function Building:getPlayerID()
    return self.mPlayerID
end

function Building:getSceneID()
    return self.mSceneID
end

function Building:getPlayer()
    return Game.singleton():getPlayerManager():getPlayerByID(self.mPlayerID)
end

function Building:getScene()
    return Game.singleton():getSceneManager():getSceneByID(self.mSceneID)
end

function Building:getParent()
    if self.mParentID then
        return self:getScene():getBuildingByID(self.mParentID)
    end
end

function Building:onInteractive()
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    if building_cfg.mInteractives then
        local player_x, player_y, player_z = GetPlayer():GetBlockPos()
        local distance = building_cfg.mInteractives.mDistance or 999999
        local test_bound = new(Utility.BlockAABB, {mPivot = self:getBound().mPivot - vector3d:new(0, distance, 0), mVolume = self:getBound().mVolume + vector3d:new(distance, distance * 2, distance)})
        if test_bound:containPoint(vector3d:new(player_x, player_y, player_z)) then
            self:sendToHost("Interactive")
            if Utility.arrayContain(building_cfg.mTypes, "贮藏")
            then
                Game.singleton():getUIManager():show("Synthesis", 1, {mInteractive = self})
            end
            return true
        end
    end
end

function Building:setEnable(enable)
    local item_cfg = self:getItemConfig()
    local building_cfg = self:getConfig()
    local need_send = not enable
    if not need_send and building_cfg.mEnable.mEnergyResources then
        for _, item in pairs(self.mProperty:cache().mBag.mItems) do
            for _, res in pairs(building_cfg.mEnable.mEnergyResources) do
                need_send = Config.Item[item.mConfigIndex].mName == res.mName
                if need_send then
                    break
                end
            end
            if need_send then
                break
            end
        end
    end
    if need_send then
        self:sendToHost("SetEnable", {mValue = enable})
    end
end

function Building:addBagItem(item, callback)
    self:requestToHost("AddBagItem", item, function(parameter)
        if callback then
            callback(parameter.mCount)
        end
    end)
end

function Building:removeBagItem(slot, count, callback)
    self:requestToHost("RemoveBagItem", {mSlot = slot, mCount = count}, function(parameter)
        if callback then
            callback(parameter.mCount)
        end
    end)
end

function Building:useBlueprint(configIndex, count)
    self:sendToHost("UseBlueprint", {mConfigIndex = configIndex, mCount = count, mSpeed = GameUtility.calcPlayerCraftingSpeed(Game.singleton():getPlayerManager():getPlayerByID():getLocalProperty().mCraftingSpeedLevel)})
end

function Building:_getSendKey()
    return "GameBuilding/" .. tostring(self.mPlayerID) .. "/" .. "/" .. tostring(self.mSceneID) .. tostring(self.mID)
end
-----------------------------------------------------------------------------------------------------------------------------------------------
local House = inherit(Building, Devil.getTable("Game.Famine.Client_GameBuilding.House"))

function House:construction(parameter)
end

function House:destruction(parameter)
end

function House:canBuild(parameter)
    if parameter.mConfig.mTypes and (Utility.arrayContain(parameter.mConfig.mTypes, "门") or Utility.arrayContain(parameter.mConfig.mTypes, "窗")) then
        if Utility.arrayContain(self:getBound():borderBlocks(), parameter.mBound:blocks(), function(v1, v2) return v1[1] == v2[1] and v1[2] == v2[2] and v1[3] == v2[3] end) then
            return true
        elseif self:getBound():intersectAABB(parameter.mBound) then
            return false
        else
            return "NotCare"
        end
    elseif self:getBound():containAABB(parameter.mBound, true) then
        return true
    end
    return House._super.canBuild(self, parameter)
end
-----------------------------------------------------------------------------------------------------------------------------------------------
local HouseComponent = inherit(Building, Devil.getTable("Game.Famine.Client_GameBuilding.HouseComponent"))

function HouseComponent:construction(parameter)
end

function HouseComponent:destruction()
end
