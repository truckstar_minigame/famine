local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Host_Game")
local Scene = Devil.getTable("Game.Famine.Host_GameScene")
local Building = Devil.getTable("Game.Famine.Host_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")

local NatureScene = inherit(Scene, Devil.getTable("Game.Famine.Host_GameNatureScene"))

function NatureScene:construction(parameter)
    self.mSamplerSize = parameter.mSamplerSize
    self.mSamplerScale = parameter.mSamplerScale or 1
    self.mSampler = {}
    self.mChunkSampler = {}
    self.mNoise = CreatePerlinNoise({mSeed = parameter.mSeed, mQuality = 3})
    self.mGenerated = {}
    self.mGenerating = {}
    self.mGenerateQueue = Framework.singleton():getCommandQueueManager():createQueue()
    if self.mBound then
        for x = 0, 16 do
            for z = 0, 16 do
                self:_asyncGenerateChunk(x, z)
            end
        end
    end
end

function NatureScene:destruction()
    DestroyPerlinNoise(self.mNoise)
    self.mNoise = nil
    self.mGenerated = nil
    self.mGenerating = nil
    self.mSampler = nil
    self.mChunkSampler = nil
    Framework.singleton():getCommandQueueManager():destroyQueue(self.mGenerateQueue)
    self.mGenerateQueue = nil
end

function NatureScene:update(deltaTime)
    NatureScene._super.update(self, deltaTime)
    for _, player in pairs(Game.singleton():getPlayerManager().mPlayers) do
        if player:getScene() == self then
            local x, y, z = player:getEntity():GetBlockPos()
            x = x - self.mBound:min()[1]
            z = z - self.mBound:min()[3]
            if y <= 1 then
                SetEntityBlockPos(player:getID(),x,100,z)
            end
            local chunk_x, chunk_z = math.floor(x / 16), math.floor(z / 16)
            if chunk_x >= 0 and chunk_z >= 0 then
                self:_generateChunk(chunk_x, chunk_z)
            end
            for u = -8, 0 do
                for v = -8, 0 do
                    self:_asyncGenerateChunk(chunk_x + u, chunk_z + v, nil, true)
                    self:_asyncGenerateChunk(chunk_x - u, chunk_z + v, nil, true)
                    self:_asyncGenerateChunk(chunk_x + u, chunk_z - v, nil, true)
                    self:_asyncGenerateChunk(chunk_x - u, chunk_z - v, nil, true)
                end
            end
        end
    end
end

function NatureScene:getTravelPosition(travelIndex, callback)
    Framework.singleton():getCommandQueueManager():post(new(Command_Callback, {mDebug = "Scene:getTravelPosition", mExecutingCallback = function(command)
        if self.mIsDelete then
            callback()
            command.mState = Command.EState.Finish
            return
        end
        if self.mEntries[travelIndex] and self.mEntries[travelIndex].mPosition then
            callback(self.mEntries[travelIndex].mPosition + vector3d:new(1, 5, 1))
            command.mState = Command.EState.Finish
        end
    end}))
end

function NatureScene:_generateChunk(chunkX, chunkZ)
    local generate_key = tostring(chunkX) .. "," .. tostring(chunkZ)
    if self.mGenerated[generate_key] then
        return
    end
    
    local res_blocks = {}
    local terrain = {26, 2051, 2055}
    res_blocks.mTerrain = terrain[math.random(1, #terrain)]
    local tree_branch = {98, 99, 126, 128, 2291, 2078}
    res_blocks.mTreeBranch = tree_branch[math.random(1, #tree_branch)]
    local leaf = {92, 85, 86, 91, 129, 2045, 2046, 2215}
    res_blocks.mTreeLeaf = leaf[math.random(1, #leaf)]
    local grass = {113, 164, 165, 114, 115, 116, 2041, 2043, 2044, 132, 12, 51, 55, 155}
    res_blocks.mGrass = grass[math.random(1, #grass)]
    local stone = {58, 60, 77}
    res_blocks.mStone = stone[math.random(1, #stone)]
    
    local x_start = chunkX * 16
    local z_start = chunkZ * 16
    for x = 1, 16 do
        for z = 1, 16 do
            local x_pos, z_pos = x_start + x, z_start + z
            self:_generateSurface(x_pos, z_pos, res_blocks)
        end
    end
    self.mGenerated[generate_key] = true
    self.mGenerating[generate_key] = nil
    local chunk_sampler_key = tostring(chunkX % (self.mSamplerSize * self.mSamplerScale / 16)) .. "," .. tostring(chunkZ % (self.mSamplerSize * self.mSamplerScale / 16))
    self.mChunkSampler[chunk_sampler_key] = true
--echo("devilwalk", "NatureScene:_generateChunk:" .. tostring(chunkX) .. "," .. tostring(chunkZ))
end

function NatureScene:_queueGenerateChunk(chunkX, chunkZ)
    self.mGenerateQueue:post(new(Command_Callback, {mDebug = "NatureScene:_queueGenerateChunk:" .. tostring(chunkX) .. "," .. tostring(chunkZ), mExecuteCallback = function(command)
        self:_generateChunk(chunkX, chunkZ)
        command.mState = Command.EState.Finish
    end}))
end

function NatureScene:_asyncGenerateChunk(chunkX, chunkZ, generateInQueue, prior)
    if chunkX < 0
        or chunkZ < 0
        or (self.mBound and (chunkX > math.ceil(self.mBound.mVolume[1] / 16)))
        or (self.mBound and (chunkZ > math.ceil(self.mBound.mVolume[3] / 16)))
    then
        return
    end
    local generate_key = tostring(chunkX) .. "," .. tostring(chunkZ)
    if self.mGenerated[generate_key] then
        return
    end
    if self.mGenerating[generate_key] then
        if prior then
            self.mSceneManager:getNatureSceneTaskDispatcher():priorTask(self.mGenerating[generate_key])
        end
        return
    end
    local chunk_sampler_key = tostring(chunkX % (self.mSamplerSize * self.mSamplerScale / 16)) .. "," .. tostring(chunkZ % (self.mSamplerSize * self.mSamplerScale / 16))
    if self.mChunkSampler[chunk_sampler_key] then
        if generateInQueue then
            self:_queueGenerateChunk(chunkX, chunkZ)
        else
            self:_generateChunk(chunkX, chunkZ)
        end
    else
        self.mGenerating[generate_key] = self.mSceneManager:getNatureSceneTaskDispatcher():addTask({mID = self.mID, mChunkX = chunkX, mChunkZ = chunkZ, mSeed = self.mNoise.mSeed, mSamplerSize = self.mSamplerSize, mSamplerScale = self.mSamplerScale}, function(result)
            for k, v in pairs(result.mSampler) do
                self.mSampler[k] = v
            end
            if generateInQueue then
                self:_queueGenerateChunk(result.mChunkX, result.mChunkZ)
            else
                self:_generateChunk(result.mChunkX, result.mChunkZ)
            end
        end)
    end
end

function NatureScene:_calcSamplerUV(blockX, blockZ)
    return ((math.ceil((blockX - 0.5) / self.mSamplerScale) - 1) % self.mSamplerSize), ((math.ceil((blockZ - 0.5) / self.mSamplerScale) - 1) % self.mSamplerSize)
end

function NatureScene:_sampler(blockX, blockZ)
    local u, v = self:_calcSamplerUV(blockX, blockZ)
    local sampler_key = v * self.mSamplerSize + u + 1
    --[[if sampler_key > self.mSamplerSize * self.mSamplerSize + 1 then
    echo("devilwalk", "NatureScene:_sampler:errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror")
    echo("devilwalk", tostring(blockX) .. "," .. tostring(blockZ) .. "," .. tostring(u) .. "," .. tostring(v) .. "," .. tostring(sampler_key))
    end]]
    if not self.mSampler[sampler_key] then
        echo("devilwalk", "NatureScene:_sampler:warningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarning")
        echo("devilwalk", tostring(blockX) .. "," .. tostring(blockZ) .. "," .. tostring(u) .. "," .. tostring(v) .. "," .. tostring(sampler_key))
        self.mSampler[sampler_key] = self.mNoise:getValue(u / (self.mSamplerSize - 1) * 2 - 1, 0, v / (self.mSamplerSize - 1) * 2 - 1)
    end
    return self.mSampler[sampler_key]
end

function NatureScene:_generate(x, z)
    local function _generateTree(blockX, blockY, blockZ)
        local tree_height = math.random(3, 10)
        local leaf_height = math.random(3, tree_height)
        local brance_res = {98, 99, 126, 128, 2291, 2078}
        local brance_id = brance_res[math.random(1, #brance_res)]
        local leaf_res = {92, 85, 86, 91, 129, 2045, 2046, 2215}
        local leaf_id = leaf_res[math.random(1, #leaf_res)]
        for y = 0, tree_height do
            SetBlock(blockX, blockY + y, blockZ, brance_id)
        end
        for y = leaf_height, tree_height + 1 do
            for block_x = -(tree_height + 1 - y), tree_height + 1 - y do
                for block_z = -(tree_height + 1 - y), tree_height + 1 - y do
                    if not Utility.hasBlock(blockX + block_x, blockY + y, blockZ + block_z)
                        and blockX + block_x > self.mBound:min()[1]
                        and blockX + block_x < self.mBound:max()[1]
                        and blockZ + block_z > self.mBound:min()[3]
                        and blockZ + block_z < self.mBound:max()[3]
                    then
                        SetBlock(blockX + block_x, blockY + y, blockZ + block_z, leaf_id)
                    end
                end
            end
        end
    end
    local function _generateGrass(blockX, blockY, blockZ)
        local res = {113, 164, 165, 114, 115, 116, 2041, 2043, 2044, 132, }
        SetBlock(blockX, blockY, blockZ, res[math.random(1, #res)])
    end
    local function _generateStone(blockX, blockY, blockZ)
        local res = {58, 60, 77}
        SetBlock(blockX, blockY, blockZ, res[math.random(1, #res)])
    end
    local x_start = self.mBound:min()[1] - 1
    local z_start = self.mBound:min()[3] - 1
    if x_start + x == self.mBound:min()[1]
        or x_start + x == self.mBound:max()[1]
        or z_start + z == self.mBound:min()[3]
        or z_start + z == self.mBound:max()[3]
    then
        for y = self.mBound:min()[2], self.mBound:max()[2] do
            SetBlock(x_start + x, y, z_start + z, 2217)
        end
    elseif x_start + x < self.mBound:min()[1]
        or x_start + x > self.mBound:max()[1]
        or z_start + z < self.mBound:min()[3]
        or z_start + z > self.mBound:max()[3]
    then
        return
    else
        local noise = self:_sampler(x, z)
        local height = math.max(math.floor((noise + 1) * 0.5 * 100), 0)
        for y = 0, math.min(30, height) do
            SetBlock(x_start + x, self.mBound:min()[2] + y, z_start + z, 53)
        end
        for y = 31, math.min(49, height) do
            SetBlock(x_start + x, self.mBound:min()[2] + y, z_start + z, 171)
        end
        for y = 50, height do
            SetBlock(x_start + x, self.mBound:min()[2] + y, z_start + z, 53)
        end
        if height > 50 then
            local block_id
            if height > 75 then
                block_id = 52
            else
                local blocks = {26, 2051, 2055}
                block_id = blocks[math.random(1, #blocks)]
            end
            SetBlock(x_start + x, self.mBound:min()[2] + height, z_start + z, block_id)
        end
        for y = height + 1, 30 do
            SetBlock(x_start + x, self.mBound:min()[2] + y, z_start + z, 76)
        end
        if (not self.mEntries[1].mPosition and math.random() < 0.1)
            or math.random() < 0.00001
        then
            SetBlock(x_start + x, self.mBound:min()[2] + height + 1, z_start + z, 200)
            local seted
            for k, entry in pairs(self.mEntries) do
                if not entry.mPosition then
                    entry.mPosition = vector3d:new(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                    seted = true
                    self:_updateEntryBillboard(k)
                    break
                end
            end
            if not seted then
                local entry = {mPosition = vector3d:new(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)}
                self.mEntries[#self.mEntries + 1] = entry
            end
        else
            if height > 30 then
                if math.random() < 0.003 then
                    Game.singleton():getMonsterManager():createMonster({mConfigIndex = math.random(1, #Config.Monster), mBlockPosition = {x_start + x, self.mBound:min()[2] + height + 30, z_start + z}})
                end
                if height > 50 and height <= 75 then
                    if math.random() < 0.01 * (75 - height) then
                        if math.random() < 0.1 then
                            _generateTree(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        elseif math.random() < 0.9 then
                            _generateGrass(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        else
                            _generateStone(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        end
                    end
                elseif height > 75 then
                    if math.random() < 0.01 * (height - 75) then
                        _generateStone(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                    elseif math.random() > 0.9 + height * 0.001 then
                        if math.random() < 0.1 then
                            _generateTree(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        else
                            _generateGrass(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        end
                    end
                end
            end
        end
    end
end

function NatureScene:_generateSurface(x, z, resBlocks)
    local function _generateTree(blockX, blockY, blockZ)
        local tree_height = math.random(3, 10)
        local leaf_height = math.random(3, tree_height)
        local brance_res = {98, 99, 126, 128, 2291, 2078}
        local brance_id = brance_res[math.random(1, #brance_res)]
        local leaf_res = {92, 85, 86, 91, 129, 2045, 2046, 2215}
        local leaf_id = leaf_res[math.random(1, #leaf_res)]
        for y = 0, tree_height do
            SetBlock(blockX, blockY + y, blockZ, resBlocks.mTreeBranch)
        end
        for y = leaf_height, tree_height + 1 do
            for block_x = -(tree_height + 1 - y), tree_height + 1 - y do
                for block_z = -(tree_height + 1 - y), tree_height + 1 - y do
                    if not Utility.hasBlock(blockX + block_x, blockY + y, blockZ + block_z)
                        and blockX + block_x > self.mBound:min()[1]
                        and blockX + block_x < self.mBound:max()[1]
                        and blockZ + block_z > self.mBound:min()[3]
                        and blockZ + block_z < self.mBound:max()[3]
                    then
                        SetBlock(blockX + block_x, blockY + y, blockZ + block_z, resBlocks.mTreeLeaf)
                    end
                end
            end
        end
    end
    local function _generateGrass(blockX, blockY, blockZ)
        local res = {113, 164, 165, 114, 115, 116, 2041, 2043, 2044, 132, 12, 155}
        SetBlock(blockX, blockY, blockZ, resBlocks.mGrass)
    end
    local function _generateStone(blockX, blockY, blockZ)
        local res = {58, 60, 77}
        SetBlock(blockX, blockY, blockZ, resBlocks.mStone)
    end
    local x_start = self.mBound:min()[1] - 1
    local z_start = self.mBound:min()[3] - 1
    if x_start + x == self.mBound:min()[1]
        or x_start + x == self.mBound:max()[1]
        or z_start + z == self.mBound:min()[3]
        or z_start + z == self.mBound:max()[3]
    then
        for y = self.mBound:min()[2], self.mBound:max()[2] do
            SetBlock(x_start + x, y, z_start + z, 2217)
        end
    elseif x_start + x < self.mBound:min()[1]
        or x_start + x > self.mBound:max()[1]
        or z_start + z < self.mBound:min()[3]
        or z_start + z > self.mBound:max()[3]
    then
        return
    else
        local noise = self:_sampler(x, z)
        local height = math.max(math.floor((noise + 1) * 0.5 * 100), 0)
        if height > 50 then
            local block_id
            if height > 75 then
                block_id = 52
            else
                local blocks = {26, 2051, 2055}
                block_id = resBlocks.mTerrain
            end
            SetBlock(x_start + x, self.mBound:min()[2] + height, z_start + z, block_id)
            for i = 1, 4 do
                SetBlock(x_start + x, self.mBound:min()[2] + height - i, z_start + z, 53)
            end
        elseif height >= 31 then
            SetBlock(x_start + x, self.mBound:min()[2] + height, z_start + z, 171)
            for i = 1, 4 do
                SetBlock(x_start + x, self.mBound:min()[2] + height - i, z_start + z, 53)
            end
        else
            SetBlock(x_start + x, self.mBound:min()[2] + height, z_start + z, 53)
            for i = 1, 4 do
                if height - i >= 0 then
                    SetBlock(x_start + x, self.mBound:min()[2] + height - i, z_start + z, 53)
                end
            end
        end
        for y = height + 1, 30 do
            SetBlock(x_start + x, self.mBound:min()[2] + y, z_start + z, 76)
        end
        if (not self.mEntries[1].mPosition and math.random() < 0.01)
            or math.random() < 0.0001
        then
            SetBlock(x_start + x, self.mBound:min()[2] + height + 1, z_start + z, 200)
            local seted
            for k, entry in pairs(self.mEntries) do
                if not entry.mPosition then
                    entry.mPosition = vector3d:new(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                    seted = true
                    self:_updateEntryBillboard(k)
                    break
                end
            end
            if not seted then
                local entry = {mPosition = vector3d:new(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)}
            --self.mEntries[#self.mEntries + 1] = entry
            end
        else
            if height > 30 then
                if math.random() < 0.001 then
                    Game.singleton():getMonsterManager():createMonster({mConfigIndex = math.random(1, #Config.Monster), mBlockPosition = {x_start + x, self.mBound:min()[2] + height + 30, z_start + z}})
                end
                if height > 50 and height <= 75 then
                    if math.random() < 0.01 * (75 - height) then
                        if math.random() < 0.1 then
                            _generateTree(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        elseif math.random() < 0.9 then
                            _generateGrass(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        else
                            _generateStone(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        end
                    end
                elseif height > 75 then
                    if math.random() < 0.01 * (height - 75) then
                        _generateStone(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                    elseif math.random() > 0.9 + height * 0.001 then
                        if math.random() < 0.1 then
                            _generateTree(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        else
                            _generateGrass(x_start + x, self.mBound:min()[2] + height + 1, z_start + z)
                        end
                    end
                end
            end
        end
    end
end

function NatureScene:_generateTest(x, z)
    local function _generateTree(blockX, blockY, blockZ)
        local tree_height = math.random(3, 10)
        local leaf_height = math.random(3, tree_height)
        local brance_res = {98, 99, 126, 128, 2291, 2078}
        local brance_id = brance_res[math.random(1, #brance_res)]
        local leaf_res = {92, 85, 86, 91, 129, 2045, 2046, 2215}
        local leaf_id = leaf_res[math.random(1, #leaf_res)]
        for y = 0, tree_height do
            SetBlock(blockX, blockY + y, blockZ, brance_id)
        end
        for y = leaf_height, tree_height + 1 do
            for block_x = -(tree_height + 1 - y), tree_height + 1 - y do
                for block_z = -(tree_height + 1 - y), tree_height + 1 - y do
                    if not Utility.hasBlock(blockX + block_x, blockY + y, blockZ + block_z)
                        and blockX + block_x > self.mBound:min()[1]
                        and blockX + block_x < self.mBound:max()[1]
                        and blockZ + block_z > self.mBound:min()[3]
                        and blockZ + block_z < self.mBound:max()[3]
                    then
                        SetBlock(blockX + block_x, blockY + y, blockZ + block_z, leaf_id)
                    end
                end
            end
        end
    end
    local function _generateGrass(blockX, blockY, blockZ)
        local res = {113, 164, 165, 114, 115, 116, 2041, 2043, 2044, 132, 12, 155}
        SetBlock(blockX, blockY, blockZ, res[math.random(1, #res)])
    end
    local function _generateStone(blockX, blockY, blockZ)
        local res = {58, 60, 77}
        SetBlock(blockX, blockY, blockZ, res[math.random(1, #res)])
    end
    local x_start = self.mBound:min()[1] - 1
    local z_start = self.mBound:min()[3] - 1
    if x_start + x == self.mBound:min()[1]
        or x_start + x == self.mBound:max()[1]
        or z_start + z == self.mBound:min()[3]
        or z_start + z == self.mBound:max()[3]
    then
        for y = self.mBound:min()[2], self.mBound:max()[2] do
            SetBlock(x_start + x, y, z_start + z, 2217)
        end
    elseif x_start + x < self.mBound:min()[1]
        or x_start + x > self.mBound:max()[1]
        or z_start + z < self.mBound:min()[3]
        or z_start + z > self.mBound:max()[3]
    then
        return
    else
        local noise = self:_sampler(x, z)
        local height = math.max(math.floor((noise + 1) * 0.5 * 100), 0)
        SetBlock(x_start + x, self.mBound:min()[2] + height, z_start + z, 123)
        if math.random() < 0.0001 then
            local pos = vector3d:new({x_start + x, self.mBound:min()[2] + height + 1, z_start + z})
            SetBlock(pos[1], pos[2], pos[3], 200)
            local seted
            for k, entry in pairs(self.mEntries) do
                if not entry.mPosition then
                    entry.mPosition = pos
                    seted = true
                    self:_updateEntryBillboard(k)
                    break
                end
            end
            if not seted then
                local entry = {mPosition = pos}
                self.mEntries[#self.mEntries + 1] = entry
            end
        end
    end
end
