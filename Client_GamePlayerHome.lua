local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Client_Game")
local Scene = Devil.getTable("Game.Famine.Client_GameScene")
local Building = Devil.getTable("Game.Famine.Client_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")
local GameUtility = Devil.getTable("Game.Famine.Utility")

local Home = inherit(Scene, Devil.getTable("Game.Famine.Client_GamePlayerHome"))
Home.sYLen = 32
Home.sEdgeLen = 65
Home.sBuildingEdgeLen = Home.sEdgeLen - 6
Home.sDoorBlockID = 200
function Home:construction(parameter)
    echo("devilwalk", "Client_GamePlayerHome:construction:self.mID,self.mPlayerID:" .. tostring(self.mID) .. "," .. tostring(self.mPlayerID))
    self.mSlot = parameter.mSlot
    if self:isMainHome() then
        local home = Utility.getSavedData().mHome or {}
    end
end

function Home:destruction()
end

function Home:getPlayerID()
    return self.mPlayerID
end

function Home:isMainHome()
    return GetPlayerId() == self.mPlayerID
end

function Home:canBuild(parameter)
    if not self:getBuildBound():containAABB(parameter.mBound) then
        return
    end
    for _, building in pairs(self.mBuildings) do
        local can_build = building:canBuild(parameter)
        if can_build == true then
            return true
        elseif not can_build then
            return
        end
    end
    if parameter.mConfig.mTypes and (Utility.arrayContain(parameter.mConfig.mTypes, "门") or Utility.arrayContain(parameter.mConfig.mTypes, "窗")) then
        return
    end
    return "NotCare"
end

function Home:getPosition()
    local row
    local col
    if self.mSlot <= 5 then
        row = 1
        col = self.mSlot
    elseif self.mSlot <= 7 then
        row = 2
        if self.mSlot == 6 then
            col = 1
        else
            col = 5
        end
    elseif self.mSlot <= 9 then
        row = 3
        if self.mSlot == 6 then
            col = 1
        else
            col = 5
        end
    elseif self.mSlot <= 11 then
        row = 4
        if self.mSlot == 6 then
            col = 1
        else
            col = 5
        end
    else
        row = 5
        col = self.mSlot - 11
    end
    local x, y, z = Framework.singleton():getHomePosition()
    return vector3d:new({x + (col - 1) * Home.sEdgeLen + 1, y + 1, z + (row - 1) * Home.sEdgeLen + 1})
end

function Home:getTravelPosition()
    return self:getPosition() + vector3d:new(3, 2, 3)
end

function Home:getBuildBound()
    self.mBuildBound = self.mBuildBound or new(
        Utility.BlockAABB,
        {
            mPivot = self:getPosition() + vector3d:new({math.floor(Home.sEdgeLen / 2), 0, math.floor(Home.sEdgeLen / 2)}),
            mVolume = {Home.sBuildingEdgeLen, Home.sYLen, Home.sBuildingEdgeLen}
        }
    )
    return self.mBuildBound
end

function Home:canMine()
    return false
end

function Home:_getSendKey()
    return "GamePlayerHome/" .. tostring(self.mPlayerID)
end

function Home:_save()
    if self:isMainHome() then
        local save_data = Utility.getSavedData() or {}
        save_data.mHome = {}
        save_data.mHome.mBuildings = {}
        for _, building in pairs(self.mBuildings) do
            local building_data = {mItemConfigIndex = building.mItemConfigIndex, mPivot = building.mPivot, mFacing = building.mFacing}
            if building:getProperty():cache().mBag then
                building_data.mBag = {mItems = {}}
                for k, item in pairs(building:getProperty():cache().mBag.mItems) do
                    building_data.mBag.mItems[k] = {mConfigIndex = item.mConfigIndex, mCount = item.mCount, mHP = item.mHP}
                end
            end
            save_data.mHome.mBuildings[#save_data.mHome.mBuildings + 1] = building_data
        end
    end
end
