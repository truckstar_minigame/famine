local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Host_Game")
local Config = Devil.getTable("Game.Famine.Config")

local Terrain = inherit(Devil.Framework.HostObjectBase,Devil.getTable("Game.Famine.Host_GameTerrain"))
function Terrain:construction(parameter)
    self.mTerrain = new(Devil.Framework.Terrain,parameter)
end

function Terrain:destruction()
    delete(self.mTerrain)
    self.mTerrain = nil
end

function Terrain:update(deltaTime)
end

function Terrain:applyTemplate(blockCallback,callback)
    self.mTerrain:applyTemplate(function(block)
        blockCallback(block)
    end,callback)
end

function Terrain:restoreTemplate()
    self.mTerrain:restoreTemplate()
end

function Terrain:isApplyComplete()
    return self.mTerrain:isApplyComplete()
end

function Terrain:getCustomPoint(blockID)
    return self.mTerrain:getCustomPoint(blockID)
end

function Terrain:getBlockOffset()
    return self.mTerrain:getBlockOffset()
end

function Terrain:_receiveMessage(parameter)
end

function Terrain:_getSendKey()
    return "GameTerrain"
end