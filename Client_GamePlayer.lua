local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Game = Devil.getTable("Game.Famine.Client_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Player = inherit(Devil.Framework.ClientObjectBase, Devil.getTable("Game.Famine.Client_GamePlayer"))
function Player:construction(parameter)
    self.mProperty = new(Devil.Game.Famine.GamePlayerProperty, {mID = self.mID})
    self.mLocalProperty = {}
    
    if self:isMainPlayer() then
        local player = Utility.getSavedData().mPlayer or {}
        --player.mSex = nil
        if not player.mSex then
            Game.singleton():getUIManager():show(
                "Gender",
                1,
                {
                    mCallback = function(sex)
                        self.mProperty:safeWrite("mSex", sex)
                        Game.singleton():getManipulationManager():pushMode("Game")
                    end
                }
        )
        else
            self.mProperty:safeWrite("mSex", player.mSex)
            Game.singleton():getManipulationManager():pushMode("Game")
        end
        player.mLevel = player.mLevel or 0
        player.mExp = player.mExp or 0
        player.mHPLevel = player.mHPLevel or 0
        player.mHP = player.mHP or GameUtility.calcPlayerHP(player.mHPLevel)
        player.mFoodLevel = player.mFoodLevel or 0
        player.mFood = player.mFood or GameUtility.calcPlayerFood(player.mFoodLevel)
        player.mWaterLevel = player.mWaterLevel or 0
        player.mWater = player.mWater or GameUtility.calcPlayerWater(player.mWaterLevel)
        player.mStaminaLevel = player.mStaminaLevel or 0
        player.mStamina = player.mStamina or GameUtility.calcPlayerStamina(player.mStaminaLevel)
        player.mOxygenLevel = player.mOxygenLevel or 0
        player.mOxygen = player.mOxygen or GameUtility.calcPlayerOxygen(player.mOxygenLevel)
        player.mWeightLevel = player.mWeightLevel or 0
        player.mMovementSpeedLevel = player.mMovementSpeedLevel or 0
        player.mMeleeDamageLevel = player.mMeleeDamageLevel or 0
        player.mFortitudeLevel = player.mFortitudeLevel or 0
        player.mCraftingSpeedLevel = player.mCraftingSpeedLevel or 0
        player.mPropertyPoint = player.mPropertyPoint or Config.Player.mLevelUpPropertyPoint * player.mLevel
        player.mBlueprintPoint = player.mBlueprintPoint or Config.Player.mLevelUpBlueprintPoint * player.mLevel
        player.mFaint = player.mFaint or 0
        player.mBag = player.mBag or {mItems = {}}
        player.mBlueprints = player.mBlueprints or {}
        player.mHandItems = player.mHandItems or {}
        player.mCrafting = player.mCrafting or {}
        
        self.mLocalProperty = clone(player)
        self.mLocalProperty.mStaminaRecoverInterval = 0
        self.mLocalProperty.mFortitude = {mHot = 0, mCold = 0}
        if Config.mDebug then
            self.mLocalProperty.mBag = {mItems = {}}
            for i, item_cfg in ipairs(Config.Item) do
                --if Utility.arrayContain(item_cfg.mTypes, "资源") then
                local item = {
                    mConfigIndex = i,
                    mCount = item_cfg.mOverlap or 500,
                    mHP = item_cfg.mHP,
                    mBagSlot = self:_emptyBagSlot()
                }
                self.mLocalProperty.mBag.mItems[item.mBagSlot] = item
            --end
            end
            self.mLocalProperty.mLevel = 100
            self.mLocalProperty.mPropertyPoint = 100000
            self.mLocalProperty.mBlueprintPoint = 100000
        end
        self.mMoveState = "Walk"
        
        self:_updateEquipProperty()
    end
    self.mProperty:addPropertyListener(
        "mSex",
        self,
        function(_, value)
            self:_refreshEquip()
        end
    )
    self.mProperty:addPropertyListener(
        "mEquip",
        self,
        function(_, value)
            self:_refreshEquip()
        end
    )
    Framework.singleton():getPlayerManager():addEventListener(
        "PlayerEntityCreate",
        self,
        function(inst, parameter)
            if parameter.mPlayerID == self:getID() then
                self:_refreshEquip()
            end
        end,
        self
)
end

function Player:destruction()
    self.mProperty:removePropertyListener("mSex", self)
    self.mProperty:removePropertyListener("mEquip", self)
    Framework.singleton():getPlayerManager():removeEventListener("PlayerEntityCreate", self)
    delete(self.mProperty)
    self.mProperty = nil
    self.mLocalProperty = nil
    self.mMoveState = nil
    if self:isMainPlayer() then
        Game.singleton():getUIManager():close("Bag")
        GetPlayer():SetSpeedScale(1)
    end
    if self:getEntity() then
        OpenAvatar(self:getEntity())
        CloseAvatar(self:getEntity())
        OpenAvatar(self:getEntity())
    end
end

function Player:getID()
    return self.mID
end

function Player:getEntity()
    local entity = GetEntityById(self.mID)
    if entity and not entity.nickname then
        echotable("devilwalk error:Player:getEntity:self.mID:" .. tostring(self.mID))
    end
    return entity
end

function Player:isMainPlayer()
    return GetPlayerId() == self.mID
end

function Player:getProperty()
    return self.mProperty
end

function Player:getLocalProperty()
    return self.mLocalProperty
end

function Player:getWeight()
    local ret = 0
    for _, item in pairs(self.mLocalProperty.mBag.mItems) do
        ret = ret + item.mCount * Config.Item[item.mConfigIndex].mWeight
    end
    if Config.mDebug then
        return 0
    end
    return ret
end

function Player:getScene()
    if self:getEntity() then
        local x, y, z = self:getEntity():GetBlockPos()
        return Game.singleton():getSceneManager():getSceneByPosition(vector3d:new(x, y, z))
    end
end

function Player:update(deltaTime)
    if not Game.singleton():getSceneManager():getPlayerHome(self.mID) or not self.mProperty:cache().mSex then
        return
    end
    if self:isMainPlayer() then
        self:_updatePropertyChange(deltaTime)
        if self.mIsDelete then
            return
        end
        local scene = self:getScene()
        if self.mScene ~= scene then
            self.mScene = scene
        end
        
        for _, item in pairs(self.mLocalProperty.mBag.mItems) do
            local item_cfg = Config.Item[item.mConfigIndex]
            if item_cfg.mHPSubtractByTime then
                item.mHP = item.mHP - deltaTime * item_cfg.mHPSubtractByTime
            end
            if item.mHP and item.mHP < 0 then
                item.mCount = item.mCount - 1
                item.mHP = Config.Item[item.mConfigIndex].mHP + item.mHP
            end
        end
        self:_refreshCrafting(deltaTime)
        self:_updateMining(deltaTime)
        self:_updateMeleeAttack(deltaTime)
        
        self:_refreshBag()
        self:_save()
    end
end

function Player:_getSendKey()
    return "GamePlayer/" .. tostring(self.mID)
end

function Player:_receiveMessage(parameter)
    if parameter.mMessage == "MinedResource" then
        local res_cfg = Config.ResourceSource[parameter.mParameter.mConfigIndex]
        self.mLocalProperty.mExp = self.mLocalProperty.mExp + (res_cfg.mExp or 0)
        local res_eff = {}
        if parameter.mParameter.mToolConfigIndex then
            local tool_cfg = Config.Item[parameter.mParameter.mToolConfigIndex]
            for _, eff in pairs(tool_cfg.mCollect.mEfficiency) do
                res_eff[eff.mName] = eff.mValue
            end
        end
        local mined_res = {}
        for _, res in pairs(res_cfg.mResources) do
            for i = 1, res.mCount do
                local name = res.mNames[math.random(1, #res.mNames)]
                mined_res[name] = mined_res[name] or 0
                mined_res[name] = mined_res[name] + 1
            end
        end
        for name, count in pairs(mined_res) do
            local item_cfg_index =
                Utility.arrayIndex(
                    Config.Item,
                    function(e)
                        return e.mName == name
                    end
            )
            local item_cfg = Config.Item[item_cfg_index]
            local add_count = math.floor(count * (res_eff[name] or 1))
            if add_count > 0 then
                self:addBagItem({mConfigIndex = item_cfg_index, mCount = add_count})
            end
        end
        if GetEntityHeadOnObject(self.mID, "MinedResource/" .. tostring(self.mID)) then
            Framework.singleton():getCommandQueueManager():post(
                new(
                    Command_Callback,
                    {
                        mDebug = "Client_GamePlayer:MinedResource/UI",
                        mExecutingCallback = function(command)
                            local ui_text = "经验值+" .. tostring(res_cfg.mExp or 0) .. "\n"
                            for name, count in pairs(mined_res) do
                                local item_cfg_index =
                                    Utility.arrayIndex(
                                        Config.Item,
                                        function(e)
                                            return e.mName == name
                                        end
                                )
                                local item_cfg = Config.Item[item_cfg_index]
                                local add_count = math.floor(count * (res_eff[name] or 1))
                                if add_count > 0 then
                                    ui_text = ui_text .. item_cfg.mName .. "+" .. tostring(add_count) .. "\n"
                                end
                            end
                            command.mUI =
                                command.mUI or
                                GetEntityHeadOnObject(self.mID, "MinedResource/" .. tostring(self.mID)):createChild(
                                    {
                                        type = "text",
                                        font_type = "微软雅黑",
                                        font_color = "255 225 0",
                                        font_size = 30,
                                        align = "_ct",
                                        y = -280,
                                        x = -150,
                                        height = 200,
                                        width = 500,
                                        visible = true,
                                        text = ui_text
                                    }
                            )
                            command.mTimer = command.mTimer or new(Utility.Timer)
                            if command.mTimer:total() > 0.8 then
                                command.mUI:destroy()
                                command.mUI = nil
                                command.mState = Command.EState.Finish
                            else
                                command.mUI.y = -80 - 150 * command.mTimer:total()
                            end
                        end
                    }
        )
        )
        end
    elseif parameter.mMessage == "AddExp" then
        self.mLocalProperty.mExp = self.mLocalProperty.mExp + parameter.mParameter.mExp
        Framework.singleton():getCommandQueueManager():post(
            new(
                Command_Callback,
                {
                    mDebug = "Client_GamePlayer:AddExp/UI",
                    mExecutingCallback = function(command)
                        local ui_text = "经验值+" .. tostring(parameter.mParameter.mExp or 0) .. "\n"
                        command.mUI =
                            command.mUI or
                            GetEntityHeadOnObject(self.mID, "AddExp/" .. tostring(self.mID)):createChild(
                                {
                                    type = "text",
                                    font_type = "微软雅黑",
                                    font_color = "255 225 0",
                                    font_size = 30,
                                    align = "_ct",
                                    y = -280,
                                    x = -150,
                                    height = 200,
                                    width = 500,
                                    visible = true,
                                    text = ui_text
                                }
                        )
                        command.mTimer = command.mTimer or new(Utility.Timer)
                        if command.mTimer:total() > 0.8 then
                            command.mUI:destroy()
                            command.mUI = nil
                            command.mState = Command.EState.Finish
                        else
                            command.mUI.y = -80 - 150 * command.mTimer:total()
                        end
                    end
                }
    )
    )
    elseif parameter.mMessage == "HitByMonster" then
        local monster_cfg = Config.Monster[parameter.mParameter.mConfigIndex]
        local damage = monster_cfg.mAttack.mDamage
        for _, equip in pairs(self.mLocalProperty.mEquip) do
            local bag_item = self.mLocalProperty.mBag.mItems[equip.mBagSlot]
            local equip_cfg = Config.Item[bag_item.mConfigIndex]
            if equip_cfg.mDefense then
                local damage_sub = math.min(equip_cfg.mDefense * 0.1, damage)
                damage = damage - damage_sub
                if bag_item.mHP then
                    bag_item.mHP = bag_item.mHP - damage_sub
                end
            end
        end
        self.mLocalProperty.mHP = self.mLocalProperty.mHP - damage
        Framework.singleton():getCommandQueueManager():post(
            new(
                Command_Callback,
                {
                    mDebug = "Client_GamePlayer:HitByMonster/UI",
                    mExecutingCallback = function(command)
                        local ui_text = "HP-" .. tostring(damage) .. "\n"
                        command.mUI =
                            command.mUI or
                            GetEntityHeadOnObject(self.mID, "SubHP/" .. tostring(self.mID)):createChild(
                                {
                                    type = "text",
                                    font_type = "微软雅黑",
                                    font_color = "255 0 0",
                                    font_size = 30,
                                    align = "_ct",
                                    y = -280,
                                    x = -150,
                                    height = 200,
                                    width = 500,
                                    visible = true,
                                    text = ui_text
                                }
                        )
                        command.mTimer = command.mTimer or new(Utility.Timer)
                        if command.mTimer:total() > 0.8 then
                            command.mUI:destroy()
                            command.mUI = nil
                            command.mState = Command.EState.Finish
                        else
                            command.mUI.y = -80 - 150 * command.mTimer:total()
                        end
                    end
                }
    )
    )
    end
end

function Player:setMoveState(state)
    self.mMoveState = state
end

function Player:useItem(bagSlot, count)
    self:stopMining()
    self:stopMeleeAttacking()
    count = count or 1
    local item = self.mLocalProperty.mBag.mItems[bagSlot]
    count = math.min(count, item.mCount)
    local item_cfg = Config.Item[item.mConfigIndex]
    if item_cfg.mUse then
        if item_cfg.mUse.mPlayerPropertyChange then
            for k, v in pairs(item_cfg.mUse.mPlayerPropertyChange) do
                self.mLocalProperty[k] = self.mLocalProperty[k] + v * count
            end
        end
        item.mCount = item.mCount - count
        if item.mCount == 0 then
            self.mLocalProperty.mBag.mItems[bagSlot] = nil
        end
    end
    if Utility.arrayContain(item_cfg.mTypes, "建筑") then
        Game.singleton():getUIManager():close("Bag")
        Game.singleton():getManipulationManager():pushMode("Build")
        Game.singleton():getManipulationManager():getManipulator():setItem(
            item_cfg,
            function(parameter)
                Game.singleton():getManipulationManager():popMode()
                self:_untakeItem()
                if parameter then
                    self:getScene():createBuilding(
                        {
                            mItemConfigIndex = item.mConfigIndex,
                            mPivot = parameter.mPivot,
                            mFacing = parameter.mFacing,
                            mBlockDirection = parameter.mBlockDirection
                        }
                    )
                    item.mCount = item.mCount - count
                    if item.mCount == 0 then
                        self.mLocalProperty.mBag.mItems[bagSlot] = nil
                    end
                end
            end
        )
        if Utility.arrayContain(item_cfg.mTypes, "手持物") then
            if item.mBagSlot ~= self.mLocalProperty.mPlaceItem then
                self:_takeItem(item.mBagSlot)
            else
                self:_untakeItem()
            end
        end
    elseif Utility.arrayContain(item_cfg.mTypes, "武器") then
        local equip = self.mLocalProperty.mEquip
        if equip.mWeapon ~= item.mBagSlot then
            equip.mWeapon = item.mBagSlot
        else
            equip.mWeapon = nil
        end
        self:_updateEquipProperty()
    elseif Utility.arrayContain(item_cfg.mTypes, "帽子") then
        local equip = self.mLocalProperty.mEquip
        equip.mHead = item.mBagSlot
        self:_updateEquipProperty()
    elseif Utility.arrayContain(item_cfg.mTypes, "衣服") then
        local equip = self.mLocalProperty.mEquip
        equip.mBody = item.mBagSlot
        self:_updateEquipProperty()
    elseif Utility.arrayContain(item_cfg.mTypes, "裤子") then
        local equip = self.mLocalProperty.mEquip
        equip.mLeg = item.mBagSlot
        self:_updateEquipProperty()
    elseif Utility.arrayContain(item_cfg.mTypes, "鞋子") then
        local equip = self.mLocalProperty.mEquip
        equip.mFoot = item.mBagSlot
        self:_updateEquipProperty()
    elseif Utility.arrayContain(item_cfg.mTypes, "手套") then
        local equip = self.mLocalProperty.mEquip
        equip.mHand = item.mBagSlot
        self:_updateEquipProperty()
    elseif Utility.arrayContain(item_cfg.mTypes, "副手") then
        local equip = self.mLocalProperty.mEquip
        equip.mSecondWeapon = item.mBagSlot
        self:_updateEquipProperty()
    end
end

function Player:useBlueprint(slot, count)
    self:stopMining()
    self:stopMeleeAttacking()
    count = count or 1
    local blueprint = self.mLocalProperty.mBlueprints[slot]
    local blueprint_cfg = Config.Blueprint[blueprint.mConfigIndex]
    local bag_item_refs = {}
    for _, res in pairs(blueprint_cfg.mResources) do
        local item =
            Utility.arrayGet(
                self.mLocalProperty.mBag.mItems,
                function(item)
                    return Config.Item[item.mConfigIndex].mName == res.mName
                end
        )
        if item and item.mCount >= res.mCount then
            bag_item_refs[#bag_item_refs + 1] = {mItem = item, mCount = res.mCount}
        else
            return false
        end
    end
    for _, item in pairs(bag_item_refs) do
        item.mItem.mCount = item.mItem.mCount - item.mCount
    end
    self:_refreshBag()
    
    local crafting =
        Utility.arrayGet(
            self.mLocalProperty.mCrafting,
            function(e)
                return e.mSlot == slot
            end
    )
    if not crafting then
        crafting = {mSlot = slot, mTime = 0}
        self.mLocalProperty.mCrafting[#self.mLocalProperty.mCrafting + 1] = crafting
    end
    crafting.mCount = crafting.mCount or 0
    crafting.mCount = crafting.mCount + 1
    return true
end

function Player:setHandItem(handIndex, bagSlot)
    self.mLocalProperty.mHandItems[handIndex] = {mBagSlot = bagSlot}
end

function Player:unequip(equipType)
    self.mLocalProperty.mEquip["m" .. equipType] = nil
    self:_updateEquipProperty()
end

function Player:startMining(parameter)
    self:stopMeleeAttacking()
    self:stopMining()
    local weapon_config_index
    if self.mLocalProperty.mEquip.mWeapon then
        weapon_config_index = self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mConfigIndex
    end
    self.mMining = {mTime = 0}
    if parameter.mBlock then
        self.mMining.mBlockPosition = vector3d:new(parameter.mBlock)
    elseif parameter.mMonster then
        self.mMining.mMonsterID = parameter.mMonster:getID()
    end
    self:getEntity():GetInnerObject():SetField("UpperAnimID", 171)
    local send_to_parameter = {}
    send_to_parameter.mBlockPosition = self.mMining.mBlockPosition
    send_to_parameter.mToolConfigIndex = weapon_config_index
    send_to_parameter.mMonsterID = self.mMining.mMonsterID
    self:sendToHost(
        "StartMining",
        send_to_parameter
)
end

function Player:stopMining()
    if self.mMining then
        if self.mMining.mUIs then
            for _, ui in pairs(self.mMining.mUIs) do
                ui:destroy()
            end
            self.mMining.mUIs = nil
        end
        delete(self.mMining.mTimer)
        self.mMining.mTimer = nil
        self.mMining.mMonsterID = nil
        self.mMining.mBlockPosition = nil
        self.mMining.mToolConfigIndex = nil
        self.mMining = nil
        self:getEntity():GetInnerObject():SetField("UpperAnimID", -1)
        self:sendToHost("StopMining")
    end
end

function Player:startMeleeAttacking()
    self:stopMining()
    self:stopMeleeAttacking()
    self.mMeleeAttacking = {}
    self:getEntity():GetInnerObject():SetField("UpperAnimID", 171)
end

function Player:stopMeleeAttacking()
    if self.mMeleeAttacking then
        self:getEntity():GetInnerObject():SetField("UpperAnimID", -1)
        self.mMeleeAttacking = nil
    end
end

function Player:studyBlueprint(blueprintConfigIndex)
    local blueprint_cfg = Config.Blueprint[blueprintConfigIndex]
    if
        not self.mLocalProperty.mBlueprints[blueprintConfigIndex] and
        blueprint_cfg.mPoint <= self.mLocalProperty.mBlueprintPoint
    then
        self.mLocalProperty.mBlueprints[blueprintConfigIndex] = {mConfigIndex = blueprintConfigIndex}
        self.mLocalProperty.mBlueprintPoint = self.mLocalProperty.mBlueprintPoint - blueprint_cfg.mPoint
    end
end

function Player:placeItem(blockPosition)
    self:requestToHost(
        "PlaceItem",
        {
            mBlockPosition = blockPosition,
            mItemConfigIndex = self.mLocalProperty.mBag.mItems[self.mLocalProperty.mPlaceItem].mConfigIndex
        },
        function(parameter)
            if parameter.mResult then
                local item = self.mLocalProperty.mBag.mItems[self.mLocalProperty.mPlaceItem]
                local item_cfg = Config.Item[item.mConfigIndex]
                item.mCount = item.mCount - 1
                self:_refreshBag()
                if not self.mLocalProperty.mBag.mItems[self.mLocalProperty.mPlaceItem] then
                    self:_untakeItem()
                end
            end
        end
)
end

function Player:removeBagItem(bagSlot, count)
    local item = self.mLocalProperty.mBag.mItems[bagSlot]
    count = math.min(count, item.mCount)
    item.mCount = item.mCount - count
    self:_refreshBag()
end

function Player:_calculateFortitude()
    local cold_fortitude = GameUtility.calcPlayerFortitude(self.mLocalProperty.mFortitudeLevel)
    local hot_fortitude = GameUtility.calcPlayerFortitude(self.mLocalProperty.mFortitudeLevel)
    if self.mLocalProperty.mEquip then
        for _, equip in pairs(self.mLocalProperty.mEquip) do
            local cfg = Config.Item[self.mLocalProperty.mBag.mItems[equip].mConfigIndex]
            if cfg.mFortitude then
                hot_fortitude = hot_fortitude + (cfg.mFortitude.mHot or 0)
                cold_fortitude = cold_fortitude + (cfg.mFortitude.mCold or 0)
            end
        end
    end
    self.mLocalProperty.mFortitudeAdditions = self.mLocalProperty.mFortitudeAdditions or {}
    for _, addition in pairs(self.mLocalProperty.mFortitudeAdditions) do
        hot_fortitude = hot_fortitude + (addition.mHot or 0)
        cold_fortitude = cold_fortitude + (addition.mCold or 0)
    end
    return {mHot=hot_fortitude,mCold=cold_fortitude}
end

function Player:_updatePropertyChange(deltaTime)
    local temp = 20
    local is_in_water = IsInWater()
    
    if Game.singleton():getTime() then
        temp = GameUtility.calcTemperature(Game.singleton():getTime())
    end
    self.mLocalProperty.mExp = self.mLocalProperty.mExp + deltaTime * Config.Player.mExpObtainStand
    local level_up_exp = GameUtility.calcPlayerLevelUpExp(self.mLocalProperty.mLevel)
    if self.mLocalProperty.mExp >= level_up_exp then
        self.mLocalProperty.mLevel = self.mLocalProperty.mLevel + 1
        self.mLocalProperty.mPropertyPoint = self.mLocalProperty.mPropertyPoint + Config.Player.mLevelUpPropertyPoint
        self.mLocalProperty.mBlueprintPoint = self.mLocalProperty.mBlueprintPoint + Config.Player.mLevelUpBlueprintPoint
        self.mLocalProperty.mExp = self.mLocalProperty.mExp - level_up_exp
    end
    
    self.mLocalProperty.mFortitude = self:_calculateFortitude()
    
    local x, y, z = self:getEntity():GetBlockPos()
    local has_head_on_block
    local test = y
    while not has_head_on_block and test < y + 100 do
        has_head_on_block = Utility.hasBlock(x, test, z)
        test = test + 1
    end
    if (not has_head_on_block and Game.singleton():getProperty():cache().mWeather == Game.EWeather.Rain) or is_in_water then
        self.mLocalProperty.mWater = GameUtility.calcPlayerWater(self.mLocalProperty.mWaterLevel)
    end
    
    if is_in_water then
        self.mLocalProperty.mOxygen = self.mLocalProperty.mOxygen - deltaTime
    else
        self.mLocalProperty.mOxygen = self.mLocalProperty.mOxygen + deltaTime * 10
    end
    
    if self.mLocalProperty.mFood then
        local escape = Config.Player.mFoodCost * (1 + (20 - math.min(20, temp + self.mLocalProperty.mFortitude.mCold * 0.1)) * 0.1)
        self.mLocalProperty.mFood = self.mLocalProperty.mFood - deltaTime * escape
    end
    if self.mLocalProperty.mWater then
        local escape = Config.Player.mWaterCost * (1 + (math.max(20, temp - self.mLocalProperty.mFortitude.mHot * 0.1) - 20) * 0.1)
        self.mLocalProperty.mWater = self.mLocalProperty.mWater - deltaTime * escape
    end
    
    if math.min(20, temp + self.mLocalProperty.mFortitude.mCold * 0.1) < 0 or math.max(20, temp - self.mLocalProperty.mFortitude.mHot * 0.1) > 50 then
        self.mLocalProperty.mHP = self.mLocalProperty.mHP - deltaTime * 1
    end
    
    if self.mLocalProperty.mHP and self.mLocalProperty.mFood and self.mLocalProperty.mFood < 0 then
        self.mLocalProperty.mHP = self.mLocalProperty.mHP + self.mLocalProperty.mFood
        self.mLocalProperty.mFood = 0
    end
    if self.mLocalProperty.mHP and self.mLocalProperty.mWater and self.mLocalProperty.mWater < 0 then
        self.mLocalProperty.mHP = self.mLocalProperty.mHP + self.mLocalProperty.mWater
        self.mLocalProperty.mWater = 0
    end
    if self.mLocalProperty.mHP and self.mLocalProperty.mOxygen and self.mLocalProperty.mOxygen < 0 then
        self.mLocalProperty.mHP = self.mLocalProperty.mHP + self.mLocalProperty.mOxygen
        self.mLocalProperty.mOxygen = 0
    end
    
    self.mLocalProperty.mMovementSpeed = GameUtility.calcPlayerMovementSpeed(self.mLocalProperty.mMovementSpeedLevel)
    local jump_up_speed = 5
    if self.mLocalProperty.mStamina <= 0 then
        self:setMoveState("Walk")
    end
    if self.mMoveState == "Run" then
        self.mLocalProperty.mMovementSpeed = self.mLocalProperty.mMovementSpeed * Config.Player.mRunSpeedScale
        jump_up_speed = 10
        self.mLocalProperty.mStamina = self.mLocalProperty.mStamina - Config.Player.mRunCost * deltaTime
    end
    
    local recover_stamina = self.mMoveState == "Walk"
    if recover_stamina then
        if self.mLocalProperty.mStaminaRecoverInterval == 0 then
            self.mLocalProperty.mStamina =
                math.min(
                    self.mLocalProperty.mStamina + Config.Player.mStaminaRecoverSpeed * deltaTime,
                    GameUtility.calcPlayerStamina(self.mLocalProperty.mStaminaLevel)
        )
        else
            self.mLocalProperty.mStaminaRecoverInterval =
                math.max(self.mLocalProperty.mStaminaRecoverInterval - deltaTime, 0)
        end
    else
        self.mLocalProperty.mStaminaRecoverInterval = Config.Player.mStaminaRecoverInterval
    end
    self.mLocalProperty.mHP = self.mLocalProperty.mHP + Config.Player.mHPRecoverSpeed * deltaTime
    self.mLocalProperty.mHP = math.min(self.mLocalProperty.mHP, GameUtility.calcPlayerHP(self.mLocalProperty.mHPLevel))
    self.mLocalProperty.mDefense = 0
    if self.mLocalProperty.mEquip then
        for _, equip in pairs(self.mLocalProperty.mEquip) do
            local cfg = Config.Item[self.mLocalProperty.mBag.mItems[equip].mConfigIndex]
            self.mLocalProperty.mDefense = self.mLocalProperty.mDefense + (cfg.mDefense or 0)
        end
    end
    self:_limitProperty()
    
    if self.mLocalProperty.mHP < 20 then
        self.mLocalProperty.mMovementSpeed = 0.5
        jump_up_speed = 0.1
    end
    local weight_scalar = self:getWeight() / GameUtility.calcPlayerWeight(self.mLocalProperty.mWeightLevel)
    if weight_scalar > 0.8 then
        GetPlayer():SetSpeedScale(math.max(0, 1 - weight_scalar) * self.mLocalProperty.mMovementSpeed)
        jump_up_speed = 0.1
    else
        GetPlayer():SetSpeedScale(math.max(0, 1 - math.pow(weight_scalar, 3)) * self.mLocalProperty.mMovementSpeed)
    end
    GetPlayer().jump_up_speed = jump_up_speed
    
    if self.mLocalProperty.mHP <= 0 then
        self:_dead()
    end
end

function Player:_limitProperty()
    self.mLocalProperty.mExp =
        math.min(self.mLocalProperty.mExp, GameUtility.calcPlayerLevelUpExp(self.mLocalProperty.mLevel))
    self.mLocalProperty.mHP = math.min(self.mLocalProperty.mHP, GameUtility.calcPlayerHP(self.mLocalProperty.mHPLevel))
    self.mLocalProperty.mFood =
        math.min(self.mLocalProperty.mFood, GameUtility.calcPlayerFood(self.mLocalProperty.mFoodLevel))
    self.mLocalProperty.mFood = math.max(self.mLocalProperty.mFood, 0)
    self.mLocalProperty.mWater =
        math.min(self.mLocalProperty.mWater, GameUtility.calcPlayerWater(self.mLocalProperty.mWaterLevel))
    self.mLocalProperty.mWater = math.max(self.mLocalProperty.mWater, 0)
    self.mLocalProperty.mStamina =
        math.min(self.mLocalProperty.mStamina, GameUtility.calcPlayerStamina(self.mLocalProperty.mStaminaLevel))
    self.mLocalProperty.mStamina = math.max(self.mLocalProperty.mStamina, 0)
    self.mLocalProperty.mOxygen =
        math.min(self.mLocalProperty.mOxygen, GameUtility.calcPlayerOxygen(self.mLocalProperty.mOxygenLevel))
    self.mLocalProperty.mOxygen = math.max(self.mLocalProperty.mOxygen, 0)
    self.mLocalProperty.mFaint = math.max(self.mLocalProperty.mFaint, 0)
end

function Player:_emptyBagSlot()
    local slot = 1
    while self.mLocalProperty.mBag.mItems[slot] do
        slot = slot + 1
    end
    return slot
end

function Player:_refreshBag()
    for slot, item in pairs(self.mLocalProperty.mBag.mItems) do
        if item.mCount <= 0 then
            self.mLocalProperty.mBag.mItems[slot] = nil
        end
    end
    for k, item in pairs(self.mLocalProperty.mHandItems) do
        if not self.mLocalProperty.mBag.mItems[item.mBagSlot] then
            self.mLocalProperty.mHandItems[k] = nil
        end
    end
    if self.mLocalProperty.mPlaceItem and not self.mLocalProperty.mBag.mItems[self.mLocalProperty.mPlaceItem] then
        self:_untakeItem()
    end
end

function Player:_refreshCrafting(deltaTime)
    if self.mLocalProperty.mCrafting and self.mLocalProperty.mCrafting[1] then
        local crafting = self.mLocalProperty.mCrafting[1]
        crafting.mTime =
            crafting.mTime + deltaTime * GameUtility.calcPlayerCraftingSpeed(self.mLocalProperty.mCraftingSpeedLevel)
        local blueprint = self.mLocalProperty.mBlueprints[crafting.mSlot]
        local blueprint_cfg = Config.Blueprint[blueprint.mConfigIndex]
        if crafting.mTime < blueprint_cfg.mCraftingTime then
            return
        end
        if blueprint_cfg.mGenerates.mItems then
            for _, v in pairs(blueprint_cfg.mGenerates.mItems) do
                self.mLocalProperty.mExp = self.mLocalProperty.mExp + (v.mExp or 0)
                self:addBagItem(
                    {
                        mConfigIndex = Utility.arrayIndex(
                            Config.Item,
                            function(e)
                                return e.mName == v.mName
                            end
                        ),
                        mCount = v.mCount
                    }
            )
            end
        end
        self:_refreshBag()
        crafting.mCount = crafting.mCount - 1
        crafting.mTime = 0
        if crafting.mCount == 0 then
            table.remove(self.mLocalProperty.mCrafting, 1)
        end
    end
end

function Player:_equip(parameter, isHead)
    if not self:getEntity() then
        return
    end
    if isHead then
        self:getEntity():SetMainAssetPath("")
        self:getEntity():SetMainAssetPath(parameter.mModel["m" .. self.mProperty:cache().mSex].mFile)
        if parameter.mTextures then
            local cfgs = parameter.mTextures["m" .. self.mProperty:cache().mSex]
            for i, cfg in pairs(cfgs) do
                if cfg.mFile then
                    SetReplaceableTexture(self:getEntity(), cfg.mFile, i + 1)
                elseif cfg.mResource then
                    GetResourceImage(
                        cfg.mResource,
                        function(path, err)
                            SetReplaceableTexture(self:getEntity(), path, i + 1)
                        end
                )
                end
            end
        end
    else
        local replace_texture = {}
        if parameter.mTextures then
            local textures = parameter.mTextures["m" .. self.mProperty:cache().mSex] or parameter.mTextures
            replace_texture = textures[1]
        end
        local model = parameter.mModel["m" .. self.mProperty:cache().mSex] or parameter.mModel
        if replace_texture.mFile then
            AddCustomAvatarComponent(model.mFile, self:getEntity(), replace_texture.mFile)
        elseif replace_texture.mResource then
            GetResourceImage(
                replace_texture.mResource,
                function(path, err)
                    AddCustomAvatarComponent(model.mFile, self:getEntity(), path)
                end
        )
        else
            AddCustomAvatarComponent(model.mFile, self:getEntity())
        end
    end
end

function Player:_takeItem(bagSlot)
    self.mLocalProperty.mEquip.mWeapon = nil
    self:_updateEquipProperty()
    self.mLocalProperty.mPlaceItem = bagSlot
    local item = self.mLocalProperty.mBag.mItems[self.mLocalProperty.mPlaceItem]
    local item_cfg = Config.Item[item.mConfigIndex]
    if item_cfg.mBlockID then
        for i = 1, 9 do
            SetItemStackToInventory(i, CreateItemStack(item_cfg.mBlockID, 999))
        end
    end
end

function Player:_untakeItem()
    if self:isMainPlayer() then
        for i = 1, 9 do
            SetItemStackToInventory(i, {})
        end
        self.mLocalProperty.mPlaceItem = nil
    end
end

function Player:_refreshEquip()
    if not self.mProperty:cache().mSex or not self:getEntity() then
        return
    end
    OpenAvatar(self:getEntity())
    CloseAvatar(self:getEntity())
    local equip = {}
    if self.mProperty:cache().mEquip and self.mProperty:cache().mEquip.mHead then
        equip.mModel = Config.Item[self.mProperty:cache().mEquip.mHead].mModel
        equip.mTextures = Config.Item[self.mProperty:cache().mEquip.mHead].mTextures
    else
        equip.mModel = Config.Default.Avatar.Head.mModel
        equip.mTextures = Config.Default.Avatar.Head.mTextures
    end
    self:_equip(equip, true)
    equip = {}
    if self.mProperty:cache().mEquip and self.mProperty:cache().mEquip.mBody then
        equip.mModel = Config.Item[self.mProperty:cache().mEquip.mBody].mModel
        equip.mTextures = Config.Item[self.mProperty:cache().mEquip.mBody].mTextures
    else
        equip.mModel = Config.Default.Avatar.Body.mModel
    end
    self:_equip(equip)
    equip = nil
    if self.mProperty:cache().mEquip and self.mProperty:cache().mEquip.mBody then
        else
        equip = {}
        equip.mModel = Config.Default.Avatar.Body.mWaist.mModel
    end
    if equip then
        self:_equip(equip)
    end
    equip = {}
    if self.mProperty:cache().mEquip and self.mProperty:cache().mEquip.mLeg then
        equip.mModel = Config.Item[self.mProperty:cache().mEquip.mLeg].mModel
        equip.mTextures = Config.Item[self.mProperty:cache().mEquip.mLeg].mTextures
    else
        equip.mModel = Config.Default.Avatar.Leg.mModel
    end
    self:_equip(equip)
    equip = {}
    if self.mProperty:cache().mEquip and self.mProperty:cache().mEquip.mHand then
        equip.mModel = Config.Item[self.mProperty:cache().mEquip.mHand].mModel
        equip.mTextures = Config.Item[self.mProperty:cache().mEquip.mHand].mTextures
    else
        equip.mModel = Config.Default.Avatar.Hand.mModel
    end
    self:_equip(equip)
    equip = {}
    if self.mProperty:cache().mEquip and self.mProperty:cache().mEquip.mFoot then
        equip.mModel = Config.Item[self.mProperty:cache().mEquip.mFoot].mModel
        equip.mTextures = Config.Item[self.mProperty:cache().mEquip.mFoot].mTextures
    else
        equip.mModel = Config.Default.Avatar.Foot.mModel
    end
    self:_equip(equip)
    equip = {}
    if self.mProperty:cache().mEquip and self.mProperty:cache().mEquip.mWeapon then
        equip.mModel = Config.Item[self.mProperty:cache().mEquip.mWeapon].mModel
        equip.mTextures = Config.Item[self.mProperty:cache().mEquip.mWeapon].mTextures
        self:_equip(equip)
        equip = {}
        self:_untakeItem()
    end
end

function Player:addBagItem(parameter)
    local item_cfg = Config.Item[parameter.mConfigIndex]
    local bag_item =
        Utility.arrayGet(
            self.mLocalProperty.mBag.mItems,
            function(e)
                return e.mConfigIndex == parameter.mConfigIndex and (not item_cfg.mOverlap or item_cfg.mOverlap > e.mCount)
            end
    )
    if bag_item then
        bag_item.mCount = bag_item.mCount + parameter.mCount
        local count = 0
        if item_cfg.mOverlap then
            count = bag_item.mCount - item_cfg.mOverlap
            bag_item.mCount = item_cfg.mOverlap
        end
        if count > 0 then
            local slot = self:_emptyBagSlot()
            self.mLocalProperty.mBag.mItems[slot] = {
                mConfigIndex = parameter.mConfigIndex,
                mCount = 0,
                mHP = item_cfg.mHP,
                mBagSlot = slot
            }
            self:addBagItem({mConfigIndex = parameter.mConfigIndex, mCount = count})
        end
    else
        local slot = self:_emptyBagSlot()
        self.mLocalProperty.mBag.mItems[slot] = {
            mConfigIndex = parameter.mConfigIndex,
            mCount = 0,
            mHP = item_cfg.mHP,
            mBagSlot = slot
        }
        self:addBagItem(parameter)
    end
end

function Player:_dead()
    local player = Utility.getSavedData().mPlayer or {}
    player.mLevel = 0
    player.mExp = 0
    player.mHPLevel = 0
    player.mHP = GameUtility.calcPlayerHP(player.mHPLevel)
    player.mFoodLevel = 0
    player.mFood = GameUtility.calcPlayerFood(player.mFoodLevel)
    player.mWaterLevel = 0
    player.mWater = GameUtility.calcPlayerWater(player.mWaterLevel)
    player.mStaminaLevel = 0
    player.mStamina = GameUtility.calcPlayerStamina(player.mStaminaLevel)
    player.mOxygenLevel = 0
    player.mOxygen = GameUtility.calcPlayerOxygen(player.mOxygenLevel)
    player.mWeightLevel = 0
    player.mMovementSpeedLevel = 0
    player.mMeleeDamageLevel = 0
    player.mFortitudeLevel = 0
    player.mCraftingSpeedLevel = 0
    player.mPropertyPoint = Config.Player.mLevelUpPropertyPoint * player.mLevel
    player.mBlueprintPoint = Config.Player.mLevelUpBlueprintPoint * player.mLevel
    player.mFaint = 0
    player.mBag = {mItems = {}}
    player.mBlueprints = {}
    player.mHandItems = {}
    player.mCrafting = {}
    player.mEquip = nil
    player.mSex = nil
    
    local id = self.mID
    Game.singleton():getPlayerManager():_destroyPlayer(id)
    Game.singleton():getPlayerManager():_createPlayer(id)
    self:sendToHost("Dead")
end

function Player:_updateEquipProperty()
    self.mLocalProperty.mEquip = self.mLocalProperty.mEquip or {}
    local property = {}
    for k, v in pairs(self.mLocalProperty.mEquip) do
        property[k] = self.mLocalProperty.mBag.mItems[v].mConfigIndex
    end
    self.mProperty:safeWrite("mEquip", property)
end

function Player:_updateMining(deltaTime)
    if self.mMining then
        self.mMining.mTimer = self.mMining.mTimer or new(Utility.Timer)
        if not self.mMining.mUIs then
            self.mMining.mUIs = {}
            self.mMining.mUIs[1] =
                CreateUI(
                    {x = 0, y = 300, width = 1, height = 50, background_color = "0 255 0 255", type = "container"}
            )
            if self.mMining.mBlockPosition then
                self.mMining.mUIs[2] =
                    CreateUI(
                        {
                            x = -50,
                            y = 300,
                            width = 50,
                            height = 50,
                            background = CreateItemStack(
                                GetBlockId(
                                    self.mMining.mBlockPosition[1],
                                    self.mMining.mBlockPosition[2],
                                    self.mMining.mBlockPosition[3]
                            )
                            ):GetIcon(),
                            type = "container"
                        }
            )
            end
            self.mMining.mUIs[3] =
                CreateUI(
                    {
                        x = 0,
                        y = 300,
                        width = 300,
                        height = 50,
                        text = "采集中...",
                        type = "text",
                        font_size = 40,
                        text_format = 4
                    }
        )
        end
        self.mMining.mUIs[1].width =
            math.floor((self.mMining.mTimer:total() - math.floor(self.mMining.mTimer:total())) * 300)
        self.mMining.mUIs[1].x =
            math.floor((self.mMining.mTimer:total() - math.floor(self.mMining.mTimer:total())) * (-150))
        if self.mLocalProperty.mEquip and self.mLocalProperty.mEquip.mWeapon then
            local item_cfg =
                Config.Item[self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mConfigIndex]
            self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mHP =
                self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mHP -
                (item_cfg.mCollect.mSelfDamage or 0) * deltaTime
        end
        if (self.mMining.mBlockPosition and
            not Utility.hasBlock(
                self.mMining.mBlockPosition[1],
                self.mMining.mBlockPosition[2],
                self.mMining.mBlockPosition[3]
            ))
            or (self.mMining.mMonsterID and not Game.singleton():getMonsterManager():getMonsterByID(self.mMining.mMonsterID))
        then
            self:stopMining()
        else
            local x, y, z = self:getEntity():GetBlockPos()
            local dst_block_pos
            if self.mMining.mBlockPosition then
                dst_block_pos = self.mMining.mBlockPosition
            elseif self.mMining.mMonsterID then
                local x, y, z = Game.singleton():getMonsterManager():getMonsterByID(self.mMining.mMonsterID):getEntity():GetBlockPos()
                dst_block_pos = vector3d:new(x, y, z)
            end
            if (vector3d:new(x, y, z) - dst_block_pos):length() > 1.75 then
                --[[Framework.singleton():getBulletScreenManager():create(
                {mText = "距离太远，请靠近采集", mSpeed = 400, mLocalOnly = true}
                )]]
                self:stopMining()
            end
        end
    end
end

function Player:_updateMeleeAttack(deltaTime)
    if self.mMeleeAttacking then
        self.mMeleeAttacking.mTimer = self.mMeleeAttacking.mTimer or new(Utility.Timer)
        local attack_time = Config.Player.mMeleeAttackSpeed
        if self.mLocalProperty.mEquip and self.mLocalProperty.mEquip.mWeapon then
            local item_cfg =
                Config.Item[self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mConfigIndex]
            attack_time = item_cfg.mMeleeAttackSpeed or attack_time
        end
        if self.mMeleeAttacking.mTimer:total() >= attack_time then
            self:_meleeAttack()
            delete(self.mMeleeAttacking.mTimer)
            self.mMeleeAttacking.mTimer = nil
        end
    end
end

function Player:_meleeAttack()
    local center = self:getEntity():getPosition()
    if GetCameraMode() == 1 or GetCameraMode() == 3 then
        center = center + Utility.getCameraDirection() + vector3d:new(0, 0.5, 0)
    else
        center = center + Utility.convertFacingToDirection(self:getEntity():GetFacing()) + vector3d:new(0, 0.5, 0)
    end
    local attack_bound = new(Utility.AABB, {mCenter = center, mExtent = {1, 1, 1}})
    local monsters = Game.singleton():getMonsterManager():intersectMonsters({attack_bound})
    if monsters then
        for _, monster in pairs(monsters) do
            monster:onHit(self)
        end
        if self.mLocalProperty.mEquip and self.mLocalProperty.mEquip.mWeapon then
            local item_cfg =
                Config.Item[self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mConfigIndex]
            if item_cfg.mAttack then
                self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mHP =
                    self.mLocalProperty.mBag.mItems[self.mLocalProperty.mEquip.mWeapon].mHP -
                    (item_cfg.mAttack.mSelfDamage or 0)
            end
        end
    end
end

function Player:_save()
    if self:isMainPlayer() then
        local save_data = Utility.getSavedData() or {}
        save_data.mPlayer = clone(self.mLocalProperty)
        save_data.mPlayer.mSex = self.mProperty:cache().mSex
    end
end
