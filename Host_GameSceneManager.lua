local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Player = Devil.getTable("Game.Famine.Host_GamePlayer")
local Scene = Devil.getTable("Game.Famine.Host_GameScene")
local PlayerHome = Devil.getTable("Game.Famine.Host_GamePlayerHome")
local BigWorld = Devil.getTable("Game.Famine.Host_GameBigWorld")
local NatureScene = Devil.getTable("Game.Famine.Host_GameNatureScene")
local NatureSceneTaskDispatcher = Devil.getTable("Game.Famine.Host_GameNatureSceneTaskDispatcher")

local SceneManager = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_GameSceneManager"))
SceneManager.EScene = {PlayerHome = 1, Nature = 2, Custom = 3, BigWorld = 4}
function SceneManager:construction(parameter)
    self.mScenes = {}
    self.mNextSceneID = 1
    self.mSceneGenerateTime = 2
    self.mLeftCustomScenes = {}
    for k, v in pairs(Config.Terrain) do
        self.mLeftCustomScenes[#self.mLeftCustomScenes + 1] = k
    end
    self.mNatureSceneTaskDispatcher = new(NatureSceneTaskDispatcher)
    PlayerHome.initialize(--[[{{mScene = self:createScene({mType = SceneManager.EScene.BigWorld})}}]])
end

function SceneManager:destruction()
    Utility.deleteArray(self.mScenes)
    self.mScenes = nil
    self.mLeftCustomScenes = nil
    delete(self.mNatureSceneTaskDispatcher)
    self.mNatureSceneTaskDispatcher = nil
    PlayerHome.uninitialize()
end

function SceneManager:update(deltaTime)
    for _, scene in pairs(self.mScenes) do
        scene:update(deltaTime)
    end
    self.mNatureSceneTaskDispatcher:update(deltaTime)
end

function SceneManager:getNatureSceneTaskDispatcher()
    return self.mNatureSceneTaskDispatcher
end

function SceneManager:createScene(parameter)
    local scene_type = parameter.mType
    if not scene_type then
        scene_type = math.random(SceneManager.EScene.Nature, SceneManager.EScene.Custom)
    end
    local ret
    local init_members = {mID = self:_generateNextSceneID(), mType = scene_type}
    local home_x, home_y, home_z = Utility.getHomePosition()
    local aabb
    while scene_type ~= SceneManager.EScene.PlayerHome
        and scene_type ~= SceneManager.EScene.BigWorld
        and not aabb
    do
        local test_mod = self.mSceneGenerateTime % 2
        local test_dev = math.floor(self.mSceneGenerateTime / 2)
        local offset = vector3d:new({test_dev * 257, 0, test_dev * 257})
        if test_mod == 0 then
            offset[1] = offset[1] + 257
        elseif test_mod == 1 then
            offset[3] = offset[3] + 257
        end
        aabb = new(Utility.BlockAABB, {mPivot = vector3d:new({home_x, home_y + 1, home_z}) - offset, mVolume = vector3d:new(257, 129, 257)})
        for _, scene in pairs(self.mScenes) do
            if scene:getType() ~= SceneManager.EScene.PlayerHome and scene_type ~= SceneManager.EScene.BigWorld and scene:getBound():intersectAABB(aabb) then
                aabb = nil
                break
            end
        end
        if aabb and PlayerHome.sBound:intersectAABB(aabb) then
            aabb = nil
        end
        self.mSceneGenerateTime = self.mSceneGenerateTime + 1
    end
    if scene_type == SceneManager.EScene.Custom then
        if #self.mLeftCustomScenes > 0 then
            local index = math.random(1, #self.mLeftCustomScenes)
            local terrain_cfg_index = self.mLeftCustomScenes[index]
            table.remove(self.mLeftCustomScenes, index)
            ret = new(Scene, {mInitMembers = init_members, mTerrain = {mConfigIndex = terrain_cfg_index}, mBound = aabb, mEntries = {{mScene = parameter.mLastScene.mInstance, mEntryIndex = parameter.mLastScene.mEntryIndex}}})
        end
    elseif scene_type == SceneManager.EScene.PlayerHome then
        init_members.mPlayerID = parameter.mPlayerID
        ret = new(PlayerHome, {mInitMembers = init_members, mSceneManager = self})
    elseif scene_type == SceneManager.EScene.Nature then
        ret = new(NatureScene, {mInitMembers = init_members, mSceneManager = self, mBound = aabb, mSamplerSize = parameter.mSamplerSize or 256, mSeed = math.random(0, 1000000000), mEntries = {{mScene = parameter.mLastScene.mInstance, mEntryIndex = parameter.mLastScene.mEntryIndex}}})
    elseif scene_type == SceneManager.EScene.BigWorld then
        ret = new(BigWorld, {mInitMembers = init_members, mSceneManager = self, mSamplerSize = parameter.mSamplerSize or 2048, mSeed = math.random(0, 1000000000), mZeroPosition = {math.ceil((home_x + 512) / 16) * 16, home_y + 1, math.ceil(home_z / 16) * 16}, mEntries = {{mPlayerHome = true, mEntryIndex = 1}}})
    end
    if ret then
        self.mScenes[#self.mScenes + 1] = ret
        local para = clone(init_members)
        para.mType = scene_type
        para.mSlot = ret.mSlot
        if ret.mNoise then
            para.mSeed = ret.mNoise.mSeed
        end
        para.mSamplerSize = ret.mSamplerSize
        para.mBound = {mPivot = ret:getBound().mPivot, mVolume = ret:getBound().mVolume}
        self:broadcast("CreateScene", para)
    end
    return ret
end

function SceneManager:getSceneByID(id)
    return Utility.arrayGet(self.mScenes, function(e) return e:getID() == id end)
end

function SceneManager:getSceneByPosition(blockPosition)
    for _, scene in pairs(self.mScenes) do
        if scene:getBound():containPoint(blockPosition) then
            return scene
        end
    end
end

function SceneManager:getPlayerHome(playerID)
    playerID = playerID or GetPlayerId()
    return Utility.arrayGet(self.mScenes, function(e) return e.mPlayerID == playerID end)
end

function SceneManager:getBigWorld()
    return Utility.arrayGet(self.mScenes, function(e) return e:getType() == SceneManager.EScene.BigWorld end)
end

function SceneManager:destroyScene(scene)
    self:broadcast("DestroyScene", {mID = scene:getID()})
    Utility.arrayRemove(
        self.mScenes,
        function(e)
            return e == scene
        end,
        delete
)
end

function SceneManager:_getSendKey()
    return "GameSceneManager"
end

function SceneManager:_receiveMessage(parameter)
    if parameter.mMessage == "Sync" then
        local scenes = {}
        for _, scene in pairs(self.mScenes) do
            local s = {mID = scene:getID(), mType = scene:getType(), mSlot = scene:getSlot(), mBound = {mPivot = scene:getBound().mPivot, mVolume = scene:getBound().mVolume}, mPlayerID = scene:getPlayerID()}
            if scene.mNoise then
                s.mSeed = scene.mNoise.mSeed
            end
            s.mSamplerSize = scene.mSamplerSize
            s.mBuildings = {}
            for k, building in pairs(scene.mBuildings) do
                s.mBuildings[k] = {mItemConfigIndex = building.mItemConfigIndex, mParentID = building.mParentID, mPivot = building.mPivot, mFacing = building.mFacing, mBlockDirection = building.mBlockDirection}
            end
            scenes[#scenes + 1] = s
        end
        self:responseToClient(parameter.mFrom, "Sync", {mResponseCallbackKey = parameter.mParameter.mResponseCallbackKey, mScenes = scenes})
    end
end

function SceneManager:_generateNextSceneID()
    local ret = self.mNextSceneID
    self.mNextSceneID = self.mNextSceneID + 1
    return ret
end
