local WeaponSystem = require("WeaponSystem")
local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local UI = Devil.getTable("Game.Famine.UI")
local Host_Game = Devil.getTable("Game.Famine.Host_Game")
local Client_Game = Devil.getTable("Game.Famine.Client_Game")

function main()
    Framework.singleton()
    if Utility.isHost() then
        Host_Game.singleton()
    else
        echotable("not Utility.isHost")
    end
    Client_Game.singleton()
end

function clear()
    delete(Client_Game.msInstance)
    delete(Host_Game.msInstance)
    delete(Framework.msInstance)
end

-- 获取输入
function handleInput(event)
    if Framework.msInstance then
        Framework.singleton():handleInput(event)
    end
    return WeaponSystem.input(event)
end

function receiveMsg(parameter)
    if parameter.mMessage == "Clear" then
        clear()
    end
    if Framework.msInstance then
        Framework.singleton():receiveMsg(parameter)
    end
end

local gTimer = new(Utility.Timer)
function update()
    local delta_time = gTimer:delta()
    if Framework.msInstance then
        Framework.singleton():update(delta_time)
    end
    if Host_Game.msInstance then
        Host_Game.singleton():update(delta_time)
    end
    if Client_Game.msInstance then
        Client_Game.singleton():update(delta_time)
    end
end