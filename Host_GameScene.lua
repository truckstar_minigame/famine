local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Host_Game")
local Config = Devil.getTable("Game.Famine.Config")
local Building = Devil.getTable("Game.Famine.Host_GameBuilding")
local SceneManager = Devil.getTable("Game.Famine.Host_GameSceneManager")

local Scene = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_GameScene"))

function Scene:construction(parameter)
    echo("devilwalk", "Host_GameScene:construction:self.mID:" .. tostring(self.mID))
    self.mProperty = new(Devil.getTable("Game.Famine.GameSceneProperty"))
    self.mEntries = parameter.mEntries
    self.mBound = parameter.mBound
    self.mSceneManager = parameter.mSceneManager
    self.mBuildings = {}
    if self.mEntries then
        for k, entry in pairs(self.mEntries) do
            if entry.mScene and entry.mScene:getType() == SceneManager.EScene.PlayerHome then
                entry.mScene = nil
                entry.mPlayerHome = true
            end
            self:_updateEntryBillboard(k)
        end
    end
    if Config.mDebug then
        if self.mBound then
            for x = self.mBound:min()[1], self.mBound:max()[1] do
                for z = self.mBound:min()[3], self.mBound:max()[3] do
                    for y = self.mBound:min()[2], 250 do
                        local entities = GetEntitiesInBlock(x, y, z)
                        if entities then
                            for k, v in pairs(entities) do
                                local p = GetEntityById(k.entityId)
                                if p and (p:GetType() ~= "PlayerMP" and p:GetType() ~= "Player") then
                                    p:SetDead(true)
                                end
                            end
                        end
                        SetBlock(x, y, z, 0)
                    end
                end
            end
        end
    end
    if parameter.mTerrain then
        if parameter.mTerrain.mConfigIndex then
            self.mTerrainConfigIndex = parameter.mTerrain.mConfigIndex
            self.mTerrain = new(Framework.Terrain, {mTemplate = Config.Terrain[parameter.mTerrain.mConfigIndex].mTemplate, mOffset = self.mBound:min()})
            self.mTerrain:applyTemplate(function(block)
                if block[4] == 200 then
                    local added = false
                    for k, entry in pairs(self.mEntries) do
                        if not entry.mPosition then
                            entry.mPosition = self.mBound:min() + vector3d:new(block[1], block[2], block[3])
                            self:_updateEntryBillboard(k)
                            added = true
                            break
                        end
                    end
                    if not added then
                        self.mEntries[#self.mEntries + 1] = {mPosition = self.mBound:min() + vector3d:new(block[1], block[2], block[3])}
                    end
                end
            end, function()
                for i, entry in pairs(self.mEntries) do
                    if not entry.mPosition then
                        entry.mPosition = self.mBound:min() + vector3d:new(2 * i, 50, 2)
                    end
                end
            end)
        end
    end
end

function Scene:destruction()
    if Config.mDebug and self.mBound then
        for x = self.mBound:min()[1], self.mBound:max()[1] do
            for z = self.mBound:min()[3], self.mBound:max()[3] do
                for y = self.mBound:min()[2], 250 do
                    local entities = GetEntitiesInBlock(x, y, z)
                    if entities then
                        for k, v in pairs(entities) do
                            local p = GetEntityById(k.entityId)
                            if p and (p:GetType() ~= "PlayerMP" and p:GetType() ~= "Player") then
                                p:SetDead(true)
                            end
                        end
                    end
                    SetBlock(x, y, z, 0)
                end
            end
        end
    end
    delete(self.mProperty)
    self.mProperty = nil
    Utility.deleteArray(self.mBuildings)
    self.mBuildings = nil
    delete(self.mTerrain)
    self.mTerrain = nil
    self.mEntries = nil
    delete(self.mBound)
    self.mBound = nil
    self.mSceneManager = nil
end

function Scene:update(deltaTime)
    for _, building in pairs(self.mBuildings) do
        building:update(deltaTime)
    end
end

function Scene:getID()
    return self.mID
end

function Scene:getType()
    return self.mType
end

function Scene:getBound()
    return self.mBound
end

function Scene:getPlayerID()
end

function Scene:getSlot()
end

function Scene:getTerrainConfigIndex()
    return self.mTerrainConfigIndex
end

function Scene:getNextScene(blockPosition)
    local entry_index = Utility.arrayIndex(self.mEntries, function(e)
        return vector3d:new(e.mPosition):equals(vector3d:new(blockPosition))
    end)
    if not entry_index then
        return
    end
    local entry = self.mEntries[entry_index]
    if not entry.mPlayerHome then
        if not entry.mScene then
            entry.mScene = self.mSceneManager:createScene({mLastScene = {mInstance = self, mEntryIndex = entry_index}})
            self:_updateEntryBillboard(entry_index)
        end
        return entry.mScene
    else
        return "PlayerHome"
    end
end

function Scene:getTravelIndex(scene)
    for index, entry in pairs(self.mEntries) do
        if entry.mScene == scene
            or ((scene:getType() == SceneManager.EScene.PlayerHome) and entry.mPlayerHome)
        then
            return index
        end
    end
end

function Scene:getTravelPosition(travelIndex, callback)
    Framework.singleton():getCommandQueueManager():post(new(Command_Callback, {mDebug = "Scene:getTravelPosition", mExecutingCallback = function(command)
        if self.mIsDelete then
            callback()
            command.mState = Command.EState.Finish
            return
        end
        if (not self.mTerrain or self.mTerrain:isApplyComplete()) and self.mEntries[travelIndex] and self.mEntries[travelIndex].mPosition then
            callback(self.mEntries[travelIndex].mPosition + vector3d:new(1, 0, 1))
            command.mState = Command.EState.Finish
        end
    end}))
end

function Scene:destroyBuilding(building)
    local parameter = {mID = building:getID()}
    Utility.arrayRemove(self.mBuildings, function(e) return e:getID() == building:getID() end)
    delete(building)
    self:broadcast("DestroyBuilding", parameter)
end

function Scene:intersectBuilding(bound, childOnly)
    local buildings = Utility.arrayGet(self.mBuildings, function(building)
        return building:getBound():intersectAABB(bound)
    end, true)
    if buildings and childOnly then
        local parents = {}
        for _, building in pairs(buildings) do
            if building:getParent() then
                parents[tostring(building)] = building:getParent()
            end
        end
        for _, building in pairs(buildings) do
            if not Utility.arrayGet(parents, function(e) return e == building end) then
                buildings = {building}
                break
            end
        end
        parents = nil
    end
    return buildings
end

function Scene:intersectBuildingPoint(point, childOnly)
    local buildings = Utility.arrayGet(self.mBuildings, function(building)
        return building:getBound():containPoint(point)
    end, true)
    if buildings and childOnly then
        local parents = {}
        for _, building in pairs(buildings) do
            if building:getParent() then
                parents[tostring(building)] = building:getParent()
            end
        end
        for _, building in pairs(buildings) do
            if not Utility.arrayGet(parents, function(e) return e == building end) then
                buildings = {building}
                break
            end
        end
        parents = nil
    end
    return buildings
end

function Scene:getBuildingByID(id)
    return Utility.arrayGet(self.mBuildings, function(e) return e:getID() == id end)
end

function Scene:_getSendKey()
    return "GameScene" .. tostring(self.mID)
end

function Scene:_receiveMessage(parameter)
    if parameter.mMessage == "CreateBuilding" then
        local building = self:_createBuilding(parameter.mParameter)
        local param = clone(parameter.mParameter)
        param.mID = building:getID()
        param.mParentID = building.mParentID
        self:broadcast("CreateBuilding", param)
    elseif parameter.mMessage == "DestroyBuilding" then
        Utility.arrayRemove(self.mBuildings, function(e) return e:getID() == parameter.mParameter.mID end, delete)
        self:broadcast("DestroyBuilding", parameter.mParameter)
    end
end

function Scene:_createBuilding(parameter)
    local building_parameter = clone(parameter)
    building_parameter.mInitMembers = {mPlayerID = self.mPlayerID, mID = self:_generateNextBuildingID(), mSceneID = self.mID}
    local item_cfg = Config.Item[parameter.mItemConfigIndex]
    local building_cfg = Utility.arrayGet(Config.Building, function(e) return e.mName == item_cfg.mName end)
    local ret
    if building_cfg.mTypes and Utility.arrayGet(building_cfg.mTypes, function(t) return t == "房屋" end) then
        ret = new(Building.House, building_parameter)
    elseif building_cfg.mTypes and (Utility.arrayContain(building_cfg.mTypes, "门") or Utility.arrayContain(building_cfg.mTypes, "窗")) then
        ret = new(Building.HouseComponent, building_parameter)
    else
        ret = new(Building, building_parameter)
    end
    self.mBuildings[#self.mBuildings + 1] = ret
    return ret
end

function Scene:_updateEntryBillboard(index)
    local pos = self.mEntries[index].mPosition
    if not pos
        or (not self.mEntries[index].mScene and not self.mEntries[index].mPlayerHome)
    then
        return
    end
    Utility.setBlock(pos[1], pos[2] + 1, pos[3], 211)
    local SignIO = require("SignIO")
    local sign = SignIO:new(pos[1], pos[2] + 1, pos[3]);
    if self.mEntries[index].mScene then
        if self.mEntries[index].mScene:getTerrainConfigIndex() then
            sign:write("传送点:\n" .. Config.Terrain[self.mEntries[index].mScene:getTerrainConfigIndex()].mName)
        elseif self.mEntries[index].mScene:getType() == SceneManager.EScene.BigWorld then
            sign:write("传送点:\n大世界")
        elseif self.mEntries[index].mScene:getType() == SceneManager.EScene.Nature then
            sign:write("传送点:\n神奇世界" .. tostring(self.mEntries[index].mScene:getID()))
        end
    elseif self.mEntries[index].mPlayerHome then
        sign:write("传送点:\n基地")
    end
end

function Scene:_generateNextBuildingID()
    self.mNextBuildingID = self.mNextBuildingID or 0
    self.mNextBuildingID = self.mNextBuildingID + 1
    return self.mNextBuildingID
end
