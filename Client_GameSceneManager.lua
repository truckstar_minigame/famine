local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Player = Devil.getTable("Game.Famine.Client_GamePlayer")
local Scene = Devil.getTable("Game.Famine.Client_GameScene")
local PlayerHome = Devil.getTable("Game.Famine.Client_GamePlayerHome")
local Game = Devil.getTable("Game.Famine.Client_Game")
local NatureScene = Devil.getTable("Game.Famine.Client_GameNatureScene")
local NatureSceneTaskReceiver = Devil.getTable("Game.Famine.Client_GameNatureSceneTaskReceiver")

local SceneManager = inherit(Devil.Framework.ClientObjectBase, Devil.getTable("Game.Famine.Client_GameSceneManager"))
SceneManager.EScene = {PlayerHome = 1, Nature = 2, Custom = 3, BigWorld = 4}
function SceneManager:construction(parameter)
    self.mScenes = {}
    self.mNatureSceneTaskReceiver = new(NatureSceneTaskReceiver,{})
    self:requestToHost("Sync",nil,function(parameter)
        for _,scene in pairs(parameter.mScenes) do
            local s = self:_createScene(scene)
            for _,building in pairs(scene.mBuildings) do
                s:_createBuilding(building)
            end
        end
    end)
end

function SceneManager:destruction()
    delete(self.mNatureSceneTaskReceiver)
    self.mNatureSceneTaskReceiver = nil
    Utility.deleteArray(self.mScenes)
    self.mScenes = nil
end

function SceneManager:update(deltaTime)
    for _, scene in pairs(self.mScenes) do
        scene:update(deltaTime)
    end
    self.mNatureSceneTaskReceiver:update(deltaTime)
end

function SceneManager:getSceneByPosition(blockPosition)
    return Utility.arrayGet(self.mScenes,function(e)return e:getBound():containPoint(blockPosition) end)
end

function SceneManager:getSceneByID(id)
    return Utility.arrayGet(self.mScenes,function(e)return e:getID() == id end)
end

function SceneManager:getPlayerHome(playerID)
    playerID = playerID or GetPlayerId()
    return Utility.arrayGet(self.mScenes,function(e)return e.mPlayerID == playerID end)
end

function SceneManager:_getSendKey()
    return "GameSceneManager"
end

function SceneManager:_receiveMessage(parameter)
    if parameter.mMessage == "CreateScene" then
        self:_createScene(parameter.mParameter)
    elseif parameter.mMessage == "DestroyScene" then
        self:_destroyScene(parameter.mParameter)
    end
end

function SceneManager:_createScene(parameter)
    local ret
    local param = {mInitMembers = {mID = parameter.mID,mType = parameter.mType,mPlayerID = parameter.mPlayerID},mBound = new(Utility.BlockAABB,parameter.mBound),mSlot = parameter.mSlot,mSeed = parameter.mSeed,mSamplerSize = parameter.mSamplerSize}
    if parameter.mType == SceneManager.EScene.PlayerHome then
        ret = new(PlayerHome, param)
    elseif parameter.mType == SceneManager.EScene.Nature then
        ret = new(NatureScene,param)
    else
        ret = new(Scene, param)
    end
    if ret then
        self.mScenes[#self.mScenes + 1] = ret
    end
    return ret
end

function SceneManager:_destroyScene(parameter)
    Utility.arrayRemove(
        self.mScenes,
        function(e)
            return e:getID() == parameter.mID
        end,
        delete
)
end