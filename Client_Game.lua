local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local GameUtility = Devil.getTable("Game.Famine.Utility")
local PlayerManager = Devil.getTable("Game.Famine.Client_GamePlayerManager")
local SceneManager = Devil.getTable("Game.Famine.Client_GameSceneManager")
local UIManager = Devil.getTable("Game.Famine.UIManager")
local ManipulationManager = Devil.getTable("Game.Famine.ManipulationManager")
local MonsterManager = Devil.getTable("Game.Famine.Client_GameMonsterManager")

local Game = inherit(Devil.Framework.ClientObjectBase, Devil.getTable("Game.Famine.Client_Game"))
Game.EWeather = {Sun = 1, Rain = 2}
function Game.singleton()
    if not Game.msInstance then
        Game.msInstance = new(Game)
        Game.msInstance:initialize()
    end
    return Game.msInstance
end

function Game:construction(parameter)
    local saved_data = Utility.getSavedData() or {}
    for k,_ in pairs(saved_data) do
        saved_data[k] = nil
    end
end

function Game:destruction()
    self.mProperty:removePropertyListener("mWeather", self)
    delete(self.mSceneManager)
    self.mSceneManager = nil
    delete(self.mPlayerManager)
    self.mPlayerManager = nil
    delete(self.mUIManager)
    self.mUIManager = nil
    delete(self.mManipulationManager)
    self.mManipulationManager = nil
    delete(self.mMonsterManager)
    self.mMonsterManager = nil
    delete(self.mProperty)
    self.mProperty = nil
    self.mTime = nil
    Game.msInstance = nil
end

function Game:initialize()
    self.mProperty = new(Devil.Game.Famine.GameProperty)
    self.mUIManager = new(UIManager)
    self.mManipulationManager = new(ManipulationManager)
    self.mSceneManager = new(SceneManager)
    self.mPlayerManager = new(PlayerManager)
    self.mMonsterManager = new(MonsterManager)
    
    self.mUIManager:show("InGame")
    
    self:requestToHost("GetTime", nil, function(parameter)
        self.mTime = parameter.mTime
    end)
    self.mProperty:addPropertyListener("mWeather", self, function(_, value, preValue)
        if value == Game.EWeather.Sun then
            cmd("/rain 1")
            cmd("/rain 0")
        elseif value == Game.EWeather.Rain then
            if self.mTime then
                if GameUtility.calcTemperature(self.mTime) <= 0 then
                    cmd("/snow 10")
                else
                    cmd("/rain 10")
                end
            else
                Framework.singleton():getCommandQueueManager():post(new(Command_Callback,{mDebug = "Weather",mExecutingCallback = function(command)
                                if self.mTime then
                                    if GameUtility.calcTemperature(self.mTime) <= 0 then
                                        cmd("/snow 10")
                                    else
                                        cmd("/rain 10")
                                    end
                                    command.mState = Command.EState.Finish
                                end
                            end}))
            end
        end
    end)
end

function Game:getProperty()
    return self.mProperty
end

function Game:getPlayerManager()
    return self.mPlayerManager
end

function Game:getSceneManager()
    return self.mSceneManager
end

function Game:getUIManager()
    return self.mUIManager
end

function Game:getManipulationManager()
    return self.mManipulationManager
end

function Game:getMonsterManager()
    return self.mMonsterManager
end

function Game:getTime()
    return self.mTime
end

function Game:update(deltaTime)
    self.mPlayerManager:update(deltaTime)
    self.mSceneManager:update(deltaTime)
    self.mUIManager:update(deltaTime)
    self.mManipulationManager:update(deltaTime)
    if self.mTime then
        self.mTime = self.mTime + deltaTime
        local percent = (self.mTime % (60 * 24)) / (60 * 24) * 2 - 1
        if self.mLastPercent ~=  Utility.processFloat(percent,1) then
            self.mLastPercent = Utility.processFloat(percent,1)
            cmd("/time " .. tostring(self.mLastPercent))
        end
    end
end

function Game:_getSendKey()
    return "Game"
end

function Game:_receiveMessage(parameter)
end
