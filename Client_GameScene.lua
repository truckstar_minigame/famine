local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Client_Game")
local Building = Devil.getTable("Game.Famine.Client_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")

local Scene = inherit(Devil.Framework.ClientObjectBase, Devil.getTable("Game.Famine.Client_GameScene"))
function Scene:construction(parameter)
    echo("devilwalk", "Client_GameScene:construction:self.mID:" .. tostring(self.mID))
    self.mBound = parameter.mBound
    self.mProperty = new(Devil.getTable("Game.Famine.GameSceneProperty"))
    self.mBuildings = {}
end

function Scene:destruction()
    delete(self.mProperty)
    self.mProperty = nil
    Utility.deleteArray(self.mBuildings)
    self.mBuildings = nil
end

function Scene:getID()
    return self.mID
end

function Scene:update(deltaTime)
    for _, building in pairs(self.mBuildings) do
        building:update(deltaTime)
    end
end

function Scene:getType()
    return self.mType
end

function Scene:getBound()
    return self.mBound
end

function Scene:canMine()
    return true
end

function Scene:getPlayerID()
end

function Scene:createBuilding(parameter)
    self:sendToHost("CreateBuilding", {mPivot = parameter.mPivot, mItemConfigIndex = parameter.mItemConfigIndex, mFacing = parameter.mFacing, mBlockDirection = parameter.mBlockDirection})
end

function Scene:destroyBuilding(id)
    self:sendToHost("DestroyBuilding", {mID = id})
end

function Scene:intersectBuilding(bound, childOnly)
    local buildings = Utility.arrayGet(self.mBuildings, function(building)
        return building:getBound():intersectAABB(bound)
    end, true)
    if buildings and childOnly then
        local parents = {}
        for _, building in pairs(buildings) do
            if building:getParent() then
                parents[tostring(building)] = building:getParent()
            end
        end
        for _, building in pairs(buildings) do
            if not Utility.arrayGet(parents, function(e) return e == building end) then
                buildings = {building}
                break
            end
        end
        parents = nil
    end
    return buildings
end

function Scene:intersectBuildingPoint(point, childOnly)
    local buildings = Utility.arrayGet(self.mBuildings, function(building)
        return building:getBound():containPoint(point)
    end, true)
    if buildings and childOnly then
        local parents = {}
        for _, building in pairs(buildings) do
            if building:getParent() then
                parents[tostring(building)] = building:getParent()
            end
        end
        for _, building in pairs(buildings) do
            if not Utility.arrayGet(parents, function(e) return e == building end) then
                buildings = {building}
                break
            end
        end
        parents = nil
    end
    return buildings
end

function Scene:canBuild(parameter)
    if not self:getBound():containAABB(parameter.mBound) then
        return
    end
    for _, building in pairs(self.mBuildings) do
        local can_build = building:canBuild(parameter)
        if can_build == true then
            return true
        elseif not can_build then
            return
        end
    end
    if parameter.mConfig.mTypes and (Utility.arrayContain(parameter.mConfig.mTypes, "门") or Utility.arrayContain(parameter.mConfig.mTypes, "窗")) then
        return
    end
    local blocks = parameter.mBound:blocks()
    for _, block in pairs(blocks) do
        if (not Utility.arrayGet(self.mBuildings, function(e) return e:getBound():containPoint(block) end)) and Utility.hasBlock(block[1], block[2], block[3]) then
            return
        end
    end
    blocks = nil
    
    return "NotCare"
end

function Scene:getBuildingByID(id)
    return Utility.arrayGet(self.mBuildings,function(e)return e:getID() == id end)
end

function Scene:_getSendKey()
    return "GameScene" .. tostring(self.mID)
end

function Scene:_receiveMessage(parameter)
    if parameter.mMessage == "CreateBuilding" then
        self:_createBuilding(parameter.mParameter)
    elseif parameter.mMessage == "DestroyBuilding" then
        Utility.arrayRemove(self.mBuildings, function(e) return e:getID() == parameter.mParameter.mID end, delete)
    end
end

function Scene:_createBuilding(parameter)
    local building_parameter = clone(parameter)
    building_parameter.mInitMembers = {mPlayerID = self.mPlayerID, mID = parameter.mID, mSceneID = self.mID}
    local item_cfg = Config.Item[parameter.mItemConfigIndex]
    local building_cfg = Utility.arrayGet(Config.Building,function(e)return e.mName == item_cfg.mName end)
    local ret
    if building_cfg.mTypes and Utility.arrayGet(building_cfg.mTypes, function(t) return t == "房屋" end) then
        ret = new(Building.House, building_parameter)
    elseif building_cfg.mTypes and (Utility.arrayContain(building_cfg.mTypes, "门") or Utility.arrayContain(building_cfg.mTypes, "窗")) then
        ret = new(Building.HouseComponent, building_parameter)
    else
        ret = new(Building, building_parameter)
    end
    self.mBuildings[#self.mBuildings + 1] = ret
end
