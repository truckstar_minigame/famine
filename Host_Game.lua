local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local UI = Devil.getTable("Game.Famine.UI")
local PlayerManager = Devil.getTable("Game.Famine.Host_GamePlayerManager")
local SceneManager = Devil.getTable("Game.Famine.Host_GameSceneManager")
local ResourceManager = Devil.getTable("Game.Famine.Host_GameResourceManager")
local MonsterManager = Devil.getTable("Game.Famine.Host_GameMonsterManager")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Config = Devil.getTable("Game.Famine.Config")

local Game = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_Game"))
Game.EWeather = {Sun = 1, Rain = 2}
function Game.singleton()
    if not Game.msInstance then
        Game.msInstance = new(Game)
        Game.msInstance:initialize()
    end
    return Game.msInstance
end

function Game:construction(parameter)
    --Config.mDebug = true
end

function Game:destruction()
    delete(self.mPlayerManager)
    self.mPlayerManager = nil
    delete(self.mSceneManager)
    self.mSceneManager = nil
    delete(self.mResourceManager)
    self.mResourceManager = nil
    delete(self.mMonsterManager)
    self.mMonsterManager = nil
    self.mProperty:safeWrite("mWeather")
    delete(self.mProperty)
    self.mProperty = nil
    self.mTime = nil
    Game.msInstance = nil
end

function Game:initialize()
    self.mProperty = new(Devil.Game.Famine.GameProperty)
    self.mSceneManager = new(SceneManager)
    self.mPlayerManager = new(PlayerManager)
    self.mResourceManager = new(ResourceManager)
    self.mMonsterManager = new(MonsterManager)
    local game = Utility.getSavedData().mGame or {}
    game.mTime = game.mTime or (7 * 60 + 60 * 24 * 30 * 8)
    self.mTime = game.mTime

    self:_nextWeather()
end

function Game:getPlayerManager()
    return self.mPlayerManager
end

function Game:getSceneManager()
    return self.mSceneManager
end

function Game:getResourceManager()
    return self.mResourceManager
end

function Game:getMonsterManager()
    return self.mMonsterManager
end

function Game:update(deltaTime)
    self.mPlayerManager:update(deltaTime)
    self.mSceneManager:update(deltaTime)
    self.mResourceManager:update(deltaTime)
    self.mMonsterManager:update(deltaTime)
    self:_updateWeather()
    self.mTime = self.mTime + deltaTime
    self:_save()
end

function Game:_getSendKey()
    return "Game"
end

function Game:_receiveMessage(parameter)
    if parameter.mMessage == "GetTime" then
        self:responseToClient(
            parameter.mFrom,
            "GetTime",
            {mTime = self.mTime, mResponseCallbackKey = parameter.mParameter.mResponseCallbackKey}
    )
    end
end

function Game:_updateWeather()
    if self.mUpdateWeather ~= GameUtility.calcTime(self.mTime).mDay then
        self:_nextWeather()
    end
end

function Game:_nextWeather()
    local value = math.random(Game.EWeather.Sun, Game.EWeather.Rain)
    self.mProperty:safeWrite("mWeather", value)
    self.mUpdateWeather = GameUtility.calcTime(self.mTime).mDay
end

function Game:_save()
    local game = Utility.getSavedData().mGame or {}
    game.mTime = self.mTime
end