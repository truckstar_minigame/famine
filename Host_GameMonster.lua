local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Config = Devil.getTable("Game.Famine.Config")
local Game = Devil.getTable("Game.Famine.Host_Game")
local GameUtility = Devil.getTable("Game.Famine.Utility")
local Monster = inherit(Devil.Framework.HostObjectBase, Devil.getTable("Game.Famine.Host_GameMonster"))
function Monster:construction(parameter)
    self.mConfigIndex = parameter.mConfigIndex
    self.mProperty = new(Devil.Game.Famine.GameMonsterProperty, {mID = self.mID})
    self.mEnemies = {}
    
    self.mEntityClientKey =
        Framework.singleton():getEntityCustomManager():createEntity(
            {
                mType = "EntityNPCOnline",
                mX = parameter.mBlockPosition[1],
                mY = parameter.mBlockPosition[2],
                mZ = parameter.mBlockPosition[3],
                mModel = self:getConfig().mModel
            },
            function(hostKey)
                if self.mIsDelete then
                    return
                end
                self.mProperty:safeWrite("mEntityHostKey", hostKey)
            end
    )
    self.mEntity = Framework.singleton():getEntityCustomManager():getEntityByClientKey(self.mEntityClientKey)
    self.mProperty:safeWrite("mHP", self:getConfig().mHP)
    self.mProperty:safeWrite("mState", "Peace")
    
    self.mProperty:addPropertyListener(
        "mHP",
        self,
        function(_, value)
            if value then
                if value <= 0 then
                    if self:getConfig().mDead then
                        Framework.singleton():getCommandQueueManager():post(new(Command_Callback, {mDebug = "DeadMonster", mExecuteCallback = function(command)
                            command.mEntityClientKey = Framework.singleton():getEntityCustomManager():createEntity({mType = "EntityNPCOnline", mX = block_pos[1], mY = block_pos[2], mZ = block_pos[3], mModel = self:getConfig().mDead.mModel})
                            command.mTimer = new(Utility.Timer)
                        end,
                        mExecutingCallback = function(command)
                            if command.mTimer:total() >= self:getConfig().mDead.mTime then
                                Framework.singleton():getEntityCustomManager():destroyEntity(command.mEntityClientKey)
                                command.mEntityClientKey = nil
                                command.mTimer = nil
                                command.mState = Command.EState.Finish
                            end
                        end}))
                    end
                    self.mEntity:moveTo()
                end
            end
        end
)
end

function Monster:destruction()
    self.mProperty:removePropertyListener("mHP", self)
    self.mProperty:safeWrite("mHP")
    self.mProperty:safeWrite("mState")
    self.mProperty:safeWrite("mEntityHostKey")
    delete(self.mProperty)
    self.mProperty = nil
    self.mEnemies = nil
    Framework.singleton():getEntityCustomManager():destroyEntity(self.mEntityClientKey)
    self.mEntityClientKey = nil
end

function Monster:update(deltaTime)
    if not self.mUping
        and not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1],
            self.mEntity:getBlockPosition()[2] - 1,
            self.mEntity:getBlockPosition()[3]
    )
    then
        self:_processDown(deltaTime)
    else
        local cur_block = Utility.hasBlock(
            self.mEntity:getBlockPosition()[1],
            self.mEntity:getBlockPosition()[2],
            self.mEntity:getBlockPosition()[3]
        )
        local down_block_id = GetBlockId(
            self.mEntity:getBlockPosition()[1],
            self.mEntity:getBlockPosition()[2] - 1,
            self.mEntity:getBlockPosition()[3]
        )
        if not self.mUping and not cur_block and down_block_id == 76 then
            self:_processDown(deltaTime)
        else
            self:_processMove(deltaTime)
        end
    end
    self:_updateAttack(deltaTime)
end

function Monster:getProperty()
    return self.mProperty
end

function Monster:getID()
    return self.mID
end

function Monster:getConfig()
    return Config.Monster[self.mConfigIndex]
end

function Monster:_getSendKey()
    return "GameMonster/" .. tostring(self.mID)
end

function Monster:_receiveMessage(parameter)
    if parameter.mMessage == "OnHit" then
        local old_hp = self.mProperty:cache().mHP or 0
        self.mProperty:safeWrite("mHP", self.mProperty:cache().mHP - parameter.mParameter.mDamage)
        self.mEnemies[parameter.mParameter.mPlayerID] = self.mEnemies[parameter.mParameter.mPlayerID] or {}
        local enemy = self.mEnemies[parameter.mParameter.mPlayerID]
        delete(enemy.mTimer)
        enemy.mTimer = nil
        enemy.mTimer = new(Utility.Timer)
        local block_pos
        if self.mEntity then
            block_pos = self.mEntity:getBlockPosition()
            CreateBlockPieces(
                224,
                block_pos[1],
                block_pos[2],
                block_pos[3]
        )
        end
        if old_hp > 0 and (self.mProperty:cache().mHP or 0) <= 0 then
            local player = Game.singleton():getPlayerManager():getPlayerByID(parameter.mParameter.mPlayerID)
            if player then
                player:killMonster(self)
            end
        end
    end
end

function Monster:_processDown(deltaTime)
    if not self.mEntity.mHostKey then
        return
    end
    self.mEntity:moveToBlock(
        self.mEntity:getBlockPosition()[1],
        self.mEntity:getBlockPosition()[2] - 1,
        self.mEntity:getBlockPosition()[3],
        self:_getMoveSpeed()
    )
    self.mDown = true
end

function Monster:_processMove(deltaTime)
    if not self.mEntity.mHostKey
        or self.mProperty:cache().mHP <= 0
    then
        return
    end
    if self.mDown then
        self.mDown = nil
        self.mEntity:moveTo()
    end
    local enemy_near
    for player_id, _ in pairs(self.mEnemies) do
        local entity = GetEntityById(player_id)
        if entity then
            local len = (entity:getPosition() - self.mEntity:getPosition()):length()
            if len < 10 then
                enemy_near = enemy_near or {}
                enemy_near[#enemy_near + 1] = {mPlayer = Game.singleton():getPlayerManager():getPlayerByID(entity.entityId), mLength = len}
            end
        end
    end
    if self.mProperty:cache().mState == "Peace" and enemy_near then
        local states = self:_enemyNearStates()
        local scalar = math.random()
        for _, state in pairs(states) do
            if state.mScalar[1] <= scalar and state.mScalar[2] > scalar then
                self.mProperty:safeWrite("mState", state.mType)
            end
        end
    elseif not enemy_near and self.mProperty:cache().mState ~= "Peace" then
        self.mProperty:safeWrite("mState", "Peace")
    end
    if self.mProperty:cache().mState == "Peace" and self.mResting then
        self.mResting = self.mResting - deltaTime
        if self.mResting <= 0 then
            self.mResting = nil
        end
    elseif not self.mEntity:hasMoveTarget() then
        if self.mProperty:cache().mState == "Peace" and math.random() < 0.5 then
            self.mResting = math.random(1, 5)
        elseif self.mProperty:cache().mState == "Peace" or self.mProperty:cache().mState == "Scared" then
            local paths = self:_calcMovePaths()
            if #paths > 0 then
                local path = paths[math.random(1, #paths)]
                self.mUping = nil
                if path[2] > self.mEntity:getBlockPosition()[2] then
                    self.mUping = true
                end
                self.mEntity:moveToBlock(path[1], path[2], path[3], self:_getMoveSpeed())
            end
        elseif self.mProperty:cache().mState == "Angry" or self.mProperty:cache().mState == "Crazy" then
            table.sort(enemy_near, function(a, b) return a.mLength < b.mLength end)
            local paths = self:_calcMovePaths()
            local nearest
            local nearest_len = enemy_near[1].mLength
            local enemy_block_x, enemy_block_y, enemy_block_z = enemy_near[1].mPlayer:getEntity():GetBlockPos()
            for k, path in pairs(paths) do
                local len = (path - vector3d:new(enemy_block_x, enemy_block_y, enemy_block_z)):length()
                if len < nearest_len then
                    nearest_len = len
                    nearest = k
                end
            end
            if nearest then
                local path = paths[nearest]
                self.mUping = nil
                if path[2] > self.mEntity:getBlockPosition()[2] then
                    self.mUping = true
                end
                self.mEntity:moveToBlock(path[1], path[2], path[3], self:_getMoveSpeed())
            end
        end
    end
end

function Monster:_calcMovePaths()
    local paths = {}
    if
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1] + 1,
            self.mEntity:getBlockPosition()[2],
            self.mEntity:getBlockPosition()[3]
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1] + 1,
                self.mEntity:getBlockPosition()[2],
                self.mEntity:getBlockPosition()[3]
    )
    elseif
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1] + 1,
            self.mEntity:getBlockPosition()[2] + 1,
            self.mEntity:getBlockPosition()[3]
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1] + 1,
                self.mEntity:getBlockPosition()[2] + 1,
                self.mEntity:getBlockPosition()[3]
    )
    end
    
    if
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1] - 1,
            self.mEntity:getBlockPosition()[2],
            self.mEntity:getBlockPosition()[3]
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1] - 1,
                self.mEntity:getBlockPosition()[2],
                self.mEntity:getBlockPosition()[3]
    )
    elseif
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1] - 1,
            self.mEntity:getBlockPosition()[2] + 1,
            self.mEntity:getBlockPosition()[3]
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1] - 1,
                self.mEntity:getBlockPosition()[2] + 1,
                self.mEntity:getBlockPosition()[3]
    )
    end
    
    if
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1],
            self.mEntity:getBlockPosition()[2],
            self.mEntity:getBlockPosition()[3] + 1
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1],
                self.mEntity:getBlockPosition()[2],
                self.mEntity:getBlockPosition()[3] + 1
    )
    elseif
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1],
            self.mEntity:getBlockPosition()[2] + 1,
            self.mEntity:getBlockPosition()[3] + 1
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1],
                self.mEntity:getBlockPosition()[2] + 1,
                self.mEntity:getBlockPosition()[3] + 1
    )
    end
    
    if
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1],
            self.mEntity:getBlockPosition()[2],
            self.mEntity:getBlockPosition()[3] - 1
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1],
                self.mEntity:getBlockPosition()[2],
                self.mEntity:getBlockPosition()[3] - 1
    )
    elseif
        not Utility.hasBlock(
            self.mEntity:getBlockPosition()[1],
            self.mEntity:getBlockPosition()[2] + 1,
            self.mEntity:getBlockPosition()[3] - 1
    )
    then
        paths[#paths + 1] =
            vector3d:new(
                self.mEntity:getBlockPosition()[1],
                self.mEntity:getBlockPosition()[2] + 1,
                self.mEntity:getBlockPosition()[3] - 1
    )
    end
    return paths
end

function Monster:_enemyNearStates()
    local states = {{mType = "Scared"}, {mType = "Angry"}, {mType = "Crazy"}}
    if self:getConfig().mType == "和平" then
        states[1].mScalar = {0, 0.9}
        states[2].mScalar = {0.9, 0.99}
        states[3].mScalar = {0.99, 1}
    elseif self:getConfig().mType == "中立" then
        states[1].mScalar = {0, 0.3}
        states[2].mScalar = {0.3, 0.95}
        states[3].mScalar = {0.95, 1}
    elseif self:getConfig().mType == "危险" then
        states[1].mScalar = {0, 0.01}
        states[2].mScalar = {0.01, 0.9}
        states[3].mScalar = {0.9, 1}
    end
    return states
end

function Monster:_getMoveSpeed()
    if self.mProperty:cache().mState == "Peace" then
        return self:getConfig().mMoveSpeed
    else
        return self:getConfig().mRunSpeed or self:getConfig().mMoveSpeed
    end
end

function Monster:_updateAttack(deltaTime)
    self.mAttackTimer = self.mAttackTimer or new(Utility.Timer)
    if (self.mProperty:cache().mState == "Angry" or self.mProperty:cache().mState == "Crazy") and self.mAttackTimer:total() >= self:getConfig().mAttack.mSpeed then
        local attacked_players
        for _, player in pairs(Game.singleton():getPlayerManager().mPlayers) do
            if player:getEntity() and (player:getEntity():getPosition() - self.mEntity:getPosition()):length() <= self:getConfig().mAttack.mRange then
                attacked_players = attacked_players or {}
                attacked_players[#attacked_players + 1] = player
            end
        end
        if attacked_players then
            for _, player in pairs(attacked_players) do
                self.mEnemies[player:getID()] = {}
                player:onHitByMonster(self)
            end
            delete(self.mAttackTimer)
            self.mAttackTimer = nil
        end
    end
end
