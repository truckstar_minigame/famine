local Common = Devil.Common
local Utility = Devil.Utility
local Command = Utility.Command
local CommandQueue = Utility.CommandQueue
local CommandQueueManager = Utility.CommandQueueManager
local Command_Callback = Utility.Command_Callback

local new = Common.new
local delete = Common.delete
local clone = Common.clone
local inherit = Common.inherit
local Framework = Devil.Framework

local Game = Devil.getTable("Game.Famine.Client_Game")
local Scene = Devil.getTable("Game.Famine.Client_GameScene")
local Building = Devil.getTable("Game.Famine.Client_GameBuilding")
local Config = Devil.getTable("Game.Famine.Config")
local GameUtility = Devil.getTable("Game.Famine.Utility")

local NatureScene = inherit(Scene, Devil.getTable("Game.Famine.Client_GameNatureScene"))

function NatureScene:construction(parameter)
    echo("devilwalk","NatureScene:construction:self.mID:" .. tostring(self.mID))
end

function NatureScene:destruction()
end